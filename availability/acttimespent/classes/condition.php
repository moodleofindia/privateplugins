<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Activity completion condition.
 *
 * @package availability_acttimespent
 * @copyright 2014 The Open University
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace availability_acttimespent;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/completionlib.php');

/**
 * Activity completion condition.
 *
 * @package availability_acttimespent
 * @copyright 2014 The Open University
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class condition extends \core_availability\condition {
    /** @var int ID of module that this depends on */
    protected $cmid;

    /** @var int Expected completion type (one of the COMPLETE_xx constants) */
    protected $expectedcompletion;
    
    protected $activitytimespent;
    
    protected $durationtype;

    /** @var array Array of modules used in these conditions for course */
    protected static $modsusedincondition = array();

    /**
     * Constructor.
     *
     * @param \stdClass $structure Data structure from JSON decode
     * @throws \coding_exception If invalid data structure.
     */
    public function __construct($structure) {
        // Get cmid.
        if (isset($structure->cm) && is_number($structure->cm)) {
            $this->cmid = (int)$structure->cm;
        } else {
            throw new \coding_exception('Missing or invalid ->cm for completion condition');
        }
        //nihar
        if (isset($structure->ats) && is_number($structure->ats)) {
            $this->activitytimespent = (int)$structure->ats;
        } else {
            throw new \coding_exception('Missing or invalid timespent for activity');
        }
        
         //nihar
        if (isset($structure->dt)) {
            $this->durationtype = (int)$structure->dt;
        } else {
            throw new \coding_exception('Missing or invalid duration type for activity');
        }

        // Get expected completion.
//        if (isset($structure->e) && in_array($structure->e,
//                array(COMPLETION_COMPLETE, COMPLETION_INCOMPLETE,
//                        COMPLETION_COMPLETE_PASS, COMPLETION_COMPLETE_FAIL))) {
            $this->expectedcompletion = $structure->e;
//        } else {
//            throw new \coding_exception('Missing or invalid ->e for completion condition');
//        }
    }

    public function save() {
        return (object)array('type' => 'activitycompletion',
                'cm' => $this->cmid, 'e' => $this->expectedcompletion,'ats' => $this->activitytimespent,'dt'=>$this->durationtype);
    }

    /**
     * Returns a JSON object which corresponds to a condition of this type.
     *
     * Intended for unit testing, as normally the JSON values are constructed
     * by JavaScript code.
     *
     * @param int $cmid Course-module id of other activity
     * @param int $expectedcompletion Expected completion value (COMPLETION_xx)
     * @return stdClass Object representing condition
     */
    public static function get_json($cmid, $expectedcompletion) {
        return (object)array('type' => 'activitycompletion', 'cm' => (int)$cmid,
                'e' => (int)$expectedcompletion,'ats' => (int)$activitytimespent,'dt'=>$durationtype);
    }

    public function is_available($not, \core_availability\info $info, $grabthelot, $userid) {
        

        //nihar
      global $DB, $USER;
      if($this->cmid != 9999999999){  //if only single activity selected
          $data = $this->time_spent_activity();
      } else { //if all course activity selected(quiz and scorm only)
          $data = $this->time_spent_all_activity($info);
      }
    return $data['allow'];
}

    /**
     * Returns a more readable keyword corresponding to a completion state.
     *
     * Used to make lang strings easier to read.
     *
     * @param int $completionstate COMPLETION_xx constant
     * @return string Readable keyword
     */
    protected static function get_lang_string_keyword($completionstate) {
        switch($completionstate) {
            case COMPLETION_INCOMPLETE:
                return 'incomplete';
            case COMPLETION_COMPLETE:
                return 'complete';
            case COMPLETION_COMPLETE_PASS:
                return 'complete_pass';
            case COMPLETION_COMPLETE_FAIL:
                return 'complete_fail';
            default:
                throw new \coding_exception('Unexpected completion state: ' . $completionstate);
        }
    }

    public function get_description($full, $not, \core_availability\info $info) {

       $durationtypearr = array(0 =>'Hour',1 =>'Minute',2 => 'Second');
        if($this->cmid != 9999999999){ //this check is for all activty(quiz and scorm)
        
        // Get name for module.
        $modinfo = $info->get_modinfo();
        if (!array_key_exists($this->cmid, $modinfo->cms)) {
            $modname = get_string('missing', 'availability_acttimespent');
        } else {
            $modname = '<AVAILABILITY_CMNAME_' . $modinfo->cms[$this->cmid]->id . '/>';
        }
        // Work out which lang string to use.
        if ($not) {
            // Convert NOT strings to use the equivalent where possible.

            switch ($this->expectedcompletion) {
                case COMPLETION_INCOMPLETE:
                    $str = 'requires_' . self::get_lang_string_keyword(COMPLETION_COMPLETE);
                    break;
                case COMPLETION_COMPLETE:
                    $str = 'requires_' . self::get_lang_string_keyword(COMPLETION_INCOMPLETE);
                    break;
                default:
                    // The other two cases do not have direct opposites.
                    $str = 'requires_not_' . self::get_lang_string_keyword($this->expectedcompletion);
                    break;
            }
        } else {
            $str = 'requires_' . self::get_lang_string_keyword($this->expectedcompletion);

        }
        $data = $this->time_spent_activity();
        $key = 'timespentin'.$durationtypearr[$this->durationtype];
        if(isset($data[$key])){
          $timespent = $data[$key];
        } else {
          $timespent = 0;
        }
        return get_string($str, 'availability_acttimespent', $modname).' '.$this->activitytimespent.' '.$durationtypearr[$this->durationtype].'.You have completed ' .'<b>'. number_format((float)$timespent, 2, '.', '').' '.$durationtypearr[$this->durationtype] .'</b>'.' of it';
   } else {
        $data = $this->time_spent_all_activity($info);
        $key = 'totaltimein'.$durationtypearr[$this->durationtype];
        if(isset($data[$key])){
          $timespent = $data[$key];
        } else {
          $timespent = 0;
        }
        return 'Total time spent in all activity must be'.' '.$this->activitytimespent.' '.$durationtypearr[$this->durationtype].'.You have completed ' .'<b>'. number_format((float)$timespent, 2, '.', '').' '.$durationtypearr[$this->durationtype] .'</b>'.' of it';
   }
   }

    protected function get_debug_string() {
        switch ($this->expectedcompletion) {
            case COMPLETION_COMPLETE :
                $type = 'COMPLETE';
                break;
            case COMPLETION_INCOMPLETE :
                $type = 'INCOMPLETE';
                break;
            case COMPLETION_COMPLETE_PASS:
                $type = 'COMPLETE_PASS';
                break;
            case COMPLETION_COMPLETE_FAIL:
                $type = 'COMPLETE_FAIL';
                break;
            default:
                throw new \coding_exception('Unexpected expected completion');
        }
        return 'cm' . $this->cmid . ' ' . $type;
    }

    public function update_after_restore($restoreid, $courseid, \base_logger $logger, $name) {
        global $DB;
        $rec = \restore_dbops::get_backup_ids_record($restoreid, 'course_module', $this->cmid);
        if (!$rec || !$rec->newitemid) {
            // If we are on the same course (e.g. duplicate) then we can just
            // use the existing one.
            if ($DB->record_exists('course_modules',
                    array('id' => $this->cmid, 'course' => $courseid))) {
                return false;
            }
            // Otherwise it's a warning.
            $this->cmid = 0;
            $logger->process('Restored item (' . $name .
                    ') has availability condition on module that was not restored',
                    \backup::LOG_WARNING);
        } else {
            $this->cmid = (int)$rec->newitemid;
        }
        return true;
    }

    /**
     * Used in course/lib.php because we need to disable the completion JS if
     * a completion value affects a conditional activity.
     *
     * @param \stdClass $course Moodle course object
     * @param int $cmid Course-module id
     * @return bool True if this is used in a condition, false otherwise
     */
    public static function completion_value_used($course, $cmid) {
        // Have we already worked out a list of required completion values
        // for this course? If so just use that.
        if (!array_key_exists($course->id, self::$modsusedincondition)) {
            // We don't have data for this course, build it.
            $modinfo = get_fast_modinfo($course);
            self::$modsusedincondition[$course->id] = array();

            // Activities.
            foreach ($modinfo->cms as $othercm) {
                if (is_null($othercm->availability)) {
                    continue;
                }
                $ci = new \core_availability\info_module($othercm);
                $tree = $ci->get_availability_tree();
                foreach ($tree->get_all_children('availability_acttimespent\condition') as $cond) {
                    self::$modsusedincondition[$course->id][$cond->cmid] = true;
                }
            }

            // Sections.
            foreach ($modinfo->get_section_info_all() as $section) {
                if (is_null($section->availability)) {
                    continue;
                }
                $ci = new \core_availability\info_section($section);
                $tree = $ci->get_availability_tree();
                foreach ($tree->get_all_children('availability_acttimespent\condition') as $cond) {
                    self::$modsusedincondition[$course->id][$cond->cmid] = true;
                }
            }
        }
        return array_key_exists($cmid, self::$modsusedincondition[$course->id]);
    }

    /**
     * Wipes the static cache of modules used in a condition (for unit testing).
     */
    public static function wipe_static_cache() {
        self::$modsusedincondition = array();
    }

    public function update_dependency_id($table, $oldid, $newid) {
        if ($table === 'course_modules' && (int)$this->cmid === (int)$oldid) {
            $this->cmid = $newid;
            return true;
        } else {
            return false;
        }
    }
    /**
    *timespent on single activity level
    *
    */
    public function time_spent_activity(){
      global $DB, $USER;
      $cmrec = $DB->get_record('course_modules', array('id'=> $this->cmid),'module,instance');
      $module = $DB->get_record('modules', array('id' => $cmrec->module),'name');
       if($module->name == 'scorm'){
          $scorm = $DB->get_records_sql("SELECT value from {scorm_scoes_track} WHERE scormid=$cmrec->instance and userid=$USER->id and element= 'cmi.core.total_time'");
          if($scorm){
            foreach($scorm as $scorms){
              $scormtimearr[] = $scorms->value;
            }
           $scormtimeval = max($scormtimearr);
           $scormexplode = explode(':', $scormtimeval);
           $scormhourtosec = $scormexplode[0]*3600;
           $scormmintosec = $scormexplode[1]*60;
           $scormsec = $scormexplode[2];
           $ats = $this->activitytimespent;
           $dt = $this->durationtype;

           $totalscormtime = $scormhourtosec + $scormmintosec +$scormsec;
           if($dt == 0){
            $atshourtosec = $ats*3600;
                if($totalscormtime >= $atshourtosec){
                       $allow = true;
                   }
           } else if($dt == 1){
            $atsmintosec = $ats*60;
                if($totalscormtime >= $atsmintosec){
                       $allow = true;
                   }
           } else if($dt == 2){
            $atstosec = $ats;
                if($totalscormtime >= $atstosec){
                       $allow = true;
                   }
           }
       }
       } else if($module->name == 'quiz'){
           $quiz = $DB->get_records_sql("SELECT id,timestart, timefinish from {quiz_attempts} WHERE quiz=$cmrec->instance and userid=$USER->id and state='finished'");
           if($quiz){
           foreach($quiz as $quizes){
            $timespent = $quizes->timefinish - $quizes->timestart;
                $quiztimearr[] = $timespent;
           }
           $quiztimeval = max($quiztimearr);
           $timespentinhour = floor($quiztimeval/3600); //convert timespent in sec to hour
           $timespentinmin = floor($quiztimeval/60);  //convert timespent in sec to min
           $timespentinsec = $quiztimeval;
           $ats = $this->activitytimespent;
           $dt = $this->durationtype;

           if($dt == 0){ //means in hour 
                if($timespentinhour >= $ats){
                  return ['allow' => true, 'timespentinHour' => $timespentinhour];
                } else {
                  return ['allow' => false, 'timespentinHour' => $timespentinhour];
                }
           } else if($dt == 1){ //means in min 
                if($timespentinmin >= $ats){
                  return ['allow' => true, 'timespentinMinute' => $timespentinmin];
                } else {
                  return ['allow' => false, 'timespentinMinute' => $timespentinmin];
                }
           } else if($dt == 2){ //means in sec 
                if($timespentinsec >= $ats){
                  return ['allow' => true, 'timespentinSecond' => $timespentinsec];
                } else {
                  return ['allow' => false, 'timespentinSecond' => $timespentinsec];
                }
           }  
       }
     }
    }
    /**
    *timespent on course all activity level
    *
    */
    public function time_spent_all_activity($info){
      global $DB, $USER;
        $scormhourtosec ='';
        $scormmintosec = '';
        $scormsec = '';
        $quiztimespent = '';
        //logic for all activites(scorm and quiz)
        $modinfo = $info->get_modinfo();
        foreach($modinfo->cms as $key => $value){
            $cmrec = $DB->get_record('course_modules', array('id'=> $key),'module,instance');
            $module = $DB->get_record('modules', array('id' => $cmrec->module),'name');
        if($module->name == 'scorm'){
          
           $scorm = $DB->get_records_sql("SELECT value from {scorm_scoes_track} WHERE scormid=$cmrec->instance and userid=$USER->id and element= 'cmi.core.total_time'");
           if($scorm){
           foreach($scorm as $scorms){
                $scormtimearr[] = $scorms->value;
           }
           $scormtimeval = max($scormtimearr);
           $scormexplode = explode(':', $scormtimeval);
           $scormhourtosec += ($scormexplode[0]*3600);
           $scormmintosec += ($scormexplode[1]*60);
           $scormsec += $scormexplode[2];
         }
       
       } else if($module->name == 'quiz'){
           $quiz = $DB->get_records_sql("SELECT id,timestart, timefinish from {quiz_attempts} WHERE quiz=$cmrec->instance and userid=$USER->id and state='finished'");
           if($quiz){
           foreach($quiz as $quizes){
            $timespent = $quizes->timefinish - $quizes->timestart;
            $quiztimearr[] = $timespent;
            $quiztimeval = max($quiztimearr);  
           }
           $quiztimespent += $quiztimeval;    
       }
     }
    }
    $totaltimeinsec = $quiztimespent + $scormhourtosec + $scormmintosec +$scormsec;
    $ats = $this->activitytimespent;
    $dt = $this->durationtype;
    if($dt == 0){ //means in hour 
        $totaltimeinhour = $totaltimeinsec/3600;
          if($totaltimeinhour >= $ats){
            return ['allow' => true, 'totaltimeinHour' => $totaltimeinhour];
          } else {
            return ['allow' => false, 'totaltimeinHour' => $totaltimeinhour];
          }
    } else if($dt == 1){ //means in min 
        $totaltimeinmin = $totaltimeinsec/60;
            if($totaltimeinmin >= $ats){
              return ['allow' => true, 'totaltimeinMinute' => $totaltimeinmin]; 
            } else {
              return ['allow' => false, 'totaltimeinMinute' => $totaltimeinmin]; 
            }
    } else if($dt == 2){ //means in sec 
            if($totaltimeinsec >= $ats){
              return ['allow' => true, 'totaltimeinSecond' => $totaltimeinsec]; 
            } else {
              return ['allow' => false, 'totaltimeinSecond' => $totaltimeinsec]; 
            }
    } 
    unset($scormtimearr);
    unset($quiztimearr);
}
}
