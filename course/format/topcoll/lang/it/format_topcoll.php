<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Collapsed Topics Information
 *
 * A topic based format that solves the issue of the 'Scroll of Death' when a course has many topics. All topics
 * except zero have a toggle that displays that topic. One or more topics can be displayed at any given time.
 * Toggles are persistent on a per browser session per course basis but can be made to persist longer by a small
 * code change. Full installation instructions, code adaptions and credits are included in the 'Readme.md' file.
 *
 * @package    course/format
 * @subpackage topcoll
 * @version    See the value of '$plugin->version' in below.
 * @copyright  &copy; 2009-onwards G J Barnard in respect to modifications of standard topics format.
 * @author     G J Barnard - gjbarnard at gmail dot com and {@link http://moodle.org/user/profile.php?id=442195}
 * @link       http://docs.moodle.org/en/Collapsed_Topics_course_format
 * @license    http://www.gnu.org/copyleft/gpl.html GNU Public License
 *
 */
/**
 * Custom string 
 * Shiuli Jana  
 */

//Tab Names
$string['introduction'] = 'Copertina';
$string['lectures'] = 'Lezioni';
$string['crstab3'] = 'Materiali e Approfondimenti';
$string['crstab4'] = 'Discuti e<br /> collabora';
$string['crstab5'] = 'Compiti e Test';
$string['crstab6'] = 'Altre Attività';

$string['tabsections'] = 'No. di Sezioni per Lezioni';
$string['activitylayoutoption'] = 'Struttura Attività';

$string['latestNews'] = 'Avvisi Recenti';

$string['aboutcrs'] = 'Il Corso';

$string['separateactivities'] = 'Attività Separate';
$string['mergeactivities'] = 'Attività Integrate';

$string['myprogress'] = 'I Miei Progressi: ';
$string['next_act'] = 'Prossima Attività Suggerita: ';

$string['tab3titleit'] = 'Tab Extra # 1 Titolo (Materiali e Approfondimenti)';
$string['tab4titleit'] = 'Tab Extra # 2 Titolo (Discuti e Collabora)';
$string['tab5titleit'] = 'Tab Extra # 3 Titolo (Compiti e Test)';
$string['tab6titleit'] = 'Tab Extra # 4 Titolo (Altre Attività)';

$string['instructors'] = 'Docenti';
$string['tutors'] = 'Tutor / Co- Istruttori';