<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Collapsed Topics Information
 *
 * A topic based format that solves the issue of the 'Scroll of Death' when a course has many topics. All topics
 * except zero have a toggle that displays that topic. One or more topics can be displayed at any given time.
 * Toggles are persistent on a per browser session per course basis but can be made to persist longer by a small
 * code change. Full installation instructions, code adaptions and credits are included in the 'Readme.txt' file.
 *
 * @package    course/format
 * @subpackage eduopen
 * @version    See the value of '$plugin->version' in below.
 * @copyright  &copy; 2012-onwards G J Barnard in respect to modifications of standard topics format.
 * @author     G J Barnard - gjbarnard at gmail dot com and {@link http://moodle.org/user/profile.php?id=442195}
 * @link       http://docs.moodle.org/en/Collapsed_Topics_course_format
 * @license    http://www.gnu.org/copyleft/gpl.html GNU Public License
 *
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_heading('format_eduopen_defaults', get_string('defaultheadingsub', 'format_eduopen'),
        format_text(get_string('defaultheadingsubdesc', 'format_eduopen'), FORMAT_MARKDOWN)));

    /* Default course display.
     * Course display default, can be either one of:
     * COURSE_DISPLAY_SINGLEPAGE or - All sections on one page.
     * COURSE_DISPLAY_MULTIPAGE     - One section per page.
     * as defined in moodlelib.php.
     */
    $name = 'format_eduopen/defaultcoursedisplay';
    $title = get_string('defaultcoursedisplay', 'format_eduopen');
    $description = get_string('defaultcoursedisplay_desc', 'format_eduopen');
    $default = COURSE_DISPLAY_SINGLEPAGE;
    $choices = array(
        COURSE_DISPLAY_SINGLEPAGE => new lang_string('coursedisplay_single'),
        COURSE_DISPLAY_MULTIPAGE => new lang_string('coursedisplay_multi')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    /* Toggle instructions - 1 = no, 2 = yes. */
    $name = 'format_eduopen/defaultdisplayinstructions';
    $title = get_string('defaultdisplayinstructions', 'format_eduopen');
    $description = get_string('defaultdisplayinstructions_desc', 'format_eduopen');
    $default = 2;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    /* Layout configuration.
       Here you can see what numbers in the array represent what layout for setting the default value below.
       1 => Toggle word, toggle section x and section number - default.
       2 => Toggle word and section number.
       3 => Toggle word and toggle section x.
       4 => Toggle word.
       5 => Toggle section x and section number.
       6 => Section number.
       7 => No additions.
       8 => Toggle section x.
       Default layout to use - used when a new Collapsed Topics course is created or an old one is accessed for the first time
       after installing this functionality introduced in CONTRIB-3378. */
    $name = 'format_eduopen/defaultlayoutelement';
    $title = get_string('defaultlayoutelement', 'format_eduopen');
    $description = get_string('defaultlayoutelement_descpositive', 'format_eduopen');
    $default = 1;
    $choices = array( // In insertion order and not numeric for sorting purposes.
        1 => new lang_string('setlayout_all', 'format_eduopen'),                             // Toggle word, toggle section x and section number - default.
        3 => new lang_string('setlayout_toggle_word_section_x', 'format_eduopen'),           // Toggle word and toggle section x.
        2 => new lang_string('setlayout_toggle_word_section_number', 'format_eduopen'),      // Toggle word and section number.
        5 => new lang_string('setlayout_toggle_section_x_section_number', 'format_eduopen'), // Toggle section x and section number.
        4 => new lang_string('setlayout_toggle_word', 'format_eduopen'),                     // Toggle word.
        8 => new lang_string('setlayout_toggle_section_x', 'format_eduopen'),                // Toggle section x.
        6 => new lang_string('setlayout_section_number', 'format_eduopen'),                  // Section number.
        7 => new lang_string('setlayout_no_additions', 'format_eduopen')                     // No additions.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    /* Structure configuration.
       Here so you can see what numbers in the array represent what structure for setting the default value below.
       1 => Topic.
       2 => Week.
       3 => Latest Week First.
       4 => Current Topic First.
       5 => Day.
       Default structure to use - used when a new Collapsed Topics course is created or an old one is accessed for the first time
       after installing this functionality introduced in CONTRIB-3378. */
    $name = 'format_eduopen/defaultlayoutstructure';
    $title = get_string('defaultlayoutstructure', 'format_eduopen');
    $description = get_string('defaultlayoutstructure_desc', 'format_eduopen');
    $default = 1;
    $choices = array(
        1 => new lang_string('setlayoutstructuretopic', 'format_eduopen'),             // Topic.
        2 => new lang_string('setlayoutstructureweek', 'format_eduopen'),              // Week.
        3 => new lang_string('setlayoutstructurelatweekfirst', 'format_eduopen'),      // Latest Week First.
        4 => new lang_string('setlayoutstructurecurrenttopicfirst', 'format_eduopen'), // Current Topic First.
        5 => new lang_string('setlayoutstructureday', 'format_eduopen')                // Day.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Default number of columns between 1 and 4.
    $name = 'format_eduopen/defaultlayoutcolumns';
    $title = get_string('defaultlayoutcolumns', 'format_eduopen');
    $description = get_string('defaultlayoutcolumns_desc', 'format_eduopen');
    $default = 1;
    $choices = array(
        1 => new lang_string('one', 'format_eduopen'),   // Default.
        2 => new lang_string('two', 'format_eduopen'),   // Two.
        3 => new lang_string('three', 'format_eduopen'), // Three.
        4 => new lang_string('four', 'format_eduopen')   // Four.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Default column orientation - 1 = vertical and 2 = horizontal.
    $name = 'format_eduopen/defaultlayoutcolumnorientation';
    $title = get_string('defaultlayoutcolumnorientation', 'format_eduopen');
    $description = get_string('defaultlayoutcolumnorientation_desc', 'format_eduopen');
    $default = 2;
    $choices = array(
        1 => new lang_string('columnvertical', 'format_eduopen'),
        2 => new lang_string('columnhorizontal', 'format_eduopen') // Default.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Default toggle foreground colour in hexadecimal RGB with preceding '#'.
    $name = 'format_eduopen/defaulttgfgcolour';
    $title = get_string('defaulttgfgcolour', 'format_eduopen');
    $description = get_string('defaulttgfgcolour_desc', 'format_eduopen');
    $default = '#000000';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $settings->add($setting);

    // Default toggle foreground hover colour in hexadecimal RGB with preceding '#'.
    $name = 'format_eduopen/defaulttgfghvrcolour';
    $title = get_string('defaulttgfghvrcolour', 'format_eduopen');
    $description = get_string('defaulttgfghvrcolour_desc', 'format_eduopen');
    $default = '#888888';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $settings->add($setting);

    // Default toggle background colour in hexadecimal RGB with preceding '#'.
    $name = 'format_eduopen/defaulttgbgcolour';
    $title = get_string('defaulttgbgcolour', 'format_eduopen');
    $description = get_string('defaulttgbgcolour_desc', 'format_eduopen');
    $default = '#e2e2f2';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $settings->add($setting);

    // Default toggle background hover colour in hexadecimal RGB with preceding '#'.
    $name = 'format_eduopen/defaulttgbghvrcolour';
    $title = get_string('defaulttgbghvrcolour', 'format_eduopen');
    $description = get_string('defaulttgbghvrcolour_desc', 'format_eduopen');
    $default = '#eeeeff';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $settings->add($setting);

    // Toggle text alignment.
    // 1 = left, 2 = center and 3 = right - done this way to avoid typos.
    $name = 'format_eduopen/defaulttogglealignment';
    $title = get_string('defaulttogglealignment', 'format_eduopen');
    $description = get_string('defaulttogglealignment_desc', 'format_eduopen');
    $default = 2;
    $choices = array(
        1 => new lang_string('left', 'format_eduopen'),   // Left.
        2 => new lang_string('center', 'format_eduopen'), // Centre.
        3 => new lang_string('right', 'format_eduopen')   // Right.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle icon position.
    // 1 = left and 2 = right - done this way to avoid typos.
    $name = 'format_eduopen/defaulttoggleiconposition';
    $title = get_string('defaulttoggleiconposition', 'format_eduopen');
    $description = get_string('defaulttoggleiconposition_desc', 'format_eduopen');
    $default = 1;
    $choices = array(
        1 => new lang_string('left', 'format_eduopen'),   // Left.
        2 => new lang_string('right', 'format_eduopen')   // Right.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle icon set.
    // arrow        => Arrow icon set.
    // bulb         => Bulb icon set.
    // cloud        => Cloud icon set.
    // eye          => Eye icon set.
    // groundsignal => Ground signal set.
    // led          => LED icon set.
    // point        => Point icon set.
    // power        => Power icon set.
    // radio        => Radio icon set.
    // smiley       => Smiley icon set.
    // square       => Square icon set.
    // sunmoon      => Sun / Moon icon set.
    // switch       => Switch icon set.
    $name = 'format_eduopen/defaulttoggleiconset';
    $title = get_string('defaulttoggleiconset', 'format_eduopen');
    $description = get_string('defaulttoggleiconset_desc', 'format_eduopen');
    $default = 'chevron';
    $choices = array(
    	'chevron' => new lang_string('chevron', 'format_eduopen'),           // Chevron icon set.
        'arrow' => new lang_string('arrow', 'format_eduopen'),               // Arrow icon set.
        'bulb' => new lang_string('bulb', 'format_eduopen'),                 // Bulb icon set.
        'cloud' => new lang_string('cloud', 'format_eduopen'),               // Cloud icon set.
        'eye' => new lang_string('eye', 'format_eduopen'),                   // Eye icon set.
        'groundsignal' => new lang_string('groundsignal', 'format_eduopen'), // Ground signal set.
        'led' => new lang_string('led', 'format_eduopen'),                   // LED icon set.
        'point' => new lang_string('point', 'format_eduopen'),               // Point icon set.
        'power' => new lang_string('power', 'format_eduopen'),               // Power icon set.
        'radio' => new lang_string('radio', 'format_eduopen'),               // Radio icon set.
        'smiley' => new lang_string('smiley', 'format_eduopen'),             // Smiley icon set.
        'square' => new lang_string('square', 'format_eduopen'),             // Square icon set.
        'sunmoon' => new lang_string('sunmoon', 'format_eduopen'),           // Sun / Moon icon set.
        'switch' => new lang_string('switch', 'format_eduopen')              // Switch icon set.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle all icon hovers.
    // 1 => No.
    // 2 => Yes.
    $name = 'format_eduopen/defaulttoggleallhover';
    $title = get_string('defaulttoggleallhover', 'format_eduopen');
    $description = get_string('defaulttoggleallhover_desc', 'format_eduopen');
    $default = 2;
    $choices = array(
        1 => new lang_string('no'),
        2 => new lang_string('yes')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Show the section summary when collapsed.
    // 1 => No.
    // 2 => Yes.
    $name = 'format_eduopen/defaultshowsectionsummary';
    $title = get_string('defaultshowsectionsummary', 'format_eduopen');
    $description = get_string('defaultshowsectionsummary_desc', 'format_eduopen');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),
        2 => new lang_string('yes')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $settings->add(new admin_setting_heading('format_eduopen_configuration', get_string('configurationheadingsub', 'format_eduopen'),
        format_text(get_string('configurationheadingsubdesc', 'format_eduopen'), FORMAT_MARKDOWN)));

    /* Toggle persistence - 1 = on, 0 = off.  You may wish to disable for an AJAX performance increase.
       Note: If turning persistence off remove any rows containing 'eduopen_toggle_x' in the 'name' field
             of the 'user_preferences' table in the database.  Where the 'x' in 'eduopen_toggle_x' will be
             a course id. */
    $name = 'format_eduopen/defaulttogglepersistence';
    $title = get_string('defaulttogglepersistence', 'format_eduopen');
    $description = get_string('defaulttogglepersistence_desc', 'format_eduopen');
    $default = 1;
    $choices = array(
        0 => new lang_string('off', 'format_eduopen'), // Off.
        1 => new lang_string('on', 'format_eduopen')   // On.
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle preference for the first time a user accesses a course.
    // 0 => All closed.
    // 1 => All open.
    $name = 'format_eduopen/defaultuserpreference';
    $title = get_string('defaultuserpreference', 'format_eduopen');
    $description = get_string('defaultuserpreference_desc', 'format_eduopen');
    $default = 0;
    $choices = array(
        0 => new lang_string('eduopenclosed', 'format_eduopen'),
        1 => new lang_string('eduopenopened', 'format_eduopen')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle icon size.
    $name = 'format_eduopen/defaulttoggleiconsize';
    $title = get_string('defaulttoggleiconsize', 'format_eduopen');
    $description = get_string('defaulttoggleiconsize_desc', 'format_eduopen');
    $default = 'medium';
    $choices = array(
        'tc-small' => new lang_string('small', 'format_eduopen'),
        'tc-medium' => new lang_string('medium', 'format_eduopen'),
        'tc-large' => new lang_string('large', 'format_eduopen')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle border radius top left.
    $name = 'format_eduopen/defaulttoggleborderradiustl';
    $title = get_string('defaulttoggleborderradiustl', 'format_eduopen');
    $description = get_string('defaulttoggleborderradiustl_desc', 'format_eduopen');
    $default = '0.7';
    $choices = array(
        '0.0' => new lang_string('em0_0', 'format_eduopen'),
        '0.1' => new lang_string('em0_1', 'format_eduopen'),
        '0.2' => new lang_string('em0_2', 'format_eduopen'),
        '0.3' => new lang_string('em0_3', 'format_eduopen'),
        '0.4' => new lang_string('em0_4', 'format_eduopen'),
        '0.5' => new lang_string('em0_5', 'format_eduopen'),
        '0.6' => new lang_string('em0_6', 'format_eduopen'),
        '0.7' => new lang_string('em0_7', 'format_eduopen'),
        '0.8' => new lang_string('em0_8', 'format_eduopen'),
        '0.9' => new lang_string('em0_9', 'format_eduopen'),
        '1.0' => new lang_string('em1_0', 'format_eduopen'),
        '1.1' => new lang_string('em1_1', 'format_eduopen'),
        '1.2' => new lang_string('em1_2', 'format_eduopen'),
        '1.3' => new lang_string('em1_3', 'format_eduopen'),
        '1.4' => new lang_string('em1_4', 'format_eduopen'),
        '1.5' => new lang_string('em1_5', 'format_eduopen'),
        '1.6' => new lang_string('em1_6', 'format_eduopen'),
        '1.7' => new lang_string('em1_7', 'format_eduopen'),
        '1.8' => new lang_string('em1_8', 'format_eduopen'),
        '1.9' => new lang_string('em1_9', 'format_eduopen'),
        '2.0' => new lang_string('em2_0', 'format_eduopen'),
        '2.1' => new lang_string('em2_1', 'format_eduopen'),
        '2.2' => new lang_string('em2_2', 'format_eduopen'),
        '2.3' => new lang_string('em2_3', 'format_eduopen'),
        '2.4' => new lang_string('em2_4', 'format_eduopen'),
        '2.5' => new lang_string('em2_5', 'format_eduopen'),
        '2.6' => new lang_string('em2_6', 'format_eduopen'),
        '2.7' => new lang_string('em2_7', 'format_eduopen'),
        '2.8' => new lang_string('em2_8', 'format_eduopen'),
        '2.9' => new lang_string('em2_9', 'format_eduopen'),
        '3.0' => new lang_string('em3_0', 'format_eduopen'),
        '3.1' => new lang_string('em3_1', 'format_eduopen'),
        '3.2' => new lang_string('em3_2', 'format_eduopen'),
        '3.3' => new lang_string('em3_3', 'format_eduopen'),
        '3.4' => new lang_string('em3_4', 'format_eduopen'),
        '3.5' => new lang_string('em3_5', 'format_eduopen'),
        '3.6' => new lang_string('em3_6', 'format_eduopen'),
        '3.7' => new lang_string('em3_7', 'format_eduopen'),
        '3.8' => new lang_string('em3_8', 'format_eduopen'),
        '3.9' => new lang_string('em3_9', 'format_eduopen'),
        '4.0' => new lang_string('em4_0', 'format_eduopen')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle border radius top right.
    $name = 'format_eduopen/defaulttoggleborderradiustr';
    $title = get_string('defaulttoggleborderradiustr', 'format_eduopen');
    $description = get_string('defaulttoggleborderradiustr_desc', 'format_eduopen');
    $default = '0.7';
    $choices = array(
        '0.0' => new lang_string('em0_0', 'format_eduopen'),
        '0.1' => new lang_string('em0_1', 'format_eduopen'),
        '0.2' => new lang_string('em0_2', 'format_eduopen'),
        '0.3' => new lang_string('em0_3', 'format_eduopen'),
        '0.4' => new lang_string('em0_4', 'format_eduopen'),
        '0.5' => new lang_string('em0_5', 'format_eduopen'),
        '0.6' => new lang_string('em0_6', 'format_eduopen'),
        '0.7' => new lang_string('em0_7', 'format_eduopen'),
        '0.8' => new lang_string('em0_8', 'format_eduopen'),
        '0.9' => new lang_string('em0_9', 'format_eduopen'),
        '1.0' => new lang_string('em1_0', 'format_eduopen'),
        '1.1' => new lang_string('em1_1', 'format_eduopen'),
        '1.2' => new lang_string('em1_2', 'format_eduopen'),
        '1.3' => new lang_string('em1_3', 'format_eduopen'),
        '1.4' => new lang_string('em1_4', 'format_eduopen'),
        '1.5' => new lang_string('em1_5', 'format_eduopen'),
        '1.6' => new lang_string('em1_6', 'format_eduopen'),
        '1.7' => new lang_string('em1_7', 'format_eduopen'),
        '1.8' => new lang_string('em1_8', 'format_eduopen'),
        '1.9' => new lang_string('em1_9', 'format_eduopen'),
        '2.0' => new lang_string('em2_0', 'format_eduopen'),
        '2.1' => new lang_string('em2_1', 'format_eduopen'),
        '2.2' => new lang_string('em2_2', 'format_eduopen'),
        '2.3' => new lang_string('em2_3', 'format_eduopen'),
        '2.4' => new lang_string('em2_4', 'format_eduopen'),
        '2.5' => new lang_string('em2_5', 'format_eduopen'),
        '2.6' => new lang_string('em2_6', 'format_eduopen'),
        '2.7' => new lang_string('em2_7', 'format_eduopen'),
        '2.8' => new lang_string('em2_8', 'format_eduopen'),
        '2.9' => new lang_string('em2_9', 'format_eduopen'),
        '3.0' => new lang_string('em3_0', 'format_eduopen'),
        '3.1' => new lang_string('em3_1', 'format_eduopen'),
        '3.2' => new lang_string('em3_2', 'format_eduopen'),
        '3.3' => new lang_string('em3_3', 'format_eduopen'),
        '3.4' => new lang_string('em3_4', 'format_eduopen'),
        '3.5' => new lang_string('em3_5', 'format_eduopen'),
        '3.6' => new lang_string('em3_6', 'format_eduopen'),
        '3.7' => new lang_string('em3_7', 'format_eduopen'),
        '3.8' => new lang_string('em3_8', 'format_eduopen'),
        '3.9' => new lang_string('em3_9', 'format_eduopen'),
        '4.0' => new lang_string('em4_0', 'format_eduopen')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle border radius bottom right.
    $name = 'format_eduopen/defaulttoggleborderradiusbr';
    $title = get_string('defaulttoggleborderradiusbr', 'format_eduopen');
    $description = get_string('defaulttoggleborderradiusbr_desc', 'format_eduopen');
    $default = '0.7';
    $choices = array(
        '0.0' => new lang_string('em0_0', 'format_eduopen'),
        '0.1' => new lang_string('em0_1', 'format_eduopen'),
        '0.2' => new lang_string('em0_2', 'format_eduopen'),
        '0.3' => new lang_string('em0_3', 'format_eduopen'),
        '0.4' => new lang_string('em0_4', 'format_eduopen'),
        '0.5' => new lang_string('em0_5', 'format_eduopen'),
        '0.6' => new lang_string('em0_6', 'format_eduopen'),
        '0.7' => new lang_string('em0_7', 'format_eduopen'),
        '0.8' => new lang_string('em0_8', 'format_eduopen'),
        '0.9' => new lang_string('em0_9', 'format_eduopen'),
        '1.0' => new lang_string('em1_0', 'format_eduopen'),
        '1.1' => new lang_string('em1_1', 'format_eduopen'),
        '1.2' => new lang_string('em1_2', 'format_eduopen'),
        '1.3' => new lang_string('em1_3', 'format_eduopen'),
        '1.4' => new lang_string('em1_4', 'format_eduopen'),
        '1.5' => new lang_string('em1_5', 'format_eduopen'),
        '1.6' => new lang_string('em1_6', 'format_eduopen'),
        '1.7' => new lang_string('em1_7', 'format_eduopen'),
        '1.8' => new lang_string('em1_8', 'format_eduopen'),
        '1.9' => new lang_string('em1_9', 'format_eduopen'),
        '2.0' => new lang_string('em2_0', 'format_eduopen'),
        '2.1' => new lang_string('em2_1', 'format_eduopen'),
        '2.2' => new lang_string('em2_2', 'format_eduopen'),
        '2.3' => new lang_string('em2_3', 'format_eduopen'),
        '2.4' => new lang_string('em2_4', 'format_eduopen'),
        '2.5' => new lang_string('em2_5', 'format_eduopen'),
        '2.6' => new lang_string('em2_6', 'format_eduopen'),
        '2.7' => new lang_string('em2_7', 'format_eduopen'),
        '2.8' => new lang_string('em2_8', 'format_eduopen'),
        '2.9' => new lang_string('em2_9', 'format_eduopen'),
        '3.0' => new lang_string('em3_0', 'format_eduopen'),
        '3.1' => new lang_string('em3_1', 'format_eduopen'),
        '3.2' => new lang_string('em3_2', 'format_eduopen'),
        '3.3' => new lang_string('em3_3', 'format_eduopen'),
        '3.4' => new lang_string('em3_4', 'format_eduopen'),
        '3.5' => new lang_string('em3_5', 'format_eduopen'),
        '3.6' => new lang_string('em3_6', 'format_eduopen'),
        '3.7' => new lang_string('em3_7', 'format_eduopen'),
        '3.8' => new lang_string('em3_8', 'format_eduopen'),
        '3.9' => new lang_string('em3_9', 'format_eduopen'),
        '4.0' => new lang_string('em4_0', 'format_eduopen')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Toggle border radius bottom left.
    $name = 'format_eduopen/defaulttoggleborderradiusbl';
    $title = get_string('defaulttoggleborderradiusbl', 'format_eduopen');
    $description = get_string('defaulttoggleborderradiusbl_desc', 'format_eduopen');
    $default = '0.7';
    $choices = array(
        '0.0' => new lang_string('em0_0', 'format_eduopen'),
        '0.1' => new lang_string('em0_1', 'format_eduopen'),
        '0.2' => new lang_string('em0_2', 'format_eduopen'),
        '0.3' => new lang_string('em0_3', 'format_eduopen'),
        '0.4' => new lang_string('em0_4', 'format_eduopen'),
        '0.5' => new lang_string('em0_5', 'format_eduopen'),
        '0.6' => new lang_string('em0_6', 'format_eduopen'),
        '0.7' => new lang_string('em0_7', 'format_eduopen'),
        '0.8' => new lang_string('em0_8', 'format_eduopen'),
        '0.9' => new lang_string('em0_9', 'format_eduopen'),
        '1.0' => new lang_string('em1_0', 'format_eduopen'),
        '1.1' => new lang_string('em1_1', 'format_eduopen'),
        '1.2' => new lang_string('em1_2', 'format_eduopen'),
        '1.3' => new lang_string('em1_3', 'format_eduopen'),
        '1.4' => new lang_string('em1_4', 'format_eduopen'),
        '1.5' => new lang_string('em1_5', 'format_eduopen'),
        '1.6' => new lang_string('em1_6', 'format_eduopen'),
        '1.7' => new lang_string('em1_7', 'format_eduopen'),
        '1.8' => new lang_string('em1_8', 'format_eduopen'),
        '1.9' => new lang_string('em1_9', 'format_eduopen'),
        '2.0' => new lang_string('em2_0', 'format_eduopen'),
        '2.1' => new lang_string('em2_1', 'format_eduopen'),
        '2.2' => new lang_string('em2_2', 'format_eduopen'),
        '2.3' => new lang_string('em2_3', 'format_eduopen'),
        '2.4' => new lang_string('em2_4', 'format_eduopen'),
        '2.5' => new lang_string('em2_5', 'format_eduopen'),
        '2.6' => new lang_string('em2_6', 'format_eduopen'),
        '2.7' => new lang_string('em2_7', 'format_eduopen'),
        '2.8' => new lang_string('em2_8', 'format_eduopen'),
        '2.9' => new lang_string('em2_9', 'format_eduopen'),
        '3.0' => new lang_string('em3_0', 'format_eduopen'),
        '3.1' => new lang_string('em3_1', 'format_eduopen'),
        '3.2' => new lang_string('em3_2', 'format_eduopen'),
        '3.3' => new lang_string('em3_3', 'format_eduopen'),
        '3.4' => new lang_string('em3_4', 'format_eduopen'),
        '3.5' => new lang_string('em3_5', 'format_eduopen'),
        '3.6' => new lang_string('em3_6', 'format_eduopen'),
        '3.7' => new lang_string('em3_7', 'format_eduopen'),
        '3.8' => new lang_string('em3_8', 'format_eduopen'),
        '3.9' => new lang_string('em3_9', 'format_eduopen'),
        '4.0' => new lang_string('em4_0', 'format_eduopen')
    );
    $settings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));
}