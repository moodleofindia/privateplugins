<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private block_viewpost functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    block_viewpost
 * @copyright  2016 lms of india
 * @license    http://lmsofindia.com
 */
require_once('../../config.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pagetitle1', 'block_viewpost'));
$PAGE->set_heading('viewpost');
$PAGE->set_url($CFG->wwwroot . '/blocks/viewpost/view.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/blocks/viewpost/script.js'));

$settingsnode = $PAGE->settingsnav;
//$editurl = new moodle_url('/blocks/viewpost/view.php');
$editnode = $settingsnode->add(get_string('viewpage', 'block_viewpost'));
$editnode->make_active();

echo $OUTPUT->header();
$id = required_param('id', PARAM_INT);
$latestpost = $DB->get_record('latest_post', array('id' => $id, 'type' => 'latestpost'));

if ($latestpost) {
    $date = $DB->get_record_sql('SELECT (FROM_UNIXTIME(publicationdate,"%d-%m-%Y")) as publicationdate  FROM {latest_post} WHERE id=' . $id);
    echo html_writer::div(html_writer::tag('h1', $latestpost->title));
    echo html_writer::div(html_writer::tag('h5', 'Author : ' . $latestpost->author));
    echo html_writer::div(html_writer::tag('h5', 'Abstract : ' . $latestpost->abstract));
    echo html_writer::div(html_writer::tag('h5', 'Publisher :' . $latestpost->publisher));
    echo html_writer::div(html_writer::tag('h5', 'Publication Date : ' . $date->publicationdate));
    echo html_writer::div(html_writer::tag('h5', 'Url : ' . $latestpost->url));
    echo html_writer::div(html_writer::tag('h5', 'Publicationtype : ' . $latestpost->publicationtype));
    echo html_writer::div(html_writer::tag('h5', 'Topics : ' . $latestpost->topics));
}

$webinar = $DB->get_record('latest_post', array('id' => $id, 'type' => 'webinar'));
if ($webinar) {
    $date = $DB->get_record_sql('SELECT (FROM_UNIXTIME(publicationdate,"%d-%m-%Y")) as publicationdate  FROM {latest_post} WHERE id=' . $id);
    echo html_writer::div(html_writer::tag('h1', $webinar->title));
    echo html_writer::div(html_writer::tag('h5', 'Abstract : ' . $webinar->abstract));
    echo html_writer::div(html_writer::tag('h5', 'Publication Date : ' . $date->publicationdate));
}

$swz = $DB->get_record('latest_post', array('id' => $id, 'type' => 'swz'));
if ($swz) {
    $date = $DB->get_record_sql('SELECT (FROM_UNIXTIME(publicationdate,"%d-%m-%Y")) as publicationdate  FROM {latest_post} WHERE id=' . $id);
    echo html_writer::div(html_writer::tag('h1', $swz->title));
    echo html_writer::div(html_writer::tag('h5', 'Abstract : ' . $swz->abstract));
    echo html_writer::div(html_writer::tag('h5', 'Publication Date : ' . $date->publicationdate));
}
echo $OUTPUT->footer();
