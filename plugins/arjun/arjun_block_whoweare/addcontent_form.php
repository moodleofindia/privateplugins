<?php
require_once('../../config.php');//D:\wamp\www\moodle2.8.2

//moodleform is defined in formslib.php
require_once($CFG->libdir."/formslib.php");
 
class addcontent extends moodleform {
    //Add elements to form
    public function definition() {
        global $CFG;
		/* $editid = optional_param('editid', 0, PARAM_INT);
		if(isset($editid)){
			$uid=$editid;
		}else {
			$uid=0;
       } */
     
        $mform = $this->_form; // Don't forget the underscore! 
		$mform->addElement('header','filehdr', get_string('add', 'block_whoweare'));

		//type top heading
         $mform->addElement('text', 'topheading', get_string('topheading', 'block_whoweare'));
         $mform->addRule('topheading', get_string('missingname'), 'required', null, 'client');
          $mform->setType('topheading', PARAM_TEXT);
          //$mform->addHelpButton('name', 'name_first','block_whoweare');
		  
		  //type  sub top heading
         $mform->addElement('text', 'subtopheading', get_string('subtopheading', 'block_whoweare'));
         $mform->addRule('subtopheading', get_string('missingname'), 'required', null, 'client');
          $mform->setType('subtopheading', PARAM_TEXT);
		  
		   //type  image1 name
         $mform->addElement('text', 'img1name', get_string('img1name', 'block_whoweare'));
         $mform->addRule('img1name', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img1name', PARAM_TEXT); 
		  $mform->addHelpButton('img1name', 'img1name_first','block_whoweare');
		  //type  image2 name
         $mform->addElement('text', 'img2name', get_string('img2name', 'block_whoweare'));
         $mform->addRule('img2name', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img2name', PARAM_TEXT);  
		  $mform->addHelpButton('img2name', 'img2name_first','block_whoweare');
		  //type  image3 name
         $mform->addElement('text', 'img3name', get_string('img3name', 'block_whoweare'));
         $mform->addRule('img3name', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img3name', PARAM_TEXT); 
		  $mform->addHelpButton('img3name', 'img3name_first','block_whoweare');
		  //type  image4 name
         $mform->addElement('text', 'img4name', get_string('img4name', 'block_whoweare'));
         $mform->addRule('img4name', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img4name', PARAM_TEXT);
		  $mform->addHelpButton('img4name', 'img4name_first','block_whoweare');
		    //type  img1heading
         $mform->addElement('text', 'img1heading', get_string('img1heading', 'block_whoweare'));
         $mform->addRule('img1heading', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img1heading', PARAM_TEXT);
		  
		  //type  img2heading
         $mform->addElement('text', 'img2heading', get_string('img2heading', 'block_whoweare'));
         $mform->addRule('img2heading', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img2heading', PARAM_TEXT);
		  
		  //type  img3heading
         $mform->addElement('text', 'img3heading', get_string('img3heading', 'block_whoweare'));
         $mform->addRule('img3heading', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img3heading', PARAM_TEXT);
		  
		  //type  img4heading
         $mform->addElement('text', 'img4heading', get_string('img4heading', 'block_whoweare'));
         $mform->addRule('img4heading', get_string('missingname'), 'required', null, 'client');
          $mform->setType('img4heading', PARAM_TEXT);
		  
		   //type  img1subheading
		 $mform->addElement('textarea', 'img1subheading', get_string('img1subheading', 'block_whoweare'), 'wrap="virtual" rows="5" cols="40"');
         $mform->addRule('img1subheading', get_string('missingimg1subhead','block_whoweare'), 'required', null, 'client');
        $mform->setType('img1subheading', PARAM_TEXT);
         //$mform->addHelpButton('address', 'address_first','block_whoweare');
		 
        //type  img2subheading
		 $mform->addElement('textarea', 'img2subheading', get_string('img2subheading', 'block_whoweare'), 'wrap="virtual" rows="5" cols="40"');
         $mform->addRule('img2subheading', get_string('missingimg1subhead','block_whoweare'), 'required', null, 'client');
        $mform->setType('img2subheading', PARAM_TEXT);
		
		//type  img3subheading
		 $mform->addElement('textarea', 'img3subheading', get_string('img3subheading', 'block_whoweare'), 'wrap="virtual" rows="5" cols="40"');
         $mform->addRule('img3subheading', get_string('missingimg1subhead','block_whoweare'), 'required', null, 'client');
        $mform->setType('img3subheading', PARAM_TEXT);
		
		 //type  img4subheading
		 $mform->addElement('textarea', 'img4subheading', get_string('img4subheading', 'block_whoweare'), 'wrap="virtual" rows="5" cols="40"');
         $mform->addRule('img4subheading', get_string('missingimg1subhead','block_whoweare'), 'required', null, 'client');
        $mform->setType('img4subheading', PARAM_TEXT);
		
		/* $mform->addElement('hidden', 'updateid', $uid);
        $mform->setType('updateid', PARAM_TEXT); */
        
        
		
       //Submit Button
		$buttonarray=array();
		$buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('savechanges'));
		$buttonarray[] = &$mform->createElement('reset', 'resetbutton', get_string('reset'));
		$mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
		$mform->closeHeaderBefore('buttonar');

		//$this->add_action_buttons();
    }
    //Custom validation should be added here
    function validation($data, $files) {
        return array();
    }
}
?>