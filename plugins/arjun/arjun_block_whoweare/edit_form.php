<?php
class block_whoweare_edit_form extends block_edit_form {

    protected function specific_definition($mform) {
        global $CFG;

        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));
        $mform->addElement('static', 'link', get_string('editlink', 'block_whoweare',
        $CFG->wwwroot.'/blocks/whoweare/addcontent.php'));
    }
}
