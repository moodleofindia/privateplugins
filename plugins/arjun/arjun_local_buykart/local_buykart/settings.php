<?php
/**
 * BuyKart Settings file
 *
 * @package     local
 * @subpackage  local_buykart
 * @author   	Thomas Threadgold
 * @copyright   2015 LearningWorks Ltd
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once $CFG->dirroot . '/local/buykart/lib.php';

if ($hassiteconfig) {
	// needs this condition or there is error on login page

	//
	// Add category to root
	//
	$ADMIN->add(
		'root',
		new admin_category(
			'buykart',
			get_string(
				'pluginname',
				'local_buykart'
			)
		)
	);

	//
	// Add settings link to root category
	//
	$ADMIN->add(
		'buykart',
		new admin_externalpage(
			'buykartsettings',
			get_string(
				'buykart_settings',
				'local_buykart'
			),
			$CFG->wwwroot . '/admin/settings.php?section=local_buykart_settings',
			'moodle/course:update'
		)
	);

	$ADMIN->add(
		'buykart',
		new admin_externalpage(
			'buykartpages',
			get_string(
				'buykart_pages',
				'local_buykart'
			),
			$CFG->wwwroot . '/admin/settings.php?section=local_buykart_pages',
			'moodle/course:update'
		)
	);

	//
	// Add category to local plugins category
	//
	$ADMIN->add(
		'localplugins',
		new admin_category(
			'local_buykart',
			get_string(
				'pluginname',
				'local_buykart'
			)
		)
	);

	//
	// Add generic settings page
	//
	$settings = new admin_settingpage(
		'local_buykart_settings',
		get_string(
			'buykart_settings',
			'local_buykart'
		)
	);
	$ADMIN->add('local_buykart', $settings);

	$paypalcurrencies = local_buykart_get_currencies();
	$settings->add(
		new admin_setting_configselect(
			'local_buykart/currency',
			get_string(
				'currency',
				'local_buykart'
			),
			'',
			'USD',
			$paypalcurrencies
		)
	);

	$settings->add(
		new admin_setting_configtext(
			'local_buykart/pagination',
			get_string(
				'pagination',
				'local_buykart'
			),
			get_string(
				'pagination_desc',
				'local_buykart'
			),
			10,
			PARAM_INT
		)
	);

	//
	// Add category to local plugins category
	//
	$ADMIN->add(
		'local_buykart',
		new admin_category(
			'buykart_payment',
			get_string(
				'payment_title',
				'local_buykart'
			)
		)
	);

	

	
//////////////////////////payumoney setting//////////////////////////


	// ADD PAYUMONEY PAYMENT SETTINGS PAGE
	//
	$settings = new admin_settingpage(
		'local_buykart_settings_payumoney',
		get_string(
			'payment_payumoney_title',
			'local_buykart'
		)
	);
	$ADMIN->add('buykart_payment', $settings);

	// 
	// Add payumoney enable checkbox
	// 
	$settings->add(
		new admin_setting_configcheckbox(
			'local_buykart/payment_payumoney_enable',
			get_string(
				'payment_enable',
				'local_buykart'
			),
			get_string(
				'payment_enable_desc',
				'local_buykart'
			),
			0
		)
	);

	// 
	// Add payumoney maerchant id setting
	// 
	$settings->add(
		new admin_setting_configtext(
			'local_buykart/payment_payumoney_key',
			get_string(
				'payment_payumoney_key',
				'local_buykart'
			),
			get_string(
				'payment_payumoney_key_desc',
				'local_buykart'
			),
			'',
			PARAM_TEXT
		)
	);

	// 
	// Add payumoney merchant salt setting
	// 
	$settings->add(
		new admin_setting_configtext(
			'local_buykart/payment_payumoney_salt',
			get_string(
				'payment_payumoney_salt',
				'local_buykart'
			),
			get_string(
				'payment_payumoney_salt_desc',
				'local_buykart'
			),
			'',
			PARAM_TEXT
		)
	);

	// 
	// Add payumoney sandbox checkbox
	// 
	$settings->add(
		new admin_setting_configcheckbox(
			'local_buykart/payment_payumoney_sandbox',
			get_string(
				'payment_payumoney_sandbox',
				'local_buykart'
			),
			get_string(
				'payment_payumoney_sandbox_desc',
				'local_buykart'
			),
			0
		)
	);



//////////////////////////ebs setting//////////////////////////


	// ADD EBS PAYMENT SETTINGS PAGE
	//
	$settings = new admin_settingpage(
		'local_buykart_settings_ebs',
		get_string(
			'payment_ebs_title',
			'local_buykart'
		)
	);
	$ADMIN->add('buykart_payment', $settings);

	// 
	// Add ebs enable checkbox
	// 
	$settings->add(
		new admin_setting_configcheckbox(
			'local_buykart/payment_ebs_enable',
			get_string(
				'payment_enable',
				'local_buykart'
			),
			get_string(
				'payment_enable_desc',
				'local_buykart'
			),
			0
		)
	);

	// 
	// Add ebs userid setting
	// 
	$settings->add(
		new admin_setting_configtext(
			'local_buykart/payment_ebs_AccountID',
			get_string(
				'payment_ebs_accountid',
				'local_buykart'
			),
			get_string(
				'payment_ebs_accountid_desc',
				'local_buykart'
			),
			'',
			PARAM_TEXT
		)
	);

	// 
	// Add ebs secret key setting
	// 
	$settings->add(
		new admin_setting_configtext(
			'local_buykart/payment_ebs_SecretKey',
			get_string(
				'payment_ebs_SecretKey',
				'local_buykart'
			),
			get_string(
				'payment_ebs_SecretKey_desc',
				'local_buykart'
			),
			'',
			PARAM_TEXT
		)
	);

	// 
	// Add ebs sandbox checkbox
	// 
	$settings->add(
		new admin_setting_configcheckbox(
			'local_buykart/payment_ebs_sandbox',
			get_string(
				'payment_ebs_sandbox',
				'local_buykart'
			),
			get_string(
				'payment_ebs_sandbox_desc',
				'local_buykart'
			),
			0
		)
	);


	//
	// ADD PAYPAL PAYMENT SETTINGS PAGE
	//
	$settings = new admin_settingpage(
		'local_buykart_settings_paypal',
		get_string(
			'payment_paypal_title',
			'local_buykart'
		)
	);
	$ADMIN->add('buykart_payment', $settings);

	// 
	// Add paypal enable checkbox
	// 
	$settings->add(
		new admin_setting_configcheckbox(
			'local_buykart/payment_paypal_enable',
			get_string(
				'payment_enable',
				'local_buykart'
			),
			get_string(
				'payment_enable_desc',
				'local_buykart'
			),
			0
		)
	);

	// 
	// Add paypal business email setting
	// 
	$settings->add(
		new admin_setting_configtext(
			'local_buykart/payment_paypal_email',
			get_string(
				'payment_paypal_email',
				'local_buykart'
			),
			get_string(
				'payment_paypal_email_desc',
				'local_buykart'
			),
			'',
			PARAM_EMAIL
		)
	);

	// 
	// Add paypal sandbox checkbox
	// 
	$settings->add(
		new admin_setting_configcheckbox(
			'local_buykart/payment_paypal_sandbox',
			get_string(
				'payment_paypal_sandbox',
				'local_buykart'
			),
			get_string(
				'payment_paypal_sandbox_desc',
				'local_buykart'
			),
			0
		)
	);

	//
	// Add page settings
	//
	$pages = new admin_settingpage(
		'local_buykart_pages',
		get_string(
			'buykart_pages',
			'local_buykart'
		)
	);
	$ADMIN->add('local_buykart', $pages);

	// Catalogue Page
	$pages->add(
		new admin_setting_heading(
			'local_buykart/page_setting_heading_catalogue',
			get_string(
				'page_setting_heading_catalogue',
				'local_buykart'
			),
			''
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_description',
			get_string(
				'page_catalogue_show_description',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_description_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_additional_description',
			get_string(
				'page_catalogue_show_additional_description',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_additional_description_desc',
				'local_buykart'
			),
			0
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_duration',
			get_string(
				'page_catalogue_show_duration',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_duration_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_image',
			get_string(
				'page_catalogue_show_image',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_image_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_category',
			get_string(
				'page_catalogue_show_category',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_category_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_price',
			get_string(
				'page_catalogue_show_price',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_price_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_catalogue_show_button',
			get_string(
				'page_catalogue_show_button',
				'local_buykart'
			),
			get_string(
				'page_catalogue_show_button_desc',
				'local_buykart'
			),
			1
		)
	);

	// Product page
	$pages->add(
		new admin_setting_heading(
			'local_buykart/page_setting_heading_product',
			get_string(
				'page_setting_heading_product',
				'local_buykart'
			),
			''
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_product_enable',
			get_string(
				'page_product_enable',
				'local_buykart'
			),
			get_string(
				'page_product_enable_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_product_show_image',
			get_string(
				'page_product_show_image',
				'local_buykart'
			),
			get_string(
				'page_product_show_image_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_product_show_description',
			get_string(
				'page_product_show_description',
				'local_buykart'
			),
			get_string(
				'page_product_show_description_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_product_show_additional_description',
			get_string(
				'page_product_show_additional_description',
				'local_buykart'
			),
			get_string(
				'page_product_show_additional_description_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_product_show_category',
			get_string(
				'page_product_show_category',
				'local_buykart'
			),
			get_string(
				'page_product_show_category_desc',
				'local_buykart'
			),
			1
		)
	);

	$pages->add(
		new admin_setting_configcheckbox(
			'local_buykart/page_product_show_related_products',
			get_string(
				'page_product_show_related_products',
				'local_buykart'
			),
			get_string(
				'page_product_show_related_products_desc',
				'local_buykart'
			),
			1
		)
	);
}
