<?php

/**
 * Moodec Gateway Payumoney
 *
 * @package     local
 * @subpackage  local_moodec
 * @author   	Thomas Threadgold
 * @copyright   2015 LearningWorks Ltd
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Load Moodle config
require_once dirname(__FILE__) . '/../../../config.php';
// Load Moodec lib
require_once dirname(__FILE__) . '/../lib.php';

class MoodecGatewayPayumoney extends MoodecGateway {

    function __construct($transaction) {
        parent::__construct($transaction);

        $this->_gatewayName = get_string('payment_payumoney_title', 'local_moodec');
    }

    // handle the IPN notification	
    public function handle($data = null) {
        global $DB, $CFG;
        require_once $CFG->libdir . '/eventslib.php';

        // Set the gateway to be Payumoney
        $this->_transaction->set_gateway(MOODEC_GATEWAY_PAYUMONEY);

        // CHECK TRANSACTION CURRENT STATUS
        if ($this->_transaction->get_status() === MoodecTransaction::STATUS_COMPLETE) {
            // this transaction has already been marked as complete, so we don't want to go
            // through the process again
            return false;
        }

        if (is_null($data)) {

            $this->_transaction->fail();

            return false;
        }

        // If status is not completed or pending then unenrol the student if already enrolled
        // and notify admin
        if ($data->payment_status != "Completed" and $data->payment_status != "Pending") {

            foreach ($this->_transaction->get_items() as $item) {

                $product = local_moodec_get_product($item->get_product_id());

                $instance = $DB->get_record('enrol', array('courseid' => $product->get_course_id(), 'enrol' => 'moodec'));

                $this->_enrolPlugin->unenrol_user($instance, $this->_transaction->get_user_id());
            }

            $this->send_error_to_admin("Status not completed or pending. User unenrolled from course", $data);

            $this->_transaction->fail();

            return false;
        }

//		// Confirm currency is correctly set and matches the plugin config
//		if ($data->mc_currency != get_config('local_moodec', 'currency')) {
//			$this->send_error_to_admin("Currency does not match course settings, received: " . $data->mc_currency, $data);
//
//			$this->_transaction->fail();
//
//			return false;
//		}
        // If status is pending and reason is other than echeck then we are on hold until further notice
        // Email user to let them know. Email admin.
        if ($data->payment_status == "Pending" and $data->pending_reason != "echeck") {
            $eventdata = new stdClass();
            $eventdata->modulename = 'moodle';
            $eventdata->component = 'local_moodec';
            $eventdata->name = 'local_moodec_payment';
            $eventdata->userfrom = get_admin();
            $eventdata->userto = $user;
            $eventdata->subject = "Moodle: Payumoney payment";
            $eventdata->fullmessage = "Your Payumoney payment is pending.";
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml = '';
            $eventdata->smallmessage = '';
            message_send($eventdata);

            $this->send_error_to_admin("Payment pending", $data);

            $this->_transaction->pending();

            return false;
        }

        // --------------------
        // At this point we only proceed with 
        // - a status of completed, or 
        // - pending with a reason of echeck
        // --------------------
        // The email address paid to does not match the one we expect
        if (core_text::strtolower($data->key) !== core_text::strtolower(get_config('local_moodec', 'payment_payumoney_key'))) {
            // Check that the email is the one we want it to be
            $this->send_error_to_admin("Payumoney Merchant Key is {$data->key} (not " .
                    get_config('local_moodec', 'payment_payumoney_key') . ")", $data);

            $this->_transaction->fail();

            return false;
        }

        // Check if the payment was less than the transaction cost
        if ($data->mc_gross < $this->_transaction->get_cost()) {

            $this->send_error_to_admin("Amount paid is not enough (" . $data->mc_gross . " < " . $this->_transaction->get_cost() . ")", $data);

            $this->_transaction->fail();

            return false;
        }

        // Lastly, verify the general transaction items and user
        if ($this->verify_transaction()) {

            $this->_transaction->set_txn_id($data->txn_id);

            $this->complete_enrolment();

            return true;
        }

        return false;
    }

    public function render() {
        global $CFG, $DB, $USER;
        print_r($_POST);

        $MERCHANT_KEY = get_config('local_moodec', 'payment_payumoney_key');
        $SALT = get_config('local_moodec', 'payment_payumoney_salt');
        $sandbox = get_config('local_moodec', 'payment_payumoney_sandbox');

        $PAYU_BASE_URL = empty($sandbox) ? 'https://secure.payu.in' : 'https://test.payu.in';
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        /**
         * Store posted data
         */
        $posted = array();

        if (!empty($_POST)) {
            $posted = $_POST;
        }
        $hash = '';
        if (empty($posted['hash']) && sizeof($posted) > 0) {

            $hash = hash('sha512', $posted['key'] . '|' . $posted['txnid'] . '|' . $posted['amount'] . '|' . $posted['productinfo'] . '|' . $posted['firstname'] . '|' . $posted['email'] . '|||||||||||' . $SALT);

            $this->_gatewayURL = $PAYU_BASE_URL . '/_payment';
        } else if (!empty($hash)) {
            $hash = $posted['hash'];
            $this->_gatewayURL = $PAYU_BASE_URL . '/_payment';
        }
        $req = 'cmd=_notify-validate';
        $data = (object) $_POST;

        foreach ($_POST as $key => $value) {
            $req .= "&$key=" . urlencode($value);
        }

        if (!$user = $DB->get_record("user", array("id" => $USER->id))) {
            send_error_to_admin("Not a valid user id", $data);
            die;
        }

//        if (!$course = $DB->get_record("course", array("id" => $data->$COURSE->id))) {
//            send_error_to_admin("Not a valid course id", $data);
//            die;
//        }
//        if (!$context = context_course::instance($course->id, IGNORE_MISSING)) {
//            send_error_to_admin("Not a valid context id", $data);
//            die;
//        }

        $posted['firstname'] = $user->firstname;
        $posted['lastname'] = $user->lastname;
        $posted['email'] = $user->email;
        $posted['city'] = $user->city;
        $posted['country'] = $user->country;

        $posted['state'] = '';
        $posted['zipcode'] = '';
        $posted['phone'] = $user->phone1;
        $posted['productinfo'] = '';


        $posted['surl'] = $CFG->wwwroot . '/enrol/payumoney/ipn.php';
        $posted['furl'] = $CFG->wwwroot . "/enrol/payumoney/return.php";
        $posted['curl'] = $CFG->wwwroot;

        $posted['udf1'] = $user->id;
        $posted['udf2'] = '';
        $html = '';
        $html .= sprintf('<html><head>'
                . '<script>
    function submitPayuForm() {
      if(empty($hash)) {
        return;
      }
      $payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script></head><body onload="submitPayuForm()">'
                . '<form action="%s" method="POST" class="payment-gateway gateway--payumoney" name="payuForm">', $this->_gatewayURL);
        $html .= sprintf(
                '<input type="hidden" name="key" value="%s"/>'
                . '<input type="hidden" name="hash" value="%s"/>'
                . '<input type="hidden" name="txnid" value="%s"/>'
                . '<input type="hidden" name="firstname" id="firstname" value="%s"/>'
                . '<input type="hidden" name="phone" value="%s"/>'
                . '<input type="hidden" name="email" id="email" value="%s"/>'
                . '<input type="hidden" name="surl" value="%s"/>'
                . '<input type="hidden" name="furl" value="%s"/>'
                . '<input type="hidden" name="udf1" value="%s"/>'
                . '<input type="hidden" name="udf2" value="%s"/>'
                . '<input type="hidden" name="service_provider" value="payu_paisa" size="64" />', $MERCHANT_KEY, $hash, $txnid, (empty($posted['firstname'])) ? '' : $posted['firstname'], (empty($posted['phone'])) ? '' : $posted['phone'], (empty($posted['email'])) ? '' : $posted['email'], (empty($posted['surl'])) ? '' : $posted['surl'], (empty($posted['furl'])) ? '' : $posted['furl'], (empty($posted['udf1'])) ? '' : $posted['udf1'], (empty($posted['udf2'])) ? '' : $posted['udf2']);
        $totalcost = 0.0;
        $productinfoall = array();
        foreach ($this->_transaction->get_items() as $item) {
            $totalcost += $item->get_cost();
            $product = local_moodec_get_product($item->get_product_id());
            $productinfoall[] = $product->get_type() === PRODUCT_TYPE_SIMPLE ? $product->get_fullname() : $product->get_fullname() . ' - ' . $product->get_variation($item->get_variation_id())->get_name();
        }

        $html .= sprintf('<input type="hidden" name="amount" value="%s">'
                . '<input type="hidden" name="productinfo" value="%s"/>', $totalcost, implode(',', $productinfoall));


        $html .= sprintf(
                '<input type="submit" name="submit"  value="%s">', get_string('button_payumoney_label', 'local_moodec')
        );
        $html .= sprintf('</form></body></html>');
        return $html;
    }

}
