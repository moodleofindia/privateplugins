<?php
function ebs_config() {

    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"EBS"),
     "accountid" => array("FriendlyName" => "Account ID", "Type" => "text", "Size" => "20", ),
     "secretkey" => array("FriendlyName" => "SECRET KEY", "Type" => "text", "Size" => "20",),
     "mode" => array("FriendlyName" => "MODE", "Type" => "text", "Description" => "TEST or LIVE", ),
	
    );
	return $configarray;
}

function ebs_link($params) {


	# Gateway Specific Variables
	$gatewayaccountid = $params['accountid'];
	$gatewaymode = $params['mode'];
	

	# Invoice Variables
	$invoiceid = $params['invoiceid'];
	$description = $params["description"];
    	$amount = $params['amount']; # Format: ##.##
    	$currency = $params['currency']; # Currency Code

	# Client Variables
	$firstname = $params['clientdetails']['firstname'];
	$lastname = $params['clientdetails']['lastname'];
	$email = $params['clientdetails']['email'];
	$address1 = $params['clientdetails']['address1'];
	$address2 = $params['clientdetails']['address2'];
	$city = $params['clientdetails']['city'];
	$state = $params['clientdetails']['state'];
	$postcode = $params['clientdetails']['postcode'];
	$country = $params['clientdetails']['country'];
	$phone = $params['clientdetails']['phonenumber'];

	# System Variables
	$companyname = 'EBS';
	$systemurl = $params['systemurl'];
	$currency = $params['currency'];

	# Enter your code submit to the EBS gateway...

	$code = '<form method="post" action="https://secure.ebs.in/pg/ma/sale/pay/" name="frmTransaction" id="frmTransaction" onSubmit="return validate()">
<input type="hidden" name="account_id" value="'.$gatewayaccountid.'" />
<input type="hidden" name="mode" value="'.$gatewaymode.'" />
<input type="hidden" name="description" value="'.$description.'" />
<input type="hidden" name="reference_no" value="'.$invoiceid.'" />
<input type="hidden" name="name" value="'.$firstname.'" />
<input type="hidden" name="address" value="'.$address1.'" />
<input type="hidden" name="city" value="'.$city.'" />
<input type="hidden" name="state" value="'.$state.'" />
<input type="hidden" name="country" value="'.$country.'" />
<input type="hidden" name="postal_code" value="'.$postcode.'" />
<input type="hidden" name="ship_name" value="'.$firstname.'" />

<input type="hidden" name="ship_address" value="'.$address1.'" />
<input type="hidden" name="ship_city" value="'.$city.'" />
<input type="hidden" name="ship_state" value="'.$state.'" />
<input type="hidden" name="ship_country" value="'.$country.'" />
<input type="hidden" name="ship_postal_code" value="'.$postcode.'" />
<input type="hidden" name="ship_phone" value="'.$phone.'" />
<input type="hidden" name="email" value="'.$email.'" />
<input type="hidden" name="phone" value="'.$phone.'" />
<input type="hidden" name="amount" value="'.$amount.'" />
<input type="hidden" name="return_url" value="'.$CFG->wwwroot.'/local/moodec/payment/ebs/response.php?DR={DR}" />
<input type="submit" value="Pay Now" />
</form>';

	return $code;

}
?>
