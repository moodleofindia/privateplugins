<?php
// $Id: inscriptions_massives.php 356 2010-02-27 13:15:34Z ppollet $
/**
 * A bulk enrolment plugin that allow teachers to massively enrol existing accounts to their courses,
 * with an option of adding every user to a group
 * Version for Moodle 1.9.x courtesy of Patrick POLLET & Valery FREMAUX  France, February 2010
 * Version for Moodle 2.x by pp@patrickpollet.net March 2012
 */

require(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once ('lib.php');


$id = required_param('courseid',PARAM_INT);
$course = $DB->get_record('course', array('id' => $id));
//if ($gen = $DB->record_exists('batch', array('courseid' => $id))) {
//	
//     /* if(isset($gen)) {
//     if($gen){ */
//     $url = new moodle_url($CFG->wwwroot . "/local/course_batches/view_batch.php?id=".$id);
//     //redirect($CFG->wwwroot . "/local/course_batches/view_promo.php?id=$id");
//     redirect($url);
//     $insertgen=false;
//     
//	
//}else{
	require_once ('batch_form.php');
//	$insertgen=true;
//}


/// Security and access check

//require_course_login($course);
require_login();
$context = context_course::instance($course->id);
require_capability('moodle/role:assign', $context);
$PAGE->set_context($context);
/// Start making page
$PAGE->set_pagelayout('course');
$url = new moodle_url($CFG->wwwroot.'/local/course_batches/batch.php',array('courseid' => $course->id));
$PAGE->set_url($url);
$PAGE->requires->css('/local/course_batches/style/styles.css');


$mform = new course_batches_form($CFG->wwwroot . '/local/course_batches/batch.php', array (
	'course' => $course));

if($generalfirst = $DB->record_exists('batch', array('courseid'=>$course->id))){
$generalalredy = new stdClass();
$generalalredy = $DB->get_records('batch', array('courseid' => $course->id));
$mform->set_data($generalalredy);
}
if(!isset($generalalredy)){
$generalalredy = new stdClass();
	$generalalredy->courseid=-1;
}
if ($mform->is_cancelled()) {
    redirect(new moodle_url('/course'));
}else
if ($data = $mform->get_data(false)) {
	
	global $USER;
	$personalcontext = context_user::instance($USER->id);
	if($data->courseid == $generalalredy->courseid) {
		
		if($insertgen==true){
			
			$DB->update_record('batch', $data);
			}
	}else{

		$generalinsertid = $DB->insert_record('batch', $data);
	}
     
    if(isset($generalinsertid)) {
		if($generalinsertid) {
			redirect($CFG->wwwroot . "/local/course_batches/view_batch.php?id=".$course->id);
		}
	} 
	
	}
$addnewpromo='Add batch';
$PAGE->navbar->add($addnewpromo);
$PAGE->set_title($addnewpromo);
$PAGE->set_heading("$course->fullname".' Add course_batches');
echo $OUTPUT->header();
$strinscriptions = get_string('ctbatch', 'local_course_batches');
echo $OUTPUT->heading_with_help($strinscriptions, 'course_batches', 'local_course_batches','icon',get_string('ctbatch', 'local_course_batches'));
echo $OUTPUT->box (get_string('course_batches_info', 'local_course_batches'), 'center');

$mform->display();
echo $OUTPUT->footer();
?>
