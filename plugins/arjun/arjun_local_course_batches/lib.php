<?php
defined('MOODLE_INTERNAL') || die();
 
function  local_course_batches_extend_settings_navigation($settingsnav, $context){
	global $CFG, $COURSE, $courseid, $DB;
	
$course = $DB->get_record('course', array('id' => $COURSE->id));	
//require_course_login($course);
$context = context_course::instance($course->id);
	if (has_capability('moodle/role:assign', $context)) {
		$coursenode = $settingsnav->get('courseadmin');
		if($coursenode)
		{
		
		
//for batch
                  $coursenode->add(get_string('batch', 'local_course_batches'), null,
                        navigation_node::TYPE_CONTAINER, null, 'batch',
                        new pix_icon('i/badge', get_string('batch', 'local_course_batches')));
            $url = "$CFG->wwwroot/local/course_batches/batch.php?courseid=$COURSE->id";

			$coursenode->get('batch')->add(get_string('addbatch', 'local_course_batches'), $url,
				navigation_node::TYPE_SETTING, null, 'batch');
   
	   
				$url = "$CFG->wwwroot/local/course_batches/view_batch.php?id=$COURSE->id";

				$coursenode->get('batch')->add(get_string('viewbatch', 'local_course_batches'), $url,
						navigation_node::TYPE_SETTING, null, 'course_batches');

             //for batch
                /* 
                 $url = "$CFG->wwwroot/local/course_batches/batch.php?courseid=$COURSE->id";

			$coursenode->get('coursepromo')->add(get_string('addbatch', 'local_course_batches'), $url,
				navigation_node::TYPE_SETTING, null, 'coursepromo');

	   
				$url = "$CFG->wwwroot/local/course_batches/view_batch.php?id=$COURSE->id";

				$coursenode->get('coursepromo')->add(get_string('viewbatch', 'local_course_batches'), $url,
						navigation_node::TYPE_SETTING, null, 'course_batches'); */
		}
	}	
}

function enrol_page_hook(stdClass $instance) {
        global $CFG, $USER, $OUTPUT, $PAGE, $DB;

        ob_start();

//        if ($DB->record_exists('user_enrolments', array('userid'=>$USER->id, 'enrolid'=>$instance->id))) {
//            return ob_get_clean();
//        }

        if ($instance->enrolstartdate != 0 && $instance->enrolstartdate > time()) {
            return ob_get_clean();
        }

        if ($instance->enrolenddate != 0 && $instance->enrolenddate < time()) {
            return ob_get_clean();
        }

        $course = $DB->get_record('course', array('id'=>$instance->courseid));
        $context = context_course::instance($course->id);

        $shortname = format_string($course->shortname, true, array('context' => $context));
        $strloginto = get_string("loginto", "", $shortname);
        $strcourses = get_string("courses");

        // Pass $view=true to filter hidden caps if the user cannot see them
        if ($users = get_users_by_capability($context, 'moodle/course:update', 'u.*', 'u.id ASC',
                                             '', '', '', '', false, true)) {
            $users = sort_by_roleassignment_authority($users, $context);
            $teacher = array_shift($users);
        } else {
            $teacher = false;
        }

        if ( (float) $instance->cost <= 0 ) {
            $cost = (float) $this->get_config('cost');
        } else {
            $cost = (float) $instance->cost;
        }

        if (abs($cost) < 0.01) { // no cost, other enrolment methods (instances) should be used
            echo '<p>'.get_string('nocost', 'enrol_paypal').'</p>';
        } else {

            // Calculate localised and "." cost, make sure we send PayPal the same value,
            // please note PayPal expects amount with 2 decimal places and "." separator.
            $localisedcost = format_float($cost, 2, true);
            $cost = format_float($cost, 2, false);

            if (isguestuser()) { // force login only for guest user, not real users with guest role
                if (empty($CFG->loginhttps)) {
                    $wwwroot = $CFG->wwwroot;
                } else {
                    // This actually is not so secure ;-), 'cause we're
                    // in unencrypted connection...
                    $wwwroot = str_replace("http://", "https://", $CFG->wwwroot);
                }
                echo '<div class="mdl-align"><p>'.get_string('paymentrequired').'</p>';
                echo '<p><b>'.get_string('cost').": $instance->currency $localisedcost".'</b></p>';
                echo '<p><a href="'.$wwwroot.'/login/">'.get_string('loginsite').'</a></p>';
                echo '</div>';
            } else {
                //Sanitise some fields before building the PayPal form
                $coursefullname  = format_string($course->fullname, true, array('context'=>$context));
                $courseshortname = $shortname;
                $userfullname    = fullname($USER);
                $userfirstname   = $USER->firstname;
                $userlastname    = $USER->lastname;
                $useraddress     = $USER->address;
                $usercity        = $USER->city;
                $instancename    = $this->get_instance_name($instance);

                include($CFG->dirroot.'/local/course_batches/batchpay_form.html');
            }

        }

        return $OUTPUT->box(ob_get_clean());
    }

  function restore_instance(restore_enrolments_structure_step $step, stdClass $data, $course, $oldid) {
        global $DB;
        if ($step->get_task()->get_target() == backup::TARGET_NEW_COURSE) {
            $merge = false;
        } else {
            $merge = array(
                'courseid'   => $data->courseid,
                'enrol'      => $this->get_name(),
                'roleid'     => $data->roleid,
                'cost'       => $data->cost,
                'currency'   => $data->currency,
            );
        }
        if ($merge and $instances = $DB->get_records('enrol', $merge, 'id')) {
            $instance = reset($instances);
            $instanceid = $instance->id;
        } else {
            $instanceid = $this->add_instance($course, (array)$data);
        }
        $step->set_mapping('enrol', $oldid, $instanceid);
    }








  
 
