

var margin = {top: 40, right: 80, bottom: 40, left: 80};

var width = 350,
        height = 350,
        radius = Math.min(width, height) / 2,
        innerRadius = 0;

var pie = d3.layout.pie()
        .sort(null)
        .value(function (d) {
            return d.width;
        });

var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([0, 0])
        .html(function (d) {
            return d.data.label + ": <span style='color:orangered'>" + d.data.score + "</span>";
        });

var arc = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(function (d) {
            return (radius - innerRadius) * (d.data.score / 100.0) + innerRadius;
        });

var outlineArc = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(radius);

var svg = d3.select('.coursegraph').append("svg")
        .attr("width", '100%')
        .attr("height", '95%')
        .attr('viewBox', '0 0 ' + Math.min(width, height) + ' ' + Math.min(width, height))
        .attr('preserveAspectRatio', 'xMinYMin')
        .append("g")
        .attr("transform", "translate(" + Math.min(width, height) / 2 + "," + Math.min(width, height) / 2 + ")");

svg.call(tip);
var sectionarray = [];

d3.csv('../my/sectionajax.php?id=' + course, function (error, data) {
    data.forEach(function (d) {
        d.id = d.id;
        d.order = +d.order;
        d.color = d.color;
        d.weight = +d.weight;
        d.score = +d.score;
        d.width = +d.weight;
        d.label = d.label;
        sectionarray[d.id] = d.label;
    });
    // for (var i = 0; i < data.score; i++) { console.log(data[i].id) }

    var outerGroup = svg.selectAll(".solidArc")
            .data(pie(data))
            .enter()
            .append("g")

    outerGroup
            .append("path")
            .attr("fill", function (d) {
                return d.data.color;
            })
            .attr("class", "solidArc")
            .attr("stroke", "gray")
            .attr("d", arc)
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .on('click', function (d) {
                quizload(d.data.id);
                $('.panel-body').slideUp(100);
                var $this = $("#sec" + d.data.id);
                $this.parents('.panel').find('.panel-body').slideDown(100);
                $this.removeClass('panel-collapsed');
                $this.removeClass('collapse');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');

            });

    outerGroup
            .append("text")
            .attr("transform", function (d) {
                return "translate(" + centroid(-100, width, d.startAngle, d.endAngle) + ")";
            })
            .attr("text-anchor", "middle")
            .attr("font-size", "8pt")
            .text(function (d) {
                return d.data.label
            });

    var outerPath = svg.selectAll(".outlineArc")
            .data(pie(data))
            .enter().append("path")
            .attr("fill", "none")
            .attr("stroke", "gray")
            .attr("class", "outlineArc")
            .attr("d", outlineArc);

    function centroid(innerR, outerR, startAngle, endAngle) {
        var r = (innerR + outerR) / 2, a = (startAngle + endAngle) / 2 - (Math.PI / 2);
        return [Math.cos(a) * r, Math.sin(a) * r];
    }

});

$(document).on('click', '.panel-heading span.clickable', function (e) {
    var $this = $(this);

    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('collapse');
        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    }
});



function quizload(id) {
    document.getElementById('section-title').innerHTML = sectionarray[id];
    document.getElementById('quizoverview').innerHTML = '';


    var margin = {top: 140, right: 80, bottom: 40, left: 80};
    var degree = Math.PI / 180;
    var width = 350,
            height = 350,
            radius = Math.min(width, height) / 2,
            innerRadius = 0;
    var pie = d3.layout.pie()
            .sort(null)
            .value(function (d) {
                return d.width;
            }).startAngle(-90 * degree).endAngle(90 * degree);

    var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([0, 0])
            .html(function (d) {
                return d.data.label + ": <span style='color:orangered'>" + d.data.score + "</span>";
            });

    var arc = d3.svg.arc()
            .innerRadius(innerRadius)
            .outerRadius(function (d) {
                return (radius - innerRadius) * (d.data.score / 100.0) + innerRadius;
            });

    var outlineArc = d3.svg.arc()
            .innerRadius(innerRadius)
            .outerRadius(radius);

    var svg = d3.select('#quizoverview').append("svg")
            .attr("width", '100%')
            .attr("height", '95%')
            .attr('viewBox', '0 0 ' + Math.min(width, height) + ' ' + Math.min(width, height))
            .attr('preserveAspectRatio', 'xMinYMin')
            .append("g")
            .attr("transform", "translate(" + Math.min(width, height) / 2 + "," + Math.min(width, height) / 2 + ")");

    svg.call(tip);

    d3.csv('../my/quizajax.php?course=' + course + '&id=' + id, function (error, data) {
        data.forEach(function (d) {
            d.id = d.id;
            d.order = +d.order;
            d.color = d.color;
            d.weight = +d.weight;
            d.score = +d.score;
            d.width = +d.weight;
            d.label = d.label;
        });
        // for (var i = 0; i < data.score; i++) { console.log(data[i].id) }

        var outerGroup = svg.selectAll(".solidArc")
                .data(pie(data))
                .enter()
                .append("g")

        outerGroup
                .append("path")
                .attr("fill", function (d) {
                    return d.data.color;
                })
                .attr("class", "solidArc")
                .attr("stroke", "gray")
                .attr("d", arc)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);

        outerGroup
                .append("text")
                .attr("transform", function (d) {
                    return "translate(" + centroid(-100, width, d.startAngle, d.endAngle) + ")";
                })
                .attr("text-anchor", "middle")
                .attr("font-size", "8pt")
                .text(function (d) {
                    return d.data.label
                });

        var outerPath = svg.selectAll(".outlineArc")
                .data(pie(data))
                .enter().append("path")
                .attr("fill", "none")
                .attr("stroke", "gray")
                .attr("class", "outlineArc")
                .attr("d", outlineArc);

        function centroid(innerR, outerR, startAngle, endAngle) {
            var r = (innerR + outerR) / 2, a = (startAngle + endAngle) / 2 - (Math.PI / 2);
            return [Math.cos(a) * r, Math.sin(a) * r];
        }

    });
}




