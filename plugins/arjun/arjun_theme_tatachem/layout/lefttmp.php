<?php
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/my/locallib.php');
require_once($CFG->libdir . '/blocklib.php');
require_once($CFG->dirroot . '/blocks/moodleblock.class.php');
require_once($CFG->dirroot . '/blocks/personallearningplan/block_personallearningplan.php');
require_once($CFG->dirroot . '/blocks/selfenrolbasedlearningplan/block_selfenrolbasedlearningplan.php');
require_once($CFG->libdir . '/grade/constants.php');
require_once($CFG->libdir . '/grade/grade_category.php');
require_once($CFG->libdir . '/grade/grade_item.php');
require_once($CFG->libdir . '/grade/grade_grade.php');
require_once($CFG->libdir . '/grade/grade_scale.php');
require_once($CFG->libdir . '/grade/grade_outcome.php');
require_once("{$CFG->libdir}/completionlib.php");
require_once("{$CFG->dirroot}/grade/querylib.php");
global $CFG, $OUTPUT, $COURSE, $USER, $DB;
require_once("{$CFG->libdir}/gradelib.php");
echo '<style type="text/css">
        .preloader {
            height:120px; background:url("' . $CFG->wwwroot . '/theme/tatachem/pix/spinner.gif") center center no-repeat;
        }
.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #434a54;
  border: none;
  line-height: 28px;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #434a54;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 14px;
  width: 11px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}
body {
  margin: 0px;
  padding: 0px;
}

    </style>';
$chartdata = user_course_completed($USER->id);
$o = html_writer::tag('h5', get_string('mycourses'));
$courses = enrol_get_my_courses();
if (count($courses) > 0) {
    $o .= html_writer::tag('center', html_writer::div(html_writer::div('', 'preloader'), '', array('id' => 'donutchart')));
} else {
    $o .= html_writer::tag('center', 'You do not have any assigned courses.');
}
$content = html_writer::div($o, 'chartheading');
$i = '<i class="fa %s fntcn"></i>';
$blocks = get_block_instance();
$header =  '<div style="display: block;position: relative;"><i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i></div>';
$link = '';
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot), sprintf($i . ' Home', 'fa-home')), array('class' => 'testlist'));

/*
  |------------------------
  |  My courses menus
  |------------------------
 */
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=my'),
        sprintf($i . ' My Courses', 'fa-book')), array('class' => 'testlist collapsed active',  'data-toggle'=>"collapse",'data-target'=>"#courses" ));
$link .= html_writer::start_tag('ul', array('class' => 'ultest', 'id'=>'coursdes'));
if(isset($blocks['personallearningplan']) && $blocks['personallearningplan']){
    $link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=personallearningplan'),
            sprintf($i . get_string('personallearningplan'), 'fa-angle-right'), array('class' => 'allfnticon')));
}
if(isset($blocks['selfenrolbasedlearningplan']) && $blocks['selfenrolbasedlearningplan']){
    $link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=selfenrolbasedlearningplan'),
        sprintf($i . get_string('selfenrolbasedlearningplan'), 'fa-angle-right'), array('class' => 'allfnticon')));
}
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=assigned'),
        sprintf($i . get_string('myassignedcourses'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=avail'),
        sprintf($i . get_string('myavailablecourses'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(new moodle_url('#'), 
        sprintf($i . 'Classroom Training', 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(new moodle_url('#'),
        sprintf($i . 'Training History', 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(
        new moodle_url($CFG->wwwroot . '/calendar/view.php', array('view' => 'month', 'time' => time(), 'course' => 1)),
        sprintf($i . get_string('skillcalender'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::end_tag('ul');
/*
  |-------------------
  | Report menus
  |-------------------
 */
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/statuscourses.php'), 
        sprintf($i . ' Reports', 'fa-book')), array('class' => 'testlist collapsed active',  'data-toggle'=>"collapse",'data-target'=>"#reports" ));

$link .= html_writer::start_tag('ul', array('class' => 'ultest', 'id'=>'repdorts'));
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot . '/my/mycsrpt.php', array('section' => 'csrpt')), sprintf($i . get_string('coursereports'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot . '/my/courses_report.php'), sprintf($i . get_string('coursestat'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot . '/my/mycsrpt.php', array('section' => 'csrpt')), sprintf($i . get_string('assessmentreports'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::end_tag('ul');


$content .= $header.html_writer::div(html_writer::div($link, 'listmndiv'),'menu-content', array('id'=>'menu-content'));

echo html_writer::div(html_writer::div($content, 'row-fluid'), 'span3 userpicdiv  nav-side-menu');
echo html_writer::tag('script','',array('type'=>'text/javascript', 'src' => $CFG->wwwroot.'/my/js/jsapi.js'));
echo html_writer::tag('script','', array('type'=>'text/javascript', 'src' => $CFG->wwwroot.'/my/js/uds_api_contents.js'));

echo '<script type="text/javascript">
        function load_dashbord_chart() {
            var chardata = '.$chartdata.';
            var data = google.visualization.arrayToDataTable(chardata);
            var options = {
                    width: "80%",
                    height: 120,
                    colors: ["#04B996", "#EE902E", "#A0A0A0"],
                    bar: {groupWidth: "40%"},
                    backgroundColor: "#D2D0D1",
                    chartArea: {width: "90%", "height": "70%"},
                    legend: {"position": "bottom"},
                    is3D: true,
                };
            new google.visualization.PieChart(document.getElementById("donutchart")).draw(data,options);
        }
        google.setOnLoadCallback(load_dashbord_chart); 
    </script>';