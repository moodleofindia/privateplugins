<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_skilldefault
 * @copyright 2014 redPIthemes
 *
 */
$standardlayout = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype();

global $PAGE, $DB, $USER;
$courseid = optional_param('id', '', PARAM_INT);
if (!empty($courseid)) {
    $coursename = $DB->get_field_sql("SELECT fullname from {course} WHERE id = $courseid");
} else {
    $coursename = '';
}
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google web fonts -->
        <?php
        require_once(dirname(__FILE__) . '/includes/fonts.php');
        require_once($CFG->libdir . '/blocklib.php');
        ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(); ?>>

        <?php echo $OUTPUT->standard_top_of_body_html(); ?>

        <div id="wrapper">

            <?php
            require_once(dirname(__FILE__) . '/includes/header.php');
            $blocks = get_block_instance();
            include_once($CFG->libdir . '/completionlib.php');
            ?>

            <!-- <div class="text-center" style="line-height:1em;">
                    <img src="<?php // echo $CFG->wwwroot;  ?>/theme/skilldefault/pix/bg/shadow.png" class="slidershadow" alt="">
            </div> -->

            <!-- Start Main Regions -->

            <div id="page-content" class="row-fluid">
               <?php include 'leftmenu.php';?>
                <div id="<?php echo $regionbsid ?>" class="span9  omitdfwidth">
                    <div class ="row-fluid" id="userbanner">
                        <img src="<?php echo $CFG->wwwroot . '/theme/tatachem/pix/sample.jpg' ?>" width="100%">
                        <!-- <h2 class ="userhdear">hhhhhhhhhhhhh</h2>
                         <p class ="userdesvc">yyyyyyyyyyyyy</p> -->
                    </div>
                    <div class ="row-fluid">
                        <div class="input-group searchfirstdiv" align ="center">
                            <?php
                            global $DB;
                            $allcourses = $DB->get_records_sql("SELECT id,fullname  FROM {course} where visible =1 AND id !=1");
                            ?>
                            <button class="btn btn-default headersearchbox" onclick="get_data()" type="button"><i class="fa fa-search fa-rotate-90"></i></button>
                            <input list="coursesearch" name="coursesearch" id ="coursename" value=""  placeholder = "Search Courses">
                            <datalist id="coursesearch">
                                <?php foreach ($allcourses as $course) { ?>
                                    <option value="<?php echo $course->fullname ?>" data-xyz ="<?php echo $course->id ?>">
                                    <?php } ?>
                            </datalist>

                        </div>

                    </div>
                    <?php if (isloggedin() and ! isguestuser()) { ?>
                        <header id="page-header1" class="clearfix">
                            <div id="page-navbar" class="clearfix">
                                <div class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></div>
                                <?php if (is_siteadmin()) { ?>
                                    <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
                                <?php } ?>
                            </div>
                        </header>
                    <?php } ?>
                    <div class ="row-fluid">
                        <?php
                        if (isset($blocks['personallearningplan']) && $blocks['personallearningplan']) {
                            ?>
                            <div class="span4 fstcsc">
                                <a href="<?php echo $CFG->wwwroot . '/my/listcourse.php?section=personallearningplan' ?>" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </div>
                                    <div align="center">
                                        <p>Personal Learning Courses</p>
                                    </div>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        if (isset($blocks['selfenrolbasedlearningplan']) && $blocks['personallearningplan']) {
                            ?>
                            <div class="span4 scdcsc">
                                <a href="<?php echo $CFG->wwwroot . '/my/listcourse.php?section=selfenrolbasedlearningplan' ?>" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </div>
                                    <div align="center">
                                        <p>Selfenrol Courses</p>
                                    </div>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        if (isset($blocks['availablecourses']) && $blocks['availablecourses']) {
                            ?>
                            <div class="span4 thrdcsc">
                                <a href="<?php echo $CFG->wwwroot . '/my/listcourse.php?section=selfenrolbasedlearningplan' ?>" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </div>
                                    <div align="center">
                                        <p>Available Courses</p>
                                    </div>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class ="row-fluid mrgtp20">
                        <div class="span4 frthcsc">
                            <a href="<?php echo $CFG->wwwroot . '/calendar/view.php?view=month&time=' . time() . '&course=1' ?>" class="text-center allfnticon">
                                <div class="rounded-icon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div align="center">
                                    <p>Calender</p>
                                </div>
                            </a>
                        </div>
                        <div class="span4 fifthcsc">
                            <a href ="#" class="text-center allfnticon">
                                <div class="rounded-icon">
                                    <i class="fa fa-bullhorn"></i>
                                </div>
                                <div align="center">
                                    <p> Latest News</p>
                                </div>
                            </a>
                        </div>
                        <div class="span4 sixthcsc">
                            <a href="<?php echo $CFG->wwwroot . '/user/profile.php?id=' . $USER->id ?>" class="text-center allfnticon">
                                <div class="rounded-icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div align="center">
                                    <p>My Profiles</p>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class ="row-fluid mrgtp20">
                        <div class="span4">
                            <div class="row-fluid">
                                <div class="span6 smallfrtcsc">
                                    <a href="<?php echo $CFG->wwwroot . '/tag/search.php' ?>" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-tags"></i>
                                        </div>
                                        <div align="center">
                                            <p>Tags</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="span6 smallscdcsc">
                                    <a href="<?php echo $CFG->wwwroot . '/user/files.php' ?>" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-files-o"></i>
                                        </div>
                                        <div align="center">
                                            <p>Private Files</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row-fluid mrgtp20">
                                <div class="span6 smallthirdcsc">
                                    <a href="#" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-commenting-o"></i>
                                        </div>
                                        <div align="center">
                                            <p>Comments</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="span6 smallfourthcsc">
                                    <a href="#" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div align="center">
                                            <p>Online Users</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="span4 fstcsc">
                            <a href="<?php echo $CFG->wwwroot . '/badges/view.php?type=1' ?>" class="text-center allfnticon">
                                <div class="rounded-icon">
                                    <i class="fa fa-certificate"></i>
                                </div>
                                <div align="center">
                                    <p>Latest Badges</p>
                                </div>
                            </a>
                        </div>
                        <div class="span4">
                            <div class="row-fluid">
                                <div class="span6 smallfrtcsc">
                                    <a href="<?php echo $CFG->wwwroot . '/my/statuscourses.php' ?>" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-bug "></i>
                                        </div>
                                        <div align="center">
                                            <p>My Reports</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="span6 smallscdcsc">
                                    <a href="<?php echo $CFG->wwwroot . '/calendar/view.php?view=upcoming&course=1' ?>" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-flag"></i>
                                        </div>
                                        <div align="center">
                                            <p>Upcoming Events</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class ="row-fluid lastcsc mrgtp20">
                                <a href="<?php echo $CFG->wwwroot . '/my/blockallcourses.php' ?>" class="text-center allfnticon">
                                    <div class="smallrounded-icon">
                                        <i class="fa fa-book"></i>
                                    </div>
                                    <div align="center">
                                        <p>My Courses</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </div>
            </div>
            <!-- End Main Regions -->
            <a href="#top" class="back-to-top"><i class="fa fa-chevron-circle-up fa-3x"></i><p><?php print_string('backtotop', 'theme_tatachem'); ?></p></a>
            <footer id="page-footer" class="container-fluid">
                <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>
            </footer> 
            <?php echo $OUTPUT->standard_end_of_body_html() ?>
        </div>
        <!--[if lte IE 9]>
        <script src="<?php echo $CFG->wwwroot; ?>/theme/skilldefault/javascript/ie/matchMedia.js"></script>
        <![endif]-->
        <script>
            jQuery(document).ready(function ($) {
                $('.navbar .dropdown').hover(function () {
                    $(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
                }, function () {
                    var na = $(this)
                    na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function () {
                        na.removeClass('extra-nav-class')
                    })
                });

            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var offset = 220;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.back-to-top').fadeIn(duration);
                    } else {
                        jQuery('.back-to-top').fadeOut(duration);
                    }
                });

                jQuery('.back-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })


                var setHeight = $('#wrapper').outerHeight();
                // returns e.g. 687
                //alert(setHeight);

                //$(".listmndiv").css({'height:' +setHeight+"px !important" });
                //$(".listmndiv").css({ 'height': setHeight + "px !important" });
                $(".listmndiv").height(setHeight);

            });
        </script>
        <script>


            var coursesearch = {pageurl: "<?php echo $CFG->wwwroot . '/course/view.php?id=' ?>", coursename: "<?php echo $coursename ?>"};

            function get_data() {
                //document.getElementById('main-panel').innerHTML ='';
                var coursename = document.getElementById('coursename').value;
                var id = $('#coursesearch option').filter(function () {
                    return this.value === coursename;
                }).data('xyz');
                if (id) {
                    var searchurl = coursesearch.pageurl + id;

                    window.location.assign(searchurl);
                }
            }
        </script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var jsondata = <?php echo $googlechart; ?>;
                var data = new google.visualization.DataTable(jsondata);

                var options = {
                    'width': 400,
                    'height': 300,
                    bar: {groupWidth: "40%"},
                    'chartArea': {'width': '90%', 'height': '80%'},
                    'legend': {'position': 'right'},
                    animation: {duration: 1000, easing: 'out'}
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, options);
            }
        </script>
    </body>
</html>