<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_skilldefault
 * @copyright 2014 redPIthemes
 *
 */
//include_once($CFG->dirroot.'/my/locallib.php');
$standardlayout = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;
$carousel_pos = $PAGE->theme->settings->carousel_position;
require_once(dirname(__FILE__) . '/includes/carousel.php');
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
echo $OUTPUT->doctype();
global $PAGE, $DB, $USER;
$courseid = optional_param('id', '', PARAM_INT);
if (!empty($courseid)) {
    $coursename = $DB->get_field_sql("SELECT fullname from {course} WHERE id = $courseid");
} else {
    $coursename = '';
}
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google web fonts -->
        <?php
        require_once(dirname(__FILE__) . '/includes/fonts.php');
        require_once($CFG->libdir . '/blocklib.php');
        ?>
        <style type="text/css">
        .badge-txt
        {
            float: right;
            position: relative;
            top: 6px;
            color: white;
            z-index: 999;
            right: -23%;
            font-weight: 600;
            border: solid 0px rgb(0, 0, 0);
            padding: 0px 9px;
            border-radius: 11px;
            background-color: rgba(0, 0, 0, 0.11);
        }
        .carousel-inner{height:200px;}
        </style>
    </head>
    <body <?php echo $OUTPUT->body_attributes(); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div id="wrapper">
            <?php
            require_once(dirname(__FILE__) . '/includes/header.php');
            $block_manager = new block_manager($PAGE);
            include_once($CFG->libdir . '/completionlib.php');
            include_once($CFG->dirroot . '/grade/querylib.php');
            include_once($CFG->libdir . '/gradelib.php');
            ?>

            <!-- <div class="text-center" style="line-height:1em;">
                    <img src="<?php // echo $CFG->wwwroot;  ?>/theme/skilldefault/pix/bg/shadow.png" class="slidershadow" alt="">
            </div> -->

            <!-- Start Main Regions -->
            <div id="page-content" class="row-fluid">
                <div class="span12 padboth10">
                    <?php include("leftmenu.php"); ?>
                    <div id="<?php echo $regionbsid ?>" class="span9  omitdfwidth">
                        <!-- carasoul slider begins -->

                        	<?php require_once('includes/dashboardslider.php'); ?>

						<!--  carasoul ends -->
                        <div class ="row-fluid">
                            <!-- <div class="input-group searchfirstdiv" align ="center">
                            <?php
                            global $DB;
                            $allcourses = $DB->get_records_sql("SELECT id,fullname  FROM {course} where visible =1 AND id !=1");
                            ?>
                                 <button class="btn btn-default headersearchbox" onclick="get_data()" type="button"><i class="fa fa-search fa-rotate-90"></i></button>
                                <input list="coursesearch" name="coursesearch" id ="coursename" value=""  placeholder = "Search Courses">
                                <datalist id="coursesearch">
                            <?php foreach ($allcourses as $course) { ?>
                                                        <option value="<?php echo $course->fullname ?>" data-xyz ="<?php echo $course->id ?>">
                            <?php } ?>
                                </datalist>
                            </div> -->
                        </div>
                        <?php if (isloggedin() and ! isguestuser()) { ?>
                            <header id="page-header1" class="clearfix">
                                <div id="page-navbar" class="clearfix">
                                    <div class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></div>
                                    <?php if (is_siteadmin()) { ?>
                                        <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
                                    <?php } ?>
                                </div>
                            </header>
                        <?php } ?>
                        <div class ="row-fluid">
                            <?php
                            if (isset($blocks['personallearningplan']) && $blocks['personallearningplan']) {
                                if(isset(recommended_course()['courses']))
                                {
                                    $recommmedcourses = recommended_course()['courses'];
                                }
                                else
                                {
                                    $recommmedcourses = 0;
                                }
                                echo '<div class="span4 fstcsc">
                                        <img src="'.$CFG->wwwroot . '/theme/'.$CFG->theme.'/pix/tr01.png" class="pull-right" />
                                        <span class="badge-txt">'.count($recommmedcourses).'</span>
                                        <a href="'.$CFG->wwwroot . '/my/listcourse.php?section=personallearningplan" class="text-center allfnticon">
                                            <div class="rounded-icon">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </div>
                                            <div align="center">
                                                <p>Personal Learning Courses</p>
                                            </div>
                                        </a>
                                    </div>';
                            }
                            if (isset($blocks['selfenrol']) && $blocks['selfenrol']) {
                                $courses = enrol_get_my_courses('id');
                                $count = 0;
                                foreach ($courses as $course) {
                                    $enrolments = user_enrolments($course->id,$USER->id);
                                    foreach ($enrolments as $enrol)
                                    {
                                        if ($enrol->enrol == 'self')
                                        {
                                            $count++;    
                                        }
                                    }
                                }
                                echo '<div class="span4 scdcsc">
                                     <img src="'.$CFG->wwwroot . '/theme/'.$CFG->theme.'/pix/tr02.png" class="pull-right" />
                                     <span class="badge-txt">'.$count.'</span>
                                     <a href="'.$CFG->wwwroot . '/my/listcourse.php?section=selfenrol" class="text-center allfnticon">
                                         <div class="rounded-icon">
                                             <i class="fa fa-pencil-square-o"></i>
                                         </div>
                                         <div align="center">
                                             <p>Self Enrolled Courses</p>
                                         </div>
                                     </a>
                                 </div>';
                            }
                            $courses = $DB->count_records('course');
                            $enrolled = enrol_get_my_courses('id');
                            $count = $courses - count($enrolled);

                            echo '<div class="span4 thrdcsc">
                                    <img src="'.$CFG->wwwroot . '/theme/'.$CFG->theme.'/pix/tr03.png" class="pull-right" />
                                    <span class="badge-txt">'.($count-1).'</span>
                                    <a href="'.$CFG->wwwroot . '/my/listcourse.php?section=avail" class="text-center allfnticon">
                                       <div class="rounded-icon">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </div>
                                        <div align="center">
                                         <p>Available Courses</p>
                                        </div>
                                    </a>
                                  </div>';
                        ?>
                        </div>
                        <div class ="row-fluid mrgtp20">
                            <?php
                            
                            if (isset($blocks['calendar_month']) && $blocks['calendar_month']) {
                                echo '<div class="span4 frthcsc">
                                        <a href="'.$CFG->wwwroot . '/calendar/view.php?view=month&time=' . time() . '&course=1" class="text-center allfnticon">
                                            <div class="rounded-icon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <div align="center">
                                                <p>Calender</p>
                                            </div>
                                        </a>
                                    </div>';
                            }                           
                            if (isset($blocks['news_items']) && $blocks['news_items']) {
                                echo '<div class="span4 fifthcsc">
                                        <a href ="#" class="text-center allfnticon">
                                            <div class="rounded-icon">
                                                <i class="fa fa-bullhorn"></i>
                                            </div>
                                            <div align="center">
                                                <p> Latest News</p>
                                            </div>
                                        </a>
                                    </div>';
                            } 
                            if (isset($blocks['myprofile']) && $blocks['myprofile']) {
                                echo '<div class="span4 sixthcsc">
                                        <a href="'.$CFG->wwwroot . '/user/profile.php?id=' . $USER->id .'" class="text-center allfnticon">
                                            <div class="rounded-icon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div align="center">
                                                <p>My Profiles</p>
                                            </div>
                                        </a>
                                    </div>';
                            }                 
                            ?>
                        </div>
                        <div class ="row-fluid mrgtp20">
                            <div class="span4">
                                <div class="row-fluid">
                                    <?php
                                    if (isset($blocks['tags']) && $blocks['tags']) {
                                        echo '<div class="span6 smallfrtcsc">
                                                <a href="'.$CFG->wwwroot . '/tag/search.php" class="text-center allfnticon">
                                                    <div class="smallrounded-icon">
                                                        <i class="fa fa-tags"></i>
                                                    </div>
                                                    <div align="center">
                                                        <p>Tags</p>
                                                    </div>
                                                </a>
                                            </div>';
                                    }
                                    if (isset($blocks['private_files']) && $blocks['private_files']) {
                                        echo '<div class="span6 smallscdcsc">
                                                <a href="'.$CFG->wwwroot . '/user/files.php" class="text-center allfnticon">
                                                    <div class="smallrounded-icon">
                                                        <i class="fa fa-files-o"></i>
                                                    </div>
                                                    <div align="center">
                                                        <p>Private Files</p>
                                                    </div>
                                                </a>
                                            </div>';
                                    }
                                    ?>
                                </div>
                                <div class="row-fluid mrgtp20">
                                    <?php
                                    if (isset($blocks['comments']) && $blocks['comments']) {
                                        echo '<div class="span6 smallthirdcsc">
                                                <a href="#" class="text-center allfnticon">
                                                    <div class="smallrounded-icon">
                                                        <i class="fa fa-commenting-o"></i>
                                                    </div>
                                                    <div align="center">
                                                        <p>Comments</p>
                                                    </div>
                                                </a>
                                            </div>';
                                    }
                                    if (isset($blocks['online_users']) && $blocks['online_users']) {
                                        echo '<div class="span6 smallfourthcsc">
                                                <a href="#" class="text-center allfnticon">
                                                    <div class="smallrounded-icon">
                                                        <i class="fa fa-users"></i>
                                                    </div>
                                                    <div align="center">
                                                        <p>Online Users</p>
                                                    </div>
                                                </a>
                                            </div>';
                                    };
                                    ?>
                                </div>
                            </div>
                            <?php if(isset($blocks['badges']) && $blocks['badges']){
                                echo '<div class="span4 fstcsc">
                                <a href="'. $CFG->wwwroot . '/badges/mybadges.php'.'" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-certificate"></i>
                                    </div>
                                    <div align="center">
                                        <p>Latest Badges</p>
                                    </div>
                                </a>
                            </div>';
                           } ?>
                            <div class="span4">
                                <div class="row-fluid">
                                    <div class="span6 smallfrtcsc">
                                        <a href="<?php echo $CFG->wwwroot . '/my/statuscourses.php' ?>" class="text-center allfnticon">
                                            <div class="smallrounded-icon">
                                                <i class="fa fa-bug "></i>
                                            </div>
                                            <div align="center">
                                                <p>My Reports</p>
                                            </div>
                                        </a>
                                    </div>
                                    <?php 
                                    if(isset($blocks['calendar_upcoming']) && $blocks['calendar_upcoming']) {
                                        echo '<div class="span6 smallscdcsc">
                                            <a href="'.$CFG->wwwroot . '/calendar/view.php?view=upcoming&course=1" class="text-center allfnticon">
                                                <div class="smallrounded-icon">
                                                    <i class="fa fa-flag"></i>
                                                </div>
                                                <div align="center">
                                                    <p>Upcoming Events</p>
                                                </div>
                                            </a>
                                        </div>';
                                    }
                                    ?>
                                </div>
                                <div class ="row-fluid lastcsc mrgtp20">
                                    <a href="<?php echo $CFG->wwwroot . '/my/blockallcourses.php' ?>" class="text-center allfnticon">
                                        <div class="smallrounded-icon">
                                            <i class="fa fa-book"></i>
                                        </div>
                                        <div align="center">
                                            <p>My Courses</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </div>
                </div> <!-- end of span12 -->
            </div>
            <!-- End Main egions -->
            <a href="#top" class="back-to-top"><i class="fa fa-chevron-circle-up fa-3x"></i><p><?php print_string('backtotop', 'theme_'.$CFG->theme); ?></p></a>
            <footer id="page-footer" class="container-fluid">
                <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>
            </footer> 
            <?php echo $OUTPUT->standard_end_of_body_html() ?>
        </div>
        <!--[if lte IE 9]>
        <script src="<?php echo $CFG->wwwroot; ?>/theme/skilldefault/javascript/ie/matchMedia.js"></script>
        <![endif]-->
        <script>
            jQuery(document).ready(function ($) {
                $('.navbar .dropdown').hover(function () {
                    $(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
                }, function () {
                    var na = $(this)
                    na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function () {
                        na.removeClass('extra-nav-class')
                    })
                });

            });
            jQuery(document).ready(function () {
                var offset = 220;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.back-to-top').fadeIn(duration);
                    } else {
                        jQuery('.back-to-top').fadeOut(duration);
                    }
                });

                jQuery('.back-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
                var setHeight = $('#wrapper').outerHeight();
                $(".listmndiv").height(setHeight);
            });
        </script>
        <script>
            var coursesearch = {pageurl: "<?php echo $CFG->wwwroot . '/course/view.php?id=' ?>", coursename: "<?php echo $coursename ?>"};
            function get_data() {
                //document.getElementById('main-panel').innerHTML ='';
                var coursename = document.getElementById('coursename').value;
                var id = $('#coursesearch option').filter(function () {
                    return this.value === coursename;
                }).data('xyz');
                if (id) {
                    var searchurl = coursesearch.pageurl + id;

                    window.location.assign(searchurl);
                }
            }
        </script>
    </body>
</html>
