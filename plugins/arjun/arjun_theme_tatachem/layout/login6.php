<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_skilldefault
 * @copyright 2014 redPIthemes
 *
 */
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google web fonts -->
        <?php require_once(dirname(__FILE__) . '/includes/fonts.php'); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(); ?>>

        <?php echo $OUTPUT->standard_top_of_body_html(); ?>

        <div id="wrapper">

            <?php require_once(dirname(__FILE__) . '/includes/header.php'); ?>

            <div class="text-center" style="line-height:1em;">
                <img src="<?php echo $CFG->wwwroot; ?>/theme/skilldefault/pix/bg/shadow.png" class="slidershadow" alt="">
            </div>

            <div id="page" class="container-fluid">

                <?php if (isloggedin() and ! isguestuser()) { ?>
                    <header id="page-header" class="clearfix">
                        <div id="page-navbar" class="clearfix">
                            <div class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></div>
                            <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
                        </div>
                    </header>
                <?php } ?>


                <div id="page-content" class="row-fluid">
                    <section id="region-main" class="span12">
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </section>
                </div>

                <a href="#top" class="back-to-top"><i class="fa fa-chevron-circle-up fa-3x"></i><p><?php print_string('backtotop', 'theme_tatachem'); ?></p></a>

            </div>

            <footer id="page-footer" class="container-fluid">
                <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>
            </footer>

            <?php echo $OUTPUT->standard_end_of_body_html() ?>


            <!--[if lte IE 9]>
            <script src="<?php echo $CFG->wwwroot; ?>/theme/skilldefault/javascript/ie/matchMedia.js"></script>
            <![endif]-->


            <script>
                jQuery(document).ready(function ($) {
                    $('.navbar .dropdown').hover(function () {
                        $(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
                    }, function () {
                        var na = $(this)
                        na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function () {
                            na.removeClass('extra-nav-class')
                        })
                    });

                });
            </script>

            <script type="text/javascript">
                jQuery(document).ready(function () {
                    var offset = 220;
                    var duration = 500;
                    jQuery(window).scroll(function () {
                        if (jQuery(this).scrollTop() > offset) {
                            jQuery('.back-to-top').fadeIn(duration);
                        } else {
                            jQuery('.back-to-top').fadeOut(duration);
                        }
                    });

                    jQuery('.back-to-top').click(function (event) {
                        event.preventDefault();
                        jQuery('html, body').animate({scrollTop: 0}, duration);
                        return false;
                    })
                    // $(".loginsub").prepend("<div class='innerheader'><span class='power'>Explore the enhanced Learning Programs</span><br><span class='redline'>Get SKILLDOMISED !</span><span class='line'></span><br></div><div class='lefttext'><span class='subtext'>Please enter login information to continue</span></div>");


                    $(".loginbox h2").css("display", "none");

                });
            </script>


    </body>
</html>
<!--
0f5c8c
font color - myriad pro 5ca5cc
-->
<style>
    #block-login-page label, #submit {
        position: absolute;
        width: 33px;
        height: 34px;
        background: #E5E5E5;
        margin: 0;
        text-align: center;
        line-height: 2.2em;
        color: #fff;

        font-size: 1.2em;
    }
    #block-login-page input[type=text] {
        width: 292px;
        height: 34px;
        padding: 0 38px;
        border: 0;
        margin-bottom: 10px;
        display: block;
        color: #626262;
        margin-bottom: 10px;
    }
    #block-login-page input[type=password] {
        width: 292px;
        height: 34px;
        padding: 0 38px;
        border: 0;
        margin-bottom: 10px;
        display: block;
        color: #626262;
        margin-bottom: 10px;
    }
    #block-login-page #user { background-color:#FF614B;}
    #block-login-page #pass { background-color:#F7B624;}

    /* #username {
                background: url('<?php echo $CFG->wwwroot . "/theme/skilldefault/pix/login_bg/people.jpg" ?>');
        background-size: 36px 28px;
        background-repeat: no-repeat;
        background-color: #fff;
    }
    #password {
                    background: url('<?php echo $CFG->wwwroot . "/theme/skilldefault/pix/login_bg/key.jpg" ?>');
        background-size: 36px 28px;
        background-repeat: no-repeat;
        background-color: #fff;
    } */
    .forgetpass {
        text-align: right;
        padding-top: 10px;
    }
    .forgetpass a {
        color:#474747;
        text-decoration:underline;
    }
    .loginbox .loginform .form-input input:focus {
        text-align:left;
    }
    .mrgtop1 {
        margin-top: 1% !important;
    }
    .loginbox h2, .loginbox .subcontent {
        text-align:left !important;
    }
    .loginpanel .error{
        padding: 4px;
        color: #fff !important;
    }
    .loginerrors {
        background:#E95460;
        text-align: center;
        font-size: 20px;
        padding: 13px;

    }
    .desc {
        display:none !important;
    }
    #wrapper {
        border-top:none !important;
        background: #fff !important;
    }
    .loginbox .subcontent {
        margin-top:6%;
    }
    .login-header {
        display:none;
    }
    #page-footer {
        border-top:none !important;
    }

    .footerlinks {
        border-top:none !important;
    }
    .navbar {
        display:none;
    }
    .text-center {
        display:none;
    }

    #page-footer {
        margin-top:0px !important;
    }
    #page {
        background-image: url('<?php echo $CFG->wwwroot . "/theme/tatachem/pix/login_bg/BG_final.png" ?>');
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height:480px;
        padding:0px !important;
    }
    .power {
        font-family: "Myriad Pro",sans-serif;
        font-size: 22px;
        color: #000;
        font-weight: bold;
    }

    .redline {
        font-family: "Raleway Bold",sans-serif;
        font-size: 44px;
        color: #BB1904;
        font-weight: bold;
        line-height:1.2em;
    }
    .lefttext {
        text-align:left !important;
    }
    .subtext {
        text-align:left;

        color: #5ca5cc;
        font-size: 17px;
        line-height: 1.7em;	
    }
    .line {
        color: #F8D306;
        font-size: 4em;
    }
    #loginbtn {
        float:right;
        text-transform: uppercase;
    }
    .loginsub{
        background-color: transparent !important;
        width: 36%;
        position: absolute;
        left: 37%;
    }   
    #wrapper {
        height: 440px;
    }
    .innerheader {
        border-bottom: 1px solid #fff;
        padding-top: 19px;
        padding-bottom: 19px;
        background-color: transparent ;
    }

    .loginbox .loginform .form-label {
        width: 0% !important; 
        text-align: none !important;  
    }
    .loginbox .loginform .form-input input {
        text-align:center;
    }
    .loginbox .loginform .form-input {
        float:left !important;
        width:100% !important;
        text-align: center;
    }
    @media screen and (min-width: 320px) and (max-width: 395px) {
        .loginsub{
            width: 62%;
            left: 22%;
        }
        #wrapper {
            height:568px;
        }
        .loginerrors{
            font-size: 14px;
            padding: 0px;
        }
        .redline {
            font-size: 17px;
        }
        .power {
            font-size: 8px;
        }
        .subtext{
            font-size: 9px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 120px;
        }
        #manonplane{
            position: absolute;
            top: 43%;
            width: 70%;
            left: -30%;
        }

    }
    @media screen and (min-width: 395px) and (max-width: 479px) {
        .loginsub{
            width: 62%;
            left: 22%;
        }
        #wrapper {
            height: 767px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 180px;
        }

        .redline {
            font-size: 24px;
        }


    }
    @media screen and (min-width: 480px) and (max-width: 602px) {
        .loginsub{
            width: 62%;
            left: 22%;
        }

        #wrapper {
            height: 320px;
        }

        .loginbox .subcontent{
            margin-top:-6%;
        }

        .redline {
            font-size: 24px;
        }
        .power{
            font-size:12px;
        }
        .innerheader{
            padding-top: 58px;
            padding-bottom: 2px;
        }
        .loginerrors{
            font-size: 14px;
            padding: 0px;
        }
        #block-login-page input[type="text"]{
            width:222px;
        }
        #block-login-page input[type="password"]{
            width:222px;
        }
        #paperplane {
            right: -10%;
            width: 15%;
        }
        #manonplane {
            top: 60%;
            width: 56%;
            left: -22%;
        }
        .subtext{
            font-size: 16px;
            line-height: 0.7em;
        }
        .helplink {
            float: left;
            padding-left: 20px;
        }
    }
    @media screen and (min-width: 603px) and (max-width: 966px) {
        .loginsub{
            width: 62%;
            left: 22%;
        }
        #wrapper {
            height: 600px
        }
        .loginbox .loginform .form-input input {

        }

        .redline {
            font-size: 33px;
        }
        .power {
            font-size: 16px;
        }
        #manonplane {
            position: absolute;
            top: 42%;
            width: 37%;
            left: -7%;
        }

    }

    @media screen and (min-width: 768px) and (max-width: 1023px) {

        #wrapper {
            height: 400px;
        }

        .redline {
            font-size: 24px;
        }


        .redline {
            font-size:35px;
        }
        .power {
            font-size:18px;
        }
        #block-login-page input[type="text"]{
            width:400px;
        }
        #block-login-page input[type="password"]{
            width:400px;
        }
        #manonplane {
            top: 64%;
            width: 35%;
            left: -6%;
        }
        #paperplane{
            right: -12%;
            width: 12%;
        }
    }

    @media screen and (min-width: 1024px) and (max-width: 1099px) {
        .loginsub{
            width: 47%;
            left: 29%;
            height: 350px;
        }
        #wrapper {
            height: 600px;
        }

        /*        #page {
                    height:760px;
                }*/
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 86%;
        }
        #manonplane {
            top: 45%;
            width: 25%;
            left: 3%;
        }
        .redline {
            font-size:35px;
        }
        .power {
            font-size:18px;
        }

    }

    @media screen and (min-width: 1100px) and (max-width: 1280px) {
        .loginsub{
            left: 35%;
        }
        #page {
            height:760px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 83%;
        }
        .power{
            font-size:16px;
        }
        .redline{
            font-size:35px;
        }
        #manonplane {
            top: 58%;
            width: 35%;
            left: 3%;
        }
        #page {
            height: 532px;
        }
    }
    @media screen and (min-width: 1281px) and (max-width: 1366px) {
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 83%;
        }
        .loginsub {
            left: 35%;
        }
        #manonplane {
            top: 55%;
            width: 26%;
            left: 3%;
        }


    }
    @media screen and (min-width: 1367px) and (max-width: 1680px) {
        .loginsub{
            left: 35%;
        }
        #page {
            min-height: 550px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 86%;
        }
        .power {
            font-size: 20px;
        }
        .redline{
            font-size:43px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 86%;
        }
        #manonplane {
            top: 60%;
            width: 24%;
            left: 3%;
        }
    }

    @media screen and (min-width: 1681px) and (max-width: 1800px) {
        .loginsub{
            left: 35%;
        }
        #page {
            height:760px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 86%;
        }
        .power { font-size:25px; }
        .redline { font-size:53px; }
        #manonplane {
            top: 70%;
            width: 26%;
            left: 5%;
        }

    }

    @media screen and (min-width: 1900px) {
        .loginsub{
            width: 30%; 
        }
        #page {
            height:760px;
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 86%;
        }
        .power { font-size:28px; }
        .redline { font-size:54px; }
    }
    @media screen and (min-width: 2000px) {
        .loginsub{
            width: 34%; 
        }
        #block-login-page input[type=text], #block-login-page input[type=password] {
            width: 86%;
        }
    }
    @media screen and (min-width: 2142px) {
        .loginsub{
            width: 23%; 
        }
    }
    @media screen and (min-width: 2380px) {
        .loginsub{
            width: 21%; 
        }
    }
    @media screen and (min-width: 2423px) {
        .loginsub{
            width: 20%; 
        }
    }
    @media screen and (min-width: 2581px) {
        .loginsub{
            width: 18%; 
            left: 39%;
        }
    }
    @media screen and (min-width: 2720px) {
        .loginsub{
            width: 17%;
            left: 40%;
        }
    }
    @media screen and (min-width: 2859px) {
        .loginsub{
            width: 15%; 
            left: 41%;
        }
    }
    @media screen and (min-width: 3040px) {
        .loginsub{
            width: 14%; 
            left: 42%;
        }
    }
    @media screen and (min-width: 3212px) {
        .loginsub{
            width: 13%; 
            left: 43%;
        }
    }
    @media screen and (min-width: 3377px) {
        .loginsub{
            width: 11%; 
            left: 44%;
        }
    }
    @media screen and (min-width: 3752px) {
        .loginsub{
            width: 11%; 
            left: 44%;
        }
    }

</style>    
