<style type="text/css">
	@font-face {
	font-family:'FontAwesome';
	src: url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/fontawesome-webfont.eot');
	src: url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/fontawesome-webfont.eot?#iefix') format('embedded-opentype'), url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/fontawesome-webfont.woff') format('woff'), url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/fontawesome-webfont.ttf') format('truetype'), url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/fontawesome-webfont.svg#fontawesomeregular') format('svg');
	font-weight:normal;
	font-style:normal;
	}
</style>
<?php if($PAGE->theme->settings->font_body == 1 || $PAGE->theme->settings->font_heading == 1) { ?>
<style type="text/css">
	@font-face {
	font-family:'open_sansregular';
	src:url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Regular-webfont.eot');
	src:url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Regular-webfont.woff') format('woff'),url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Regular-webfont.ttf') format('truetype'),url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Regular-webfont.svg#open_sansregular') format('svg');
	font-weight:normal;
	font-style:normal;
	}
	@font-face {
	font-family:'open_sansbold';
	src:url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Bold-webfont.eot');
	src:url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Bold-webfont.eot?#iefix') format('embedded-opentype'),url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Bold-webfont.woff') format('woff'),url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Bold-webfont.ttf') format('truetype'),url('<?php echo $CFG->wwwroot ?>/theme/tatachem/fonts/OpenSans-Bold-webfont.svg#open_sansbold') format('svg');
	font-weight:normal;
	font-style:normal;
	}
</style>
<?php }
if($PAGE->theme->settings->font_heading == 2) {
    echo '<link href="//fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 2 || $PAGE->theme->settings->font_heading == 3) {
    echo '<link href="//fonts.googleapis.com/css?family=Arimo" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 3 || $PAGE->theme->settings->font_heading == 4) {
    echo '<link href="//fonts.googleapis.com/css?family=Arvo" rel="stylesheet" type="text/css">';
} 
if($PAGE->theme->settings->font_heading == 5) {
    echo '<link href="//fonts.googleapis.com/css?family=Bevan" rel="stylesheet" type="text/css">';
} 
if($PAGE->theme->settings->font_body == 4 || $PAGE->theme->settings->font_heading == 6) {
    echo '<link href="//fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 5 || $PAGE->theme->settings->font_heading == 7) {
    echo '<link href="//fonts.googleapis.com/css?family=Cabin" rel="stylesheet" type="text/css">';
} 
if($PAGE->theme->settings->font_body == 6 || $PAGE->theme->settings->font_heading == 8) {
    echo '<link href="//fonts.googleapis.com/css?family=Cantata+One" rel="stylesheet" type="text/css">';
 }
if($PAGE->theme->settings->font_body == 7 || $PAGE->theme->settings->font_heading == 9) {
    echo '<link href="//fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 8 || $PAGE->theme->settings->font_heading == 10) {
    echo '<link href="//fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">';
} 
if($PAGE->theme->settings->font_body == 9 || $PAGE->theme->settings->font_heading == 11) {
    echo '<link href="//fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet" type="text/css">';
} 
if($PAGE->theme->settings->font_body == 10 || $PAGE->theme->settings->font_heading == 12) {
    echo '<link href="//fonts.googleapis.com/css?family=Gudea" rel="stylesheet" type="text/css">';
} 
if($PAGE->theme->settings->font_body == 11 || $PAGE->theme->settings->font_heading == 13) {
    echo '<link href="//fonts.googleapis.com/css?family=Imprima" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_heading == 14) {
    echo '<link href="//fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 12 || $PAGE->theme->settings->font_heading == 15) {
    echo '<link href="//fonts.googleapis.com/css?family=Lekton" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_heading == 16) {
    echo '<link href="//fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 13 || $PAGE->theme->settings->font_heading == 17) {
    echo '<link href="//fonts.googleapis.com/css?family=Nixie+One" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 14 || $PAGE->theme->settings->font_heading == 18) {
    echo '<link href="//fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_heading == 19) {
    echo '<link href="//fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 15 || $PAGE->theme->settings->font_heading == 20) {
    echo '<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 16 || $PAGE->theme->settings->font_heading == 21) {
    echo '<link href="//fonts.googleapis.com/css?family=Pontano+Sans" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 17 || $PAGE->theme->settings->font_heading == 22) {
    echo '<link href="//fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 18 || $PAGE->theme->settings->font_heading == 23) {
    echo '<link href="//fonts.googleapis.com/css?family=Raleway:300" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_heading == 24) {
    echo '<link href="//fonts.googleapis.com/css?family=Sansita+One" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 19 || $PAGE->theme->settings->font_heading == 25) {
    echo '<link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">';
}
if($PAGE->theme->settings->font_body == 20 || $PAGE->theme->settings->font_heading == 26) {
    echo '<link href="//fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet" type="text/css">';
}