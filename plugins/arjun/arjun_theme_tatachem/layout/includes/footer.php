<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_tatachem
 * @copyright 2014 redPIthemes
 *
 */
$footerl = 'footer-left';
$footerm = 'footer-middle';
$footerr = 'footer-right';
$defaultfootnote = html_writer::tag('p', 'Copyright © SKILLDOM Learning Solutions Pvt. Ltd. | All Rights Reserved');

$rightlink1 = html_writer::link('http://www.skilldom.co.in', 'http://www.skilldom.co.in', array('target' => '_blank'));
$defaultfootrightlink = html_writer::tag('span',$rightlink1);

$text = $PAGE->theme->settings->footrightlink;
$note = $PAGE->theme->settings->footnote;
$rightlink2 = html_writer::link($text,$text, array('target' => '_blank'));
$footrightlink = html_writer::tag('span',$rightlink2);

$hasfootnote = (empty($note)) ? $defaultfootnote : $note;
$hasfootrightlink = (empty($text)) ? $defaultfootrightlink : $footrightlink;

//$hasfooterleft = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('footer-left', $OUTPUT));
//$hasfootermiddle = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('footer-middle', $OUTPUT));
//$hasfooterright = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('footer-right', $OUTPUT));
echo html_writer::start_div('footerlinks');
echo html_writer::start_div('row-fluid');
$tag = '';
if($hasfootrightlink) {
$tag .= html_writer::tag('p', 'For more details visit: '.$hasfootrightlink, array('class' => 'helplink'));
}
if ($hasfootnote) {
    $tag .= html_writer::div($hasfootnote, 'footnote');
}
echo $tag;
echo html_writer::end_div();
if ($PAGE->theme->settings->socials_position == 0) {
    require_once('socials.php');
}
echo html_writer::end_div();
