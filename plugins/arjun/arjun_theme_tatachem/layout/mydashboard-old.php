<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parent theme: Bootstrapbase by Bas Brands
 * Built on: Essential by Julian Ridden
 *
 * @package   theme_tatachem
 * @copyright 2014 redPIthemes
 *
 */
$standardlayout = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
echo $OUTPUT->doctype();
global $PAGE, $DB, $USER;
$courseid = optional_param('id', '', PARAM_INT);
if (!empty($courseid)) {
    $coursename = $DB->get_field_sql("SELECT fullname from {course} WHERE id = $courseid");
} else {
    $coursename = '';
}
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google web fonts -->
        <?php
        require_once(dirname(__FILE__) . '/includes/fonts.php');
        require_once($CFG->libdir . '/blocklib.php');
        ?>
    </head>
    <body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div id="wrapper">
            <?php
            require_once(dirname(__FILE__) . '/includes/header.php');
            $block_manager = new block_manager($PAGE);
            include_once($CFG->libdir . '/completionlib.php');
            include_once($CFG->dirroot . '/grade/querylib.php');
            include_once($CFG->libdir . '/gradelib.php');
            ?>
            <!-- <div class="text-center" style="line-height:1em;">
                    <img src="<?php // echo $CFG->wwwroot; ?>/theme/tatachem/pix/bg/shadow.png" class="slidershadow" alt="">
            </div> -->
            <!-- Start Main Regions -->  
            <div id="page-content" class="row-fluid">		
                <div class="span12 padboth10">
<?php include("leftmenu.php"); ?>
                    <div id="<?php echo $regionbsid ?>" class="span9  omitdfwidth">
                        <div class ="row-fluid" id="userbanner">
                            <img src="<?php echo $CFG->wwwroot . '/theme/tatachem/pix/sample.jpg' ?>" width="100%">
                        </div>
                        <div class ="row-fluid">
                        </div>
<?php if (isloggedin() and ! isguestuser()) { ?>
                            <header id="page-header1" class="clearfix">
                                <div id="page-navbar" class="clearfix">
                                    <div class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></div>
                                    <?php if (is_siteadmin()) { ?>
                                        <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
    <?php } ?>
                                </div>
                            </header>
<?php } ?>
                        <div class ="row-fluid">
                            <!-- commenting this Mihir 15th feb -->
                            <!-- for available course -->
                            <?php
                            global $USER, $DB;
                            $courseuser = enrol_get_my_courses();
                            //$courseuser = $DB->get_records_sql("SELECT * from {course} where id !=1");
                            if ($courseuser) {
                                $maxnum3 = sizeof($courseuser);
                            } else {
                                $maxnum3 = 0;
                            }
                            ?>
                            <div class="span4 fstcsc" style=" ">
                                <img src="<?php echo $CFG->wwwroot . '/theme/tatachem/pix/tr01.png' ?>" class="pull-right" />
                                <span class="fstcsctext"><?php echo $maxnum3; ?></span>
                                <a href="<?php echo $CFG->wwwroot . '/my/listcourse.php?listname=assigned' ?>" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </div>
                                    <div align="center">
                                        <p>Assigned Courses</p>
                                    </div>
                                </a>
                            </div>
                            <div class="span4 scdcsc">
                                <a href="<?php echo $CFG->wwwroot . '/calendar/view.php?view=month&time=' . time() . '&course=1' ?>" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <div align="center">
                                        <p>Calender</p>
                                    </div>
                                </a>
                            </div>
                            <div class="span4 thrdcsc">
                                <a href="<?php echo $CFG->wwwroot . '/user/profile.php?id=' . $USER->id ?>" class="text-center allfnticon">
                                    <div class="rounded-icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <div align="center">
                                        <p>My Profiles</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class ="row-fluid mrgtp20">               
                        </div>
                        <div class ="row-fluid mrgtp20">
                        </div>
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </div>
                </div> <!-- end of span12 -->
            </div>
            <!-- End Main Regions -->
            <a href="#top" class="back-to-top"><i class="fa fa-chevron-circle-up fa-3x"></i><p><?php print_string('backtotop', 'theme_tatachem'); ?></p></a>
            <footer id="page-footer" class="container-fluid">
                <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>
            </footer> 
            <?php echo $OUTPUT->standard_end_of_body_html() ?>
        </div>
        <!--[if lte IE 9]>
        <script src="<?php echo $CFG->wwwroot; ?>/theme/tatachem/javascript/ie/matchMedia.js"></script>
        <![endif]-->
        <script>
            jQuery(document).ready(function ($) {
                $('.navbar .dropdown').hover(function () {
                    $(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
                }, function () {
                    var na = $(this)
                    na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function () {
                        na.removeClass('extra-nav-class')
                    })
                });
            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var offset = 220;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.back-to-top').fadeIn(duration);
                    } else {
                        jQuery('.back-to-top').fadeOut(duration);
                    }
                });
                jQuery('.back-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
                var setHeight = $('#wrapper').outerHeight() - 450;
                $(".listmndiv").height(setHeight);

            });
        </script>
        <script>
            var coursesearch = {pageurl: "<?php echo $CFG->wwwroot . '/course/view.php?id=' ?>", coursename: "<?php echo $coursename ?>"};
            function get_data() {
                //document.getElementById('main-panel').innerHTML ='';
                var coursename = document.getElementById('coursename').value;
                var id = $('#coursesearch option').filter(function () {
                    return this.value === coursename;
                }).data('xyz');
                if (id) {
                    var searchurl = coursesearch.pageurl + id;

                    window.location.assign(searchurl);
                }
            }
        </script>
    </body>
</html>