<?php

require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/my/locallib.php');
require_once($CFG->libdir . '/blocklib.php');
require_once($CFG->dirroot . '/blocks/moodleblock.class.php');
require_once($CFG->libdir . '/grade/constants.php');
require_once($CFG->libdir . '/grade/grade_category.php');
require_once($CFG->libdir . '/grade/grade_item.php');
require_once($CFG->libdir . '/grade/grade_grade.php');
require_once($CFG->libdir . '/grade/grade_scale.php');
require_once($CFG->libdir . '/grade/grade_outcome.php');
require_once("{$CFG->libdir}/completionlib.php");
require_once("{$CFG->dirroot}/grade/querylib.php");
global $CFG, $OUTPUT, $COURSE, $USER, $DB;
require_once("{$CFG->libdir}/gradelib.php");
echo html_writer::tag('script', '', array('type' => 'text/javascript', 'src' => $CFG->wwwroot . '/theme/tatachem/jquery/jquery.min.js'));
echo html_writer::tag('script', '', array('type' => 'text/javascript', 'src' => $CFG->wwwroot . '/theme/tatachem/jquery/bootstrap.min.js'));
echo '<style type="text/css">
        .preloader {
            height:120px; background:url("' . $CFG->wwwroot . '/theme/tatachem/pix/spinner.gif") center center no-repeat;
        }
    </style>';
$chartdata = user_course_completed($USER->id);
$o = html_writer::tag('h5', get_string('mycourses'));
$courses = enrol_get_my_courses();
if (count($courses) > 0) {
    $o .= html_writer::tag('center', html_writer::div(html_writer::div('', 'preloader'), '', array('id' => 'donutchart')));
} else {
    $o .= html_writer::tag('center', 'You do not have any assigned courses.');
}
$content = html_writer::div($o, 'chartheading');
$i = '<i class="fa %s fntcn"></i>';
$blocks = get_block_instance();
$link = '';

/*
  |------------------------
  |  My courses menus
  |------------------------
 */
$link .='<div class="panel-group" id="accordion">';
$link .='<div class="panel panel-default">
            <div class="panel-heading">
             <h6 class="panel-title">';
/*$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot), sprintf($i . ' Home', 'fa-home')));*/
$link .= html_writer::link(new moodle_url($CFG->wwwroot), sprintf($i . 'Home', 'fa-home'), array('class' => 'testlist', 'id' => 'testlistdp'));
$link .='<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1">
          </a><i class="glyphicon glyphicon-plus"></i></h6></div>';
if (is_siteadmin()) { 
	$link .='<div id="panel1" class="panel-collapse collapse in">
		    <div class="panel-body">';
		    $link .= html_writer::start_tag('ul', array('class' => 'ultest'));
		    $link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/admin/index.php'), sprintf($i . get_string('myadmin','theme_tatachem'), 'fa-angle-right'), array('class' => 'allfnticon')));
	$link .='</div></div>';
		 }
$link .='</div>';

$link .='<div class="panel panel-default">
        <div class="panel-heading">
        <h6 class="panel-title">';
$link .= html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=my'), sprintf($i . ' My Courses', 'fa-book'), array('class' => 'testlist', 'id' => 'testlistdp2'));
$link .= '<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#panel2">
          </a><i class="glyphicon glyphicon-minus"></i></h6>
        </div>
        <div id="panel2" class="panel-collapse collapse">
            <div class="panel-body">';

$link .= html_writer::start_tag('ul', array('class' => 'ultest'));
if (isset($blocks['personallearningplan']) && $blocks['personallearningplan']) {
    $link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=personallearningplan'), sprintf($i . get_string('personallearningplan'), 'fa-angle-right'), array('class' => 'allfnticon')));
}
if (isset($blocks['selfenrol']) && $blocks['selfenrol']) {
    $link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=selfenrol'), sprintf($i . get_string('selfenrol'), 'fa-angle-right'), array('class' => 'allfnticon')));
}
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=assigned'), sprintf($i . get_string('myassignedcourses'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot . '/my/listcourse.php?section=avail'), sprintf($i . get_string('myavailablecourses'), 'fa-angle-right'), array('class' => 'allfnticon')));
if (isset($blocks['classroomhistory']) && $blocks['classroomhistory']) {
    $link .= html_writer::tag('li', html_writer::link(new moodle_url('#'), sprintf($i . 'Classroom Training', 'fa-angle-right'), array('class' => 'allfnticon')));
}
$link .= html_writer::tag('li', html_writer::link(new moodle_url('#'), sprintf($i . 'Training History', 'fa-angle-right'), array('class' => 'allfnticon')));
if (isset($blocks['calendar_month']) && $blocks['calendar_month']) {
    $link .= html_writer::tag('li', html_writer::link(
                            new moodle_url($CFG->wwwroot . '/calendar/view.php', array('view' => 'month', 'time' => time(), 'course' => 1)), sprintf($i . get_string('calendar_month'), 'fa-angle-right'), array('class' => 'allfnticon')));
}

$link .= html_writer::end_tag('ul');
$link .='</div>
        </div>
    </div>';
/*
  |-------------------
  | Report menus
  |-------------------
 */
$link .='<div class="panel panel-default">
        <div class="panel-heading">
        <h6 class="panel-title">';
$link .= html_writer::link(new moodle_url($CFG->wwwroot . '/my/statuscourses.php'), sprintf($i . ' Reports', 'fa-book'),array('class' => 'testlist', 'id' => 'testlistdp3'));
$link .= '<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#panel3">
          </a><i class="glyphicon glyphicon-minus"></i></h6>
        </div>
        <div id="panel3" class="panel-collapse collapse">
            <div class="panel-body">';

$link .= html_writer::start_tag('ul', array('class' => 'ultest navbar-collapse js-navbar-collapse'));
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot . '/my/mycsrpt.php', array('section' => 'csrpt')), sprintf($i . get_string('coursereports'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot . '/my/courses_report.php'), sprintf($i . get_string('coursestat'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(
                        new moodle_url($CFG->wwwroot . '/my/mycsrpt.php', array('section' => 'assignrpt')), sprintf($i . get_string('assessmentreports'), 'fa-angle-right'), array('class' => 'allfnticon')));
$link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot.'/my/certificate.php'), sprintf($i . get_string('certificate'), 'fa-angle-right'), array('class' => 'allfnticon')));
if (isset($blocks['badges']) && $blocks['badges']) {
    $link .= html_writer::tag('li', html_writer::link(
                            new moodle_url($CFG->wwwroot . '/badges/mybadges.php'), sprintf($i . get_string('badges'), 'fa-angle-right'), array('class' => 'allfnticon')));
}
$link .= html_writer::end_tag('ul');    

$link .= '</div></div></div>';

$link .='<div class="panel panel-default">
            <div class="panel-heading">
             <h6 class="panel-title">';
$link .= html_writer::link(new moodle_url($CFG->wwwroot.'/user/profile.php?id='.$USER->id), sprintf($i .get_string('myprofiles'), 'fa-user'), array('class' => 'testlist', 'id' => 'testlistdp4'));
$link .='<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#panel4">
          </a><i class="glyphicon glyphicon-plus"></i></h6></div>';
$link .='</div></div>';

/*$link .='<div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h6 class="panel-title testlist">';
                     $link .= html_writer::tag('li', html_writer::link(new moodle_url($CFG->wwwroot.'/user/profile.php?id='.$USER->id), sprintf($i .get_string('myprofiles'), 'fa-user')));

$link .='</h6></div></div></div>';*/

$content .= html_writer::div(html_writer::div($link, 'listmndiv'), 'row-fluid ');
echo html_writer::div(html_writer::div($content, 'row-fluid'), 'span3 userpicdiv');
echo html_writer::tag('script', '', array('type' => 'text/javascript', 'src' => $CFG->wwwroot . '/my/js/jsapi.js'));
echo html_writer::tag('script', '', array('type' => 'text/javascript', 'src' => $CFG->wwwroot . '/my/js/uds_api_contents.js'));
echo '<script type="text/javascript">
          $(window).resize(function () {
             google.charts.setOnLoadCallback(left_dashboard_chart);
          });

        function load_dashbord_chart() {
            var chardata = ' . $chartdata . ';
            var data = google.visualization.arrayToDataTable(chardata);
            var options = {
                    width: "80%",
                    height: 120,
                    colors: ["#04B996", "#EE902E", "#A0A0A0"],
                    bar: {groupWidth: "40%"},
                    backgroundColor: "#D2D0D1",
                    chartArea: {width: "90%", "height": "70%"},
                    legend: {"position": "bottom"},
                    is3D: true,
                };
            new google.visualization.PieChart(document.getElementById("donutchart")).draw(data,options);
        }
        google.setOnLoadCallback(load_dashbord_chart); 
        
    </script>';
echo '<style type="text/css">
        #accordion .panel .panel-heading a:before{
            float:right;
            background: #31353e;
            border-radius: 15px;
            width: 25px;
            height: 25px;
        }
        a#testlistdp:before{
            display:none;
        }
        .panel-default {
            border-color: #434A54;
        }
        .panel {
            background-color: #434A54;
        }
        .panel-default>.panel-heading {
            background-color:rgba(59, 59, 59, 0);
            border-color: rgba(59, 59, 59, 0);
         }
        .panel .panel-heading a{
            color:#fff;
         }
        a#testlistdp2:before{
            display:none;
        }
        a#testlistdp3:before{
            display:none;
        }
        a#testlistdp4:before{
            display:none;
        }
</style>';

