<?php
$string['pluginname'] = 'CBSI-Connect Course Creation Wizard';
$string['whatwant'] = 'What do you want to do today ?';
$string['Creatingnew'] = 'Creating a new meeting or training';
$string['whatwantclass'] = 'What type of class do you want to create?';
$string['whatlanguage'] = 'What language will this be available in ?';
$string['Whattitle'] = 'What is title ?';
$string['whatdesc'] = 'What is the description ? ';
$string['Setdate'] = 'Set the start date of training and meeting? ';
$string['whattrain'] = 'When do you want this training or meeting begin ?';
$string['Choosetime'] = 'Choose a time ?';
$string['trainingsetup'] = 'Congratulation your meeting or training is setup. What would you like  to do  now ?';
$string['SaveHomePage'] = 'Save and Go Back Home Page';
$string['DeletePage'] = 'Delete and Go Back Home Page';
$string['GoPage'] = 'Save and Customize';
$string['Goview'] = 'Save and View Course';
$string['SaveUp'] = 'Save and Start Set Up';
$string['whattrainend'] = 'When do you want this training or meeting end ?';


?>
