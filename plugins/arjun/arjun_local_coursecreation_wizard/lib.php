<?php
defined('MOODLE_INTERNAL') || die();
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->dirroot . '/mod/folder/lib.php');
require_once($CFG->dirroot. '/enrol/locallib.php');
include_once($CFG->dirroot . '/course/modlib.php');
/*
|------------------------------
| Course creation function
|
| @return array
|------------------------------
*/
function course_creation($category, $fullname, $summary,$subcategory)
{   
	global $DB, $USER,$PAGE;
	if($DB->record_exists('course_categories', array('id'=> $category, 'name' => 'Meeting'))) {
		$format = 'meeting';
	}else
	{
		$format = 'topics';
	}
	$std = (object) array('category' => $category, 'fullname' => $fullname, 'summary' => $summary,'shortname'=>$fullname, 'format' => $format);

	if($course = create_course($std)) {

		$std = new stdClass();
		$std->courseid = $course->id;
		$std->course_creator = $USER->id;
		$std->default_shortname = $course->fullname;
		$std->categoryid = $category; 
		$std->parent_category = $subcategory;

		$DB->insert_record('block_cbsi', $std);
			/*
			|------------------------------------------
			| Updating section value 0
			|------------------------------------------
			*/
		    $section = $DB->get_record('course_format_options',array('courseid'=>$course->id, 'name'=>'numsections'));
			if($DB->record_exists('course_categories', array('id'=> $category, 'name' => 'Training'))) {

				$DB->update_record('course',array('id'=>$course->id, 'newsitems'=> 0, 'format' => 'topics', 'enablecompletion' => 1));
                 //$DB->update_record('course_format_options',array('id'=>$section->id, 'value'=> '5'));
                 if(!empty($section)) {
				    $DB->update_record('course_format_options',array('id'=>$section->id, 'value'=> 5));
                    purge_all_caches();
		        	}

			}
			/*if(!empty($section)) {
				$DB->update_record('course_format_options',array('id'=>$section->id, 'value'=> '0'));
			}*/
			/*
			|------------------------------------------
			| Updating course field {newitem, format,completion tracking}
			|------------------------------------------
			*/
			if($DB->record_exists('course_categories', array('id'=> $category, 'name' => 'Meeting'))) {
				
				$DB->update_record('course',array('id'=>$course->id, 'newsitems'=> 0, 'format' => 'meeting'));
                 $DB->update_record('course_format_options',array('id'=>$section->id, 'value'=> 0));

			/*
			|------------------------------------------
			| creating mod_folder in the course {Meeting Materials}
			|------------------------------------------
			*/	
				$temp = $DB->get_field_sql("SELECT id from {modules} WHERE name = 'folder'");
				$folder = new stdClass();
				$folder->course = $course->id;
				$folder->timemodified = time();
				$folder->modulename = 'folder';
				$folder->name = 'Meeting Materials';
				$folder->intro = 'The meeting presenter can upload documents to this folder for participants. Meeting viewers can download materials from this folder.';
				$folder->introformat = 1;
				$folder->revision = 1;
				$folder->module = $temp;
				$folder->visible = 1;
				$folder->files = false;
				$folder->section = 0;

				add_moduleinfo($folder, $course);
				$fid = $DB->get_field_sql("SELECT id from {folder} WHERE course = $course->id");
				if($fid){
					$fff = $DB->get_record('course_modules',array('course'=>$course->id, 'instance'=>$fid,'module'=>$temp),'id');
					$DB->update_record('course_modules',array('id' => $fff->id, 'showdescription' => 1));

				}

			}
            
			
			

			/*
			|------------------------------------------
			| enroll course creator into course {instructor,content distributor}
			|------------------------------------------
			*/
			$context   = context_course::instance($course->id);
			$manager   = new course_enrolment_manager($PAGE, $course);
			$instances = $manager->get_enrolment_instances();
			$today     = date('U');
			$role_id   = 2; //content contributor

			//find the manual one
			foreach ($instances as $instance) {
				if ($instance->enrol == 'manual') {
				    break;
				}
			}

			$plugins = $manager->get_enrolment_plugins();
			$plugin  = $plugins['manual'];
			$plugin->enrol_user($instance, $USER->id, $role_id, $today, 0);
			$plugin->enrol_user($instance, $USER->id, 3, $today, 0); // 3 = instructor role_id


			$data = array('status'=>true, 'id'=> $course->id, 'fullname'=> $course->fullname); 
    }
	else
	{
	    $data = array('status'=>false, 'message'=>'can\'t create course try later'); 
	}
	return $data;
}

/*
|------------------------------
| Course time modify
|
| @return array
|------------------------------
*/
function course_time_modify($courseid, $datestamp,$timestamp,$enddate,$endtime)
{
	global $DB;
	$date = $datestamp.$timestamp;
	$stdate = strtotime($date);
	$endstampdate = $enddate.$endtime;
	$stenddate = strtotime($endstampdate);
	$cseid = $DB->get_record('block_cbsi',array('courseid' =>$courseid));
	//var_dump(expression)
	$update = (object) array('id' => $courseid, 'startdate' => $stdate);
	$update12 = (object) array('id' => $cseid->id, 'start_datetime' => $stdate,'end_datetime'=>$stenddate);
	// print_object($update12);
	// exit();
	if($DB->update_record('course', $update)){

		if($DB->update_record('block_cbsi', $update12)){
			return array('status' => true);
		} else {
			return array('status' => false);

		}
		
	}
	
}

/*
|------------------------------
| Course delete
|
| @return array
|------------------------------
*/
function course_finaldelete($courseid)
{
	global $DB;
	if($DB->delete_records('course', array('id' => $courseid)))
	{ 
		$DB->delete_records('block_cbsi', array('courseid' => $courseid));
    	$data = array('status'=>true); 
	}
	else
	{
	    $data = array('status'=>false); 
	}
	return $data;
}

/*
|------------------------------
| Navigation
|
| @return array
|------------------------------
*/
function  local_coursecreation_wizard_extend_settings_navigation($settingsnav, $context){
	// global $CFG, $COURSE, $courseid, $DB;
	
// $course = $DB->get_record('course', array('id' => $COURSE->id));	
// //require_course_login($course);
// $context = context_course::instance($course->id);
// 	if (has_capability('moodle/role:assign', $context)) {
// 		$coursenode = $settingsnav->get('courseadmin');
// 		if($coursenode)
// 		{
//             $url = "$CFG->wwwroot/local/course_batches/view_batch.php?id=$COURSE->id";
            
//             //for batch
//             $coursenode->add(get_string('batch', 'local_course_batches'), null,
//                         navigation_node::TYPE_CONTAINER, null, 'batch',
//                         new pix_icon('i/badge', get_string('batch', 'local_course_batches')));
            
// //            $url = "$CFG->wwwroot/local/course_batches/batch.php?courseid=$COURSE->id";
// //
// //			$coursenode->get('batch')->add(get_string('addbatch', 'local_course_batches'), $url,
// //				navigation_node::TYPE_SETTING, null, 'batch');            

//             $coursenode->get('batch')->add(get_string('viewbatch', 'local_course_batches'), $url,
//                     navigation_node::TYPE_SETTING, null, 'course_batches');

//             //for batch
//             /* 
//             $url = "$CFG->wwwroot/local/course_batches/batch.php?courseid=$COURSE->id";

//             $coursenode->get('coursepromo')->add(get_string('addbatch', 'local_course_batches'), $url,
//             navigation_node::TYPE_SETTING, null, 'coursepromo');


//            $url = "$CFG->wwwroot/local/course_batches/view_batch.php?id=$COURSE->id";
//            $coursenode->get('coursepromo')->add(get_string('viewbatch', 'local_course_batches'), $url,
//                    navigation_node::TYPE_SETTING, null, 'course_batches'); 
//             */
// 		}
// 	}	
}
