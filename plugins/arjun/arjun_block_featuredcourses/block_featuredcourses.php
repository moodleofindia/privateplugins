<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Newblock block caps.
 *
 * @package    block_featuredcourses
 * @copyright  Daniel Neis <danielneis@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot . '/my/customlib.php');
include_once($CFG->dirroot.'/blocks/featuredcourses/classes/block_featuredlib.php');

class block_featuredcourses extends block_base {
  /** 
  * this will hold call variable,hold course content class object 
  */
    private $call;
    function init() {
      global $DB;
        $this->title = get_string('pluginname', 'block_featuredcourses');
        $this->call = new \contentall\course_contents($DB);
    }

    function get_content() {
        global $CFG, $OUTPUT, $PAGE;
    
        $PAGE->requires->css('/blocks/featuredcourses/css/ihover.min.css');
        $PAGE->requires->css('/blocks/featuredcourses/styles.css');
        $PAGE->requires->css('/blocks/featuredcourses/css/owl.carousel.css');
        $PAGE->requires->css('/blocks/featuredcourses/css/owl.theme.css');

        $PAGE->requires->js(new moodle_url('/blocks/featuredcourses/js/jquery.js'));
        $PAGE->requires->js(new moodle_url('/blocks/featuredcourses/js/owl.carousel.min.js'));
        $PAGE->requires->js(new moodle_url('/blocks/featuredcourses/js/custom.js'));
        
        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';
        $this->content->text = '';
        $this->content->text = '<div class="container-fluid">';

        // user/index.php expect course context, so get one if page has module context.
        $currentcontext = $this->page->context->get_course_context(false);

        if (empty($currentcontext)) {
            return $this->content;
        }
        if ($this->page->course->id == SITEID) {
            
            require_once($CFG->libdir. '/coursecatlib.php');
            $chelper = new coursecat_helper();
            $this->content->text .= '<div>';
            $this->content->text .= '<h2 class="ct-fw-700 ct-u-marginBottom30 ct-u-paddingTop20">Featured Courses</h2>';
            $this->content->text .= '<span class="featuredline"><span></span></span>';
            $this->content->text .= '</div>';
            $this->content->text .= '<div id="owl-demo">';
            /**
            *get_featured_course return all the featured courses,
            */
            foreach ($this->call->get_featured_courses() as $course) {

                $course = new course_in_list($course);
                $contentteacher = $contentimages = $contentfiles = $imagelink = $detailslink = '';
                $content = '<div style="opacity: 1; display: block;" class="wrapper-item">
                                 <div class="col-sm-12">';
                // display course overview files (course image)
                foreach ($course->get_course_overviewfiles() as $file) {
                    $isimage = $file->is_valid_image();
                    $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                                 '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                                    $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                    if ($isimage) {
                        $contentimages .= html_writer::empty_tag('img', array('src' => $url, 'class' =>'attachment-medium wp-post-image', 'a                                    lt' => '02', 'itemprop' => 'image', 'style' => ''));
                        $imagelink = html_writer::link(new moodle_url('/my/coursedetails.php', array('id' => $course->id)),$contentimages);
                    } else {
                        $url1 = "$CFG->wwwroot/theme/fmubeta/pix/courses-icon.png";
                        $contentimages .= html_writer::empty_tag('img', array('src' => $url, 'class' =>'attachment-medium wp-post-image', 'a                                    lt' => '02', 'itemprop' => 'image', 'style' => ''));
                    }
                }
                $content.='<div class="inner-course">
                                <div class="course-thumbnail">
                                    '.$imagelink.'
                             <div class="course-info">';
              // display course summary
                if ($course->has_summary()) {
                    //$content .= $chelper->get_course_formatted_summary($course,array('overflowdiv' => true, 'noclean' => true, 'para' => false));
            $detailslink = $CFG->wwwroot.'/my/coursedetails.php?id='.$course->id;
            $content.='<a href="'.$detailslink.'" class="button"><span>View Details</span></a>';
                    }
                         $content.='</div></div>';
               // course name
                $coursename = $chelper->get_course_formatted_name($course);
                $coursenamelink = html_writer::link(new moodle_url('/my/coursedetails.php', array('id' => $course->id)),
                                                    $coursename, array('class' => $course->visible ? '' : 'dimmed'));
                $content.='<div class="course-title" itemprop="name">
                                <h2>'.$coursenamelink.'</h2>
                           </div>';
                // for teacher, student count
                $coursehascontacts = $course->has_course_contacts();
                if ($coursehascontacts) {
                    //echo '<pre>',print_r($course->get_course_contacts());
                    foreach ($course->get_course_contacts() as $userid => $coursecontact) {
                        $contentteacher = $coursecontact['rolename'].' : '.
                            html_writer::link(new moodle_url('/user/view.php',
                                        array('id' => $userid, 'course' => SITEID)),
                                    $coursecontact['username']);

                        $urlimg = $CFG->wwwroot.'/user/pix.php?file=/'.$userid.'/f1.jpg';
                        $contentteacherimg = html_writer::empty_tag('img', array('src' => $urlimg,'class' => 'avatar user-12-avatar avatar-32 photo','width'=>'32','height'=>'32'));
                        $contentimage = '<div class="course-instructor" itemprop="creator">
                            <span>'.$contentteacher.'</span>
                            <span class="avatar">'.$contentteacherimg.'</span>
                            </div>'; 
                        $content.= $contentimage;
                    }
                }else{
                    //$urlimg = $CFG->wwwroot.'/user/pix/noteacher.png';
                   // $contentteacherimg = html_writer::empty_tag('img', array('src' => $urlimg,'class' => 'avatar user-12-avatar avatar-32 photo','width'=>'32','height'=>'32'));
                    $contentimage = '<div class="course-instructor" itemprop="creator">
                        <span>No Teacher Available</span>
                        <span class="avatar"><i class="fa fa-user"></i></span></div>';
                    $content.= $contentimage;
                }

                $content.='<div class="course-price">
                               <a href="'.$CFG->wwwroot.'/my/coursedetails.php?id='.$course->id.'">Join Course</a>    
                           </div>
                    <div class="course-student">
                        <span>
                            <i class="dashicons dashicons-groups"></i>
                                    14 students
                        </span>
                        <span class="course-rating">
                            <div class="course-rate">
                                <div class="review-stars-rated">
                                    <div class="review-stars thim-review">
                                        <span style="width:80%;"></span>
                                    </div>
                                </div>
                                <p class="review-number">1 rating</p>
                            </div>                      
                        </span>
                    </div>
                </div>
           </div>'; 
           $this->content->text .= $content. '</div>';
            }
           $this->content->text .= '</div>'; // for slider
        }
        $this->content->text .= '</div>'; // for container fluid on top
        return $this->content;
    }
    // my moodle can only have SITEID and it's redundant here, so take it away
    public function applicable_formats() {
        return array('all' => false,
                'site' => true,
                'site-index' => true);
    }
    public function instance_allow_multiple() {
        return false;
    }
    function has_config() {return true;}

    public function cron() {
        return true;
    }
    function hide_header() {
        return true;
    }
    static function get_featured_courses() {
        global $DB;

        $sql = 'SELECT c.id, c.shortname, c.fullname, fc.sortorder
                  FROM {block_featuredcourses} fc
                  JOIN {course} c
                    ON (c.id = fc.courseid)
              ORDER BY sortorder';
        return $DB->get_records_sql($sql);
    }

    static function delete_featuredcourse($courseid) {
        global $DB;
        return $DB->delete_records('block_featuredcourses', array('courseid' => $courseid));
    }
}
?>
