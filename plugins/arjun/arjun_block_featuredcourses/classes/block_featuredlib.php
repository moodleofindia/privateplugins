<?php
namespace contentall;

class course_contents{
	private $courseid;
	private $userid;
	private $cfg;
	private $db;

	public function __construct($db = null, $cfg = null, $courseid = null, $userid = null)
	{
		$this->db = $db;
		$this->cfg = $cfg;
		$this->userid = $userid;
		$this->courseid = $courseid;

	}
	
	public function get_featured_courses() {
        

        $sql = 'SELECT c.id, c.shortname, c.fullname, fc.sortorder
                  FROM {block_featuredcourses} fc
                  JOIN {course} c
                    ON (c.id = fc.courseid)
              ORDER BY sortorder';

        return $this->db->get_records_sql($sql);
    }
}

