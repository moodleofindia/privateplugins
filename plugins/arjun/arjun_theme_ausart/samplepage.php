<?php
//  Display the student zone page.

	require_once('../../config.php'); 
	$PAGE->set_context(context_system::instance(0));
	$PAGE->set_url('/theme/marble/samplepage.php'); 
	
	$PAGE->set_title('Sample');
	$PAGE->set_heading('Sample Pages');
	
	$PAGE->set_pagelayout('columns2');        

	echo $OUTPUT->header();	
	?>	
    
	   
 
	<!-- content 
			================================================== -->
		<div id="content">
			
		<h2>Styles available in the theme</h2>
		
		<!-- statistic-section2 
				================================================== -->
			<div class="section-content statistic-section2">
				<div class="container2">
					<div class="statistic-box">
						<div class="row">
							<div class="col-md-3 col-sm-6 span3">
								<div class="statistic-post">
									<div class="statistic-counter">
										<p><span class="timer" data-from="0" data-to="256"></span></p>
										<p>Satisfied Customers</p>
									</div>
								</div>								
							</div>
							<div class="col-md-3 col-sm-6 span3">
								<div class="statistic-post">
									<div class="statistic-counter">
										<p><span class="timer" data-from="0" data-to="478"></span></p>
										<p>Registered Users</p>
									</div>
								</div>								
							</div>
							<div class="col-md-3 col-sm-6 span3">
								<div class="statistic-post">
									<div class="statistic-counter">
										<p><span class="timer" data-from="0" data-to="28"></span></p>
										<p>Released Projects</p>
									</div>
								</div>							
							</div>
							<div class="col-md-3 col-sm-6 span3">
								<div class="statistic-post">
									<div class="statistic-counter">
										<p><span class="timer" data-from="0" data-to="759"></span></p>
										<p>Hours of Support</p>
									</div>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			
			<!-- tab-skill-section
				================================================== -->
			<div class="section-content tab-skill-section">
				<div class="container2">
					<div class="row-fluid">
						<div class="col-md-6">
							<div class="title-section title2" style="margin-top: 0px; padding-top: 0px;">
								<h1>What we offer</h1>
							</div>
							<div class="vertical-tab-box">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" id="myTab2">
									<li class="active">
										<a href="#web-design" data-toggle="tab">Website Design</a>
									</li>
									<li>
										<a href="#web-development" data-toggle="tab">Development</a>
									</li>
									<li>
										<a href="#retina-ready" data-toggle="tab">Retina ready</a>
									</li>
									<li>
										<a href="#premium-sliders" data-toggle="tab">Premium Sliders</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="web-design">
										<h3>Phasellus ultrices nulla quis nibh. Quisque a lectus.</h3>
										<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
										<img src="http://placehold.it/460x390" alt="">
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
									</div>
									<div class="tab-pane" id="web-development">
										<h3>Phasellus ultrices nulla quis nibh. Quisque a lectus.</h3>
										<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
									</div>
									<div class="tab-pane" id="retina-ready">
										<h3>Phasellus ultrices nulla quis nibh. Quisque a lectus.</h3>
										<img src="http://placehold.it/460x390" alt="">
										<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
									</div>
									<div class="tab-pane" id="premium-sliders">
										<h3>Phasellus ultrices nulla quis nibh. Quisque a lectus.</h3>
										<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </p>
									</div>
								</div>
							</div>	
						</div>
					
					
					</div>
				</div>
			</div>
			
			
	<!-- faqs-section
				================================================== -->
			<div class="section-content faqs-section">
				<div class="container">
					<div class="accordion-box triggerAnimation animated" data-animate="slideInUp">
						<div class="accord-elem active">
							<div class="accord-title">
								<a class="accord-link" href="#"></a>
								<h2>Vivamus vestibulum nulla nec ante.</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </p>
							</div>
							<div class="accord-content">
								<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>
							</div>
						</div>

						<div class="accord-elem">
							<div class="accord-title">
								<a class="accord-link" href="#"></a>
								<h2>Morbi in sem quis dui placerat ornare. </h2>
								<p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </p>
							</div>
							<div class="accord-content">
								<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>
							</div>
						</div>

						<div class="accord-elem">
							<div class="accord-title">
								<a class="accord-link" href="#"></a>
								<h2>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </h2>
								<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
							</div>
							<div class="accord-content">
								<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>
							</div>
						</div>

						<div class="accord-elem">
							<div class="accord-title">
								<a class="accord-link" href="#"></a>
								<h2>Donec quis dui at dolor tempor interdum.</h2>
								<p>Morbi in sem quis dui placerat ornare.</p>
							</div>
							<div class="accord-content">
								<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>
							</div>
						</div>

						<div class="accord-elem">
							<div class="accord-title">
								<a class="accord-link" href="#"></a>
								<h2>Sed adipiscing ornare risus.</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </p>
							</div>
							<div class="accord-content">
								<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>
							</div>
						</div>

						<div class="accord-elem">
							<div class="accord-title">
								<a class="accord-link" href="#"></a>
								<h2>Aenean dignissim pellentesque felis.</h2>
								<p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </p>
							</div>
							<div class="accord-content">
								<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>
							</div>
						</div>
					</div>
				</div>
			</div>		
			
			
			
		</div>
		<!-- End content -->

<?php		
	echo $OUTPUT->footer();
?>
<script>
jQuery(document).ready(function($) {

$('body').addClass('zoomin');

});
</script>	

<style>
#breadwrap {
	display: none;
}
</style>