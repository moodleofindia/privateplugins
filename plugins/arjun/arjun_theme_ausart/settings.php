<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's ausart theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_ausart
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$settings = null;
require_once(__DIR__.'/libs/admin_confightmleditor.php');
defined('MOODLE_INTERNAL') || die;

if (is_siteadmin()) {
 $ADMIN->add('themes', new admin_category('theme_ausart', 'ausart'));
    // Invert Navbar to dark background.
   
$temp = new admin_settingpage('theme_ausart_brand', "General Branding Settings");
//logo
$name = 'theme_ausart/logo';
	$title = get_string('logo','theme_ausart');
	$description = get_string('logodesc', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    
	
	// Turn on fluid width
    $name = 'theme_ausart/headeralign';
    $title = get_string('headeralign', 'theme_ausart');
    $description = get_string('headeraligndesc', 'theme_ausart');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $temp->add($setting);
    
     // footerline setting
    $name = 'theme_ausart/headcontact';
    $title = get_string('headcontact','theme_ausart');
    $description = get_string('headcontactdesc', 'theme_ausart');
    $setting = new admin_setting_configtext($name, $title, $description, '<span class="phone"><i class="moon-phone"></i>+1 00 222 654</span>');
    $temp->add($setting);
    
    $name = 'theme_ausart/headcontact2';
    $title = get_string('headcontact2','theme_ausart');
    $description = get_string('headcontact2desc', 'theme_ausart');
    $setting = new admin_setting_configtext($name, $title, $description, '<span class="email"><i class="moon-envelop"></i>info@austart.to</span>');
    $temp->add($setting);
    
   	
	
	// Foot note setting
 // footerline setting
    $name = 'theme_ausart/footnote';
    $title = get_string('footnote','theme_ausart');
    $description = get_string('footnotedesc', 'theme_ausart');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $temp->add($setting);
    
    
    //social settings
    $name = 'theme_ausart/socialone';
	$title = get_string('socialone','theme_ausart');
	$description = get_string('socialonedesc', 'theme_ausart');
	$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
	$temp->add($setting);
	
	 $name = 'theme_ausart/socialtwo';
	$title = get_string('socialtwo','theme_ausart');
	$description = get_string('socialtwodesc', 'theme_ausart');
	$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
	$temp->add($setting);
	
	 $name = 'theme_ausart/socialthree';
	$title = get_string('socialthree','theme_ausart');
	$description = get_string('socialthreedesc', 'theme_ausart');
	$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
	$temp->add($setting);
	
	 $name = 'theme_ausart/socialfour';
	$title = get_string('socialfour','theme_ausart');
	$description = get_string('socialfourdesc', 'theme_ausart');
	$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
	$temp->add($setting);

// link color setting
	$name = 'theme_ausart/linkcolor';
	$title = get_string('linkcolor','theme_ausart');
	$description = get_string('linkcolordesc', 'theme_ausart');
	$default = '#009dcd';
	$previewconfig = NULL;
	$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
	$temp->add($setting);

	// link hover color setting
	$name = 'theme_ausart/linkhover';
	$title = get_string('linkhover','theme_ausart');
	$description = get_string('linkhoverdesc', 'theme_ausart');
	$default = '#666666';
	$previewconfig = NULL;
	$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
	$temp->add($setting);

	// main color setting
	$name = 'theme_ausart/maincolor';
	$title = get_string('maincolor','theme_ausart');
	$description = get_string('maincolordesc', 'theme_ausart');
	$default = '#009dcd';
	$previewconfig = NULL;
	$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
	$temp->add($setting);
	
	// main color setting
	$name = 'theme_ausart/secondcolor';
	$title = get_string('secondcolor','theme_ausart');
	$description = get_string('secondcolordesc', 'theme_ausart');
	$default = '#323232';
	$previewconfig = NULL;
	$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
	$temp->add($setting);
	
	// main color setting
	$name = 'theme_ausart/headingcolor';
	$title = get_string('headingcolor','theme_ausart');
	$description = get_string('headingcolor', 'theme_ausart');
	$default = '#2f383d';
	$previewconfig = NULL;
	$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
	$temp->add($setting);
	

$ADMIN->add('theme_ausart', $temp);


$temp = new admin_settingpage('theme_ausart_slider', "Frontpage Slider Settings");


  //custom login

//set pictures for frontpage
$name = 'theme_ausart/p1';
$title = get_string('p1','theme_ausart');
$description = get_string('p1desc', 'theme_ausart');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'p1');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

$name = 'theme_ausart/p2';
$title = get_string('p2','theme_ausart');
$description = get_string('p2desc', 'theme_ausart');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'p2');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

$name = 'theme_ausart/p3';
$title = get_string('p3','theme_ausart');
$description = get_string('p3desc', 'theme_ausart');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'p3');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

$name = 'theme_ausart/p1cap';
$title = get_string('p1cap','theme_ausart');
$description = get_string('p1capdesc', 'theme_ausart');
$default = ' <!-- LAYER NR. 1 -->
                            <div class="tp-caption sfr mmaincap" data-x="-2" data-y="114" data-speed="700" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5;">One Minute to Create <br>an Awesome Moodle Site
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption sfr msubcap" data-x="-2" data-y="198" data-speed="700" data-start="1000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6;">
                                <div style="" class="tp-layer-inner-rotation   rs-pulse" data-easing="Power3.easeInOut" data-speed="2" data-zoomstart="1" data-zoomend="1">Longer text title can go here 
                                </div>
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lfb msubsubcap" data-x="2" data-y="258" data-speed="700" data-start="1300" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7;">
<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
                            
                            </div>';
$setting = new admin_setting_configtextarea($name, $title, $description, $default);
$temp->add($setting);

$name = 'theme_ausart/p2cap';
$title = get_string('p2cap','theme_ausart');
$description = get_string('p2capdesc', 'theme_ausart');
$default = ' <!-- LAYER NR. 1 -->
                            <div class="tp-caption sfr mmaincap" data-x="-2" data-y="114" data-speed="700" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5;">One Minute to Create <br>an Awesome Moodle Site
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption sfr msubcap" data-x="-2" data-y="198" data-speed="700" data-start="1000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6;">
                                <div style="" class="tp-layer-inner-rotation   rs-pulse" data-easing="Power3.easeInOut" data-speed="2" data-zoomstart="1" data-zoomend="1">Longer text title can go here 
                                </div>
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lfb msubsubcap" data-x="2" data-y="258" data-speed="700" data-start="1300" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7;">
<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
                            
                            </div>';
$setting = new admin_setting_configtextarea($name, $title, $description, $default);
$temp->add($setting);

$name = 'theme_ausart/p3cap';
$title = get_string('p3cap','theme_ausart');
$description = get_string('p3capdesc', 'theme_ausart');
$default = ' <!-- LAYER NR. 1 -->
                            <div class="tp-caption sfr mmaincap" data-x="-2" data-y="114" data-speed="700" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5;">One Minute to Create <br>an Awesome Moodle Site
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption sfr msubcap" data-x="-2" data-y="198" data-speed="700" data-start="1000" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6;">
                                <div style="" class="tp-layer-inner-rotation   rs-pulse" data-easing="Power3.easeInOut" data-speed="2" data-zoomstart="1" data-zoomend="1">Longer text title can go here 
                                </div>
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lfb msubsubcap" data-x="2" data-y="258" data-speed="700" data-start="1300" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7;">
<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
                            
                            </div> ';
$setting = new admin_setting_configtextarea($name, $title, $description, $default);
$temp->add($setting);

$ADMIN->add('theme_ausart', $temp);

$temp = new admin_settingpage('theme_ausart_front', "Frontpage Marketing Settings");

 //coursetabs
$name = 'theme_ausart/callout';
$title = get_string('callout','theme_ausart');
$description = get_string('calloutdesc', 'theme_ausart');
$default = ' <h1 style="font-size:36px; color:#3d3d3d; margin-bottom: 15px;">Business Template for a Startup Business </h1>
                                    <div class="line_under">
                                        <div class="line_center"></div>
                                    </div>
                                    <div class="line_under below_line">
                                        <div class="line_center"></div>
                                    </div>
                                    <p class="description style_3">This is the ultimate and definitive guide for an effective business plan for any type of startup.</p>';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

	$name = 'theme_ausart/coursetab1';
    $title = get_string('coursetab1','theme_ausart');
    $description = get_string('coursetab1', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, ' <div class="overlay"></div><i class="moon-pencil-2"></i></div>
                                    <h4><a href="#">GREAT DESIGN LAYOUTS</a></h4>
                                    <p>Nulla consequat massa quis enim. Veronum versus Donec pede justo, fringilla vel, aliquet nec, vulputate eget, In enim justo, rhoncus ut, imperdiet a, venenatis vitae</p>
                                    <div class="read_more"><a href="#" class="readmore">Read More</a></div>');
    $temp->add($setting);
    
    $name = 'theme_ausart/coursetab2';
    $title = get_string('coursetab2','theme_ausart');
    $description = get_string('coursetab2', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '  <div class="overlay"></div><i class="moon-screen-4"></i></div>
                                    <h4><a href="#">WEB DESIGN</a></h4>
                                    <p>Nulla consequat massa quis enim. Veronum versus Donec pede justo, fringilla vel, aliquet nec, vulputate eget, In enim justo, rhoncus ut, imperdiet a, venenatis vitae</p>
                                    <div class="read_more"><a href="#" class="readmore">Read More</a></div>');
    $temp->add($setting);
    
    $name = 'theme_ausart/coursetab3';
    $title = get_string('coursetab3','theme_ausart');
    $description = get_string('coursetab3', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '  <div class="overlay"></div><i class="moon-mobile-2"></i></div>
                                    <h4><a href="#">100% RESPONSIVE</a></h4>
                                    <p>Nulla consequat massa quis enim. Veronum versus Donec pede justo, fringilla vel, aliquet nec, vulputate eget, In enim justo, rhoncus ut, imperdiet a, venenatis vitae</p>
                                    <div class="read_more"><a href="#" class="readmore">Read More</a></div>');
    $temp->add($setting);
    
    $name = 'theme_ausart/coursetab4';
    $title = get_string('coursetab4','theme_ausart');
    $description = get_string('coursetab4', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, ' <div class="overlay"></div><i class="moon-screen-4"></i></div>
                                    <h4><a href="#">RETINA READY</a></h4>
                                    <p>Nulla consequat massa quis enim. Veronum versus Donec pede justo, fringilla vel, aliquet nec, vulputate eget, In enim justo, rhoncus ut, imperdiet a, venenatis vitae</p>
                                    <div class="read_more"><a href="#" class="readmore">Read More</a></div>  ');
    $temp->add($setting);
	
		// footerline setting
    $name = 'theme_ausart/fteach';
    $title = get_string('fteach','theme_ausart');
    $description = get_string('fteach', 'theme_ausart');
    $setting = new admin_setting_configtext($name, $title, $description, 'Teachers');
    $temp->add($setting);
	
	$name = 'theme_ausart/coursetab1image';
$title = get_string('coursetab1image','theme_ausart');
$description = get_string('coursetab1image', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'coursetab1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

	$name = 'theme_ausart/coursetab2image';
$title = get_string('coursetab2image','theme_ausart');
$description = get_string('coursetab2image', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'coursetab2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

	$name = 'theme_ausart/coursetab3image';
$title = get_string('coursetab3image','theme_ausart');
$description = get_string('coursetab3image', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'coursetab3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

	$name = 'theme_ausart/coursetab4image';
$title = get_string('coursetab4image','theme_ausart');
$description = get_string('coursetab4image', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'coursetab4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

	$name = 'theme_ausart/coursetab1text';
    $title = get_string('coursetab1text','theme_ausart');
    $description = get_string('coursetab1text', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '
     <div class="content ">
                                            <h6>Joe Doe</h6>
                                            <div class="position_title"><span class="position">Ceo & Developer</span></div>
                                            <p>Sed at odio ut arcu fringilla dictum magna nisi.Aliquam at erat in purus velit vel dolo dictum magna nisi.</p>
                                        </div>
                                        <div class="social_widget">
                                            <ul>
                                                <li class="facebook"><a href="#" title="Facebook"><i class="moon-facebook"></i></a></li>
                                                <li class="twitter"><a href="#" title="Twitter"><i class="moon-twitter"></i></a></li>
                                                <li class="google_plus"><a href="#" title="Google Plus"><i class="moon-google_plus"></i></a></li>
                                                <li class="pinterest"><a href="#" title="Pinterest"><i class="moon-pinterest"></i></a></li>
                                                <li class="linkedin"><a href="#" title="Linkedin"><i class="moon-linkedin"></i></a></li>
                                                <li class="main"><a href="#" title="Mail"><i class="moon-mail"></i></a></li>
                                            </ul>
                                        </div>
    ');
    $temp->add($setting);
    
    $name = 'theme_ausart/coursetab2text';
    $title = get_string('coursetab2text','theme_ausart');
    $description = get_string('coursetab2text', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '
     <div class="content ">
                                            <h6>Joe Doe</h6>
                                            <div class="position_title"><span class="position">Ceo & Developer</span></div>
                                            <p>Sed at odio ut arcu fringilla dictum magna nisi.Aliquam at erat in purus velit vel dolo dictum magna nisi.</p>
                                        </div>
                                        <div class="social_widget">
                                            <ul>
                                                <li class="facebook"><a href="#" title="Facebook"><i class="moon-facebook"></i></a></li>
                                                <li class="twitter"><a href="#" title="Twitter"><i class="moon-twitter"></i></a></li>
                                                <li class="google_plus"><a href="#" title="Google Plus"><i class="moon-google_plus"></i></a></li>
                                                <li class="pinterest"><a href="#" title="Pinterest"><i class="moon-pinterest"></i></a></li>
                                                <li class="linkedin"><a href="#" title="Linkedin"><i class="moon-linkedin"></i></a></li>
                                                <li class="main"><a href="#" title="Mail"><i class="moon-mail"></i></a></li>
                                            </ul>
                                        </div>
    ');
    $temp->add($setting);
    
    $name = 'theme_ausart/coursetab3text';
    $title = get_string('coursetab3text','theme_ausart');
    $description = get_string('coursetab3text', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '
     <div class="content ">
                                            <h6>Joe Doe</h6>
                                            <div class="position_title"><span class="position">Ceo & Developer</span></div>
                                            <p>Sed at odio ut arcu fringilla dictum magna nisi.Aliquam at erat in purus velit vel dolo dictum magna nisi.</p>
                                        </div>
                                        <div class="social_widget">
                                            <ul>
                                                <li class="facebook"><a href="#" title="Facebook"><i class="moon-facebook"></i></a></li>
                                                <li class="twitter"><a href="#" title="Twitter"><i class="moon-twitter"></i></a></li>
                                                <li class="google_plus"><a href="#" title="Google Plus"><i class="moon-google_plus"></i></a></li>
                                                <li class="pinterest"><a href="#" title="Pinterest"><i class="moon-pinterest"></i></a></li>
                                                <li class="linkedin"><a href="#" title="Linkedin"><i class="moon-linkedin"></i></a></li>
                                                <li class="main"><a href="#" title="Mail"><i class="moon-mail"></i></a></li>
                                            </ul>
                                        </div>
    ');
    $temp->add($setting);
    
    $name = 'theme_ausart/coursetab4text';
    $title = get_string('coursetab4text','theme_ausart');
    $description = get_string('coursetab4text', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '
     <div class="content ">
                                            <h6>Joe Doe</h6>
                                            <div class="position_title"><span class="position">Ceo & Developer</span></div>
                                            <p>Sed at odio ut arcu fringilla dictum magna nisi.Aliquam at erat in purus velit vel dolo dictum magna nisi.</p>
                                        </div>
                                        <div class="social_widget">
                                            <ul>
                                                <li class="facebook"><a href="#" title="Facebook"><i class="moon-facebook"></i></a></li>
                                                <li class="twitter"><a href="#" title="Twitter"><i class="moon-twitter"></i></a></li>
                                                <li class="google_plus"><a href="#" title="Google Plus"><i class="moon-google_plus"></i></a></li>
                                                <li class="pinterest"><a href="#" title="Pinterest"><i class="moon-pinterest"></i></a></li>
                                                <li class="linkedin"><a href="#" title="Linkedin"><i class="moon-linkedin"></i></a></li>
                                                <li class="main"><a href="#" title="Mail"><i class="moon-mail"></i></a></li>
                                            </ul>
                                        </div>
    ');
    $temp->add($setting);
    
    
    $name = 'theme_ausart/popcoursepic1';
$title = get_string('popcoursepic1','theme_ausart');
$description = get_string('popcoursepic1', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'popcoursepic1');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    	$name = 'theme_ausart/popcoursetext1';
    $title = get_string('popcoursetext1','theme_ausart');
    $description = get_string('popcoursetext1', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '
     <div class="vc_col-sm-6 span6 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="header " style="">
                                    <h2>Recent News</h2></div>
                                <div class="recent_news wpb_content_element">
                                    
                                    <div class="row">
                                        <div class="news-carousel ">
                                            <div class="news-carousel-item">
                                                <dl class="news-article blog-article dl-horizontal">
                                                    <dt><img src="/templates/ausart/assets/uploads/2015/01/shutterstock_138847463w-80x77.jpg" alt=""></dt>
                                                    <dd>
                                                        <h5><a href="post.html">First Post with Featured Image</a></h5>
                                                        <div class="blog-content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered nel alteration in some...</div>
                                                        <div class="read_more"><a href="post.html">Read More</a></div>
                                                    </dd>
                                                </dl>
                                            </div>
                                            <div class="news-carousel-item">
                                                <dl class="news-article blog-article dl-horizontal">
                                                    <dt><img src="/templates/ausart/assets/uploads/2015/01/shutterstock_195025238-80x77.jpg" alt=""></dt>
                                                    <dd>
                                                        <h5><a href="post.html">Second Post with Featured Image</a></h5>
                                                        <div class="blog-content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered nel alteration in some...</div>
                                                        <div class="read_more"><a href="post.html">Read More</a></div>
                                                    </dd>
                                                </dl>
                                            </div>
                                            <div class="news-carousel-item">
                                                <dl class="news-article blog-article dl-horizontal">
                                                    <dt><img src="/templates/ausart/assets/uploads/2015/01/shutterstock_137898407-80x77.jpg" alt=""></dt>
                                                    <dd>
                                                        <h5><a href="post.html">New Post with Featured Image</a></h5>
                                                        <div class="blog-content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered nel alteration in some...</div>
                                                        <div class="read_more"><a href="post.html">Read More</a></div>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    ');
    $temp->add($setting);
    



	$name = 'theme_ausart/statspic';
$title = get_string('statspic','theme_ausart');
$description = get_string('statspic', 'theme_ausart');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'statspic');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
       $name = 'theme_ausart/statstext';
    $title = get_string('statstext','theme_ausart');
    $description = get_string('statstext', 'theme_ausart');
    $setting = new admin_setting_configtextarea($name, $title, $description, '
     <div class="vc_col-sm-3 wpb_column column_container span3" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="animated_counter">
                                    <div class="icons"><i class="moon-users-3"></i></div>
                                    <div class="count_to animate_onoffset">
                                        <div class="odometer" data-number="235" data-duration="2000"></div>
                                    </div>
                                    <div class="title_counter">
                                        <h4>Clients</h4></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_col-sm-3 wpb_column column_container span3" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="animated_counter">
                                    <div class="icons"><i class="moon-file-3"></i></div>
                                    <div class="count_to animate_onoffset">
                                        <div class="odometer" data-number="145" data-duration="2000"></div>
                                    </div>
                                    <div class="title_counter">
                                        <h4>Projects</h4></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_col-sm-3 wpb_column column_container span3" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="animated_counter">
                                    <div class="icons"><i class="moon-users-3"></i></div>
                                    <div class="count_to animate_onoffset">
                                        <div class="odometer" data-number="2357" data-duration="2000"></div>
                                    </div>
                                    <div class="title_counter">
                                        <h4>Cups of Caffe</h4></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_col-sm-3 wpb_column column_container span3" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="animated_counter">
                                    <div class="icons"><i class="moon-code"></i></div>
                                    <div class="count_to animate_onoffset">
                                        <div class="odometer" data-number="8234" data-duration="2000"></div>
                                    </div>
                                    <div class="title_counter">
                                        <h4>Lines of Code</h4></div>
                                </div>
                            </div>
                        </div>

    ');
    $temp->add($setting);
   

$name = 'theme_ausart/callout2';
$title = get_string('callout2','theme_ausart');
$description = get_string('callout2', 'theme_ausart');
$default = '<h1 style="color: #222222; font-weight: 700;">Ausart. Business has two functions - marketing &amp; innovation</h1><span class="subtitle" style="color: #222222"></span><a href="2.html" class="btn-system normal second_btn" style="color:#222222 background:;">PurcHase Now</a>';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);
    	 
$ADMIN->add('theme_ausart', $temp);

	
$temp = new admin_settingpage('theme_ausart_misc', "Misc Settings");
    
    // Custom CSS file.
    $name = 'theme_ausart/customcss';
    $title = get_string('customcss', 'theme_ausart');
    $description = get_string('customcssdesc', 'theme_ausart');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
	
	// footerline setting
    $name = 'theme_ausart/fsize';
    $title = get_string('fsize','theme_ausart');
    $description = get_string('fsizedesc', 'theme_ausart');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $temp->add($setting);
    
    $name = 'theme_ausart/credit';
    $title = get_string('credit', 'theme_ausart');
    $description = get_string('credit', 'theme_ausart');
    $choices = array(
        1 => "Show footer credit",
        2 => "Hide footer credit",
    );
    $setting = new admin_setting_configselect($name, $title, $description, 1, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
	
	$ADMIN->add('theme_ausart', $temp);
	
}
