<?php 

require_once(dirname(__FILE__) . '/header.php');

?>

<div id="breadwrap" class="clearfix"><div class="container">
 <?php echo $OUTPUT->page_heading(); ?>
 <div id="page-navbar" class="clearfix">
            <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
            <?php echo $OUTPUT->navbar(); ?>
 </div>
</div></div>

<div id="page-content" class="row-fluid">
    
    <div id="padder" class="clearfix">
         <section id="region-main" class="span9 desktop-first-column">
            <?php
            echo $OUTPUT->course_content_header();
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </section>
        <?php
        $classextra = '';
        if ($left) {
            $classextra = ' 2desktop';
        }
        echo $OUTPUT->blocks('side-pre', 'span3'.$classextra);
        ?>
    </div>
        
    </div>

</div>


<?php require_once(dirname(__FILE__) . '/footer.php'); ?>