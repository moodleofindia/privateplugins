<div id="footer-wrap" class="clearfix">
 <footer id="page-footer" class="container">
        <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
 
<div class="pull-left">            
 <div class="info container2 clearfix">
<div class="pull-left2">
<?php
        //echo $html->footnote;
       // echo $OUTPUT->login_info();
       // echo $OUTPUT->home_link();
        //echo $OUTPUT->standard_footer_html();

?>  
</div> 
<div>
<!--added by Arjun Singh >>starts -->
<?php if(($CFG->wwwroot == 'http://lms.compliance.world') || ($CFG->wwwroot == 'http://localhost/redstone')){ ?>
  <div><img src="<?php echo $CFG->wwwroot.'/theme/ausart/pix/cwlogo.png'; ?>" class="F_logo"/></div>
<?php }elseif($CFG->wwwroot == 'http://lms.ezcertifications.com'){ ?>
   <div><img src="<?php echo $CFG->wwwroot.'/theme/ausart/pix/ezfooterlogo.png'; ?>" class="F_logo"/></div>
<?php } ?>
<!--added by Arjun Singh >>ends -->
<ul class="copyright pull-left">
	<?php 
	echo "<li> ".$PAGE->theme->settings->footnote."</li>";
	?>
		<!--<?php if ($PAGE->theme->settings->credit == 1 || empty($PAGE->theme->settings->credit)) { ?>
			<li id="designby" style="opacity: .4; font-size: 85%;">theme by <a href="http://www.newschoolthemes.com">Newschool Themes</a></li>
		<?php } ?>-->
</ul>
</div>
</div>
</div>
      
<div class="pull-right socials">
<?php 
if (!empty($PAGE->theme->settings->socialone)) {
echo '<a href="'.$PAGE->theme->settings->socialone.'" ><i class="fa fa-facebook-square"></i></a>';
}

if (!empty($PAGE->theme->settings->socialtwo)) {
echo '<a href="'.$PAGE->theme->settings->socialtwo.'" ><i class="fa fa-twitter-square"></i></a>';
}

if (!empty($PAGE->theme->settings->socialthree)) {
echo '<a href="'.$PAGE->theme->settings->socialthree.'" ><i class="fa fa-google-plus-square"></i></a>';
}

if (!empty($PAGE->theme->settings->socialfour)) {
echo '<a href="'.$PAGE->theme->settings->socialfour.'" ><i class="fa fa-linkedin-square"></i></a>';
}
?>
</div>
<div class="pull-left" style="width:100%">
	<?php echo $OUTPUT->login_info(); ?>
</div>
       
</footer>
</div>
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
    
</body>
</html>