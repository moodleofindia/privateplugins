<?php 

//require_once(dirname(__FILE__) . '/header.php');


/*<div id="breadwrap" class="clearfix">
<div class="container">
 <?php echo $OUTPUT->page_heading(); ?>
 <div id="page-navbar" class="clearfix">
            <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
            <?php echo $OUTPUT->navbar(); ?>
 </div>
</div>
</div> */
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/theme/'.$CFG->theme.'/style/jPushMenu.css'), true);	

$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/'.$CFG->theme.'/jquery/bootstrap.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/'.$CFG->theme.'/jquery/index.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/'.$CFG->theme.'/jquery/jquery-min1.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/'.$CFG->theme.'/jquery/bootstrap-submenu.js'), true);

$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);

$knownregionpre = $PAGE->blocks->is_known_region('side-pre');
$knownregionpost = $PAGE->blocks->is_known_region('side-post');
global $DB,$USER;
$roleid = $DB->get_record('role',array('shortname' => 'catadmin'),'id');
$checkuserrole = $DB->get_record('role_assignments',array('userid' => $USER->id,'roleid' => $roleid->id),'id,contextid');
                        
if (is_siteadmin() || !empty($checkuserrole)) {
	$regions = array('content' => 'span9 desktop-first-column');
}else{
	if ($hassidepre && $hassidepost) {
            $regions = array('content' => 'col-sm-4 col-md-6 col-lg-8');
            $regions['pre'] = 'col-sm-4 col-md-3 col-lg-2';
            $regions['post'] = 'col-sm-4 col-md-3 col-lg-2';
        } else if ($hassidepre && !$hassidepost) {
            $regions = array('content' => 'col-sm-7 col-md-8 col-lg-9');
            $regions['pre'] = 'col-sm-5 col-md-4 col-lg-3';
            $regions['post'] = 'emtpy';
        } else if (!$hassidepre && $hassidepost) {
            $regions = array('content' => 'col-sm-7 col-md-8 col-lg-9');
            $regions['pre'] = 'empty';
            $regions['post'] = 'col-sm-5 col-md-4 col-lg-3';
        } else if (!$hassidepre && !$hassidepost) {
            $regions = array('content' => 'col-md-12');
            $regions['pre'] = 'empty';
            $regions['post'] = 'empty';
        }
}


        if ('rtl' === get_string('thisdirection', 'langconfig')) {
            if ($hassidepre && $hassidepost) {
                $regions = array('content' => 'col-sm-4 col-sm-push-8 col-md-6 col-md-push-6 col-lg-8 col-lg-push-4');
                $regions['pre'] = 'col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-6 col-lg-2 col-lg-pull-4';
                $regions['post'] = 'col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-6 col-lg-2 col-lg-pull-4';
            } else if ($hassidepre && !$hassidepost) {
                $regions = array('content' => 'col-sm-8 col-sm-push-4 col-md-9 col-md-push-3 col-lg-10 col-lg-push-2');
                $regions['pre'] = 'col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9 col-lg-2 col-lg-pull-10';
                $regions['post'] = 'empty';
            } else if (!$hassidepre && $hassidepost) {
                $regions = array('content' => 'col-sm-8 col-sm-push-4 col-md-9 col-md-push-3 col-lg-10 col-lg-push-2');
                $regions['pre'] = 'empty';
                $regions['post'] = 'col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-6 col-lg-2 col-lg-pull-4';
            }
        }

$PAGE->set_popup_notification_allowed(false);
theme_ausart_initialise_zoom($PAGE);
$setzoom = theme_ausart_get_zoom();

if ($setzoom == "") {
	$setzoom = "zoomin";
}

echo $OUTPUT->doctype() 
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300,regular%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php p($CFG->wwwroot) ?>/theme/ausart/style/vector-icons.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
<style type="text/css">
	
	/* By Mihir */
	#navbuttons {float:left !important;}
	
	#page {
    	min-height: 511px;
	}
	.mediaplugin_html5video video {
    	width: 60%!important;
	}
	#region-main h2 {
    	padding-top: 20px;
	}
	.stage-head h2{
		padding-left: 25px;
	}
	.bs-wizard>.bs-wizard-step {
    	width: 58px;
	}
	.bs-wizard>.bs-wizard-step:last-child>.progress {
    	width: 50%;
	}
	.bs-wizard>.bs-wizard-step>.progress {
	    position: relative;
	    border-radius: 0px;
	    height: 5px;
	    box-shadow: none;
	    margin: 18px 0;
	}
	.stage2 .row { padding-left: 5%;}
</style>
<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>
			<div id="page-2" class="container-fluid">
			<div class="row">
			<?php
			global $COURSE, $CFG, $DB, $USER;

			$userid = $USER->id;
			//$cid=4;
			$course=$COURSE->id;
			$modinfo = get_fast_modinfo($course);
			//print_r($modinfo);
			?>
			<div class="stage2">

			<div class="stage-head">
			<div class="stage-title pull-left">
			<h2 class="text-white"><?php echo $COURSE->fullname; ?></h2>

			<!--<h5 class="text-grey">pmp info</h5>-->
			</div>	

			<div class="stage-title-right pull-right link">
			<a href="#" class="hlink" >Help & Support</a>
			<a  href="<?php echo $CFG->wwwroot.'/my';?>" ><i class="fa fa-3x fa-times"></i></a>
			<?php 
			
			echo $OUTPUT->page_heading_button(); ?>
			</div>


			</div>
			<div class="stage-progress">


			<div class="row bs-wizard" >
			<?php
			$cm = $modinfo->get_cms();
			//print_r($cm);
			foreach($cm as $cm2) {
			if ($cm2->modname == 'scorm' or $cm2->modname=='quiz' or $cm2->modname=='resource') {
			if($cm2->modname !=='label'){ $linkurl = $CFG->wwwroot.'/mod/'.$cm2->modname.'/view.php?id='.$cm2->id;	}

			if (isset($_REQUEST['id'])) {
			$currid = $_REQUEST['id'];	
			} else {
			$currid = 999999999999999999999999;	
			}



			$status = 'active';

			if ($currid == $cm2->id) {
			$status = 'active';
			} else {

			//get completion status
			$getcmstatq = "Select completionstate from {course_modules_completion} where coursemoduleid=$cm2->id and userid = $USER->id";
			$getcmstatr = $DB->get_field_sql($getcmstatq);
			//var_dump($getcmstatr.'-'.$cm2->id);

			if($getcmstatr == 1) {
			$status = 'complete';
			} else {
			$status = 'disabled';
			}

			}

			?>
			<div class="col-xs-1 bs-wizard-step <?php echo $status; ?>">

			<div class="progress"><div class="progress-bar"></div></div>
			<a href="<?php echo $linkurl; ?>" class="bs-wizard-dot"></a>

			</div>
			<?php
			}
			}
			?>


			</div>

			</div>
			</div>
			</div>
			</div>
<!-- 
<div id="page-content" class="row-fluid">
    
    <div id="padder" class="clearfix">
         <section id="region-main" class="span9 desktop-first-column">
            <?php
            echo $OUTPUT->course_content_header();
            ?> -->
			<div id="page" class="container-fluid">
			<div id="page-area" class="row">
			<?php 
			require_once(dirname(__FILE__) . '/tiles/navbarincourse.php'); ?>

			<?php //require_once(\theme_redstonev1\toolbox::get_tile_file('pageheaderincourse')); ?>

			<div id="page-content" class="row-fluid">
			<div id="region-main" class="<?php echo $regions['content']; ?>">
			<section id="region-main-redstonev1">

			<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
				<a class="close"><i class="fa  fa-times"></i></a>
				<div class="notes tabs ">
					<h3>Notes</h3>
					<div id="successmsg"> </div>
					<div class="form-group">
						<label for="comment">Type Notes:</label>
						<textarea class="form-control" rows="5" name="mynote" id="mynote"></textarea>
						<input type="hidden" value="<?php echo $COURSE->id; ?>" id="csid" name="csid">
						<input type="submit" value="Save" id="submit" name="submit"><br>
					</div>
					<hr>
					<div style="height:600px; overflow:scroll;">
					
						<ul id="addhere">
						
						</ul>
					</div>
					
					<script>
					$(document).ready(function(){
					$("#submit").click(function(){
					var mynote1 = $("#mynote").val();
					var csid1 = $("#csid").val();
					document.getElementById("successmsg").innerHTML = "";


					// Returns successful data submission message when the entered information is stored in database.
					var dataString = 'mynote='+ mynote1 + '&csid='+ csid1 ;
					if(mynote1=='')
					{
					alert("Please Fill All Fields");
					}
					else
					{
					// AJAX Code To Submit Form.
					$.ajax({
					type: "POST",
					url: "<?php echo $CFG->wwwroot.'/theme/ausart/layout/submitnote.php' ; ?>",
					data: dataString,
					cache: false,
					success: function(result){
					//alert(result);
					document.getElementById("successmsg").innerHTML = result;
					//document.getElementById("addhere").innerHTML = mynote1;
					$("#addhere").prepend('<li  class="noteclass">'+mynote1+'</li>');
					document.getElementById("mynote").value = "";
					}
					});
					}
						return false;
					});
					});
					</script>
				</div>

				<div class="forum tabs ">
					<h3>Forum</h3>
					<?php 
					$cmf = $modinfo->get_cms();
					foreach($cmf as $cm4) {
						if($cm4->modname == 'forum'){ 
							$forumurl = $CFG->wwwroot.'/mod/'.$cm4->modname.'/view.php?id='.$cm4->id;	
							?>
							<p><a href="<?php echo $forumurl; ?>" target="_blank"><?php echo $cm4->name; ?></a></p>
							<?php
						}
					}
					?>
				</div>

				<div class="download tabs ">
					<h3>Downloads</h3>
					<?php 
					$cmr = $modinfo->get_cms();
					foreach($cmr as $cm3) {
						if($cm3->modname == 'resource'){ 
							$downloadurl = $CFG->wwwroot.'/mod/'.$cm3->modname.'/view.php?id='.$cm3->id;	
							?>
							<p><a href="<?php echo $downloadurl; ?>" target="_blank"><?php echo $cm3->name; ?></a></p>
							<?php
						}
					}
					?>
				</div>
			</nav>
			<div class="menu-icon">
				<div class="icons">
					<a href="#" class="toggle-menu menu-left push-body" name="notes"><i class="fa fa-3x fa-pencil-square"></i><br><span>Notes</span></a>
					<a href="#" class="toggle-menu menu-left push-body" name="forum"><i class="fa fa-3x fa-comments"></i><br><span>Forum</span></a>
					<a href="#" class="toggle-menu menu-left push-body" name="download"><i class="fa fa-3x fa-cloud-download"></i><br><span>Downloads</span></a>
				</div>
			</div>
            <?php
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </section>
        </div>
		<?php
		if (is_siteadmin() || !empty($checkuserrole)) {
			echo $OUTPUT->blocks('side-pre', 'span3 2desktop');
		}
		?>
        
		</div>
		</div>
		</div>


<?php require_once(dirname(__FILE__) . '/footer.php'); ?>
</body>
</html>
<script>
$(function(){
	
	$("#homebtn").mouseover(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/homeicon-hover.png'; ?>");
	})
	.mouseout(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/homeicon.png'; ?>");
	});
	
	$("#firstbtn").mouseover(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/firsticon-hover.png'; ?>");
	})
	.mouseout(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/firsticon.png'; ?>");
	});
	
	$("#lastbtn").mouseover(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/lasticon-hover.png'; ?>");
	})
	.mouseout(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/lasticon.png'; ?>");
	});
	
	$("#nextbtn").mouseover(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/nexticon-hover.png'; ?>");
	})
	.mouseout(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/nexticon.png'; ?>");
	});
	
	$("#prevbtn").mouseover(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/previcon-hover.png'; ?>");
	})
	.mouseout(function () {
	$(this).attr("src", "<?php echo $CFG->wwwroot.'/blocks/navbuttons/pix/previcon.png'; ?>");
	});
	
});
</script>
<script src="<?php echo $CFG->wwwroot . '/theme/ausart/jquery/jPushMenu.js';?>" type="text/javascript"></script>

<script>
jQuery(document).ready(function($) {
	$('.toggle-menu').jPushMenu();
});
</script>
<script>
    var removeClass = true;
    $('#list').click(function(){
               if(removeClass){
					$('#region-main').addClass('col-md-12');
					$('#region-main').removeClass('col-sm-7 col-md-8 col-lg-9');
					$("#block-region-side-pre").hide();
			        removeClass = false;
             }else{
	                $('#region-main').addClass('col-sm-7 col-md-8 col-lg-9');
					$('#region-main').removeClass('col-md-12');
					$("#block-region-side-pre").show();
	                 removeClass = true;
             }
		
    });
  </script>