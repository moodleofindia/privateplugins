<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's ausart theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_ausart
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */

function theme_ausart_page_init(moodle_page $page) {
    $page->requires->jquery();
}  
 
function theme_ausart_process_css($css, $theme) {

    // Set the font size
    if (!empty($theme->settings->fsize)) {
        $fsize = $theme->settings->fsize;
    } else {
        $fsize = null;
    }
    $css = theme_ausart_set_fsize($css, $fsize);
    
      // Set the font size
    if (!empty($theme->settings->intopacity)) {
        $intopacity = $theme->settings->intopacity;
    } else {
        $intopacity = null;
    }
    $css = theme_ausart_set_intopacity($css, $intopacity);
    
    
          // Set the font size
    if (!empty($theme->settings->logotop)) {
        $logotop = $theme->settings->logotop;
    } else {
        $logotop = null;
    }
    $css = theme_ausart_set_logotop($css, $logotop);
    
    // Set the font size
    if (!empty($theme->settings->fpheight)) {
        $fpheight = $theme->settings->fpheight;
    } else {
        $fpheight = null;
    }
    $css = theme_ausart_set_fpheight($css, $fpheight);
    
    // Set the font size
    if (!empty($theme->settings->intheight)) {
        $intheight = $theme->settings->intheight;
    } else {
        $intheight = null;
    }
    $css = theme_ausart_set_intheight($css, $intheight);
	
    // Set the link color
    if (!empty($theme->settings->linkcolor)) {
        $linkcolor = $theme->settings->linkcolor;
    } else {
        $linkcolor = null;
    }
    $css = theme_ausart_set_linkcolor($css, $linkcolor);

	// Set the link hover color
    if (!empty($theme->settings->linkhover)) {
        $linkhover = $theme->settings->linkhover;
    } else {
        $linkhover = null;
    }
    $css = theme_ausart_set_linkhover($css, $linkhover);
    
    // Set the main color
    if (!empty($theme->settings->maincolor)) {
        $maincolor = $theme->settings->maincolor;
    } else {
        $maincolor = null;
    }
    $css = theme_ausart_set_maincolor($css, $maincolor);
    
    
     // Set the main color
    if (!empty($theme->settings->secondcolor)) {
        $secondcolor = $theme->settings->secondcolor;
    } else {
        $secondcolor = null;
    }
    $css = theme_ausart_set_secondcolor($css, $secondcolor);
	
	  // Set the main color
    if (!empty($theme->settings->headingcolor)) {
        $headingcolor = $theme->settings->headingcolor;
    } else {
        $headingcolor = null;
    }
    $css = theme_ausart_set_headingcolor($css, $headingcolor);
	
   // Set the main headings color
    if (!empty($theme->settings->footcolor)) {
        $footcolor = $theme->settings->footcolor;
    } else {
        $footcolor = null;
    }
    $css = theme_ausart_set_footcolor($css, $footcolor);
    
    
    if (!empty($theme->settings->p1)) {
        $p1 = $theme->settings->p1;
    } else {
        $p1 = null;
    }
    $css = ausart_set_pone($css, $p1, $theme);
    
    
     // Set the body background image
    if (!empty($theme->settings->slidep1)) {
        $background = $theme->settings->slidep1;
    } else {
        $background = null;
    }
    $css = ausart_set_background($css, $background, $theme);
    
    
    // Set the body background image
    if (!empty($theme->settings->servicebk)) {
        $servicebk = $theme->settings->servicebk;
    } else {
        $servicebk = null;
    }
    $css = ausart_set_servicebk($css, $servicebk, $theme);
    
     // Set the body background image
    if (!empty($theme->settings->statbk)) {
        $statbk = $theme->settings->statbk;
    } else {
        $statbk = null;
    }
    $css = ausart_set_statbk($css, $statbk, $theme);
    
    
     // Set the body background image
    if (!empty($theme->settings->intbk)) {
        $intbk = $theme->settings->intbk;
    } else {
        $intbk = null;
    }
    $css = ausart_set_intbk($css, $intbk, $theme);


    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_ausart_set_customcss($css, $customcss);
	
    return $css;
}


/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_ausart_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

function ausart_set_background($css, $background, $theme) {
    $tag = '[[setting:background]]';
    $replacement = $background;
    if (is_null($replacement)) {
        $replacement = $theme->pix_url('newbk', 'theme');
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function ausart_set_servicebk($css, $servicebk, $theme) {
    $tag = '[[setting:servicebk]]';
    $replacement = $servicebk;
    if (is_null($replacement)) {
        $replacement = "http://placehold.it/1920x1080";
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}


function ausart_set_statbk($css, $statbk, $theme) {
    $tag = '[[setting:statbk]]';
    $replacement = $statbk;
    if (is_null($replacement)) {
        $replacement = "http://placehold.it/1920x1080";
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function ausart_set_intbk($css, $intbk, $theme) {
    $tag = '[[setting:intbk]]';
    $replacement = $intbk;
    if (is_null($replacement)) {
        $replacement = "http://placehold.it/1920x610";
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_fsize($css, $fsize) {
    $tag = '[[setting:fsize]]';
    $replacement = $fsize;
    if (is_null($replacement)) {
        $replacement = '92';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_intopacity($css, $intopacity) {
    $tag = '[[setting:intopacity]]';
    $replacement = $intopacity;
    if (is_null($replacement)) {
        $replacement = '.3';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_logotop($css, $logotop) {
    $tag = '[[setting:logotop]]';
    $replacement = $logotop;
    if (is_null($replacement)) {
        $replacement = '0';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_fpheight($css, $fpheight) {
    $tag = '[[setting:fpheight]]';
    $replacement = $fpheight;
    if (is_null($replacement)) {
        $replacement = '200';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_intheight($css, $intheight) {
    $tag = '[[setting:intheight]]';
    $replacement = $intheight;
    if (is_null($replacement)) {
        $replacement = '175';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
 
function theme_ausart_set_linkcolor($css, $linkcolor) {
    $tag = '[[setting:linkcolor]]';
    $replacement = $linkcolor;
    if (is_null($replacement)) {
        $replacement = '#009dcd';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_linkhover($css, $linkhover) {
    $tag = '[[setting:linkhover]]';
    $replacement = $linkhover;
    if (is_null($replacement)) {
        $replacement = '#666666';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_maincolor($css, $maincolor) {
    $tag = '[[setting:maincolor]]';
    $replacement = $maincolor;
    if (is_null($replacement)) {
        $replacement = '#009dcd';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_headingcolor($css, $headingcolor) {
    $tag = '[[setting:headingcolor]]';
    $replacement = $headingcolor;
    if (is_null($replacement)) {
        $replacement = '#2f383d';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_secondcolor($css, $secondcolor) {
    $tag = '[[setting:secondcolor]]';
    $replacement = $secondcolor;
    if (is_null($replacement)) {
        $replacement = '#323232';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_ausart_set_footcolor($css, $footcolor) {
    $tag = '[[setting:footcolor]]';
    $replacement = $footcolor;
    if (is_null($replacement)) {
        $replacement = '#282a2b';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function ausart_set_pone($css, $p1, $theme) {
    $tag = '[[setting:bannerpic]]';
    $replacement = $p1;
    if (is_null($replacement)) {
        $replacement = $theme->pix_url('paint', 'theme');
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

/**
 * Deprecated: Please call theme_ausart_process_css instead.
 * @deprecated since 2.5.1
 */
function ausart_process_css($css, $theme) {
    debugging('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__, DEBUG_DEVELOPER);
    return theme_ausart_process_css($css, $theme);
}

/**
 * Deprecated: Please call theme_ausart_set_customcss instead.
 * @deprecated since 2.5.1
 */
function ausart_set_customcss($css, $customcss) {
    debugging('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__, DEBUG_DEVELOPER);
    return theme_ausart_set_customcss($css, $customcss);
}

function theme_ausart_initialise_zoom(moodle_page $page) {
    user_preference_allow_ajax_update('theme_ausart_zoom', PARAM_TEXT);
    $page->requires->yui_module('moodle-theme_ausart-zoom', 'M.theme_ausart.zoom.init', array());
}

/**
 * Get the user preference for the zoom function.
 */
function theme_ausart_get_zoom() {
    return get_user_preferences('theme_ausart_zoom', '');
}


function theme_ausart_get_setting($setting, $format = false) {
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('ausart');
    }

    if (empty($theme->settings->$setting)) {
        return false;
    } else if (!$format) {
        return $theme->settings->$setting;
    } else if ($format === 'format_text') {
        return format_text($theme->settings->$setting, FORMAT_PLAIN);
    } else if ($format === 'format_html') {
        return format_text($theme->settings->$setting, FORMAT_HTML, array('trusted' => true));
    } else {
        return format_string($theme->settings->$setting);
    }
}

function theme_ausart_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('ausart');
    }
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        if ($filearea === 'logo') {
            return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
        } else if ($filearea === 'style') {
            theme_essential_serve_css($args[1]);
        }
        else if ($filearea === 'homebk') {
            return $theme->setting_file_serve('homebk', $args, $forcedownload, $options);
        }
        else if ($filearea === 'pagebackground') {
            return $theme->setting_file_serve('pagebackground', $args, $forcedownload, $options);
        } else if (preg_match("/p[1-9][0-9]/", $filearea) !== false) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if ((substr($filearea, 0, 9) === 'marketing') && (substr($filearea, 10, 5) === 'image')) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if ($filearea === 'iphoneicon') {
            return $theme->setting_file_serve('iphoneicon', $args, $forcedownload, $options);
        } else if ($filearea === 'iphoneretinaicon') {
            return $theme->setting_file_serve('iphoneretinaicon', $args, $forcedownload, $options);
        } else if ($filearea === 'ipadicon') {
            return $theme->setting_file_serve('ipadicon', $args, $forcedownload, $options);
        } else if ($filearea === 'ipadretinaicon') {
            return $theme->setting_file_serve('ipadretinaicon', $args, $forcedownload, $options);
        } else if ($filearea === 'fontfilettfheading') {
            return $theme->setting_file_serve('fontfilettfheading', $args, $forcedownload, $options);
        } else if ($filearea === 'fontfilettfbody') {
            return $theme->setting_file_serve('fontfilettfbody', $args, $forcedownload, $options);
        } else if ($filearea === 'bcumarkettingimages') {
            return $theme->setting_file_serve('bcumarkettingimages', $args, $forcedownload, $options);
        } else {
            send_file_not_found();
        }
    } else {
        send_file_not_found();
    }
}