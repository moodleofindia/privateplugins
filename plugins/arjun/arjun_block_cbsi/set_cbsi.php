<?php
require_once('../../config.php');

global $DB, $PAGE,$CFG;
require_once(__DIR__.'/set_cbsi_form.php');
//$courseid = optional_param('courseid',false, PARAM_INT);
$PAGE->set_context(context_system::instance());
$PAGE->set_title("Set | CBSI");
$PAGE->set_heading("CBSI Course Settings");
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/set_cbsi.php');
$PAGE->navbar->add(get_string('sidebar-manage-courses', 'block_cbsi'), new moodle_url($CFG->wwwroot . '/blocks/cbsi/manage_content.php'));
$PAGE->set_pagelayout('admin');
echo $OUTPUT->header();
require_login();
$courseid = $_GET['courseid'];
$sql        = "SELECT * FROM mdl_block_cbsi WHERE courseid = {$courseid}";
$record     = $DB->get_record_sql($sql);
$mform = new set_cbsi_form($PAGE->url . "?courseid=" . $courseid, (array)$record);
$fromform = $mform->get_data();
if ($mform->is_cancelled()) {
    redirect($PAGE->url);
} else if ($fromform) {
    if(empty($record)){
        $courserecords = $DB->get_record('course',array('id' => $courseid),'id,shortname,category,startdate');
        $parent_category= $DB->get_record('course_categories',array('id'=>$courserecords->category),'parent');
        if($parent_category->parent == 0){
          $parent_category->parent = $courserecords->category;
        }
        $inrecords = new stdClass();
        $inrecords->courseid = $courserecords->id;
        $inrecords->course_creator = $USER->id;
        $inrecords->start_datetime = $courserecords->startdate;
        $inrecords->end_datetime = $fromform->end_datetime;
        $inrecords->default_shortname = $courserecords->shortname;
        $inrecords->categoryid = $courserecords->category;
        $inrecords->parent_category = $parent_category->parent;
        $insertrecords [] = $inrecords;
        //print_object($insertrecords);
        $DB->insert_records('block_cbsi', $insertrecords);
        redirect($PAGE->url . "?courseid=" . $courseid);
    }else{
    $record->end_datetime = $fromform->end_datetime;
    $DB->update_record('block_cbsi', $record);
    redirect($PAGE->url . "?courseid=" . $courseid);
    }

} else {
  $mform->set_data($record);
  $mform->display();
}
echo $OUTPUT->footer();