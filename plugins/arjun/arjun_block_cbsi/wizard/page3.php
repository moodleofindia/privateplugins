<div class="formalign">
    <tr>
        <td align="right" valign="top"><b><?php echo get_string("course_name", "block_cbsi");?></b></td>
        <td valign="top" align="left">
            <input id="course_name" class="courseName" name="course_name" type="text" />
        </td>
    </tr>

    <P>&nbsp;</P>

<?php /*    <tr>
        <td align="right" valign="top">
            <b><?php echo get_string("course_language", "block_cbsi");?></b>
        </td>
        <td valign="top" align="left">
            <select id="language" class="languageName" name="course_language">
                <option value="0"><?php echo get_string('lang_select', 'block_cbsi');?></option>
                <option value="1"><?php echo get_string('lang_english', 'block_cbsi');?></option>
                <option value="2"><?php echo get_string('lang_spanish', 'block_cbsi');?></option>
            </select>
        </td>
    </tr>
*/?>

    <P>&nbsp;</P>

    <tr>
        <td align="right" valign="top">
            <b><?php echo get_string("course_description", "block_cbsi");?></b>
        </td>
        <td valign="top" align="left">
            <textarea id="description" wrap="virtual" rows="20" cols="50" name="course_description"></textarea>
        </td>
    </tr>

    <P>&nbsp;</P>
</div><!-- end formalign -->
