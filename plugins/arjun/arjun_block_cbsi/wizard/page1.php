<form action="/local/metrostarwizard/index.php" method="post" name="pageone" id="pageone">
    <div id="container">
        <div class="wizard">
            <h2><?php echo get_string("page_one_question", "block_cbsi");?></h2>
            <div class="wizardChoices oneoption" id="what_to_do">
                <div class="wizardChoice">
                    <p><?php echo get_string("create_new_meeting_training", "block_cbsi");?></p>
                </div><!-- end wizardChoice -->
            </div><!-- end wizardChoices -->
        </div><!-- end wizard-->
    </div><!-- end container -->
</form>
