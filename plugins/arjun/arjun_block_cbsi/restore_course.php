<?php
/* refer to https://docs.moodle.org/dev/Restore_2.0_for_developers */
defined('MOODLE_INTERNAL') || die();
define('CLI_SCRIPT', true);

require_once('../../config.php');
require_once('lib.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');

global $CFG,$DB,$USER;

$cohort     = get_cohort_idnumber();
$filename   = $_POST['backup_filename'];

/* $params
    [0] = type (not used)
    [1] = original course id
    [2] = original category id
    [3] = shortname
    [4] = date of backup
*/
$params     = explode("--", $filename);
$categoryid = $params[2];

// Transaction.
$transaction = $DB->start_delegated_transaction();

// Create new course.
$folder             = "$CFG->dataroot/backups/$cohort/";
$userdoingrestore   = $USER->id;
$courseid           = restore_dbops::create_new_course('', '', $categoryid);

// Restore backup into course.
$controller = new restore_controller(
    $folder,
    $courseid,
    backup::INTERACTIVE_NO,
    backup::MODE_SAMESITE,
    $userdoingrestore,
    backup::TARGET_NEW_COURSE
);

$controller->execute_precheck();
$controller->execute_plan();

//set response type to JSON for easy jQuery parsing
header('Content-Type: application/json');

try {
    // Commit.
    $transaction->allow_commit();
    echo json_encode("course restoration successful.");
} catch(Exception $e) {
    echo json_encode("course restoration failed.");
}
