<?php

require_once('../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot."/enrol/locallib.php");
require_once($CFG->dirroot."/enrol/renderer.php");

require_login();

$users    = $_POST['users'];
$courseid = $_POST['courseid'];
$failed_unenrollment = array();

// $users     = array(437); //debug value
// $courseid  = 549;       //debug value

function unenroll_user($userid, $courseid) {
    global $DB;
    $instance = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'manual'));
    // print_r($instance);
    // die(__FILE__.":".__LINE__);
    $plugin  = enrol_get_plugin($instance->enrol);

    try {
        $plugin->unenrol_user($instance, $userid);
        return true;
    } catch (Exception $e) {
        return false;
    }
}

foreach($users as $userid) {
    if(!unenroll_user($userid,$courseid)) {
        array_push($failed_unenrollment, $userid);
    }
}


header('Content-Type: application/json');

if(count($failed_unenrollment) === 0) {
    echo json_encode($failed_unenrollment);
} else {
    $html = "The following user IDs could not be unenrolled.\n\n";

    foreach($failed_unenrollment as $f) {
       $html .= "$f\n";
    }
    echo json_encode($html);
}
