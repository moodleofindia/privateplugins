<?php
// file: mod/yourplugin/jquery/plugins.php
$plugins = array(
    'cbsi-datatables' => array(
        'files' => array(
            //'bootstrap/bootstrap.css',
            'datatables/media/js/jquery.js',
            'bootstrap/bootstrap.min.js',
            'datatables/media/css/jquery.dataTables.css',
            'datatables/media/js/jquery.dataTables.js',
            'datatables/plugins/filtering/type-based/html.js'
        ),
     ),
);
