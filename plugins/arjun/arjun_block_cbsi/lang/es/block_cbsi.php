<?php

$string['pluginname']           = "CBSI";
$string['page_header']          = "Haga clic en el título para editar la información del curso.";

$string['checklist_disclaimer'] = "Se comprueben los artículos si se les hayan editado por lo menos una vez.";
$string['checklist_title']      = "Lista de Verificación";

$string['view_course']         = "Ver Curso";
$string['view_course_copy']     = "Ver cómo aparecerá el curso una vez finalizada la configuración.";

$string['edit_course']          = "Editar el Curso";
$string['edit_course_copy']     = "Estos ajustes se utilizan para editar los detalles del curso.";

$string['editing_toggle']       = "Activar Edición/ Desactivar Edición";
$string['editing_toggle_copy']  = "Esta configuración le permite modificar cada una de las secciones y agregar funciones al curso";

$string['enrollment_methods']   = "Elija un Método de Inscripción";
$string['enrollment_methods_copy']   = "Elija si los asistentes pueden auto-inscribirse o si un facilitador tendrá que inscribirles manualmente.";

$string['add_user']             = "Agregar  Usuario";
$string['add_user_copy']        = "Matricular usuarios en el curso.";

$string['remove_user']          = "Eliminar Usuario";
$string['remove_user_copy']     = "Eliminar usuarios del curso.";

$string['assign_instructor']    = "Asignar un Instructor";
$string['assign_instructor_copy']    = "Designar quien facilitará el curso. El instructor también tendrá que estar matriculado.";

$string['completion_criteria']  = "Establecer Criterios de Realización";
$string['completion_criteria_copy']  = "Determinar los criterios para la realización de un curso para los estudiantes.";

$string['create_badge']         = "Crear / Asignar una Insignia";
$string['create_badge_copy']    = "Las insignias reconocen la realización exitosa de un curso. Las insignias pueden ser asignadas manualmente o automaticamente por seleccionados criterios de realización.";

$string['delete_course']        = "Eliminar Curso";
$string['delete_course_copy']   = "Eliminar el curso.";

$string['backup_course']        = "Copia de Seguridad";
$string['backup_course_copy']   = "Copia de Seguridad del curso.";

$string['restore_course']       = "Restaurar";
$string['restore_course_copy']  = "Restauar una copia de seguridad del curso.";

$string['change_shortname']     = "Cambiar Nombre Corto";
$string['change_shortname_copy']= "Cambiar el nombre corto. El nombre corto identifica el curso y se utiliza para la navegación del sitio";

$string['set_shared']          	= "Compartir en Cursos Abiertos";
$string['set_shared_copy']      = "Compartir un curso con todas las academias como un Curso Abierto.";

$string['unset_shared']         = "Eliminar de Cursos Abiertos";
$string['unset_shared_copy']    = "Eliminar de la categoria de Cursos Abiertos.";

$string['edit_off'] = "Desactivar Edición";
$string['edit_off_copy'] = "Desactivar edición del curso.";

$string['edit_on'] = "Activar Edición";
$string['edit_on_copy'] = "Activar edición del curso.";

$string['pluginname'] = 'Asistente';

//exists for multiple pages
$string['exit_wizard'] = 'Salir';
$string['wizard_name'] = 'Asistente de la Creación de Cursos de CBSI-Connect';
$string['previous'] = 'Anterior';
$string['next'] = 'Próxima';


//page one
$string['page_one_question'] = '¿Qué es lo que usted desea hacer hoy?';
$string['create_new_meeting_training'] = 'Crear una reunión o capacitación nueva';



//page two
$string['page_two_question'] = '¿Qué es lo que usted desea crear?';
$string['meeting'] = 'Reunión';
$string['training'] = 'Capacitación';


//page three
$string['page_three_question'] = '¿Usted quiere que sea disponible a otras academias?';
$string['no_private_course'] = 'No:<br>Será privada';
$string['yes_all'] = 'Sí:<br> A todas las Academias';
$string['yes_selected'] = 'Sí:<br> Sólamente para las academias seleccionadas';


//page four
$string['course_name'] = '¿Qué es el título?';
$string['course_language'] = '¿En cuál idioma estará disponible?';
$string['course_description'] = '¿Cuál es la descripción?';
$string['course_sections'] = '¿Cuántas secciones/temas?';

//$string['page_four_question']       = 'Set the start date of the training or meeting?';
$string['page_four_header']          = 'Establezca la fecha de comienzo y la fecha de finalización y la hora de comienzo.';
$string['page_four_start']          = 'Fecha de comienzo';
$string['page_four_end']            = 'Fecha de finalización';
$string['page_four_date_startdate'] = '¿Cuándo desea que esta formación o reunión para comenzar?';
$string['page_four_date_starttime'] = 'Elija una hora de inicio:';
$string['page_four_date_enddate']   = '¿Cuándo quieres este curso hasta el final?';
$string['page_four_date_endtime']   = 'Elija una hora de finalización:';


//last page
$string['final_page_header'] = '¡Felicidades! Su formación o reunión está configurado. ¿Qué le gustaría hacer ahora? ';
$string['save_go_home'] = 'Guárdela y vuélvame a la página de inicio';
$string['delete_go_home'] = 'Elimine y Vuelva a la Página de Inicio';
$string['save_start'] = 'Guarde y Empiece Configuración';

$string['lang_select']  = "Select";
$string['lang_english'] = "inglés";
$string['lang_spanish'] = "español";
$string['lang_dutch']   = "holandés";

/*
 * Validation errors
 */
$string['page_two_course_type'] = "'Por favor, provea el título de curso!'";
$string['page_two_course_name'] = "'Por favor, añada un nombre, por supuesto!'";
$string['page_two_startdate']   = "'Por favor, añada una fecha de inicio!'";
$string['page_two_starttime']   = "'Por favor, añada una hora de inicio!'";
$string['page_two_enddate']     = "'Por favor, añada una fecha final!'";
$string['page_two_endtime']     = "'Por favor, añada una hora de finalización!'";
$string['need_admin'] = "'Había un problema. Por favor notifique al creador del sitio.'";

$string['or'] = "o";

$string['timed_out_session'] = 'Su sesión se ha terminado porque se agotó el tiempo de espera por una falta de actividad. Por favor entre de nuevo.';
$string['scroll_bottom'] = 'Desplácese hasta la parte inferior';
$string['scroll_top'] = 'Desplácese a la parte superior';
$string['attendees'] = 'Asistentes';
$string['seats_open'] = 'Asientos Abiertos';
$string['seats_open'] = 'Asientos Abiertos';
$string['start/join'] = 'Comenzar/Unirse';
$string['manage'] = 'Gestionar';
$string['edit'] = 'Editar';
$string['summary'] = 'Resumen';
$string['shared'] = 'Compartida';
$string['description'] = 'Descripción';
$string['joined'] = 'Se unió hace';
$string['hours_ago'] = 'horas';
$string['last_longin'] = 'último inicio de sesión';
$string['at'] = 'a';

//gab_add_more
//$string['help_desk'] = 'Mesa de Ayuda';
$string['instructor_not_yet_assigned'] = 'todavía no asignado(a)';
$string['reunion'] = 'Reunión';
$string['capacitcion'] = 'Capacitación';
$string['logging_in_as_viewer'] = 'Entrando como un(a) espectador(a)';
$string['enter_date_and_time'] = 'Establezca la fecha de comienzo y la fecha de finalización y la hora de comienzo.';

//edit_settings
$string['content_contributor'] = 'Contribuyente de Contenido';
$string['instructor'] = 'Instructor(a)';
$string['site_admin'] = 'Administador del Sitio';

//enroll_users
$string['filter_users_by_academy'] = 'Filtre Usuarios por Academia';
$string['filter_users_by_academy'] = 'Filtre Usuarios por Academia';
$string['selected-users'] = 'Usuarios Seleccionados';
$string['enroll_users'] = 'Matricule los seleccionados';
$string['clear_users'] = 'Borre los seleccionados';
$string['show_entries'] = 'Muestre usuarios';
$string['search'] = 'Buscar';
$string['academy'] = 'Academia';
$string['first_name'] = 'Nombre';
$string['last_name'] = 'Apellido';
$string['email'] = 'Correo Electronico';
$string['next'] = 'Próxima';
$string['previous'] = 'Anterior';


//unenroll_users
$string['selected_users'] = 'Usuarios Seleccionados';
$string['unenroll_users'] = 'Destituir Usuarios';
$string['clear_users'] = 'Borre los seleccionados';
$string['search'] = 'Buscar';
$string['academy'] = 'Academia';
$string['first_name'] = 'Nombre';
$string['last_name'] = 'Apellido';
$string['email'] = 'Correo Electronico';
$string['next'] = 'Próxima';
$string['previous'] = 'Anterior';
$string['show_entries'] = 'Muestre usuarios';
//$string['Showing x to x of x entries'] = 'Mostrando x hasta x de x entrados';


//remove_instructor
$string['selected_users'] = 'Usuarios Seleccionados';
$string['unenroll_users'] = 'Destituir Instructores';
$string['clear_users'] = 'Borre los seleccionados';
$string['search'] = 'Buscar';
$string['academy'] = 'Academia';
$string['first_name'] = 'Nombre';
$string['last_name'] = 'Apellido';
$string['email'] = 'Correo Electronico';
$string['next'] = 'Próxima';
$string['previous'] = 'Anterior';
$string['show_entries'] = 'Muestre usuarios';
$string['unassign_instructor'] = "Jubile a un Instructor";
$string['retire_instructor_button'] = 'Jubile a un Instructor';
$string['retire_instructor_description'] = 'Jubile a un Instructor';
//$string['Showing x to x of x entries'] = 'Mostrando x hasta x de x entrados';


//add_instructor
$string['selected_users'] = 'Usuarios Seleccionados';
$string['assign_instructors'] = 'Asignar Instructores';
$string['slear_users'] = 'Borre los seleccionados';
$string['show_entries'] = 'Muestre usuarios';
$string['search'] = 'Buscar';
$string['academy'] = 'Academia';
$string['first_name'] = 'Nombre';
$string['last_name'] = 'Apellido';
$string['email'] = 'Correo Electronico';
$string['next'] = 'Próxima';
$string['previous'] = 'Anterior';
$string['sidebar-siteadmin'] = "Administrador del Sitio";

$string['meetings'] = 'Meetings';
