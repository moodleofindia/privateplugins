<?php
require_once('../../config.php');
global $DB, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title("Enroll Users");
$PAGE->set_heading("Enroll Users");
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/users.php');
$PAGE->navbar->add("Manage Courses", new moodle_url($CFG->wwwroot . '/?name=portal'));
$PAGE->navbar->add("Enroll Users", new moodle_url($CFG->wwwroot . '/blocks/cbsi/users.php'));
$courseid = $_GET['courseid'];

echo $OUTPUT->header();
require_login();

//get cohort list as an array of cohort objects
$academies = $DB->get_records_sql('SELECT id,name,idnumber from mdl_cohort order by name');

//reset indexes for array for easier iteration
$academies = array_values($academies);

// print_r($academies);
// die();

//get all users in an academy (cohort)
$sql_users = "SELECT *
    FROM {cohort_members}
    INNER JOIN {user}
    ON {cohort_members}.userid = {user}.id
    WHERE cohortid = ? ORDER BY lastname ASC";

$filter = '
    <div class="filtering">
        <div id="filters">
            <span>'.get_string('filter_users_by_academy', 'block_cbsi').'</span>
            <ul id="academy-names">';
                foreach($academies as $a) {
                    $filter .= '
                        <li>
                            <input type="checkbox" value="' . $a->name . '" class="categoryFilter" name="categoryFilter">
                            <label for="">' . $a->name. ' </label>
                        </li>';
                }

    $filter .= '
            </ul>
        </div>
        <div id="selected-users">
            <span>'.get_string('selected_users', 'block_cbsi').'</span><span style="float:right"><button id="enroll_users" >'.get_string('enroll_users', 'block_cbsi').'</button></span>
            <ul id="user-names">
            </ul>
        </div><!-- end selected-users -->
        <button id="clear_users" style="float:right; margin-bottom: 10px;">'.get_string('clear_users', 'block_cbsi').'</button>
    </div><!-- end container -->';


$table = '
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>'.get_string('academy', 'block_cbsi').'</th>
                <th>'.get_string('first_name', 'block_cbsi').'</th>
                <th>'.get_string('last_name', 'block_cbsi').'</th>
                <th>'.get_string('email', 'block_cbsi').'</th>
            </tr>
        </thead>
        <tbody>';

foreach($academies as $a) {
    $users = $DB->get_records_sql($sql_users, array($a->id));

    foreach($users as $user) {
        $table .= "
            <tr id='$user->id'>
                <td>$a->name</td>
                <td>$user->firstname</td>
                <td>$user->lastname</td>
                <td>$user->email</td>
            </tr>";
    }
}

$table .= "</tbody></table>";
?>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="js/datatables/media/css/jquery.dataTables.css">

<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="js/jquery/jquery-1.11.1.js"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="js/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/datatables/plugins/filtering/type-based/html.js"></script>
<style>

</style>

<?php
echo $filter;
echo $table;
?>

<script>
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var categoryFilter,categoryCol,categoryArray,found;

            //creates selected checkbox array
            categoryFilter = $('.categoryFilter:checked').map(function () {
                  return this.value;
                }).get();

            if(categoryFilter.length){

                categoryCol = data[0]; //filter column

                categoryArray =  $.map( categoryCol.split(','), $.trim); // splites comma seprated string into array

                // finding array intersection
                found = $(categoryArray).not($(categoryArray).not(categoryFilter)).length;

                if(found == 0){
                    return false;
                }
                else{
                    return true;
                }
            }
            // default no filter
            return true;
        }
    );

    $(document).ready(function() {
        $('#selected-users > #user-names').css({'height' : $('#filters > #academy-names').height()});

        var users = $('#users').DataTable({
            <?php

            if($USER->lang == "es" || $_GET['lang'] == "es") {
                echo '"language": {"url": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"}';
            }
            ?>
        });
        var selected_users = [];

        function clear_users() {
            $('#user-names').find("li").remove();
            $(".selected").removeClass('selected');
        }

        $('.categoryFilter').click(function(){
            users.draw();
        });

        $('#users tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('selected');

            var academy     = $(this).find('td:eq(0)').text();
            var first_name  = $(this).find('td:eq(1)').text();
            var last_name   = $(this).find('td:eq(2)').text();
            var email       = $(this).find('td:eq(3)').text();
            var userid      = $(this).attr('id');

            if($("#user-names > #" + userid).length != 0) {
                //remove user from select user list if already there
                $("#user-names > #" + userid).remove();
            } else {
                //add user to selected users list
                $('#user-names').append("<li id='" + userid + "'>" + first_name + " " + last_name + " (" + email + ")</li>");
            }
        });

        //remove user name from selected users list
        $("#user-names").on("dblclick", "li", function() {
            //remove user from selected users list
            $(this).remove();
            //clear selected class on user table for selected user
            $("tr[id=" + $(this).attr('id') + "]").removeClass("selected");
        });

        //get selected users and call add user service to enroll them
        $('#enroll_users').on('click', function() {
            //clear array to avoid duplicates
            selected_users = [];
            var cid = <?php echo $_GET['courseid'];?>;
            var user_names = $('#user-names li');

            //get current selected users list
            $.each(user_names, function(i, val) {
                selected_users.push(val.id);
            });

            //console.log("selected users JSON: " + JSON.stringify(selected_users));

            $.ajax({
                url: "/blocks/cbsi/add_user.php",
                type: "POST",
                data: {users : selected_users, courseid : cid },
                dataType: "json",
                success: function(data) {
                    //clear selected users list
                    clear_users();
                    console.log(data);
                    if(data == "") {
                        alert("All users registered successfully.");
                    } else {
                        alert(data);
                    }
                },
                error: function(data) {
                    console.log(data);
                    alert("There was a problem. Please notify the site creator.");
                }
            });
        });

        $("#clear_users").click(function() {
            clear_users();
        });
    });
</script>

<?php
echo $OUTPUT->footer();
