<?php

require_once('../../config.php');
require_once('lib.php');
require_login();
global $DB;

//get course id
$courseid = $_POST['courseid'];

//get original course category
$sql = <<< SQL
    SELECT *
    FROM mdl_block_cbsi
    WHERE courseid = {$courseid}
SQL;

$record = $DB->get_record_sql($sql);

//get course object
$course   = get_course($courseid);

//update course category to original category (meeting/training)
$course->category = $record->categoryid;

//set response type to JSON for easy jQuery parsing
header('Content-Type: application/json');
//update course in database or return error
try {
    $DB->update_record('course', $course);
    //echo "sucessfully moved to original category";
} catch(Exception $e) {
    echo "Unable to remove course from Shared category.";
}
