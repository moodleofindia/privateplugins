 <?php
require_once('../../config.php');
global $DB, $USER, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('sidebar-manage-courses', 'theme_msu'));
$PAGE->set_heading(get_string('sidebar-manage-courses', 'theme_msu'));
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/portal.php');
$PAGE->navbar->add(get_string('sidebar-manage-courses', 'theme_msu'), new moodle_url($CFG->wwwroot . '/blocks/cbsi/portal_tabs.php'));

require_once("{$CFG->dirroot}/blocks/cbsi/lib.php");
echo $OUTPUT->header();
require_login();

$course_creator     = $USER->id;
$course_creator_sql = "SELECT * FROM {block_cbsi} WHERE course_creator = $course_creator";
$course_list        = $DB->get_records_sql($course_creator_sql);

if(is_user_siteadmin()) {
  $meetings           = get_courses_by_type_and_parent("Meeting");
  $trainings          = get_courses_by_type_and_parent("Training");
} else {
  $meetings           = get_courses_by_type("Meeting");
  $trainings          = get_courses_by_type("Training");
}

$scripts            = scripts();
$train              = '';
$meet               = '';
//$page_header        = get_string('page_header', 'block_cbsi');

function render_select($courses, $type) {
  // print_r($courses);
  // die();

  $html .= <<<HTML
    <div><select id="course-listing-{$type}" class="manage-content-select">
      <option>Select A Course</option>
HTML;

  foreach($courses as $c) {
    $html .= <<<HTML
      <option data-course-id="{$c->courseid}">{$c->fullname}</option>
HTML;
}

  $html .= <<<HTML
    </select></div>
HTML;

  return $html;
}

/**
 * [get_courses description]
 * @param  [type] $type [description]
 * @return [type]       [description]
 */
function get_courses_by_type($type) {
  global $DB,$USER;

  $sql = <<<SQL
  SELECT mdl_block_cbsi.courseid, mdl_block_cbsi.course_creator, mdl_course.id, mdl_course.fullname, mdl_course_categories.id, mdl_course_categories.name
  FROM mdl_block_cbsi
  JOIN mdl_course ON mdl_block_cbsi.courseid = mdl_course.id
  JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
  WHERE mdl_course_categories.name = "{$type}" and mdl_block_cbsi.course_creator = {$USER->id}
SQL;

  // print_r($sql);
  // die(__FILE__.":".__LINE__);
  $results = $DB->get_records_sql($sql);

  return $results;
}

function get_courses_by_type_and_parent($type) {
  global $DB,$USER;
  $parent_id = get_parent_id();

  $sql = <<<SQL
  SELECT mdl_block_cbsi.courseid, mdl_block_cbsi.course_creator, mdl_course.id, mdl_course.fullname, mdl_course_categories.id, mdl_course_categories.name
  FROM mdl_block_cbsi
  JOIN mdl_course ON mdl_block_cbsi.courseid = mdl_course.id
  JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
  WHERE mdl_course_categories.name = "{$type}" and mdl_course_categories.parent = {$parent_id}
SQL;

  // print_r($sql);
  // die(__FILE__.":".__LINE__);
  $results = $DB->get_records_sql($sql);

  return $results;
}

function get_editing($id) {
  global $USER;

  $edit_off_button            = get_string('edit_off', 'block_cbsi');
  $edit_off_desc              = get_string('edit_off_copy', 'block_cbsi');
  $edit_on_button             = get_string('edit_on', 'block_cbsi');
  $edit_on_desc               = get_string('edit_on_copy', 'block_cbsi');

  $on = <<<EOT
    <tr>
      <td><a href="/course/view.php?id={$id}&sesskey={$USER->sesskey}&edit=on"><button>{$edit_on_button}</button></a></td>
      <td>{$edit_on_desc}</td>
      <td></td>
    </tr>
EOT;

  $off = <<<EOT
    <tr>
      <td><a href="/course/view.php?id={$id}&sesskey={$USER->sesskey}&edit=off"><button>{$edit_off_button}</button></a></td>
      <td>{$edit_off_desc}</td>
      <td></td>
    </tr>

EOT;

  if($USER->editing == 1) {
    return $off;
  } else {
    return $on;
  }
}



/**
 * Create table rows for each button and render html
 *
 * @param  int    $id
 * @param  string $name
 * @return string html
 */
function panel($id, $name, $list) {
  global $USER;

  $view_course_button         = get_string('view_course', 'block_cbsi');
  $view_course_desc           = get_string('view_course_copy', 'block_cbsi');
  $add_user_button            = get_string('add_user', 'block_cbsi');
  $add_user_desc              = get_string('add_user_copy', 'block_cbsi');
  $remove_user_button         = get_string('remove_user', 'block_cbsi');
  $remove_user_desc           = get_string('remove_user_copy', 'block_cbsi');
  $edit_course_button         = get_string('edit_course', 'block_cbsi');
  $edit_course_desc           = get_string('edit_course_copy', 'block_cbsi');
  $enrollment_methods_button  = get_string('enrollment_methods', 'block_cbsi');
  $enrollment_methods_desc    = get_string('enrollment_methods_copy', 'block_cbsi');
  $change_shortname_button    = get_string('change_shortname', 'block_cbsi');
  $change_shortname_desc      = get_string('change_shortname_copy', 'block_cbsi');
  $completion_criteria_button = get_string('completion_criteria', 'block_cbsi');
  $completion_criteria_desc   = get_string('completion_criteria_copy', 'block_cbsi');
  $create_badge_button        = get_string('create_badge', 'block_cbsi');
  $create_badge_desc          = get_string('create_badge_copy', 'block_cbsi');
  $delete_course_button       = get_string('delete_course', 'block_cbsi');
  $delete_course_desc         = get_string('delete_course_copy', 'block_cbsi');
  $add_instructor             = get_string('add-instructor', 'theme_msu');
  $remove_instructor          = get_string('remove-instructor', 'theme_msu');
  $backup_course_button       = get_string('backup_course', 'block_cbsi');
  $backup_course_desc         = get_string('backup_course_copy', 'block_cbsi');
  $restore_course_button      = get_string('restore_course', 'block_cbsi');
  $restore_course_desc        = get_string('restore_course_copy', 'block_cbsi');
  $set_shared_button          = get_string('set_shared', 'block_cbsi');
  $set_shared_desc            = get_string('set_shared_copy', 'block_cbsi');
  $unset_shared_button        = get_string('unset_shared', 'block_cbsi');
  $unset_shared_desc          = get_string('unset_shared_copy', 'block_cbsi');
  $editing                    = get_editing($id);


  $html = <<<EOT
    {$select}
    <!-- hidden, don't need anymore since using select box
    <a href="#course-id-{$id}" data-toggle="collapse" class="collapse">
      <strong>{$name}</strong>
    </a><br>
    -->
    <div class="collapse" id="course-id-{$id}">
      <div class=""><!-- begin course left -->
        <table class="portal-table">
          <!-- buttons -->
          {$editing}
          <tr>
            <td><a href="/course/view.php?id={$id}"><button>{$view_course_button}</button></a></td>
            <td>{$view_course_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/blocks/cbsi/users.php?courseid={$id}"><button>{$add_user_button}</button></a></td>
            <td>{$add_user_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/blocks/cbsi/unenroll_users.php?courseid={$id}"><button>{$remove_user_button}</button></a></td>
            <td>{$remove_user_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/course/edit.php?id={$id}"><button>{$edit_course_button}</button></a></td>
            <td>{$edit_course_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/enrol/instances.php?id={$id}"><button>{$enrollment_methods_button}</button></a></td>
            <td>{$enrollment_methods_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/course/edit.php?id={$id}"><button>{$change_shortname_button}</button></a></td>
            <td>{$change_shortname_desc}</td>
            <td><!--<input type="checkbox" id="shortname" value="shortname" name="shortname" '  . $shortname       . ' disabled><label for="shortname"><span></span></label>--></td>
          </tr>
          <tr>
            <td><a href="/course/completion.php?id={$id}"><button>{$completion_criteria_button}</button></a></td>
            <td>{$completion_criteria_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td>
              <a href="/badges/index.php?type=2&id={$id}">
                <button>{$create_badge_button}</button>
              </a>
            </td>
            <td>{$create_badge_desc}</td>
            <td><!-- <input type="checkbox" name="badge" id="badge" '      . $badge           . ' disabled>
              <label for="badge"><span></span></label>--></td>
          </tr>
          <tr>
            <td><a href=""><button id="delete-course-button" onclick=delete_course({$id})>{$delete_course_button}</button></a></td>
            <td>{$delete_course_desc}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/blocks/cbsi/instructor.php?courseid={$id}"><button>{$add_instructor}</button></a></td>
            <td>{$add_instructor}</td>
            <td></td>
          </tr>
          <tr>
            <td><a href="/blocks/cbsi/remove_instructor.php?courseid={$id}"><button>{$remove_instructor}</button></a></td>
            <td>{$remove_instructor}</td>
            <td></td>
          </tr>
EOT;

  if(has_siteadmin_role($id)) {
    $html  .= <<<EOT
      <!-- site admin section -->
      <tr>
        <td><a href="/backup/backup.php?id={$id}"><button>{$backup_course_button}</button></a></td>
        <td>{$backup_course_desc}</td>
        <td></td>
      </tr>

      <tr>
        <td><a href="#"><button>{$restore_course_button}</button></a></td>
        <td>{$restore_course_desc}</td>
        <td></td>
      </tr>

      <tr>
        <td><a href=""><button class="set-shared" id="{$id}">{$set_shared_button}</button></a></td>
        <td>{$set_shared_desc}</td>
        <td></td>
      </tr>

      <tr>
        <td><a href=""><button class="unset-shared" id="{$id}">{$unset_shared_button}</button></a></td>
        <td>{$unset_shared_desc}</td>
        <td></td>
      </tr>
      <!-- end site-admin -->
EOT;
        }

  $html .= <<<EOT
            </tbody>
          </table>
        </div><!-- end course-left -->
      </div><!-- end collapse -->
EOT;

  return $html;
}

$train .= render_select($trainings, "trainings");
foreach($trainings as $t) {
  $train .= panel($t->courseid, $t->fullname);
}

$meet .= render_select($meetings, "meetings");
foreach($meetings as $t) {
  $meet .= panel($t->courseid, $t->fullname);
}

$html = <<<EOT
  <style>
    .panel-body { padding: 0 0 !important;}

    td, th {
      padding: .5em !important;
    }

    .manage-content-select {
      margin: 10px 10px;
    }
  </style>

  <script>
    $(function() {
      $( "#tabs, #tabs1, #tabs2, #tabs3, #tabs4, #tabs5" ).tabs();
    });

    function delete_course(id) {
      //var id = $("#delete-course-button").data("course-id");
      //console.log("You are trying to delete course: " + id);

      $.ajax({
        url: "/blocks/cbsi/ajax.php",
        type: "POST",
        data: {"delete" : id},
        dataType: "json",
        success: function(data) {
            console.log("success data:");
            console.log(data);
            alert("Course deleted successfully.");
            window.location.replace("/?name=portal");
        },
        error: function(data) {
          console.log("error data:");
          console.log(data);
            alert("Course not deleted successfully.");

        }
      });
    }
  </script>


  <div class="course-creator">
    <h3>{$page_header}</h3>
    <div id="course-creator">
      <div class="line-right ax_horizontal_line"></div>
      <div id="tabs">
        <!-- <button class="showMore">Show More</button> -->
        <ul>
           <li><a href="#tabs-1">Meetings</a></li>
           <li><a href="#tabs-2">Trainings</a></li>
           <li style="display:none"><a href="#tabs-3"></a></li>
        </ul>

        <div id="tabs-1" class="extra">
          {$meet}
        </div>

        <div id="tabs-2" class="extra">
           {$train}
        </div>
      </div><!-- #tabs -->
    </div><!-- #course-creator -->
  </div><!-- .course-creator -->

  {$scripts}

  <script>
    $(document).ready(function () {
      $('#course-listing-trainings').change(function() {
        $('.in').removeClass('in');

        var id = $("#course-listing-trainings option:selected").data("course-id");
        $('#course-id-' + id).addClass("in");
      });

      $('#course-listing-meetings').change(function() {
        $('.in').removeClass('in');

        var id = $("#course-listing-meetings option:selected").data("course-id");
        $('#course-id-' + id).addClass("in");
      });

      $(".set-shared").on("click", function() {
        //console.log( $(this).attr("id") );
        var courseid = $(this).attr("id");

        $.ajax({
          url: "/blocks/cbsi/shared_category.php",
          type: "POST",
          data: {"courseid" : courseid},
          dataType: "json",
          success: function(data) {
              alert("Course moved to shared category.");
          },
          error: function(data) {
              alert("Course moved to shared category.");
          }
        });
      });

      $(".unset-shared").on("click", function() {
        //console.log( $(this).attr("id") );
        var courseid = $(this).attr("id");

        $.ajax({
          url: "/blocks/cbsi/original_category.php",
          type: "POST",
          data: {"courseid" : courseid},
          dataType: "json",
          success: function(data) {
              alert("Course removed from shared category.");
          },
          error: function(data) {
              alert("Course removed from shared category.");
          }
        });
      });

      $("#delete-button").on("click", function() {
        var id = $("#delete-button").data("course-id");
        console.log("You are trying to delete course: " + id);

        $.ajax({
          url: "/blocks/cbsi/ajax.php",
          type: "POST",
          data: {"delete" : id},
          dataType: "json",
          success: function(data) {
              console.log("success data:");
              console.log(data);
              alert("Course deleted successfully.");
          },
          error: function(data) {
            console.log("error data:");
            console.log(data);
              alert("Course deleted successfully.");
              window.location.replace("/?name=portal");
          }
        });
      });
    });
  </script>
EOT;

echo $html;
echo $OUTPUT->footer();
