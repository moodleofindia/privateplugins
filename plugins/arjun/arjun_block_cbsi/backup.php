<?php
/* Shamelessly copied from https://moodle.org/mod/forum/discuss.php?d=229842 */
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/controller/backup_controller.class.php');

require_login();

global $USER, $DB;

$courseid = $_POST['courseid'];
$userid   = $USER->id;

try {
    $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
} catch(Exception $e) {
    header('Content-Type: application/json');
    echo json_encode($e->getMessage());
}

function cbsi_backup_course($course) {
    global $CFG;

    $cohort   = get_cohort_idnumber();
    $bc       = new backup_controller(
        backup::TYPE_1COURSE,
        $course->id,
        backup::FORMAT_MOODLE,
        backup::INTERACTIVE_NO,
        backup::MODE_AUTOMATED,
        2
    );

    $outcome   = $bc->execute_plan();
    $results   = $bc->get_results();
    $file      = $results['backup_destination'];
    $cbsi_file = "backup--{$course->id}--{$course->shortname}--" . date('Ymd') . ".zip";
    $cbsi_path = "/backups/" . $cohort . "/";

    //check if path to academy folder exists.
    //if not, create it
    if(!file_exists("$CFG->dataroot/backups/$cohort/")) {
        mkdir("$CFG->dataroot/backups/$cohort/", 0777);
    }

    //move file to backups/<academy> and destroy
    $file->copy_content_to($CFG->dataroot . $cbsi_path . $cbsi_file);
    $bc->destroy();
    unset($bc);

    return true;
}

//TODO: require capability to backup this course
//set response type to JSON for easy jQuery parsing
header('Content-Type: application/json');
try{
    cbsi_backup_course($course);
    echo json_encode("success");
} catch(Exception $e) {
    echo json_encode($e->getMessage());
}
