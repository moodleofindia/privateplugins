<?php

require_once('../../config.php');
global $DB,$CFG,$USER,$PAGE;
require_once($CFG->dirroot.'/lib/moodlelib.php');

$data = $_POST;
require_login();

$error   = null;
$success = null;

function delete($id) {
    try {
        $success = delete_course($id,false);
    } catch(Exception $e) {
        $error = $e->getMessage();
    }
}

switch ($data) {
    case isset($data['delete']):
        $id = $data['delete'];
        delete($id);
        break;
    default:
        echo json_encode(array('error' => $error));
        break;
}

//return course id for processing on return page
//set response type to JSON for easy jQuery parsing
header('Content-Type: application/json');

if($error !== null) {
    echo json_encode(array('error' => $error));
} else {
    echo json_encode(array('success' => $success));
}
purge_all_caches();
