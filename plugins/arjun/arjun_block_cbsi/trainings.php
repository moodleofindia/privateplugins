<?php
require_once('../../config.php');
require_once('lib.php');
global $DB, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title("Academy Trainings");
$PAGE->set_heading("Academy Trainings");
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/trainings.php');
$PAGE->navbar->add("Academy Trainings", new moodle_url($CFG->wwwroot . '/blocks/cbsi/trainings.php'));

echo $OUTPUT->header();
require_login();

$trainings = get_courses(get_training_id());

$html = '

      <div>
        <h2>' . get_string('tab-trainings', 'theme_msu') . '</h2>
        <p>' . get_string('training-header', 'theme_msu') . '</p>
        <div class="line-right ax_horizontal_line"></div>';
/*
        <div id="tabs">
          <ul>
             <li><a href="#tabs-1">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
             <li><a href="#tabs-2">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
             <li><a href="#tabs-3">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
          </ul>';
*/
$html .= '
      <div id="tabs-1" class="extra">
        <ul>';

foreach($trainings as $m)
{
  if ($m->visible == 1)
  {
    $a         = get_nstudents($m->id);
    $s         = get_nseats($m->id);
    $so        = $s - $a;
    $image     = get_course_image_url($m->id);
    $startdate = gmdate('m.d.y', $m->startdate);
    $starttime = gmdate('H:i', $m->startdate);

    $html .= <<<HTML
            <li>
              <div class="tabInfo" class="first-img" />
                <img src="{$image}" class="first-img" />
                <div class="tabContent">
                  <a href="/course/view.php?id={$m->id}"><h2>{$m->fullname}</h2></a><span class="course-date time">{$startdate} at {$starttime}hrs</span>
                </div><!--end tabContent --><br />
                <span class="attendees"> Attendees: {$a} | Seats Open: {$so} of {$s}</span>
              </div><!--end tabInfo -->
            </li>
HTML;
  }
}

$html .= <<<HTML
      </div><!-- end tabs -->
    <div>
HTML;

$html .= '<script>
  $(document).ready(function() {

  var currentPage =  $("form").attr("id");

    var newHeight = $("div.left-column").outerHeight() + 40;
    var unit = "px";
    var cssHeight = newHeight + unit;
    $("div.right-column, iframe").css("min-height", cssHeight);


   if($( "iframe" ).contents().find( "pagelayout-frontpage")) {


  }else {

    alert("didnt work");
  }

  $( "#tabs").tabs();
  $("#tabs1").tabs({active: 10});
  $("#tabs2").tabs({active: 10});

  $("ul").each(function() {

  if($(this).html() === "") {
  $(this).parent().parent().addClass("nothingThere");
  $(".nothingThere").tabs({active: 2});
  //$(".nothingThere ul li").find(".ui-tabs-active").removeClass("ui-tabs-active");
  //$(".nothingThere ul li").find(".ui-state-active").removeClass("ui-state-active");
  //$(".nothingThere ul li:last-child").addClass("ui-tabs-active").addClass("ui-state-active");

  }





  });




     $(".extra ul li").click(function() {

  if ($(this).hasClass("activeCol")) {

      $(".activeCol").find(".gear").show();
      $(".activeCol").find(".arrow").show();
      $(".activeCol").find(".buttons").hide();
      $(".activeCol").find(".attendees").hide();
      $(".activeCol").removeClass("activeCol")

  }else {
      $(".activeCol").find(".gear").show();
      $(".activeCol").find(".arrow").show();
      $(".activeCol").find(".buttons").hide();
      $(".activeCol").find(".attendees").hide();
      $(".activeCol").removeClass("activeCol")

      $(this).addClass("activeCol");
      $(".activeCol").find(".gear").hide();
      $(".activeCol").find(".arrow").hide();
      $(".activeCol").find(".buttons").fadeIn(700);
       $(".activeCol").find(".attendees").fadeIn(700).css("display", "block");
   }
      });



  $(".span").hide();
  $("span").on("click",  function(event) {

   if($(this).hasClass("current")){

   }else {


      var newCurrent =  $(this).attr("id")


    if (newCurrent === "english") {
    $(".labelUsernameEnglish").html("<label for="username">Username</label>");
    $(".labelPasswordEnglish").html("<label for="password">Password</label>")
   $(".forgetpass").html("<a href="forgot_password.php">Forgotten your username or password?</a>")
    $(".spanishLan").removeClass("spanishLan");
     $(".theLogin").html(" <input type="submit" id="loginbtn" value="Login &raquo;" />");

   $(".eng").show();
   $(".span").hide();
     $(".spanish").hide();
    $(".english").show();


    }else {

     $(".labelUsernameEnglish").html("<label for="username">Usuario</label>");
       $(".labelPasswordEnglish").html("<label for="password">ContraseÃ±a</label>");
       $(".forgetpass").html("<a href="forgot_password.php">Â¿OlvidÃ³ su usuario o contraseÃ±a?</a>");
       $(".languages").addClass("spanishLan");
       $(".theLogin").html(" <input type="submit" id="loginbtn" value="Entrar &raquo;" />");
       $(".span").show();
        $(".eng").hide();
    $(".spanish").show();
    $(".english").hide();


       }
     }
  });

  $(".showMore").click(function() {
  $(this).closest("div").find(".extra ul").children().removeClass("hidden");
  });




  //if ($(".ui-tabs .ui-tabs-nav li:last-child").hasClass("ui-state-active")) {
  //$(".showMore").hide();

  //} else {

  //}

  });
</script>';

echo $html;
