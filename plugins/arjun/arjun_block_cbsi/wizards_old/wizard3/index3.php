<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="jquery.timepicker.css">
        <link rel="stylesheet" type="text/css" href="/js/jquery-ui/jquery-ui.css">
                <!-- jQuery -->
        <script src="/js/jquery/jquery-1.11.1.js" type="text/javascript"></script>
        <script src="/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="jquery.timepicker.js" type="text/javascript"></script>
        <style>
        fieldset {
            border: none;
        }
        .ui-widget-header {
            background: #64ABB7 !important;
            color: #fff !important;
        }

        .ui-state-default, .ui-widget-content .ui-state-default {
            background: #64ABB7 !important;
            color: #fff !important;
        }
                    /* Fonts */

            @font-face {
                font-family: 'proxima_nova_rgbold';
                src: url('font/proximanova-bold.eot');
                src: url('font/proximanova-bold.eot?#iefix') format('embedded-opentype'),
                     url('font/proximanova-bold.woff') format('woff'),
                     url('font/proximanova-bold.ttf') format('truetype'),
                     url('font/proximanova-bold.svg#proxima_nova_rgbold') format('svg');
                font-weight: normal;
                font-style: normal;
            }

            @font-face {
                font-family: 'proxima_nova_rgregular';
                src: url('font/proximanova-regular.eot');
                src: url('font/proximanova-regular.eot?#iefix') format('embedded-opentype'),
                     url('font/proximanova-regular.woff') format('woff'),
                     url('font/proximanova-regular.ttf') format('truetype'),
                     url('font/proximanova-regular.svg#proxima_nova_rgregular') format('svg');
                font-weight: normal;
                font-style: normal;
            }

            @font-face {
                font-family: 'proxima_nova_ltlight';
                src: url('font/proximanova-light.eot');
                src: url('font/proximanova-light.eot?#iefix') format('embedded-opentype'),
                     url('font/proximanova-light.woff') format('woff'),
                     url('font/proximanova-light.ttf') format('truetype'),
                     url('font/proximanova-light.svg#proxima_nova_ltlight') format('svg');
                font-weight: normal;
                font-style: normal;
            }

            body {
                background: url(http://cbsi-connect.net/theme/msu/pix/dust.png);
                font-family: 'proxima_nova_rgregular';
            }

            #container {
                width: 750px;
                margin: 0 auto;
                /* outline: 1px solid red; */
                padding-top: 0px;
            }

             h1 {
                color: #4f617a;
                margin-top: 20px;
            }

            h2 {
                /*text-align: center;*/
                color: #4f617a;
                font-size: 28px;
                margin: 25px auto 0px auto;
            }

            .navWrap {
                margin-left: auto;
                margin-right: auto;
                width: 750px;
                background: url(http://cbsi-connect.net/theme/msu/pix/stripebg.png);
                border-bottom: 1px dotted #64ABB7 !important;
                padding: 20px 50px 20px 50px;
            }

            .navWrap h1 {
                color: #4f617a;
                margin-top: 20px;
            }

            .wizardButton {
                background: #64ABB7;
                color: #fff;
                border: none;
                border-radius: 5px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                width: 85px;
                height: 30px;
                float: left;
                font-size: 14px;
                visibility: hidden;
            }

            .wizardSelect {
                width: 242px;
                height: 45px;
                border: 2px solid #4f617a;
                -webkit-border-radius: 5px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                padding-left: 10px;
                margin-bottom: 15px;
            }

        </style>


    </head>
    <body>
        <div class="navWrap">
            <h1>CBSI-Connect Course Creation Wizard</h1>
            <!--<input class="wizardButton" id="previous_page" onclick="" value="Previous" type="button">-->
            <!-- progressbar -->
            <ul id="progressbar">
                <li class="active"></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>

            <input class="wizardButton" id="next_page" onclick="" value="Next" type="button">
        </div>
            <!-- multistep form -->
            <form id="msform">
                <!-- fieldsets -->
                <fieldset>
                    <h2>What do you want to do today?</h2>
                    <!-- <h3 class="fs-subtitle">This is step 1</h3> -->
                    <input type="button" name="next" class="next action-button next-position" value="Create a new meeting or training" />
                </fieldset>
                <fieldset>
                    <h2>What type of class do you want to create?</h2>
                    <!-- <h3 class="fs-subtitle">This is step 2</h3> -->
                    <input id="meeting" type="button" name="meeting" class="action-button" value="Meeting" />
                    <input id="training" type="button" name="training" class="action-button" value="Training" />
                    <br/>
                    <input type="button" name="previous" class="previous action-button previous-position" value="Previous" />
                    <input type="button" name="next" class="next action-button next-position" value="Next" />
                </fieldset>
                <fieldset>
                    <!-- <h2 class="fs-title">Course Details</h2> -->
                    <!-- <h3 class="fs-subtitle">This is step 3</h3> -->
                    <h2>What is the title?</h2>
                    <input id="title" type="text" name="title" /><br/>
                    <br/>
                    <h2>What language will this be available in?</h2>
                    <select id="language" class="wizardSelect">
                        <option value="English">Select</option>
                        <option value="English">English</option>
                        <option value="Spanish">Spanish</option>
                    </select><br/>
                    <h2>What is the description?</h2>
                    <textarea id ="description" name="course_description"></textarea><br/>
                    <input type="button" name="previous" class="previous action-button previous-position" value="Previous" />
                    <input type="button" name="next" class="next action-button next-position" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Set the start date of the training or meeting?</h2>
                    <!-- <h3 class="fs-subtitle">This is step 4</h3> -->
                    <input id="set_date_yes" type="button" value="Yes"/>
                    <input id="set_date_no"  type="button" value="No"/>
                    <br/>
                    <div class="formWrap" id="class_begin_date" style="display: none;">

                        <b>When do you want this training or meeting to begin?</b>
                            <input id="datepicker" type="text"><img class="hasDatepicker">
                        <img src="http://cbsi-connect.net/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock">

                            <b> Choose a time:</b>
                                <input id="class_begin_timetxt" name="class_begin_timetxt" type="text"  class="ui-timepicker-input" autocomplete="off">
                                <br/>
                    </div>
                    <input type="button" name="previous" class="previous action-button previous-position" value="Previous" />
                    <input type="button" name="next" class="next action-button next-position" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Finish</h2>
                   <!--  <h3 class="fs-subtitle">This is step 5</h3> -->
                    <input type="button" name="previous" class="previous action-button previous-position" value="Previous" />
                    <br/>
                    <input type="button" id="save_home" name="save_home" value="Save and go back to home page">
                    <input type="button" id="delete_home" name="delete_home" value="Delete and go back to home page">
                    <input type="button" id="save_setup" name="save_setup" value="Save and Start Setup">
                </fieldset>
            </form>

        <script>
            $(document).ready(function() {
                // $('[name="next"]').addClass("next-position");
                // $('[name="previous"]').addClass("previous-position");

                $('#class_begin_timetxt').timepicker({ 'timeFormat': 'H:i:s' });

                // Date Functionality
                $( "#datepicker" ).datepicker({
                    showOn: "both",
                    buttonImage: "http://cbsi-connect.net/theme/msu/pix/bluecalendar.png",
                    buttonImageOnly: true,
                    dateFormat: "yy-mm-dd",
                    minDate: 0
                  });
                            var course      = {};
                var type        = null;
                var title       = null;
                var language    = null;
                var description = null;
                var startdate   = null;
                var starttime   = null;

                function get_type() {
                    return type;
                }

                function get_title() {
                    return $('#title').val();
                }

                function get_language() {
                    return $('#language').val();
                }

                function get_description() {
                    return $('#description').val();
                }

                function get_startdate() {
                    return $('class_begin_datetxt').val();
                }

                function get_starttime() {
                    return $('class_begin_timetxt').val();
                }

                function create_course() {
                    course.title       = get_title();
                    course.description = get_description();
                    course.type        = get_type();
                    course.language    = get_language();
                    console.log(course);

                    //send course via ajax for actual creation
                }

                $('#set_date_yes').click(function() {
                    $('#class_begin_date').toggle();
                })

                $('#set_date_no').click(function() {
                    $('#datepicker').val('');
                    $('#class_begin_timetxt').val('');
                    $('#class_begin_date').hide();
                });

                $('meeting').click(function() {
                    type = $("#meeting").val();
                });

                $('#training').click(function() {
                    type = $("#training").val();
                });

                $('#save_home').click(function() {
                    create_course();
                });


                //TODO: get selected language
                //TODO: get date from datepicker
                //TODO: get start time from date picker



                //jQuery time
                var current_fs, next_fs, previous_fs; //fieldsets
                var left, opacity, scale; //fieldset properties which we will animate
                var animating; //flag to prevent quick multi-click glitches

                $(".next").click(function(){
                    if(animating) return false;
                    animating = false;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function(now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50)+"%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({'transform': 'scale('+scale+')'});
                            next_fs.css({'left': left, 'opacity': opacity});
                        },
                        duration: 800,
                        complete: function(){
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                });

                $(".previous").click(function(){
                    if(animating) return false;
                    animating = false;

                    current_fs = $(this).parent();
                    previous_fs = $(this).parent().prev();

                    //de-activate current step on progressbar
                    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                    //show the previous fieldset
                    previous_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function(now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale previous_fs from 80% to 100%
                            scale = 0.8 + (1 - now) * 0.2;
                            //2. take current_fs to the right(50%) - from 0%
                            left = ((1-now) * 50)+"%";
                            //3. increase opacity of previous_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({'left': left});
                            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                        },
                        duration: 800,
                        complete: function(){
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                });

                $(".submit").click(function(){
                    return false;
                })
            });
        </script>
    </body>
</html>

