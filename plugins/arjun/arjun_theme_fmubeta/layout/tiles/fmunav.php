<div class="ct-topBar text-center">
    <div class=" ">
        <ul class="ct-panel--user list-inline text-uppercase pull-left">
		
		<?php
		global $CFG;
		if (isloggedin()) {
        $sessionkey = sesskey();
			?>
			<li><a href="<?php echo $CFG->wwwroot.'/login/logout.php?sesskey='.$sessionkey ?>" class="ct-js-login">logout<i class="fa fa-lock"></i></a></li>
		<?php	
		} else {
		?>
		<li><a href="<?php echo $CFG->wwwroot.'/login/index.php'?>" class="ct-js-login">login<i class="fa fa-lock"></i></a></li>
            <li><a href="<?php echo $CFG->wwwroot.'/login/signup.php'?>" class="ct-js-signup">sign up<i class="fa fa-user"></i></a></li>
		<?php
			
		}
		?>
            
        </ul>
        <div class="ct-widget--group pull-right">
		<!--
                    <ul class="ct-widget--socials list-inline text-uppercase">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-wordpress"></i></a></li>
                    </ul>
                   
                    <div class="btn-group">
                        <button type="button" class="btn btn-md dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <?php //echo '<img src="' . $CFG->wwwroot . '/pix/flags/png/england.png" / alt="UK">';?>English <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#"><?php // echo '<img src="' . $CFG->wwwroot . '/pix/flags/png/england.png" / alt="UK">';?>English</a></li>
                             <li><a href="#"><?php //echo '<img src="' . $CFG->wwwroot . '/pix/flags/png/de.png" / alt="UK">';?>German</a></li>
                              <li><a href="#"><?php //echo '<img src="' . $CFG->wwwroot . '/pix/flags/png/pl.png" / alt="UK">';?>Polish</a></li>
                           
                        </ul>
                    </div>
					-->
                </div>
        <div class="clearfix"></div>
    </div>
</div>
