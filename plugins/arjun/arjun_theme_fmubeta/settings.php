<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * fmubeta theme with the underlying Bootstrap theme.
 *
 * @package    theme
 * @subpackage fmubeta
 * @copyright  &copy; 2014-onwards G J Barnard in respect to modifications of the Bootstrap theme.
 * @author     G J Barnard - gjbarnard at gmail dot com and {@link http://moodle.org/user/profile.php?id=442195}
 * @author     Based on code originally written by Bas Brands, David Scotson and many other contributors.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

// Settings.
    $settings = null;

    $readme = new moodle_url('/theme/fmubeta/Readme.md');
    $readme = html_writer::link($readme, 'Readme.md', array('target' => '_blank'));

    $ADMIN->add('themes', new admin_category('theme_fmubeta', 'fmubeta'));

    $generalsettings = new admin_settingpage('theme_fmubeta_general', get_string('generalsettings', 'theme_fmubeta'));

    /* CDN Fonts - 1 = no, 2 = yes. */
    $name = 'theme_fmubeta/cdnfonts';
    $title = get_string('cdnfonts', 'theme_fmubeta');
    $description = get_string('cdnfonts_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Use FontAwesome font.
    $name = 'theme_fmubeta/fontawesome';
    $title = get_string('fontawesome', 'theme_fmubeta');
    $description = get_string('fontawesome_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Use Docking - 1 = no, 2 = yes.
    $name = 'theme_fmubeta/docking';
    $title = get_string('docking', 'theme_fmubeta');
    $description = get_string('docking_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Use Accordion side blocks - 1 = no, 2 = yes.
    $name = 'theme_fmubeta/accordion';
    $title = get_string('accordion', 'theme_fmubeta');
    $description = get_string('accordion_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Compact Navbar.
    $name = 'theme_fmubeta/compactnavbar';
    $title = get_string('compactnavbar', 'theme_fmubeta');
    $description = get_string('compactnavbar_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Navbar fixed at the top of the page.
    $name = 'theme_fmubeta/navbarfixedtop';
    $title = get_string('navbarfixedtop', 'theme_fmubeta');
    $description = get_string('navbarfixedtop_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Invert Navbar to dark background.
    $name = 'theme_fmubeta/inversenavbar';
    $title = get_string('inversenavbar', 'theme_fmubeta');
    $description = get_string('inversenavbar_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Show old messages.
    $name = 'theme_fmubeta/showoldmessages';
    $title = get_string('showoldmessages', 'theme_fmubeta');
    $description = get_string('showoldmessagesdesc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $generalsettings->add($setting);

    // Display My Courses Menu.
    $name = 'theme_fmubeta/displaymycoursesmenu';
    $title = get_string('displaymycoursesmenu', 'theme_fmubeta');
    $description = get_string('displaymycoursesmenu_desc', 'theme_fmubeta');
    $choices = array(
        0 => new lang_string('no'),
        1 => new lang_string('myclasses', 'theme_fmubeta'),
        2 => new lang_string('mycourses', 'theme_fmubeta'),
        3 => new lang_string('mymodules', 'theme_fmubeta'),
        4 => new lang_string('mysubjects', 'theme_fmubeta'),
        5 => new lang_string('myunits', 'theme_fmubeta')
    );
    $default = 2;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $generalsettings->add($setting);

    // Display my courses on the my home page - 1 = no, 2 = yes.
    $name = 'theme_fmubeta/displaymycourses';
    $title = get_string('displaymycourses', 'theme_fmubeta');
    $description = get_string('displaymycourses_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $generalsettings->add($setting);

    // Use course tiles for activities and resources - 1 = no, 2 = yes.
    $name = 'theme_fmubeta/coursetiles';
    $title = get_string('coursetiles', 'theme_fmubeta');
    $description = get_string('coursetiles_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $generalsettings->add($setting);

    // Activate syntax highlighting - 1 = no, 2 = yes.
    $name = 'theme_fmubeta/syntaxhighlight';
    $title = get_string('syntaxhighlight', 'theme_fmubeta');
    $description = get_string('syntaxhighlight_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $generalsettings->add($setting);

    // Logo file setting.
    $name = 'theme_fmubeta/logo';
    $title = get_string('logo', 'theme_fmubeta');
    $description = get_string('logo_desc', 'theme_fmubeta');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Number of page bottom blocks.
    $name = 'theme_fmubeta/numpagebottomblocks';
    $title = get_string('numpagebottomblocks', 'theme_fmubeta');
    $description = get_string('numpagebottomblocks_desc', 'theme_fmubeta');
    $choices = array(
        1 => new lang_string('one', 'theme_fmubeta'),
        2 => new lang_string('two', 'theme_fmubeta'),
        3 => new lang_string('three', 'theme_fmubeta'),
        4 => new lang_string('four', 'theme_fmubeta')
    );
    $default = 2;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $generalsettings->add($setting);

    // Footer menu.
    $name = 'theme_fmubeta/footermenu';
    $title = get_string('footermenu', 'theme_fmubeta');
    $description = get_string('footermenu_desc', 'theme_fmubeta');
    $default = 'About fmubeta|[[site]]/theme/fmubeta/pages/about.php|About fmubeta';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Fitvids.
    $name = 'theme_fmubeta/fitvids';
    $title = get_string('fitvids', 'theme_fmubeta');
    $description = get_string('fitvidsdesc', 'theme_fmubeta');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Copyright text.
    $name = 'theme_fmubeta/copyright';
    $title = get_string('copyright', 'theme_fmubeta');
    $description = get_string('copyright_desc', 'theme_fmubeta');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // User load chart.
    $name = 'theme_fmubeta/userload';
    $title = get_string('userload', 'theme_fmubeta');
    $description = get_string('userloaddesc', 'theme_fmubeta');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Custom CSS file.
    $name = 'theme_fmubeta/customcss';
    $title = get_string('customcss', 'theme_fmubeta');
    $description = get_string('customcss_desc', 'theme_fmubeta');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    // Login message.
    $generalsettings->add(new admin_setting_heading('theme_fmubeta_loginmessage', get_string('loginpage', 'theme_fmubeta'),
            format_text(get_string('loginpage_desc', 'theme_fmubeta'), FORMAT_MARKDOWN)));

    $name = 'theme_fmubeta/showloginmessage';
    $title = get_string('showloginmessage', 'theme_fmubeta');
    $description = get_string('showloginmessage_desc', 'theme_fmubeta').html_writer::tag('a',
        get_string('showloginmessage_urlname', 'theme_fmubeta'), array('href' => get_string('showloginmessage_urllink', 'theme_fmubeta'),
        'target' => '_blank'))."'.";
    $default = 1;
    $choices = array(
        1 => new lang_string('no'),   // No.
        2 => new lang_string('yes')   // Yes.
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    $name = 'theme_fmubeta/loginmessage';
    $title = get_string('loginmessage', 'theme_fmubeta');
    $description = get_string('loginmessage_desc', 'theme_fmubeta');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $generalsettings->add($setting);

    $ADMIN->add('theme_fmubeta', $generalsettings);

    // Font....
    $fontsettings = new admin_settingpage('theme_fmubeta_font', get_string('fontsettings', 'theme_fmubeta'));
    // This is the descriptor for the font settings
    $name = 'theme_fmubeta/fontheading';
    $heading = get_string('fontheadingsub', 'theme_fmubeta');
    $information = get_string('fontheadingdesc', 'theme_fmubeta');
    $setting = new admin_setting_heading($name, $heading, $information);
    $fontsettings->add($setting);

    // Heading font name
    $name = 'theme_fmubeta/fontnameheading';
    $title = get_string('fontnameheading', 'theme_fmubeta');
    $description = get_string('fontnameheadingdesc', 'theme_fmubeta');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // Text font name
    $name = 'theme_fmubeta/fontnamebody';
    $title = get_string('fontnamebody', 'theme_fmubeta');
    $description = get_string('fontnamebodydesc', 'theme_fmubeta');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // Heading.
    // TTF Font.
    $name = 'theme_fmubeta/fontfilettfheading';
    $title = get_string('fontfilettfheading', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilettfheading');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // OTF Font.
    $name = 'theme_fmubeta/fontfileotfheading';
    $title = get_string('fontfileotfheading', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileotfheading');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // WOFF Font.
    $name = 'theme_fmubeta/fontfilewoffheading';
    $title = get_string('fontfilewoffheading', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewoffheading');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // WOFF2 Font.
    $name = 'theme_fmubeta/fontfilewofftwoheading';
    $title = get_string('fontfilewofftwoheading', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewofftwoheading');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // EOT Font.
    $name = 'theme_fmubeta/fontfileeotheading';
    $title = get_string('fontfileeotheading', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileweotheading');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // SVG Font.
    $name = 'theme_fmubeta/fontfilesvgheading';
    $title = get_string('fontfilesvgheading', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilesvgheading');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // Body.
    // TTF Font.
    $name = 'theme_fmubeta/fontfilettfbody';
    $title = get_string('fontfilettfbody', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilettfbody');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // OTF Font.
    $name = 'theme_fmubeta/fontfileotfbody';
    $title = get_string('fontfileotfbody', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileotfbody');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // WOFF Font.
    $name = 'theme_fmubeta/fontfilewoffbody';
    $title = get_string('fontfilewoffbody', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewoffbody');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // WOFF2 Font.
    $name = 'theme_fmubeta/fontfilewofftwobody';
    $title = get_string('fontfilewofftwobody', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewofftwobody');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // EOT Font.
    $name = 'theme_fmubeta/fontfileeotbody';
    $title = get_string('fontfileeotbody', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileweotbody');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    // SVG Font.
    $name = 'theme_fmubeta/fontfilesvgbody';
    $title = get_string('fontfilesvgbody', 'theme_fmubeta');
    $description = '';
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilesvgbody');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $fontsettings->add($setting);

    $ADMIN->add('theme_fmubeta', $fontsettings);

    // Front page slider page....
    // Number of front page slides.
    $name = 'theme_fmubeta/frontpagenumberofslides';
    $title = get_string('frontpagenumberofslides', 'theme_fmubeta');
    $description = get_string('frontpagenumberofslides_desc', 'theme_fmubeta');
    $default = 3;
    $choices = array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16'
    );
    $slidersettings = new admin_settingpage('theme_fmubeta_slider', get_string('frontpagesliderheading', 'theme_fmubeta'));
    $slidersettings->add(new admin_setting_heading('theme_fmubeta_slider', get_string('frontpagesliderheadingsub', 'theme_fmubeta'),
            format_text(get_string('frontpagesliderheadingdesc', 'theme_fmubeta'), FORMAT_MARKDOWN)));
    $slidersettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Slide speed.
    $name = 'theme_fmubeta/frontpagesliderspeed';
    $title = get_string('frontpagesliderspeed', 'theme_fmubeta');
    $description = get_string('frontpagesliderspeed_desc', 'theme_fmubeta');
    $default = 5000;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $slidersettings->add($setting);

    // Show on mobile.
    $name = 'theme_fmubeta/frontpageslidermobile';
    $title = get_string('frontpageslidermobile', 'theme_fmubeta');
    $description = get_string('frontpageslidermobile_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $slidersettings->add($setting);

    // Show on tablet.
    $name = 'theme_fmubeta/frontpageslidertablet';
    $title = get_string('frontpageslidertablet', 'theme_fmubeta');
    $description = get_string('frontpageslidertablet_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $slidersettings->add($setting);

    // Language variables for settings....
    $langpackurl = new moodle_url('/admin/tool/langimport/index.php');
    $langsinstalled = array_merge(array('all' => get_string('all')), get_string_manager()->get_list_of_translations());

    $numberofslides = get_config('theme_fmubeta', 'frontpagenumberofslides');
    for ($i = 1; $i <= $numberofslides; $i++) {
        $slidersettings->add(new admin_setting_heading('theme_fmubeta_frontpageslide_heading'.$i, get_string('frontpageslidersettingspageheading', 'theme_fmubeta', array('slide' => $i)), null));

        // Image.
        $name = 'theme_fmubeta/frontpageslideimage'.$i;
        $title = get_string('frontpageslideimage', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslideimage_desc', 'theme_fmubeta', array('slide' => $i));
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'frontpageslideimage'.$i);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $slidersettings->add($setting);

        // URL.
        $name = 'theme_fmubeta/frontpageslideurl'.$i;
        $title = get_string('frontpageslideurl', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslideurl_desc', 'theme_fmubeta', array('slide' => $i));
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $slidersettings->add($setting);

        // Caption title.
        $name = 'theme_fmubeta/frontpageslidecaptiontitle'.$i;
        $title = get_string('frontpageslidecaptiontitle', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslidecaptiontitle_desc', 'theme_fmubeta', array('slide' => $i));
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $slidersettings->add($setting);

        // Caption text.
        $name = 'theme_fmubeta/frontpageslidecaptiontext'.$i;
        $title = get_string('frontpageslidecaptiontext', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslidecaptiontext_desc', 'theme_fmubeta', array('slide' => $i));
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $slidersettings->add($setting);

        // Status.
        $name = 'theme_fmubeta/frontpageslidestatus'.$i;
        $title = get_string('frontpageslidestatus', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslidestatus_desc', 'theme_fmubeta');
        $default = 1;
        $choices = array(
            1 => new lang_string('draft', 'theme_fmubeta'),
            2 => new lang_string('published', 'theme_fmubeta')
        );
        $slidersettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

        // Display.
        $name = 'theme_fmubeta/frontpageslidedisplay'.$i;
        $title = get_string('frontpageslidedisplay', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslidedisplay_desc', 'theme_fmubeta', array('slide' => $i));
        $default = 1;
        $choices = array(
            1 => new lang_string('always', 'theme_fmubeta'),
            2 => new lang_string('loggedout', 'theme_fmubeta'),
            3 => new lang_string('loggedin', 'theme_fmubeta')
        );

        $slidersettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));
        // Slider language only.
        $name = 'theme_fmubeta/frontpageslidelang'.$i;
        $title = get_string('frontpageslidelang', 'theme_fmubeta', array('slide' => $i));
        $description = get_string('frontpageslidelang_desc', 'theme_fmubeta', array('slide' => $i, 'url' => html_writer::tag('a', get_string('langpack_urlname', 'theme_fmubeta'), array(
                       'href' => $langpackurl, 'target' => '_blank'))));
        $default = 'all';
        $setting = new admin_setting_configselect($name, $title, $description, $default, $langsinstalled);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $slidersettings->add($setting);
    }
    $ADMIN->add('theme_fmubeta', $slidersettings);

    // Image bank....
    // Number of images in the image bank.
    $name = 'theme_fmubeta/numberofimagebankimages';
    $title = get_string('numberofimagebankimages', 'theme_fmubeta');
    $description = get_string('numberofimagebankimages_desc', 'theme_fmubeta');
    $default = 0;
    $choices = array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16'
    );

    $theme = theme_config::load('fmubeta');
    $imagebanksettings = new admin_settingpage('theme_fmubeta_imagebank', get_string('imagebankheading', 'theme_fmubeta'));
    $imagebanksettings->add(new admin_setting_heading('theme_fmubeta_marketingspots', get_string('imagebankheadingsub', 'theme_fmubeta'),
            format_text(get_string('imagebankheadingdesc', 'theme_fmubeta'), FORMAT_MARKDOWN)));

    $imagebanksettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));
    $numberofimagebankimages = get_config('theme_fmubeta', 'numberofimagebankimages');
    for ($i = 1; $i <= $numberofimagebankimages; $i++) {
        $name = 'imagebankimage'.$i;
        $settingname = 'theme_fmubeta/'.$name;
        $title = get_string('imagebankimage', 'theme_fmubeta').$i;
        if (empty($theme->settings->$name)) {
            $imagedesc = get_string('none', 'theme_fmubeta');
        } else {
            $imageurl = new moodle_url('/theme/fmubeta/imagebank.php');
            $imageurl->param('imageid', $i);
            $imagedesc = preg_replace('|^https?://|i', '//', $imageurl->out(false));
        }
        $description = get_string('imagebankimage_desc', 'theme_fmubeta', array('imagedesc' => $imagedesc));
        $setting = new admin_setting_configstoredfile($settingname, $title, $description, $name);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $imagebanksettings->add($setting);
    }
    $ADMIN->add('theme_fmubeta', $imagebanksettings);

    // Login page background image changer page....
    // Number of images.
    $name = 'theme_fmubeta/loginbackgroundchangernumberofimages';
    $title = get_string('loginbackgroundchangernumberofimages', 'theme_fmubeta');
    $description = get_string('loginbackgroundchangernumberofimages_desc', 'theme_fmubeta');
    $default = 3;
    $choices = array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16'
    );
    $loginpagesettings = new admin_settingpage('theme_fmubeta_loginbackgroundchanger', get_string('loginbackgroundchangerheading', 'theme_fmubeta'));
    $loginpagesettings->add(new admin_setting_heading('theme_fmubeta_loginbackgroundchanger', get_string('loginbackgroundchangerheadingsub', 'theme_fmubeta'),
            format_text(get_string('loginbackgroundchangerheadingdesc', 'theme_fmubeta'), FORMAT_MARKDOWN)));
    $loginpagesettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // Image speed.
    $name = 'theme_fmubeta/loginbackgroundchangerspeed';
    $title = get_string('loginbackgroundchangerspeed', 'theme_fmubeta');
    $description = get_string('loginbackgroundchangerspeed_desc', 'theme_fmubeta');
    $default = 3000;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $loginpagesettings->add($setting);

    // Image fade.
    $name = 'theme_fmubeta/loginbackgroundchangerfade';
    $title = get_string('loginbackgroundchangerfade', 'theme_fmubeta');
    $description = get_string('loginbackgroundchangerfade_desc', 'theme_fmubeta');
    $default = 750;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $loginpagesettings->add($setting);

    // Show on mobile.
    $name = 'theme_fmubeta/loginbackgroundchangermobile';
    $title = get_string('loginbackgroundchangermobile', 'theme_fmubeta');
    $description = get_string('loginbackgroundchangermobile_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $loginpagesettings->add($setting);

    // Show on tablet.
    $name = 'theme_fmubeta/loginbackgroundchangertablet';
    $title = get_string('loginbackgroundchangertablet', 'theme_fmubeta');
    $description = get_string('loginbackgroundchangertablet_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $loginpagesettings->add($setting);

    $numberofimages = get_config('theme_fmubeta', 'loginbackgroundchangernumberofimages');
    for ($i = 1; $i <= $numberofimages; $i++) {
        // Image.
        $name = 'theme_fmubeta/loginbackgroundchangerimage'.$i;
        $title = get_string('loginbackgroundchangerimage', 'theme_fmubeta', array('image' => $i));
        $description = get_string('loginbackgroundchangerimage_desc', 'theme_fmubeta', array('image' => $i));
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'loginbackgroundchangerimage'.$i);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $loginpagesettings->add($setting);
    }
    $ADMIN->add('theme_fmubeta', $loginpagesettings);

    // Look and feel.
    $landfsettings = new admin_settingpage('theme_fmubeta_landf', get_string('landfheading', 'theme_fmubeta'));

    // Colours.
    $landfsettings->add(new admin_setting_heading('theme_fmubeta_landf_colours', get_string('landfcolours', 'theme_fmubeta'),
            format_text(get_string('landfcolours_desc', 'theme_fmubeta'), FORMAT_MARKDOWN)));

    // Text.
    $name = 'theme_fmubeta/textcolour';
    $title = get_string('textcolour', 'theme_fmubeta');
    $description = get_string('textcolourdesc', 'theme_fmubeta');
    $default = '#1F4D87';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Link.
    $name = 'theme_fmubeta/linkcolour';
    $title = get_string('linkcolour', 'theme_fmubeta');
    $description = get_string('linkcolourdesc', 'theme_fmubeta');
    $default = '#1F4D87';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Navbar Text.
    $name = 'theme_fmubeta/navbartextcolour';
    $title = get_string('navbartextcolour', 'theme_fmubeta');
    $description = get_string('navbartextcolourdesc', 'theme_fmubeta');
    $default = '#653CAE';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Navbar Background.
    $name = 'theme_fmubeta/navbarbackgroundcolour';
    $title = get_string('navbarbackgroundcolour', 'theme_fmubeta');
    $description = get_string('navbarbackgroundcolourdesc', 'theme_fmubeta');
    $default = '#FFD974';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Navbar Border.
    $name = 'theme_fmubeta/navbarbordercolour';
    $title = get_string('navbarbordercolour', 'theme_fmubeta');
    $description = get_string('navbarbordercolourdesc', 'theme_fmubeta');
    $default = '#FFD053';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Page Top.
    $name = 'theme_fmubeta/pagetopcolour';
    $title = get_string('pagetopcolour', 'theme_fmubeta');
    $description = get_string('pagetopcolourdesc', 'theme_fmubeta');
    $default = '#1F4D87';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Page Bottom.
    $name = 'theme_fmubeta/pagebottomcolour';
    $title = get_string('pagebottomcolour', 'theme_fmubeta');
    $description = get_string('pagebottomcolourdesc', 'theme_fmubeta');
    $default = '#C9E6FF';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Footer Text.
    $name = 'theme_fmubeta/footertextcolour';
    $title = get_string('footertextcolour', 'theme_fmubeta');
    $description = get_string('footertextcolourdesc', 'theme_fmubeta');
    $default = '#B8D2E9';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Footer top.
    $name = 'theme_fmubeta/footertopcolour';
    $title = get_string('footertopcolour', 'theme_fmubeta');
    $description = get_string('footertopcolourdesc', 'theme_fmubeta');
    $default = '#269F00';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Footer bottom.
    $name = 'theme_fmubeta/footerbottomcolour';
    $title = get_string('footerbottomcolour', 'theme_fmubeta');
    $description = get_string('footerbottomcolourdesc', 'theme_fmubeta');
    $default = '#267F00';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Front page.
    $landfsettings->add(new admin_setting_heading('theme_fmubeta_landf_frontpage', get_string('landffontpage', 'theme_fmubeta'),
            format_text(get_string('landffontpage_desc', 'theme_fmubeta'), FORMAT_MARKDOWN)));

    // Front page background image.
    $name = 'theme_fmubeta/landffrontpagebackgroundimage';
    $title = get_string('landffrontpagebackgroundimage', 'theme_fmubeta');
    $description = get_string('landffrontpagebackgroundimage_desc', 'theme_fmubeta');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'landffrontpagebackgroundimage');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // Front page content transparency.
    $name = 'theme_fmubeta/landffrontpagecontenttransparency';
    $title = get_string('landffrontpagecontenttransparency', 'theme_fmubeta');
    $description = get_string('landffrontpagecontenttransparency_desc', 'theme_fmubeta');
    $default = 100;
    $choices = array(
        100 => get_string('zeropercent', 'theme_fmubeta'),
        95  => get_string('fivepercent', 'theme_fmubeta'),
        90  => get_string('tenpercent', 'theme_fmubeta'),
        85  => get_string('fifteenpercent', 'theme_fmubeta'),
        80  => get_string('twentypercent', 'theme_fmubeta'),
        75  => get_string('twentyfivepercent', 'theme_fmubeta'),
        70  => get_string('thirtypercent', 'theme_fmubeta'),
        65  => get_string('thirtyfivepercent', 'theme_fmubeta'),
        60  => get_string('fortypercent', 'theme_fmubeta'),
        55  => get_string('fortyfivepercent', 'theme_fmubeta'),
        50  => get_string('fiftypercent', 'theme_fmubeta'),
        45  => get_string('fifyfivepercent', 'theme_fmubeta'),
        40  => get_string('sixtypercent', 'theme_fmubeta'),
        35  => get_string('sixtyfivepercent', 'theme_fmubeta'),
        30  => get_string('seventypercent', 'theme_fmubeta'),
        25  => get_string('seventyfivepercent', 'theme_fmubeta'),
        20  => get_string('eightypercent', 'theme_fmubeta'),
        15  => get_string('eightyfivepercent', 'theme_fmubeta'),
        10  => get_string('ninetypercent', 'theme_fmubeta'),
        5   => get_string('ninetyfivepercent', 'theme_fmubeta'),
        0   => get_string('onehundredpercent', 'theme_fmubeta')
    );
    $landfsettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    // All other pages.
    $landfsettings->add(new admin_setting_heading('theme_fmubeta_landf_allpages', get_string('landfallpages', 'theme_fmubeta'),
            format_text(get_string('landfallpages_desc', 'theme_fmubeta'), FORMAT_MARKDOWN)));

    // All other pages background image.
    $name = 'theme_fmubeta/landfallpagesbackgroundimage';
    $title = get_string('landfallpagesbackgroundimage', 'theme_fmubeta');
    $description = get_string('landfallpagesbackgroundimage_desc', 'theme_fmubeta');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'landfallpagesbackgroundimage');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $landfsettings->add($setting);

    // All other pages content transparency.
    $name = 'theme_fmubeta/landfallpagescontenttransparency';
    $title = get_string('landfallpagescontenttransparency', 'theme_fmubeta');
    $description = get_string('landfallpagescontenttransparency_desc', 'theme_fmubeta');
    $default = 100;
    $choices = array(
        100 => get_string('zeropercent', 'theme_fmubeta'),
        95  => get_string('fivepercent', 'theme_fmubeta'),
        90  => get_string('tenpercent', 'theme_fmubeta'),
        85  => get_string('fifteenpercent', 'theme_fmubeta'),
        80  => get_string('twentypercent', 'theme_fmubeta'),
        75  => get_string('twentyfivepercent', 'theme_fmubeta'),
        70  => get_string('thirtypercent', 'theme_fmubeta'),
        65  => get_string('thirtyfivepercent', 'theme_fmubeta'),
        60  => get_string('fortypercent', 'theme_fmubeta'),
        55  => get_string('fortyfivepercent', 'theme_fmubeta'),
        50  => get_string('fiftypercent', 'theme_fmubeta'),
        45  => get_string('fifyfivepercent', 'theme_fmubeta'),
        40  => get_string('sixtypercent', 'theme_fmubeta'),
        35  => get_string('sixtyfivepercent', 'theme_fmubeta'),
        30  => get_string('seventypercent', 'theme_fmubeta'),
        25  => get_string('seventyfivepercent', 'theme_fmubeta'),
        20  => get_string('eightypercent', 'theme_fmubeta'),
        15  => get_string('eightyfivepercent', 'theme_fmubeta'),
        10  => get_string('ninetypercent', 'theme_fmubeta'),
        5   => get_string('ninetyfivepercent', 'theme_fmubeta'),
        0   => get_string('onehundredpercent', 'theme_fmubeta')
    );
    $landfsettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $ADMIN->add('theme_fmubeta', $landfsettings);


    // Marketing spots....
    // Number of marketing spots.
    $name = 'theme_fmubeta/numberofmarketingspots';
    $title = get_string('numberofmarketingspots', 'theme_fmubeta');
    $description = get_string('numberofmarketingspots_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4'
    );
    $marketingspotssettings = new admin_settingpage('theme_fmubeta_marketingspots', get_string('marketingspotsheading', 'theme_fmubeta'));
    $marketingspotssettings->add(new admin_setting_heading('theme_fmubeta_marketingspots', get_string('marketingspotsheadingsub', 'theme_fmubeta'),
            format_text(get_string('marketingspotsheadingdesc', 'theme_fmubeta'), FORMAT_MARKDOWN)));
    $marketingspotssettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $numberofmarketingspots = get_config('theme_fmubeta', 'numberofmarketingspots');
    for ($i = 1; $i <= $numberofmarketingspots; $i++) {
        $marketingspotssettings->add(new admin_setting_heading('theme_fmubeta_marketingspot_heading'.$i, get_string('marketingspotsettingspageheading', 'theme_fmubeta', array('spot' => $i)), null));

        // Marketing spot heading.
        $name = 'theme_fmubeta/marketingspotheading'.$i;
        $title = get_string('marketingspotheading', 'theme_fmubeta', array('spot' => $i));
        $description = get_string('marketingspotheading_desc', 'theme_fmubeta', array('spot' => $i));
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $marketingspotssettings->add($setting);

        // Marketing spot content.
        $name = 'theme_fmubeta/marketingspotcontent'.$i;
        $title = get_string('marketingspotcontent', 'theme_fmubeta', array('spot' => $i));
        $description = get_string('marketingspotcontent_desc', 'theme_fmubeta', array('spot' => $i));
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $marketingspotssettings->add($setting);

        // Status.
        $name = 'theme_fmubeta/marketingspotstatus'.$i;
        $title = get_string('marketingspotstatus', 'theme_fmubeta', array('spot' => $i));
        $description = get_string('marketingspotstatus_desc', 'theme_fmubeta');
        $default = 1;
        $choices = array(
            1 => new lang_string('draft', 'theme_fmubeta'),
            2 => new lang_string('published', 'theme_fmubeta')
        );
        $marketingspotssettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

        // Display.
        $name = 'theme_fmubeta/marketingspotdisplay'.$i;
        $title = get_string('marketingspotdisplay', 'theme_fmubeta', array('spot' => $i));
        $description = get_string('marketingspotdisplay_desc', 'theme_fmubeta', array('spot' => $i));
        $default = 1;
        $choices = array(
            1 => new lang_string('always', 'theme_fmubeta'),
            2 => new lang_string('loggedout', 'theme_fmubeta'),
            3 => new lang_string('loggedin', 'theme_fmubeta')
        );
        $marketingspotssettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

        // Marketing spot language only.
        $name = 'theme_fmubeta/marketingspotlang'.$i;
        $title = get_string('marketingspotlang', 'theme_fmubeta', array('spot' => $i));
        $description = get_string('marketingspotlang_desc', 'theme_fmubeta', array('spot' => $i, 'url' => html_writer::tag('a', get_string('langpack_urlname', 'theme_fmubeta'), array(
                       'href' => $langpackurl, 'target' => '_blank'))));
        $default = 'all';
        $setting = new admin_setting_configselect($name, $title, $description, $default, $langsinstalled);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $marketingspotssettings->add($setting);
    }
    $ADMIN->add('theme_fmubeta', $marketingspotssettings);

    // Site pages....
    // Number of site pages.
    $name = 'theme_fmubeta/numberofsitepages';
    $title = get_string('numberofsitepages', 'theme_fmubeta');
    $description = get_string('numberofsitepages_desc', 'theme_fmubeta');
    $default = 1;
    $choices = array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16'
    );

    $sitepagessettings = new admin_settingpage('theme_fmubeta_sitepages', get_string('sitepagesheading', 'theme_fmubeta'));
    $sitepagessettings->add(new admin_setting_heading('theme_fmubeta_sitepages', get_string('sitepagesheadingsub', 'theme_fmubeta'),
            format_text(get_string('sitepagesheadingdesc', 'theme_fmubeta'), FORMAT_MARKDOWN)));
    $sitepagessettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $numberofsitepages = get_config('theme_fmubeta', 'numberofsitepages');
    for ($i = 1; $i <= $numberofsitepages; $i++) {
        $sitepagessettings->add(new admin_setting_heading('theme_fmubeta_sitepage_heading'.$i, get_string('sitepagesettingspageheading', 'theme_fmubeta', array('pageid' => $i)), null));

        // Site page title.
        $name = 'theme_fmubeta/sitepagetitle'.$i;
        $title = get_string('sitepagetitle', 'theme_fmubeta', array('pageid' => $i));
        $description = get_string('sitepagetitle_desc', 'theme_fmubeta', array('pageid' => $i));
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $sitepagessettings->add($setting);

        // Site page heading.
        $name = 'theme_fmubeta/sitepageheading'.$i;
        $title = get_string('sitepageheading', 'theme_fmubeta', array('pageid' => $i));
        $description = get_string('sitepageheading_desc', 'theme_fmubeta', array('pageid' => $i));
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $sitepagessettings->add($setting);

        // Site page content.
        $name = 'theme_fmubeta/sitepagecontent'.$i;
        $title = get_string('sitepagecontent', 'theme_fmubeta', array('pageid' => $i));
        $description = get_string('sitepagecontent_desc', 'theme_fmubeta', array('pageid' => $i));
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $sitepagessettings->add($setting);

        // Status.
        $name = 'theme_fmubeta/sitepagestatus'.$i;
        $title = get_string('sitepagestatus', 'theme_fmubeta', array('pageid' => $i));
        $description = get_string('sitepagestatus_desc', 'theme_fmubeta');
        $default = 1;
        $choices = array(
            1 => new lang_string('draft', 'theme_fmubeta'),
            2 => new lang_string('published', 'theme_fmubeta')
        );
        $sitepagessettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

        // Display.
        $name = 'theme_fmubeta/sitepagedisplay'.$i;
        $title = get_string('sitepagedisplay', 'theme_fmubeta', array('pageid' => $i));
        $description = get_string('sitepagedisplay_desc', 'theme_fmubeta', array('pageid' => $i));
        $default = 1;
        $choices = array(
            1 => new lang_string('always', 'theme_fmubeta'),
            2 => new lang_string('loggedout', 'theme_fmubeta'),
            3 => new lang_string('loggedin', 'theme_fmubeta')
        );
        $sitepagessettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

        // Site page language only.
        $name = 'theme_fmubeta/sitepagelang'.$i;
        $title = get_string('sitepagelang', 'theme_fmubeta', array('pageid' => $i));
        $description = get_string('sitepagelang_desc', 'theme_fmubeta', array('pageid' => $i, 'url' => html_writer::tag('a', get_string('langpack_urlname', 'theme_fmubeta'), array(
                       'href' => $langpackurl, 'target' => '_blank'))));
        $default = 'all';
        $setting = new admin_setting_configselect($name, $title, $description, $default, $langsinstalled);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $sitepagessettings->add($setting);
    }
    $ADMIN->add('theme_fmubeta', $sitepagessettings);

    // Social links page....
    // Number of social links.
    $name = 'theme_fmubeta/numberofsociallinks';
    $title = get_string('numberofsociallinks', 'theme_fmubeta');
    $description = get_string('numberofsociallinks_desc', 'theme_fmubeta');
    $default = 2;
    $choices = array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16'
    );

    $socialsettings = new admin_settingpage('theme_fmubeta_social', get_string('socialheading', 'theme_fmubeta'));
    $socialsettings->add(new admin_setting_heading('theme_fmubeta_social', get_string('socialheadingsub', 'theme_fmubeta'),
            format_text(get_string('socialheadingdesc', 'theme_fmubeta'), FORMAT_MARKDOWN)));
    $socialsettings->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $numberofsociallinks = get_config('theme_fmubeta', 'numberofsociallinks');
    for ($i = 1; $i <= $numberofsociallinks; $i++) {
        // Social url setting.
        $name = 'theme_fmubeta/social'.$i;
        $title = get_string('socialnetworklink', 'theme_fmubeta').$i;
        $description = get_string('socialnetworklink_desc', 'theme_fmubeta').$i;
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $socialsettings->add($setting);

        // Social icon setting.
        $name = 'theme_fmubeta/socialicon'.$i;
        $title = get_string('socialnetworkicon', 'theme_fmubeta').$i;
        $description = get_string('socialnetworkicon_desc', 'theme_fmubeta').$i;
        $default = 'globe';
        $choices = array(
            'dropbox' => 'Dropbox',
            'facebook-square' => 'Facebook',
            'flickr' => 'Flickr',
            'github' => 'Github',
            'google-plus-square' => 'Google Plus',
            'instagram' => 'Instagram',
            'linkedin-square' => 'Linkedin',
            'pinterest-square' => 'Pinterest',
            'skype' => 'Skype',
            'tumblr-square' => 'Tumblr',
            'twitter-square' => 'Twitter',
            'users' => 'Unlisted',
            'vimeo-square' => 'Vimeo',
            'vk' => 'Vk',
            'globe' => 'Website',
            'youtube-square' => 'YouTube'
        );
        $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $socialsettings->add($setting);
    }

    // Use the signpost.
    $name = 'theme_fmubeta/socialsignpost';
    $title = get_string('socialsignpost', 'theme_fmubeta');
    $description = get_string('socialsignpost_desc', 'theme_fmubeta');
    $setting = new admin_setting_configcheckbox($name, $title, $description, '1');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $socialsettings->add($setting);

    $ADMIN->add('theme_fmubeta', $socialsettings);
