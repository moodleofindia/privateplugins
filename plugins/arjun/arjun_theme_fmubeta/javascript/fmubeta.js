require(['core/first'], function() {
    require(['theme_fmubeta/bootstrap', 'theme_fmubeta/anti_gravity', 'core/log'], function(b, ag, log) {
        log.debug('fmubeta JavaScript initialised');
    });
});
