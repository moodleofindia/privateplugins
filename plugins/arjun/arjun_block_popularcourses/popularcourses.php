<?php

require_once('../../config.php');
require_once($CFG->libdir.'/coursecatlib.php');
require_once($CFG->dirroot.'/blocks/moodleblock.class.php');
require_once($CFG->dirroot.'/blocks/popularcourses/block_popularcourses.php');
require_once($CFG->dirroot.'/blocks/popularcourses/popularcourses_form.php');

require_login();

$system_context = context_system::instance();

require_capability('block/popularcourses:addinstance', $system_context);

$PAGE->set_pagelayout('admin');
$PAGE->set_url('/blocks/popularcourses/popularcourses.php');
$PAGE->set_context($system_context);

$args = array(
    'availablecourses' => coursecat::get(0)->get_courses(array('recursive' => true)),
    'popularcourses' => block_popularcourses::get_popular_courses()
);

$editform = new popularcourses_form(null, $args);
if ($editform->is_cancelled()) {
    redirect($CFG->wwwroot.'/?redirect=0');
} else if ($data = $editform->get_data()) {

    if (isset($data->doadd) && $data->doadd == 1) {
        try {
            $DB->insert_record('block_popularcourses', $data->newpopular);
        } catch (Exception $e) {
            throw $e;
        }
    }

    if (isset($data->popular) && !empty($data->popular)) {
        try {
            foreach ($data->popular as $f) {
                $DB->update_record('block_popularcourses', $f);
            }
        } catch (Exception $e) {
            throw new moodle_exception();
        }
    }
    redirect($CFG->wwwroot.'/blocks/popularcourses/popularcourses.php');
}

$site = get_site();

$PAGE->set_title(get_string('editpagetitle', 'block_popularcourses'));
$PAGE->set_heading($site->fullname . ' - ' . 
                   get_string('pluginname', 'block_popularcourses'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('editpagedesc', 'block_popularcourses'));

$editform->display();

echo $OUTPUT->footer();
