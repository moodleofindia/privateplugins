<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_popularcourses', language 'en'
 *
 * @package   block_popularcourses
 * @copyright Daniel Neis <danielneis@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['popularcourses:addinstance'] = 'Add a "Popular courses" block';
$string['popularcourses:myaddinstance'] = 'Add a "Popular courses" block to my moodle';
$string['pluginname'] = 'Popular Courses';
$string['editlink'] = '<a href="{$a}">Click here to edit or add Popular courses</a>';
$string['editpagedesc'] = 'Editing the list of Popular courses';
$string['popularcourses'] = 'Popular courses';
$string['addPopularcourse'] = 'Add a new exiting course to Popular list';
$string['courseid'] = 'Course';
$string['editpagetitle'] = 'Popular courses - editing';
$string['Popularcourse'] = 'Popular course: {$a}';
$string['doadd'] = 'Check to add new Popular course';
$string['missingcourseid'] = 'You must select a course.';
$string['sortorder'] = 'Sort order';
$string['missingsortorder'] = 'You must set a sort order.';
$string['deletelink'] = '<a href="{$a}">Delete</a>';
$string['delete_Popularcourse'] = 'Delete Popular course';
$string['confirmdelete'] = 'Are you sure you want to delete this course from the list of Popular courses?';
