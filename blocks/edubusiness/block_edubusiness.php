<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course summary block
 *
 * @package    block_edubusiness
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_edubusiness extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_edubusiness');
    }

    function applicable_formats() {
        return array('all' => true, 'mod' => false, 'tag' => false, 'my' => false);
    }

    function specialization() {
        if($this->page->pagetype == PAGE_COURSE_VIEW && $this->page->course->id != SITEID) {
            $this->title = get_string('coursesummary', 'block_edubusiness');
        }
    }

    function get_content() {
        global $CFG, $OUTPUT,$PAGE;

        require_once($CFG->libdir . '/filelib.php');

        $PAGE->requires->css('/blocks/edubusiness/styles.css'); 

        if($this->content !== NULL) {
            return $this->content;
        }

        if (empty($this->instance)) {
            return '';
        }

        $this->content = new stdClass();
        $options = new stdClass();
        $options->noclean = true;    // Don't clean Javascripts etc
        $options->overflowdiv = true;
        $context = context_course::instance($this->page->course->id);
        //$this->page->course->summary = file_rewrite_pluginfile_urls($this->page->course->summary, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
        //$this->content->text = format_text($this->page->course->summary, $this->page->course->summaryformat, $options);
		// <a class="text-uppercase btn btn-transparent btn-bordered--dark btn-lg" href="/my/coursebrowse.php">take tour</a>
		//<a class="text-uppercase colorwhite btn btn-primary btn-lg" href="/my/coursebrowse.php">view courses</a>
		
		
		$this->content->text = '<div class="siteorigin-panels-stretch panel-row-style" style="margin-left: -70px; margin-right: -70px; padding-left: 70px; padding-right: 70px; border-left-width: 0px; border-right-width: 0px; background-color: rgb(93, 190, 89); background-repeat: no-repeat;" data-stretch-type="full">
        <div class="panel-grid-cell" id="pgc-1642-6-0">
        <div class="so-panel widget widget_single-images panel-first-child panel-last-child" id="panel-1642-6-0-0">
        <div class="images-student panel-widget-style">
        <div class="thim-widget-single-images thim-widget-single-images-base">
        <div class="single-image center">
        <img src="'.$CFG->wwwroot.'/blocks/edubusiness/pix/students.png" width="367" height="261" alt="">
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="panel-grid-cell" id="pgc-1642-6-1">
        <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-1642-6-1-0">
        <div class="custom-readmore panel-widget-style" style="background-image: url(http://demo.thimpress.com/elearningwp/wp-content/uploads/2015/06/bg-instructor.jpg);background-repeat: repeat;"><div class="thim-widget-icon-box thim-widget-icon-box-base">
        <div class="wrapper-box-icon text-left " data-text-readmore="#fff">
        <div class="smicon-box icon-left">
        <div class="boxes-icon" style="width: 80px;height: 80px;">
        <span class="inner-icon">
        <span class="icon icon-images">
        <img src="'.$CFG->wwwroot.'/blocks/edubusiness/pix/icon-book.png" width="80" height="62" alt="">
        </span>
        </span>
        </div>
        <div class="content-inner" style="width: calc( 100% - 80px - 15px);">
        <div class="widget-title-icon-box ">
        <h3 class="icon-box-title" style="color: #ffffff;font-size: 32px; line-height: 32px;font-weight: 700;">Become an instructor?</h3>
        </div>
        <div class="desc-icon-box">
        <p style="color: #ffffff;font-size: 13px;line-height: 20px;">Join thousand of instructors and earn money hassle free!</p>
        <a class="smicon-read sc-btn" href="'.$CFG->wwwroot.'/my/coursebrowse.php" style="border-color: #ffffff;color: #ffffff;">Get Started Now !</a>
        </div>
        </div>
        </div>
        <!--end smicon-box-->
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>';  // end of container fluid
        
        $this->content->footer = '';

        return $this->content;
    }

    function hide_header() {
        return true;
    }

    function preferred_width() {
        return 210;
    }

}


