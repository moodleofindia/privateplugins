<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the news item block class, based upon block_base.
 *
 * @package    block_latest_news
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Class block_latest_news
 *
 * @package    block_latest_news
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_latest_news extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_latest_news');
    }

    function get_content() {
        global $CFG, $USER, $PAGE;
        $PAGE->requires->css('/blocks/latest_news/styles.css');
        $PAGE->requires->js('/blocks/latest_news/slide.js');

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if (empty($this->instance)) {
            return $this->content;
        }


        if ($this->page->course->newsitems) {   // Create a nice listing of recent postings
            require_once($CFG->dirroot . '/mod/forum/lib.php');   // We'll need this

            $text = '';

            if (!$forum = forum_get_course_forum($this->page->course->id, 'news')) {
                return '';
            }

            $modinfo = get_fast_modinfo($this->page->course);
            if (empty($modinfo->instances['forum'][$forum->id])) {
                return '';
            }
            $cm = $modinfo->instances['forum'][$forum->id];

            if (!$cm->uservisible) {
                return '';
            }

            $context = context_module::instance($cm->id);

            /// User must have perms to view discussions in that forum
            if (!has_capability('mod/forum:viewdiscussion', $context)) {
                return '';
            }

            /// First work out whether we can post to this group and if so, include a link
            $groupmode = groups_get_activity_groupmode($cm);
            $currentgroup = groups_get_activity_group($cm, true);


            if (forum_user_can_post_discussion($forum, $currentgroup, $groupmode, $cm, $context)) {
                $text .= '<div class="newlink"><a href="' . $CFG->wwwroot . '/mod/forum/post.php?forum=' . $forum->id . '">' .
                        get_string('addanewtopic', 'forum') . '</a>...</div>';
            }

            /// Get all the recent discussions we're allowed to see

            if (!$discussions = forum_get_discussions($cm)) {
                $text .= '(' . get_string('nonews', 'forum') . ')';
                $this->content->text = $text;
                return $this->content;
            }

            /// Actually create the listing now

            $strftimerecent = get_string('strftimerecent');
            $strmore = get_string('more', 'forum');

            /// Accessibility: markup as a list.
            // $text .= "\n<ul class='unlist'>\n";
            //echo '<pre>',print_r($discussions);
            $text .='<div class="container">
                            <div class="row">
                                <div class="span3" style="padding-right:14px;">
                                 <!-- Carousel
                                     ================================================== -->
                                     <div id="myCarousel" class="carousel slide">        
                                     <div class="carousel-inner">';

            foreach ($discussions as $discussion) {
//echo "<pre>".print_r($discussion);
                $discussion->subject = $discussion->name;

                $discussion->subject = format_string($discussion->subject, true, $forum->course);
                $discussion->message = format_string($discussion->message, true, $forum->course);

                $text.='<div class="item"> 
                                    <div class="caption">
                                        <div class="wrap-info">';
                                            $urlimg = $CFG->wwwroot.'/user/pix.php?file=/'.$discussion->userid.'/f1.jpg';
                                                    $contentteacherimg = html_writer::empty_tag('img', array('src' => $urlimg,'class' => 'avatar user-12-avatar avatar-32 photo','width'=>'40','height'=>'35'));       
                                            $text .='<div class="userinfo">'.$contentteacherimg.'
                                            <div class="name">' . fullname($discussion).'
                                            <div class="date">' . userdate($discussion->modified, $strftimerecent) . '</div></div></div>
                                            <div class="info"><h4><a href="' . $CFG->wwwroot . '/mod/forum/discuss.php?d=' . $discussion->discussion . '">'. $discussion->subject .'</a></h4></div>
                                            <div class="message">'.substr($discussion->message, 0,350).'<a href="' . $CFG->wwwroot . '/mod/forum/discuss.php?d=' . $discussion->discussion . '"> ...</a>'.'</div> 
                                        </div>
                                    </div>
                        </div>';
            }
            $text .='</div>                                                                                              
                         </div><!-- End Carousel -->  
                    </div>
                </div>
            </div>';
            $this->content->text = $text;

            // $this->content->footer = '<a href="'.$CFG->wwwroot.'/mod/forum/view.php?f='.$forum->id.'">'.
            get_string('oldertopics', 'forum') . '</a> ...';

            /// If RSS is activated at site and forum level and this forum has rss defined, show link
            if (isset($CFG->enablerssfeeds) && isset($CFG->forum_enablerssfeeds) &&
                    $CFG->enablerssfeeds && $CFG->forum_enablerssfeeds && $forum->rsstype && $forum->rssarticles) {
                require_once($CFG->dirroot . '/lib/rsslib.php');   // We'll need this
                if ($forum->rsstype == 1) {
                    $tooltiptext = get_string('rsssubscriberssdiscussions', 'forum');
                } else {
                    $tooltiptext = get_string('rsssubscriberssposts', 'forum');
                }
                if (!isloggedin()) {
                    $userid = $CFG->siteguest;
                } else {
                    $userid = $USER->id;
                }

                $this->content->footer .= '<br />' . rss_get_link($context->id, $userid, 'mod_forum', $forum->id, $tooltiptext);
            }
        }

        return $this->content;
    }

}
