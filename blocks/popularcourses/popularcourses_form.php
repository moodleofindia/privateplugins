<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling popular courses.
 */
class popularcourses_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG;

        $mform = $this->_form;
        $availablecourses  = $this->_customdata['availablecourses']; // this contains the data of this form
        $popularcourses  = $this->_customdata['popularcourses']; // this contains the data of this form
        $availablecourseslist = array();
        foreach ($availablecourses as $c) {
            $availablecourseslist[$c->id] = $c->shortname . ' : ' . $c->fullname;
        }

        // Forms to edit existing popular courses 
        foreach ($popularcourses as $c) {
            $mform->addElement('header','popular', get_string('popularcourse', 'block_popularcourses', $c->shortname . ' - '. $c->fullname));

            $mform->addElement('hidden', 'popular['.$c->id.'][id]', null);
            $mform->setType('popular['.$c->id.'][id]', PARAM_INT);
            $mform->setConstant('popular['.$c->id.'][id]', $c->id);

            $mform->addElement('text', 'popular['.$c->id.'][sortorder]', get_string('sortorder', 'block_popularcourses'));
            $mform->addRule('popular['.$c->id.'][sortorder]', get_string('missingsortorder', 'block_popularcourses'), 'required', null, 'client');
            $mform->setType('popular['.$c->id.'][sortorder]', PARAM_INT);
            $mform->setDefault('popular['.$c->id.'][sortorder]', $c->sortorder);

            $mform->addElement('static', 'link', get_string('deletelink', 'block_popularcourses', $CFG->wwwroot.'/blocks/popularcourses/delete_popularcourse.php?courseid='.$c->id));

        }

        // Add a new popular course
        $mform->addElement('header','add', get_string('addpopularcourse', 'block_popularcourses'));

        $mform->addElement('checkbox', 'doadd', get_string('doadd', 'block_popularcourses'));

        $mform->addElement('select', 'newpopular[courseid]', get_string('courseid', 'block_popularcourses'), $availablecourseslist);
        $mform->addRule('newpopular[courseid]', get_string('missingcourseid', 'block_popularcourses'), 'required', null, 'client');
        $mform->disabledIf('newpopular[courseid]', 'doadd', 'notchecked');

        $mform->addElement('text', 'newpopular[sortorder]', get_string('sortorder', 'block_popularcourses'));
        $mform->addRule('newpopular[sortorder]', get_string('missingsortorder', 'block_popularcourses'), 'required', null, 'client');
        $mform->setType('newpopular[sortorder]', PARAM_INT);
        $mform->disabledIf('newpopular[sortorder]', 'doadd', 'notchecked');

        $mform->addElement('submit', 'save', get_string('savechanges'));

        $mform->closeHeaderBefore('save');
    }
}
