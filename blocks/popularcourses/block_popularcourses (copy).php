<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Newblock block caps.
 *
 * @package    block_popularcourses
 * @copyright  Daniel Neis <danielneis@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class block_popularcourses extends block_base {

 
    function init() {
         
        $this->title = get_string('pluginname', 'block_popularcourses');
        
    }
	
    function get_content() {
        global $CFG, $OUTPUT, $PAGE;
		
		//$PAGE->requires->css('/blocks/popularcourses/css/ihover.min.css',false);

        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';
        $this->content->text = '';
        $this->content->text = '<div class="container-fluid-no">';

        // user/index.php expect course context, so get one if page has module context.
        /* $currentcontext = $this->page->context->get_course_context(false);

        if (empty($currentcontext)) {
            return $this->content;
        } */
        //if ($this->page->course->id >= SITEID) {
            $courses = self::get_popular_courses();
            require_once($CFG->libdir. '/coursecatlib.php');
            $chelper = new coursecat_helper();
			$this->content->text .= '<div>';
			$this->content->text .= '<h4 id="">Popular Courses</h4>';
			$this->content->text .= '<span class="popularline"><span></span></span>';
			$this->content->text .= '</div>';
			$this->content->text .= '<div class="popular-slick">';
            foreach ($courses as $course) {

                $course = new course_in_list($course);

                $this->content->text .= '<div class="coursebox">';

                $content = '';
				$bookicon = '<i class="fa fa-book fa-5"></i>';
                // course name
                $coursename = $chelper->get_course_formatted_name($course);
                $coursenamelink = $bookicon.html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),
                                                    $coursename, array('class' => $course->visible ? '' : 'dimmed'));
                $content .= html_writer::tag('div', $coursenamelink, array('class' => 'coursename'));

                // display course overview files
                $contentimages = $contentfiles = '';
				
				//$contentimages .='<div class="firstdiv">'; // Mihir
				
                foreach ($course->get_course_overviewfiles() as $file) {
                    $isimage = $file->is_valid_image();
                    $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                            '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                            $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                    if ($isimage) {
                        $contentimages .= html_writer::tag('div',
                               // html_writer::empty_tag('img', array('src' => $url, 'style' => 'max-height: 150px')),
                                html_writer::empty_tag('img', array('src' => $url, 'style' => 'max-height:40px; max-width:40px')),
                                array('class' => 'courseimage crsfeature'));
                    } else {
                        $image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
                        $filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
                                html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
                        $contentfiles .= html_writer::tag('span',
                                html_writer::link($url, $filename),
                                array('class' => 'coursefile fp-filename-icon'));
                    }
                }

				

                //$content .= $contentimages. $contentfiles. $contentdate. $contentmore;

                $this->content->text .= $content. '</div>';
            }
			
			$this->content->text .= '</div>'; // for slider
			
			
        //}
		$this->content->text .= '</div>'; // for container fluid on top

        return $this->content;
    }

    // my moodle can only have SITEID and it's redundant here, so take it away
    public function applicable_formats() {
        return array('all' => true,
                     'site' => true,
                     'user-profile' => true,
                     'site-index' => true);
    }

    public function instance_allow_multiple() {
          return false;
    }

    function has_config() {return true;}

    public function cron() {
        return true;
    }
	
	function hide_header() {
        return true;
    }

    static function get_popular_courses() {
       
       global $CFG,$DB;
       include_once($CFG->dirroot.'/my/classes/site.php');
       return (new \my\site($DB))->get_popular_courses();

    }


    static function delete_popularcourse($courseid) {
        global $DB;
        return $DB->delete_records('block_popularcourses', array('courseid' => $courseid));
    }
}
