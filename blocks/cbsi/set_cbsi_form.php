<?php

//moodleform is defined in formslib.php
require_once("$CFG->libdir/formslib.php");
require_once("$CFG->dirroot/theme/lambda/msulib.php");
require_once("$CFG->dirroot/lib/datalib.php");

class set_cbsi_form extends moodleform {
    //Add elements to form
    public function definition() {
        global $DB, $CFG;

        $mform = $this->_form; // Don't forget the underscore!

        $html = <<<HTML

            <h2>Edit Course</h2>
            <p>Change the end date of a course, this can prevent a course from moving from the Upcoming to Completed on the users Home Page.</p>
HTML;
        $mform->addElement('html', $html);
/*
 * not showing this portion of the form because not ready for clients to change data via this form
 * needs to be "user-proofed"

        $mform->addElement('text', 'courseid', 'Course ID');
        $mform->setType('courseid', PARAM_NOTAGS);
        $mform->setDefault('courseid', $this->_customdata['courseid']);

        $mform->addElement('text', 'course_creator', 'Course Creator');
        $mform->setType('course_creator', PARAM_NOTAGS);
        $mform->setDefault('course_creator', '');

        $mform->addElement('text', 'course_category', 'Course Category', $mform->data->course_category);
        $mform->setType('course_category', PARAM_NOTAGS);
        $mform->setDefault('course_category', $this->_customdata['categoryid']);

        $mform->addElement('text', 'shortname', 'Shortname');
        $mform->setType('shortname', PARAM_NOTAGS);
        //$mform->setDefault('text', $this->_customdata['shortname']);
        $mform->setDefault('shortname', $this->_customdata['default_shortname']);

        // By default Moodle stores the course start date in the mdl_course table. If you decide to change the course
        // start date with this custom form, then you'll need to update that entry as well for consistency. Either manually // via a function or via a '\event\course_updated' event trigger
        $mform->addElement('date_time_selector', 'start_datetime', 'Start Date'); // Add elements to your form
        $mform->setType('start_datetime', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('start_datetime', 'Set Start Date');        //Default value
*/
        $mform->addElement('date_time_selector', 'end_datetime', 'End Date'); // Add elements to your form
        $mform->setType('end_datetime', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('end_datetime', 'Set End Date');        //Default value
        
        $mform->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);
        
        $this->add_action_buttons();
     
    }
    //Custom validation should be added here
    function validation($data, $files) {
        return array();
    }
}
