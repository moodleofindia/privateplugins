<?php

require_once('../../config.php');
require_once('lib.php');

require_login();
global $DB;

//echo $_POST['courseid'];
//die(__FILE__.":".__LINE__);

//get course id
$courseid         = $_POST['courseid'];
//get course object
$course           = get_course($courseid);
//update course object to shared category
$course->category = 4;
//update course in database
//set response type to JSON for easy jQuery parsing
header('Content-Type: application/json');
try {
    $DB->update_record('course', $course);
} catch (Exception $e) {
    echo "Unable to move course to Shared category.";
}
