<?php
require_once('../../config.php');
global $CFG;
require_once($CFG->dirroot.'/blocks/cbsi/lib.php');
//require_login();

$users    = $_POST['users'];
$courseid = $_POST['courseid'];

// $users    = array($_POST['users']); //debug value
// $courseid = 657;                    //debug value

$failed_enrollment = array();

//$contextid = context_course::instance($courseid);

foreach($users as $userid) {
    if(!enroll_user($userid,$courseid)) {
        array_push($failed_enrollment, $userid);
    }
}

header('Content-Type: application/json');

if(count($failed_enrollment) === 0) {
    echo json_encode($failed_enrollment);
} else {
    $html = "The following user IDs could not be registered for this course.\n\n";

    foreach($failed_enrollment as $f) {
       $html .= "$f\n";
    }
    echo json_encode($html);
}
