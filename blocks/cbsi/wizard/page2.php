<form action="/local/metrostarwizard/index.php" method="post" name="pagetwo" id="pagetwo">
<h2><?php echo get_string("page_two_question", "block_cbsi");?></h2>
<div class="wizardChoices">
    <div class="wizardChoice" id="meeting">
        <p><?php echo get_string("meeting", "block_cbsi");?></p>
    </div><!-- end wizardChoice -->
    <div class="wizardChoice" id="training">
        <p><?php echo get_string("training", "block_cbsi");?></p>
    </div><!-- end wizardChoice -->
</div><!-- end wizardChoices -->
</form>
