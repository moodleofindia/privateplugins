<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block displaying information about current logged-in user.
 *
 * This block can be used as anti cheating measure, you
 * can easily check the logged-in user matches the person
 * operating the computer.
 *
 * @package    block
 * @subpackage msuprofile
 * @copyright  2010 Remote-Learner.net
 * @author     Olav Jordan <olav.jordan@remote-learner.ca>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

//include_once($CFG->dirroot.'/theme/msu/lib.php');

/**
 * Displays the current user's profile information.
 *
 * @copyright  2010 Remote-Learner.net
 * @author     Olav Jordan <olav.jordan@remote-learner.ca>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_cbsi extends block_base {
    /**
     * block initializations
     */
    public function init() {
        $this->title   = get_string('pluginname', 'block_cbsi');
        // $this->check_cohort();
        // $this->check_teacher_ou();
    }


    public function get_required_javascript() {
      $this->page->requires->jquery_plugin('cbsi-datatables', 'block_cbsi', true);
    }

    /**
     * block contents
     *
     * @return object
     */
    public function get_content() {
        global $USER,$CFG,$DB;

        //make profile the default renderer
        $name = '';

        if(!isset($_GET['name']))
        {
            $name = 'home'; //renderer name
        }

        else
        {
            $name = $_GET['name'];
        }

        $id = '';

        if(!isset($_GET['id']))
        {
            $id = 0;
        }

        else
        {
            $id = $_GET['id'];   //record id
        }

        $this->content = new stdClass();
        $this->content->text = '';
        $renderer = $this->page->get_renderer('block_cbsi');

        switch($name)
        {
    	    case "wizard":
    		      $this->content->text .= $renderer->course_creation_wizard();
              break;
    	    case "xyz":
    		      $this->content->text .= $renderer->teacher_page();
    		      break;
          case "training":
              $this->content->text .= $renderer->trainings();
              break;
          case "meeting":
              $this->content->text .= $renderer->meetings();
              break;
          case "edit":
              $this->content->text .= $renderer->edit($id);
              break;
          case "create":
              $this->content->text .= $renderer->create();
              break;
          case "delete":
              $this->content->text .= $renderer->delete();
              break;
          case "profile":
              $this->content->text .= $renderer->profile();
              break;
          case "teacher":
              $this->content->text .= $renderer->teacher_panel();
              break;
          case "test":
              $this->content->text .= $renderer->test();
              break;
          case "helpdesk":
              $this->content->text .= $renderer->helpdesk();
              break;
          case "home":
              $this->content->text .= $renderer->home();
              break;
          case "users":
              $this->content->text .= $renderer->enroll_users_iframe();
              break;
          case "test-wizard":
              $this->content->text .= $renderer->test_wizard();
              break;
          default:
              $this->content->text .= $renderer->home();
        }

        return $this->content;

    }

    /**
     * Check if user is in Teacher organizational unit
     *
     * @param $user object
     * @return bool
     */
    //// prior to using event triggers in /var/www/html/moodle/local/cbsi to setup on first login
    // protected function check_teacher_ou() {
    //     global $USER;
    //     $uid      = $USER->id;
    //     $pid      = get_parent_id();
    //     $context  = context_coursecat::instance($pid);
    //   //TODO: use try catch blocks for proper error control
    //   //troubleshooting
    //   //set_time_limit(30);
    //   //error_reporting(E_ALL);
    //   //ini_set('error_reporting', E_ALL);
    //   //ini_set('display_errors',1);

    //   // config
    //   $ldapserver = 'cbsi-ad.cbsi-connect.net';
    //   $ldapuser   = 'moodleadmin';
    //   $ldappass   = 'Hello123!';
    //   $ldaptree   = "OU=Academies,DC=cbsi-AD,DC=cbsi-connect,DC=net";

    //   // connect
    //   $ldapconn = ldap_connect($ldapserver) or die("Could not connect to LDAP server.");

    //   if($ldapconn) {
    //     // binding to ldap server
    //     $ldapbind = ldap_bind($ldapconn, $ldapuser, $ldappass) or die ("Error trying to bind: ".ldap_error($ldapconn));
    //     // verify binding
    //     if ($ldapbind) {
    //       //debug
    //       //echo "LDAP bind successful...<br /><br />";

    //       $result = ldap_search($ldapconn,$ldaptree, "(sAMAccountName=".$USER->username.")") or die ("Error in search query: ".ldap_error($ldapconn));
    //       $data = ldap_get_entries($ldapconn, $result);

    //       if(strpos($data[0]['dn'],'ContentContributor')) {
    //         //assign user Content Contributer role
    //         role_assign(9, $uid, $context->id);
    //       } elseif (strpos($data[0]['dn'],'SystemUser')) {
    //         //assign user System User role
    //         role_assign(5, $uid, $context->id);
    //       } elseif (strpos($data[0]['dn'],'SiteAdmin')) {
    //         //assign user Site Admin role
    //         role_assign(13, $uid, $context->id);
    //       } else {
    //       //debug
    //         if(has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
    //           //echo "LDAP bind failed: See your Moodle administrator";
    //         }
    //       }
    //     }
    //   }
    // }

    //// prior to using event triggers in /var/www/html/moodle/local/cbsi to setup on first login
    // protected function check_cohort() {
    //   global $DB, $USER;

    //   $sql_cohort_members = "SELECT * FROM mdl_cohort_members WHERE userid = $USER->id";
    //   $cohort_member      = $DB->get_records_sql($sql_cohort_members);
    //   $sql_cohort         = "SELECT * FROM mdl_cohort WHERE idnumber = '" . $USER->institution . "'";
    //   $cohort             = $DB->get_record_sql($sql_cohort);
    //   $date               = new DateTime();

    //   if($cohort_member == null)
    //   {
    //     $record            = new stdClass;
    //     $record->id        = null;
    //     $record->cohortid  = $cohort->id;
    //     $record->userid    = $USER->id;
    //     $record->timeadded = $date->getTimestamp();;

    //     $DB->insert_record('cohort_members', $record, false);
    //   }
    // }

    /**
     * allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return false;
    }

    /**
     * allow more than one instance of the block on a page
     *
     * @return boolean
     */
    public function instance_allow_multiple() {
        //allow more than one instance on a page
        return false;
    }

    /**
     * allow instances to have their own configuration
     *
     * @return boolean
     */
    function instance_allow_config() {
        //allow instances to have their own configuration
        return false;
    }

    /**
     * instance specialisations (must have instance allow config true)
     *
     */
    public function specialization() {
    }

    /**
     * displays instance configuration form
     *
     * @return boolean
     */
    function instance_config_print() {
        return false;

        /*
        global $CFG;

        $form = new block_msuprofile.phpConfigForm(null, array($this->config));
        $form->display();

        return true;
        */
    }

    /**
     * locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
        return array('all'=>true);
    }

    /**
     * post install configurations
     *
     */
    public function after_install() {
    }

    /**
     * post delete configurations
     *
     */
    public function before_delete() {
    }

}
