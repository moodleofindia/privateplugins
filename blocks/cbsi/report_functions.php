<?php
/**
 * Number of law enforcement officers trained via CBSI Connect,
 * total and disaggregated as outlined below:
 * By gender, and
 * By partner nation.
 *
 * mdl_user_institution gets academy
 * @return string json
 */
  function totalOfficersTrainedByGender()
  {
    global $DB;
    $sql = <<<SQL
        SELECT mdl_user.id, mdl_user.institution, mdl_user_info_data.data FROM mdl_user
        INNER JOIN mdl_user_info_data ON mdl_user.id = mdl_user_info_data.userid
        WHERE mdl_user_info_data.data = "Male" OR mdl_user_info_data.data = "Female"
SQL;

    $results = $DB->get_records_sql($sql);

    return json_encode($results);
  }

/**
 * Number of law enforcement officials trained with USG assistance to serve as a local CBSI Connect system administrator,
 * total and disaggregated as outlined below:
 * By gender, and
 * By partner nation.
 */
function siteAdminsTrainedByGender()
{
  global $DB;
  $sql = <<<SQL
      SELECT mdl_user.id, mdl_user.institution, mdl_user_info_data.data FROM mdl_user
      INNER JOIN mdl_user_info_data ON mdl_user.id = mdl_user_info_data.userid
      INNER JOIN mdl_role_assignments ON mdl_user.id = mdl_user.id = mdl_role_assignments.userid
      WHERE mdl_user_info_data.data = "Male" OR mdl_user_info_data.data = "Female"
      AND mdl_role_assignments.roleid = 13
SQL;

  $results = $DB->get_records_sql($sql);

  return json_encode($results);
}

/**
 * Number of law enforcement officers trained by a CBSI partner nation instructor(s) through the CBSI Connect system, total and disaggregated as outlined below:
 * By gender, and
 * By partner nation (of trainee).
 */
function instructorsTrainedByGender()
{
  global $DB;
  $sql = <<<SQL
      SELECT mdl_user.id, mdl_user.institution, mdl_user_info_data.data FROM mdl_user
      INNER JOIN mdl_user_info_data ON mdl_user.id = mdl_user_info_data.userid
      INNER JOIN mdl_role_assignments ON mdl_user.id = mdl_user.id = mdl_role_assignments.userid
      WHERE mdl_user_info_data.data = "Male" OR mdl_user_info_data.data = "Female"
      AND mdl_role_assignments.roleid = 12
SQL;

  $results = $DB->get_records_sql($sql);

  return json_encode($results);
}

/**
 * Number of CBSI partner nation institutions utilizing the CBSI Connect system to deliver virtual training internally.
 */
function getCoursesByInstitution()
{
  global $DB;
  results = [];

  //check if top level categories have children
  //get parent categories (academies)
  $sql = <<< SQL
    SELECT id,name
    FROM mdl_course_categories
    WHERE parent = 0
SQL;

  $academies = $DB->get_records_sql($sql);

  //get number of courses for each academy
  foreach($academies as $academy)
  {
    $courses = <<< SQL
      SELECT COUNT(id)
      FROM mdl_courses
      WHERE category = {$academy->id}
SQL;
    $count = get_records_sql($courses);
    $results[$academy->name] = $count;
  }

  return json_encode($results);
}

/**
 * Number of CBSI partner nation institutions utilizing the CBSI Connect system for domestic inter-agency training and communication.
 */
/**
 * Number of CBSI partner nation institutions utilizing the CBSI Connect system for training and communication with one (or more) other country (ies).
 */

/**
 * Number of domestic/sub-regional (RSS) courses delivered virtually, disaggregated by partner institution.
 */

/**
 * Number of regional (at least one other partner nation receiving instruction) courses delivered through CBSI Connect by:
 * Regional Security System
 * Trinidad and Tobago Police Service Training Academy
 * Jamaica Constabulary Force Training Branch
 */

/**
 * Number of regional (at least one other partner nation receiving instruction) participants trained through CBSI Connect by:
 * Regional Security System
 * Trinidad and Tobago Police Service Training Academy
 * Jamaica Constabulary Force Training Branch
 */

/**
 * Number of self-enroll (text- and/or video-based) courses available to trainees in CBSI Connect virtual course libraries.
 */

/**
 * Estimated cost-savings across all countries to date.
 */

/**
 * Help desk usage, disaggregated by:
 * Number of inquiries
 * Number of successful resolutions
 */
