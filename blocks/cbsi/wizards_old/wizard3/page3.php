<div class="formalign">
    <tr>
        <td align="right" valign="top"><b>What is the title?</b></td>
        <td valign="top" align="left">
            <input id="course_name" class="courseName" name="course_name" type="text" />
        </td>
    </tr>

    <P>&nbsp;</P>

    <tr>
        <td align="right" valign="top">
            <b>What language will this be available in?</b>
        </td>
        <td valign="top" align="left">
            <select id="language" class="languageName" name="course_language">
                <option value="0">Select</option>
                <option value="1">English</option>
                <option value="2">Spanish</option>
            </select>
        </td>
    </tr>

    <P>&nbsp;</P>

    <tr>
        <td align="right" valign="top">
            <b>What is the description?</b>
        </td>
        <td valign="top" align="left">
            <textarea id="description" wrap="virtual" rows="20" cols="50" name="course_description"></textarea>
        </td>
    </tr>

    <P>&nbsp;</P>
</div><!-- end formalign -->
