<?php
$string['pluginname'] = 'Metrostar Wizard';

//exists for multiple pages
$string['exit_wizard'] = 'Exit Wizard';
$string['wizard_name'] = 'CBSI-Connect Course Creation Wizard';
$string['previous'] = 'Previous';
$string['next'] = 'Next';

//page one
$string['page_one_question'] = 'What do you want to do today?';
$string['create_new_meeting_training'] = 'Create a new meeting or training';


//page two
$string['page_two_question'] = 'What type of class do you want to create?';
$string['meeting'] = 'Meeting';
$string['training'] = 'Training';


//page three
$string['page_three_question'] = 'Do you want to make it available to other academies?';
$string['no_private_course'] = 'No:<br>Private';
$string['yes_all'] = 'Yes:<br>All Academies';
$string['yes_selected'] = 'Yes:<br>Selected Academies';


//page four
$string['course_name'] = 'What is the title?';
$string['course_language'] = 'What language will this be available in?';
$string['course_description'] = 'What is the description?';
$string['course_sections'] = 'How many sections/topics?';

//page six
//$string['page_six_question'] = 'Set the date, time, and duration of the course?';
$string['page_six_question'] = 'Set the start date of the training or meeting?';
$string['page_six_no'] = 'No:<br>Decide Later';
//$string['page_six_yes'] = 'Yes:<br> Set Dates and Time';
$string['page_six_yes'] = 'Yes:<br> Set Date';
$string['page_six_date_question'] = 'When do you want this training or meeting to begin?';


//page seven
$string['page_seven_question'] = 'Congratulations! Your training or meeting is setup. What would you like to do now?';
$string['save_go_home'] = 'Save and Go Back to Home Page';
$string['delete_go_home'] = 'Delete and Go Back to Home Page';
$string['save_start'] = 'Save and Start Setup';

?>











