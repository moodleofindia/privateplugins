<?php

require_once('../../config.php');
require_once("{$CFG->dirroot}/lib/filestorage/file_storage.php");
require_once("lib.php");
global $DB, $USER, $PAGE;

define('CLI_SCRIPT', true);
require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');

//$PAGE->set_context(context_coursecat::instance(get_parent_id()));
$PAGE->set_pagelayout('standard');
$PAGE->set_title('Test Page');
$PAGE->set_heading('Test Page');
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/test.php');
$PAGE->navbar->add('Test Page', new moodle_url($CFG->wwwroot . '/blocks/cbsi/test.php'));

echo $OUTPUT->header();
require_login();

// $context = context_coursecat::instance(get_parent_id());
// $context = context_coursecat::instance(meeting_id());
// $context = context_coursecat::instance(training_id());

// $fs = get_file_storage();
// $files = $fs->get_area_files($context->id, 'backup', 'course');
// // print_r($files);
// foreach ($files as $f) {
//     // $f is an instance of stored_file
//     echo $f->get_filename();
// }

/////////////////////

function get_courses_by_parent_category() {
  global $DB,$USER;
  $parent_id = get_parent_id();

  $sql = <<<SQL
    SELECT mdl_course.id
    FROM mdl_course
    INNER JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
    WHERE mdl_course_categories.name = "meeting"
    OR  mdl_course_categories.name   = "training"
    AND mdl_course_categories.parent = {$parent_id}
SQL;

  $results = $DB->get_records_sql($sql);

  //echo $sql;
  return $results;
}

$courses = get_courses_by_parent_category();
//   foreach($courses as $c) {
//     $context = context_course::instance($c->id);

//     $sql = <<<SQL
//       SELECT *
//       FROM mdl_files
//       WHERE contextid = {$context->id}
//       AND component = "backup"
// SQL;

//     $results = $DB->get_record_sql($sql);
//     array_push($files, $results);
//   }

// $fs = get_file_storage();
// $files = $fs->get_area_files($context->id, 'backup', 'course');
// // print_r($files);
// foreach ($files as $f) {
//     // $f is an instance of stored_file
//     echo $f->get_filename() . "<br/>";
// }
//
//
//print_r($courses);
$out = array();

foreach($courses as $c)
{
  $context = context_course::instance($c->id);
  $fs      = get_file_storage();
  $files   = $fs->get_area_files($context->id, 'backup', 'course');

  print_r($fs->get_filepath());
  die();
  foreach ($files as $f) {
      $filename =  $f->get_filename();
      $url      = moodle_url::make_file_url(
        '/pluginfile.php',
        array(
          $f->get_contextid(),
          'backup',
          'course',
          $f->get_itemid(),
          $f->get_filepath(),
          $filename
          )
      );
    $out[] = html_writer::link($url, $filename);
  }
}

$br = html_writer::empty_tag('br');
echo implode($br, $out);

// $destdir = $CFG->dataroot.'/temp/backup/'.md5($course->shortname);
// $zipfile = $CFG->dataroot.'/course_templates/folder.mbz';
// if(file_exists($zipfile))
// {
//     define('CLI_SCRIPT', true);
//     require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');
//     $packer = new zip_packer();
//     $packer->extract_to_pathname($zipfile, $destdir);

//     // Transaction
//     $transaction = $DB->start_delegated_transaction();
//     // Restore backup into course
//     $controller = new restore_controller(
//       md5($course->shortname),
//       null,
//       backup::INTERACTIVE_NO,
//       backup::MODE_SAMESITE,
//       2,
//       backup::TARGET_NEW_COURSE
//     );

//     $controller->execute_precheck();
//     $controller->execute_plan();
//     // Commit
//     $transaction->allow_commit();
//     define('CLI_SCRIPT', false);

//     $mods = unserialize($course->modinfo);
//     //here you can edit and resource or activity

//     rebuild_course_cache($course->id, true);
// }
