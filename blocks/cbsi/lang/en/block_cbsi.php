<?php

$string['pluginname']           = "CBSI";
$string['page_header']          = "Click on the course title link to edit the course.";

$string['checklist_disclaimer'] = "The checklist is just a reminder tool. Items are checked if they have been set at least once. There is no way to know if the course creator fully or correctly completed the items at the time they were set.";
$string['checklist_title']      = "Checklist";

$string['view_grade']         = "View Grade";
$string['view_grade_desc']     = "View the course grade.";

$string['view_course']         = "View Course";
$string['view_course_copy']     = "View the course you are creating to see what your students will see when the course is completed.";

$string['edit_course']          = "Edit Course";
$string['edit_course_copy']     = "Use these settings to change the full name of the course, the short name used for naviagation and uniqueness, the date, time, format and other overall settings for yours course.";

$string['edit_category']          = "Edit Category";
$string['edit_category_copy']     = "Use these settings to change the category of the course and other settings for your course.";

$string['editing_toggle']       = "Turn Editing On/Off";
$string['editing_toggle_copy']  = "Use this feature to make changes to each section ofyour class including descriptions, pictures, and adding interactive activites and linked resources to the course.";

$string['enrollment_methods']   = "Choose an Enrollment Method";
$string['enrollment_methods_copy']   = "Choose if you would like attendees to enroll themselves through self-enrollment or if a facilitator will enroll the students manually.";

$string['add_user']             = "Add User";
$string['add_user_copy']        = "Enroll users in your course.";

$string['enrol_user']             = "Enroll User";
$string['enrol_user_copy']        = "Enroll users in your course.";

$string['remove_user']          = "Remove User";
$string['remove_user_copy']     = "Remove users from your course.";

$string['assign_instructor']    = "Assign an Instructor";
$string['assign_instructor_copy']    = "Designate who will be facilitating the course (if you are the instructor you must also be enrolled in the course).";

$string['completion_criteria']       = "Set Completion Criteria";
$string['completion_criteria_copy']  = "Determine what you users will need to accomplish to complete the training. Settings include: activity completion, completion of other courses, date, enrolment duration, required course grade, manual self-completion, manual completion by others.";
$string['create_badge']          = "Create/Assign a Badge";
$string['create_badge_copy']     = "Badges are recognition for completing a training; badges are typically not associated with meetings. Use these settings to assign a badge to students who successfully fulfill the completion criteria.";

$string['delete_course']         = "Delete Course";
$string['delete_course_copy']    = "If you wish to delete the course, you may do so. A Site Administrator can back-up your course to removable media, copy your course, or share your course with other academies through the Open Courses area of the site.";
$string['backup_course']         = "Backup";
$string['backup_course_copy']    = "Backup a course.";
$string['restore_course']        = "Restore";
$string['restore_course_copy']   = "Restore the backup of a course.";
$string['change_shortname']      = "Change Shortname";
$string['change_shortname_copy'] = "Change the shortname of course, this will be used in the breadcrumb trail.";
$string['set_shared']            = "Share in Open Courses";
$string['set_shared_copy']       = "Share in Open Courses";
$string['unset_shared']          = "Remove from Open Courses";
$string['unset_shared_copy']     = "Remove from Open Courses";
$string['edit_off']       = "Edit Off";
$string['edit_off_copy']  = "Turn off course editing.";
$string['edit_on']        = "Edit On";
$string['edit_on_copy']   = "Turn on course editing.";
$string['attend_meeting'] = "The following is a list of meetings whithin your academy. You must be enrolled in the meeting in order to attend.";


/****
 **** WIZARD
 ****/
//$string['pluginname'] = 'Metrostar Wizard';

//exists for multiple pages
$string['exit_wizard'] = 'Exit Wizard';
$string['wizard_name'] = 'CBSI-Connect Creation Wizard';
$string['previous']    = 'Previous';
$string['next']        = 'Next';

//page one
$string['page_one_question']           = 'Click Start to Create a New Meeting or Training';
$string['create_new_meeting_training'] = 'Start';


//page two
$string['page_two_question'] = 'Select Meeting or Training';
$string['meeting']           = 'Meeting';
$string['training']          = 'Training';


//page three
$string['page_three_question'] = 'Do you want to make it available to other academies?';
$string['no_private_course']   = 'No:<br>Private';
$string['yes_all']             = 'Yes:<br>All Academies';
$string['yes_selected']        = 'Yes:<br>Selected Academies';


//page four
$string['course_name']        = 'Meeting or Training Title';
$string['course_language']    = 'Choose your language';
$string['course_description'] = 'Provide a brief description?';
$string['course_sections']    = 'How many sections/topics?';

$string['page_four_header'] = 'Enter the beginning and ending date and time for your meeting or training';
$string['page_four_question']       = 'Set the start date of the training or meeting?';
$string['page_four_start']          = 'Start';
$string['page_four_end']            = 'End';
$string['page_four_date_startdate'] = 'When do you want this training or meeting to begin?';
$string['page_four_date_starttime'] = 'Choose a start time:';
$string['page_four_date_enddate']   = 'When do you want this course to end?';
$string['page_four_date_endtime']   = 'Choose an end time:';


//last page
$string['final_page_header'] = 'Congratulations! Your training or meeting is setup. What would you like to do now?';
$string['save_go_home']      = 'Save and Go Back to Home Page';
$string['delete_go_home']    = 'Delete and Go Back to Home Page';
$string['save_start']        = 'Save and Customize';

$string['lang_select']  = "Select";
$string['lang_english'] = "English";
$string['lang_spanish'] = "Spanish";
$string['lang_dutch']   = "Dutch";

/*
 * Validation errors
 */
$string['page_two_course_type'] = "'Please select a course type!'";
$string['page_two_course_name'] = "'Please add a course name!'";
$string['page_two_startdate']   = "'Please add a start date!'";
$string['page_two_starttime']   = "'Please add a start time!'";
$string['page_two_enddate']     = "'Please add an end date!'";
$string['page_two_endtime']     = "'Please add an end time!'";

$string['need_admin'] = "'There was a problem. Please notify the site creator.'";
$string['or'] = "or";

$string['timed_out_session'] = 'Your session has timed out due to lack of activity. Please login again.';
$string['scroll_bottom'] = 'Scroll to the Bottom';
$string['scroll_top'] = 'Scroll to the Top';
$string['Attendees'] = 'Attendees';
$string['Seats Open'] = 'Seats Open';
$string['Start/Join'] = 'Start/Join';
$string['Manage'] = 'Manage';
$string['Edit'] = 'Edit';
$string['Summary'] = 'Summary';
$string['Shared'] = 'Shared';
$string['Description'] = 'Description';
$string['Joined'] = 'Joined';
$string['hours ago'] = 'hours ago';
$string['last longin'] = 'last login';
$string['at'] = 'at';
$string['Retire Instructor'] = 'Retire an Instructor';

//gab_add_more
$string['help_desk'] = 'Help Desk';
$string['instructor_not_yet_assigned'] = 'Not Yet Assigned';
$string['reunion'] = 'Meeting';
$string['capacitcion'] = 'Training';
$string['logging_in_as_viewer'] = 'Logging in as a viewer';
$string['enter_date_and_time'] = 'Enter the beginning and the ending date and time for your meeting or training';

//edit_settings
$string['content_contributor'] = 'Content Contributor';
$string['instructor'] = 'Instructor';
$string['site_admin'] = 'Site Admin';

//enroll_users
$string['filter_users_by_academy'] = 'Filter Users by Academy';
$string['selected_users'] = 'Selected Users';
$string['enroll_users'] = 'Enroll Users';
$string['clear_users'] = 'Clear Users';
$string['show_entries'] = 'Show entries';
$string['search'] = 'Search';
$string['academy'] = 'Academy';
$string['first_name'] = 'First Name';
$string['last_name'] = 'Last Name';
$string['email'] = 'E-mail';
$string['next'] = 'Next';
$string['previous'] = 'Previous';
$string['showing_x_to_x_of_x_entries'] = 'Showing x to x of x entries';


//unenroll_users
$string['unenroll_users'] = 'Unenroll Users';

//remove_instructor
$string['showing_x_to_x_of_x_entries'] = 'Showing x to x of x entries';


//add_instructor
$string['assign_instructors'] = 'Assign Instructors';

$string['meetings'] = 'Meetings';
$string['tab-meetings'] = 'Meetings';
$string['tab-trainings'] = 'Trainings';
$string['trainings'] = 'Trainings';
$string['sidebar-manage-courses'] = 'Manage Content';
$string['add-instructor'] = 'Add Instructors';
$string['remove-instructor'] = 'Remove Instructors';
