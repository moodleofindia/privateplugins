$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var categoryFilter,categoryCol,categoryArray,found;

        //creates selected checkbox array
        categoryFilter = $('.categoryFilter:checked').map(function () {
              return this.value;
            }).get();

        if(categoryFilter.length){

            categoryCol = data[0]; //filter column

            categoryArray =  $.map( categoryCol.split(','), $.trim); // splites comma seprated string into array

            // finding array intersection
            found = $(categoryArray).not($(categoryArray).not(categoryFilter)).length;

            if(found == 0){
                return false;
            }
            else{
                return true;
            }
        }
        // default no filter
        return true;
    }
);

$(document).ready(function() {
    $('#selected-users > #user-names').css({'height' : $('#filters > #academy-names').height()});

    var users = $('#users').DataTable();
    var selected_users = [];

    $('.categoryFilter').click(function(){
        users.draw();
    });

    $('#users tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

        var academy     = $(this).find('td:eq(0)').text();
        var first_name  = $(this).find('td:eq(1)').text();
        var last_name   = $(this).find('td:eq(2)').text();
        var email       = $(this).find('td:eq(3)').text();
        var userid      = $(this).attr('id');

        if($("#user-names > #" + userid).length != 0) {
            //remove user from select user list if already there
            $("#user-names > #" + userid).remove();
        } else {
            //add user to selected users list
            $('#user-names').append("<li id='" + userid + "'>" + first_name + " " + last_name + " (" + email + ")</li>");
        }
    });

    //remove user name from selected users list
    $("#user-names").on("dblclick", "li", function() {
        $(this).remove();
    });

    //get selected users and call add user service to enroll them
    $('#enroll_users').click(function() {});

    $("#clear_users").click(function() {
        $('#user-names').find("li").remove(); //clear user names in selected users list
        $('#users').find("tr").removeClass('selected'); //clear select rows in users table
    });
});
