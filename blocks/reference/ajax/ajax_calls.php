<?php
// This file is part of MoodleofIndia - http://moodleofindia.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Receive ajax calls and perform operation.
 *
 * @package    block_reference
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@page http://www.moodleofindia.com}
 */
require_once('./../../../config.php');
define(AJAX_SCRIPT, true);
if (!isloggedin()) {
    echo json_encode(['message' => 'Unauthoriesd access']);
    exit();
}
header('Content-Type:application\json');
$mode = trim(optional_param('mode', false, PARAM_RAW));
if (!empty($mode)) {
    try {
        require_once('../class/reference.php');
        global $DB, $CFG, $USER;
        $ref = new reference($DB, $CFG, $USER);
        $stdobj = new stdClass();
        switch ($mode) {
            case 'ADD_CATEGORY':
                echo $ref->add_category(optional_param('category', false, PARAM_RAW));
                break;
            case 'UPDATE_CATEGORY':
                $categoryid = optional_param('catid', false, PARAM_INT);
                $category = optional_param('category', false, PARAM_RAW);
                echo $ref->update_category($categoryid, $category);
                break;
            case 'DELETE_REFERENCE':
                $refid = optional_param('refid', false, PARAM_INT);
                echo $ref->delete_reference($refid);
                break;
            case 'DELETE_CATEGORY':
                $catid = optional_param('catid', false, PARAM_INT);
                echo $ref->delete_category($catid);
                break;
            case 'ADD_REFERENCE':
                $stdobj->title = optional_param('title', false, PARAM_RAW);
                $stdobj->categoryid = optional_param('catid', false, PARAM_INT);
                $stdobj->url = optional_param('url', false, PARAM_URL);
                $stdobj->createdtime = time();
                $stdobj->userid = $USER->id;
                echo $ref->add_reference($stdobj);
                break;
            case 'GET_REFERENCE':
                echo json_encode($ref->get_references_json(optional_param('refid', false, PARAM_INT)));
                break;
            case 'EDIT_REFERENCE':
                echo json_encode($ref->edit_reference_data(optional_param('refid', false, PARAM_INT)));
                break;
            case 'GET_CATEGORY':
                echo json_encode($ref->get_category_json(optional_param('catid', false, PARAM_INT)));
                break;
            case 'CATEGORY_LIST':
                echo json_encode($ref->get_category());
                break;
            case 'UPDATE_REFERENCE':
                $stdobj->id = optional_param('refid', false, PARAM_RAW);
                $stdobj->title = optional_param('title', false, PARAM_RAW);
                $stdobj->categoryid = optional_param('catid', false, PARAM_INT);
                $stdobj->url = optional_param('url', false, PARAM_URL);
                $stdobj->modifiedtime = time();
                $stdobj->userid = $USER->id;
                echo $ref->update_reference($stdobj);
                break;
            default :
                echo json_encode(['status'  => false,
                    'message' => get_string('requstinvalid', 'block_reference')]);
                break;
        }
    } catch (Exception $ex) {
        echo json_encode(['status'  => false,
            'message' => $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine()]);
    }
}