<?php
// This file is part of MoodleofIndia - http://moodleofindia.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Classes to enforce the various operation on reference block .
 *
 * @package    block_reference
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@page http://www.moodleofindia.com}
 */

class reference {

    private $db;
    private $cfg;
    private $user;
    private $context;
    protected $lang = 'block_reference';

    public function __construct($DB = null, $CFG = null, $USER = null, $context = null) {
        try {
            $this->db = $DB;
            $this->cfg = $CFG;
            $this->user = $USER;
            $this->context = $context;
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function add_reference(stdClass $obj = null) {
        try {
            if ($obj == null) {
                throw new Exception('Insert data error');
            }
            $insertid = $this->db->insert_record('cli_reference', $obj);
            if ($insertid) {
                return json_encode(['status' => true,'id'=>$insertid,'message' => get_string('addedreference', $this->lang)]);
            } else {
                return json_encode(['status' => true, 'message' => get_string('addedreferenceerror', $this->lang)]);
            }
        } catch (Exception $ex) {
            $message = get_string('exaddref', $this->lang).$ex->getMessage().'Location'.$ex->getFile().'/'.$ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }

    public function update_reference($dataobject) {
        $flag = $this->db->update_record('cli_reference', $dataobject);
        if ($flag) {
            return json_encode(['status' => true, 'message' => get_string('referenceupdated', $this->lang)]);
        } else {
            return json_encode(['status' => false, 'message' => get_string('referenceupdatedproblem', $this->lang)]);
        }
    }

    public function delete_reference($referenceid) {
        try {
            $flag = $this->db->get_recordset('cli_reference', array('id' => $referenceid, 'userid' => $this->user->id));
            if ($flag) {
                $this->db->delete_records('cli_reference', array('id' => $referenceid));
                return json_encode(['status' => true, 'message' => get_string('referencedeleted', $this->lang)]);
            } else {
                return json_encode(['status' => true, 'message' => get_string('referencedeletedproblem', $this->lang)]);
            }
        } catch (Exception $ex) {
             $message = get_string('exdelref', $this->lang) . $ex->getMessage() .'Location' . $ex->getFile().'/' . $ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }

    public function add_category($categoryname) {
        try {
            $std = new stdClass();
            $std->categoryname = $categoryname;
            $std->userid = $this->user->id;
            $std->createdtime = time();
            $insertid = $this->db->insert_record('cli_reference_category', $std);
            if ($insertid) {
                return json_encode(['status' => true,'id'=>$insertid, 'message' => get_string('addedcategory', $this->lang)]);
            } else {
                return json_encode(['status' => false, 'message' => get_string('addingcategoryerror', $this->lang)]);
            }
        } catch (Exception $ex) {
             $message = get_string('exaddcat', $this->lang) . $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }

    public function get_category() {
        try {
            $record = $this->db->get_records('cli_reference_category', array('userid' => $this->user->id));
            $list = array();
            foreach ($record as $row) {
                $list[$row->id] =$row->categoryname;
            }
            $response = array('status'=>false);
            if(!empty($list)){
                $response['list']=$list;
                $response['status']= true;
            }
            return $response;
        } catch (Exception $ex) {
            $message = get_string('exgetcat', $this->lang). $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine();
            return ['status' => false, 'message' => $message];
        }
    }

    public function get_references($id = null) {
        try {
            if ($id == null) {
                return $this->db->get_records('cli_reference', array('userid' => $this->user->id));
            } else {
                return $this->db->get_records('cli_reference', array('categoryid' => $id, 'userid' => $this->user->id));
            }
        } catch (Exception $ex) {
            $message = get_string('exgetref', $this->lang) . $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }

    public function get_references_json($id = null) {
        try {
            return ($id == null) ? $this->db->get_records('cli_reference') : $this->db->get_record('cli_reference', array('id' => $id));
        } catch (Exception $ex) {
             $message = get_string('exgetrefjsn', $this->lang) . $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }

    public function get_category_json($id = null) {
        global $USER;
        try {
            if($id == null) {
                return $this->db->get_records('cli_reference_category', array('userid' =>$USER->id ));
            } else {
                $this->db->get_record('cli_reference_category', array('id' => $id, 'userid' => $USER->id));
            }
        } catch (Exception $ex) {
             $message = get_string('excatjsn', $this->lang) . $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }
    public function update_category($categoryid, $category) {
        $dataobject = new stdClass();
        $dataobject->id = $categoryid;
        $dataobject->categoryname = $category;
        $dataobject->modifiedtime = time();
        $flag = $this->db->update_record('cli_reference_category', $dataobject);
        if ($flag) {
            return json_encode(['status' => true, 'message' => get_string('categoryupdated', $this->lang)]);
        } else {
            return json_encode(['status' => false, 'message' => get_string('categoryupdatedproblem', $this->lang)]);
        }
    }

    public function delete_category($categoryid) {
        try {
            if ($this->db->record_exists('cli_reference_category', array('id' => $categoryid, 'userid' => $this->user->id))) {
                $this->db->delete_records('cli_reference', array('categoryid' => $categoryid));
                $this->db->delete_records('cli_reference_category', array('id' => $categoryid, 'userid' => $this->user->id));
                return json_encode(['status' => true, 'message' => get_string('categorydeleted', $this->lang)]);
            } else {
                return json_encode(['status' => false, 'message' => get_string('categorydeletedproblem', $this->lang)]);
            }
        } catch (Exception $ex) {
            $message = get_string('exdelcat', $this->lang) . $ex->getMessage() . 'Location' . $ex->getFile() . '/' . $ex->getLine();
            return json_encode(['status' => false, 'message' => $message]);
        }
    }
    /**
     * Returns html format edit page
     * @param type $refid
     */
    public function edit_reference_data($refid) {
        global $USER;
        $categorylist = $this->db->get_records('cli_reference_category', array('userid' => $USER->id));
        $reference = $this->db->get_record('cli_reference', array('id' => $refid));
        $options = '';
        foreach ($categorylist as $category) {
            if($category->id ==$reference->categoryid ){
                $options .='<option value="'.$category->id.'" selected="selected" >'.$category->categoryname.'</option>';
            } else {
                $options .='<option value="'.$category->id.'" >'.$category->categoryname.'</option>';
            }
        }
        return array('title' => $reference->title, 'url' => $reference->url,'options' => $options);
    }
}