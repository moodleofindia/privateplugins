$(document).ready(function () {
    $("#addcategory").click(function () {
        BootstrapDialog.show({
            title: 'Add category',
            message: $('<div class="error"></div><input type="text" class="form-control" style="height:32px;width:240px"  id="cat-field" placeholder="Enter category name"/>'),
            buttons: [{
                    label: 'Submit',
                    cssClass: 'btn-primary',
                    hotkey: 13, // Enter.
                    action: function (dialogItself) {
                        var category = $("#cat-field").val();
                        if (category === '') {
                            $(".error").html('<div class="alert alert-warning">Please enter category name</div>');
                            return;
                        }
                        if (!category.match(/^[\w\-\s]+$/)) {
                            $(".error").html('<div class="alert alert-error">Please enter only alphabet character</div>');
                            return;
                        }
                        var mode = 'ADD_CATEGORY';
                        $.post(refconfig.url, {mode: mode, category: category}, function (json) {
                            if (json.status) {
                                $(".ref-main-list").append('<li class="contains_branch" id="c' + json.id + '" aria-expanded="true"><p class="tree_item branch" data-loaded="0">' + category + '<a onclick="deleteCategory(' + json.id + ')"><i class="icon icon-trash pull-right"></i></a><a onclick="editCategory(' + json.id + ')"><i class="icon icon-edit pull-right"></i></a></p><ul></ul></li>');
                                dialogItself.close();
                                BootstrapDialog.show({
                                    message: json.message
                                });
                            } else {
                                BootstrapDialog.show({
                                    message: json.message
                                });
                            }
                        }, 'json');
                    }
                }]
        });
    });
    $("#addreference").click(function () {
        var categorylist = '<option value=""> Select a categoty</option>';
        $.post(refconfig.url, {mode: 'CATEGORY_LIST'}, function (json) {
            if (json.status) {
                for (id in json.list) {
                    categorylist += '<option value="' + id + '">' + json.list[id] + '</option>';
                }
                BootstrapDialog.show({
                    title: 'Add reference',
                    message: $('<div class="error"></div><p><select id="cat-list" class="form-control">' + categorylist + '</select></p><p><input type="text" class="form-control" style="height:32px;"  id="ref-title" placeholder="Enter reference title"/></p><input type="url" value="' + refconfig.pageurl + '" title="' + refconfig.pageurl + '"disabled="disabled" id="ref-url" style="height:32px;width:280px" />'),
                    buttons: [{
                            label: 'Submit',
                            cssClass: 'btn-primary',
                            hotkey: 13, // Enter.
                            action: function (dialogItself) {
                                var catid = $("#cat-list").val();
                                var title = $("#ref-title").val();

                                if (catid === '') {
                                    $(".error").html('<div class="alert alert-warning">Please select category name</div>');
                                    return;
                                }

                                if (title === '') {
                                    $(".error").html('<div class="alert alert-warning">Please enter reference title</div>');
                                    return;
                                }

                                if (!title.match(/^[\w\-\s]+$/)) {
                                    $(".error").html('<div class="alert alert-error">Please enter only alphabet character</div>');
                                    return;
                                }

                                var url = refconfig.pageurl;
                                $.post(refconfig.url, {mode: 'ADD_REFERENCE', catid: catid, title: title, url: url}, function (json) {
                                    if (json.status) {
                                        $("#c" + catid + " ul").append('<li class="tree_item leaf"><a  href="' + url + '" target="_blank">' + title + '</a><a onclick="deleteReference(' + json.id + ')"><i class="icon icon-trash pull-right"></i></a> <a onclick="editReference(' + json.id + ')"><i class="icon icon-edit pull-right"></i></a></li>');
                                        dialogItself.close();
                                        BootstrapDialog.show({
                                            message: json.message
                                        });
                                    } else {
                                        $(".login-error").addClass('alert alert-warning');
                                        $(".login-error").text(json.message);
                                        console.log('not logged in');
                                    }
                                }, 'json');
                            }
                        }]
                });
            } else {
                BootstrapDialog.show({
                    message: 'Please create category first.'
                });
            }
        }, 'json');

    });
});

function updateflag() {
    var refid = document.getElementById("deleteitem").value;
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("responseText").innerHTML = JSON.parse(xmlhttp.responseText).message;
            $('#delete').modal('hide');
            $('#static').modal('show');
        }
    }
    xmlhttp.open("GET", site.url + "/blocks/reference/ajax/ajax_calls.php?action=DELETE_REFERENCE&refid=" + refid, true);
    xmlhttp.send();
}

function deleteReference(refid) {
    var bootdobj = BootstrapDialog.confirm('Are you sure you want to delete ?', function (result) {
        if (result) {
            $.post(refconfig.url, {mode: 'DELETE_REFERENCE', refid: refid}, function (json) {
                if (json.status) {
                    $('#r' + refid).remove();
                    BootstrapDialog.show({
                        message: json.message
                    });
                } else {
                    $(".login-error").addClass('alert alert-warning');
                    $(".login-error").text(json.message);
                    console.log('not logged in');
                }
            }, 'json');
        } else {
            BootstrapDialog.show({
                message: 'You have cancelled delete reference.'
            });
        }
    });
}
function editReference(refid) {

    $.post(refconfig.url, {mode: 'EDIT_REFERENCE', refid: refid}, function (catlist) {
        BootstrapDialog.show({
            title: 'Edit reference',
            message: $('<div class="error"></div><p><select id="cat-list" class="form-control">' + catlist.options + '</select></p><p><input type="text" value="' + catlist.title + '" class="form-control" style="height:32px;"  id="ref-title" placeholder="Enter reference title"/></p><input type="url" value="' + catlist.url + '" title="' + catlist.url + '"disabled="disabled" id="ref-url" style="height:32px;width:280px" />'),
            buttons: [{
                    label: 'Submit',
                    cssClass: 'btn-primary',
                    hotkey: 13, // Enter.
                    action: function (dialogItself) {
                        var catid = $("#cat-list").val();
                        var title = $("#ref-title").val();
                        var url = $("#ref-url").val();
                        if (catid === '') {
                            $(".error").html('<div class="alert alert-warning">Please select category</div>');
                            return;
                        }
                        if (!title.match(/^[\w\-\s]+$/)) {
                            $(".error").html('<div class="alert alert-error">Please enter only alphabet character</div>');
                            return;
                        }
                        var mode = 'UPDATE_REFERENCE';
                        $.post(refconfig.url, {mode: mode, refid: refid, catid: catid, title: title, url: url}, function (json) {
                            if (json.status) {
                                $("#r" + refid + " a span").empty();
                                $("#r" + refid + " a span").html(title);
                                dialogItself.close();
                                BootstrapDialog.show({
                                    message: json.message
                                });
                            } else {
                                $(".login-error").addClass('alert alert-warning');
                                $(".login-error").text(json.message);
                                console.log('not logged in');
                            }
                        }, 'json');
                    }
                }]
        });
    }, 'json');
}

function editCategory(catid) {
    $.post(refconfig.url, {mode: 'GET_CATEGORY', catid: catid}, function (json) {
        BootstrapDialog.show({
            title: 'Edit category',
            message: $('<div class="error"></div><input type="text" class="form-control" value="' + json.categoryname + '" style="height:32px;"  id="cat-field" placeholder="Enter category name"/>'),
            buttons: [{
                    label: 'Submit',
                    cssClass: 'btn-primary',
                    hotkey: 13, // Enter.
                    action: function (dialogItself) {
                        var category = $("#cat-field").val();
                        if (category === '') {
                            $(".error").html('<div class="alert alert-warning">Please enter category name</div>');
                            return;
                        }
                        if (!category.match(/^[\w\-\s]+$/)) {
                            $(".error").html('<div class="alert alert-error">Please enter only alphabet character</div>');
                            return;
                        }
                        var mode = 'UPDATE_CATEGORY';
                        $.post(refconfig.url, {mode: mode, catid: catid, category: category}, function (json) {
                            if (json.status) {
                                $("#c" + catid + " p span").empty();
                                $("#c" + catid + " p span").html(category);
                                dialogItself.close();
                                BootstrapDialog.show({
                                    message: json.message
                                });
                            } else {
                                $(".login-error").addClass('alert alert-warning');
                                $(".login-error").text(json.message);
                                console.log('not logged in');
                            }
                        }, 'json');
                    }
                }]
        });
    }, 'json');
}

function deleteCategory(catid) {
    var bootdobj = BootstrapDialog.confirm('If you delete this category, references under this category would be deleted.<br/> Are you sure you want to delete?', function (result) {
        if (result) {
            $.post(refconfig.url, {mode: 'DELETE_CATEGORY', catid: catid}, function (json) {
                if (json.status) {
                    $('#c' + catid).remove();
                    BootstrapDialog.show({
                        message: json.message
                    });
                } else {
                    BootstrapDialog.show({
                        message: json.message
                    });
                }
            }, 'json');
        } else {
            BootstrapDialog.show({
                message: 'You have cancelled delete category.'
            });
        }
    });
}