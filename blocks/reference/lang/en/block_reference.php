<?php
// This file is part of MoodleofIndia - http://moodleofindia.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file for reference block.
 *
 * @package    block_reference
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
$string['pluginname'] = 'Reference';
$string['langfile'] = 'block_reference';
$string['reference'] = 'block_reference';
$string['referencetitle'] = 'Reference';
$string['addreference'] = 'Add Reference';
$string['addedreference'] = 'Reference has been added successfully!!!';
$string['editreference'] = 'Edit Reference';
$string['deletereference'] = 'Delete Reference';
$string['insertdatabaseerror'] = 'Insert Databse error :';
$string['addcategory'] = 'Add category';
$string['categoryname'] = 'Category name';
$string['categoryselect'] = 'Category select';
$string['title'] = 'Title';
$string['url'] = 'Url';
$string['confirmdelete'] = 'Are you sure you want to delete.';
$string['invalid'] = 'Invalid operation.';
$string['requstinvalid'] = 'Request Invalid operation.';
$string['addedreferenceerror'] = 'Request Invalid operation.';
$string['referencedeleted'] = 'Reference has been deleted successfully!.';
$string['referencedeletedproblem'] = 'Reference deleted Problem!.';
$string['addingcategoryerror'] = 'Adding category problem!.';
$string['addedcategory'] = 'Category has been added successfully.';
$string['referenceupdated'] = 'Reference has been updated successfully.';
$string['referenceupdatedproblem'] = 'Reference update problem!.';
$string['categorydeleted'] = 'Category has been deleted succesfully!.';
$string['categorydeletedproblem'] = 'Category delete problem!.';
$string['categoryupdated'] = 'Category has been updated succesfully!.';
$string['categoryupdateproblem'] = 'Category update problem!.';
// Exceptions Messages.
$string['exaddcat'] = 'Exception add category :';
$string['exgetcat'] = 'Exception get category :';
$string['exgetref'] = 'Exception get reference :';
$string['exgetrefjsn'] = 'Exception get reference json :';
$string['excatjsn'] = 'Exception category json:';
$string['exdelcat'] = 'Exception delete category :';
