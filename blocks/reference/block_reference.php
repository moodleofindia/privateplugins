<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Classes to enforce the various opertion of reference.
 *
 * @package    block_reference
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
global $PAGE, $CFG,$USER;
require_once('class/reference.php');

class block_reference extends block_base {

    private $reference;
    private $lang = 'block_reference';

    public function __construct() {
        parent::__construct();
        global $DB, $CFG, $USER;
        $this->reference = new reference($DB, $CFG, $USER);
    }

    /** @var bool */
    protected $contentgenerated = false;

    /** @var bool|null */
    protected $docked = null;

    /**
     * Set the initial properties for the block
     */
    public function init() {
        $this->blockname = get_class($this);
        $this->title = get_string('referencetitle', $this->lang);
    }

    /**
     * All multiple instances of this block
     * @return bool Returns false
     */
    public function instance_allow_multiple() {
        return false;
    }

    /**
     * Set the applicable formats for this block to all
     * @return array
     */
    public function applicable_formats() {
        if (has_capability('moodle/site:config', context_system::instance())) {
            return array('all' => true);
        } else {
            return array('site' => true);
        }
    }

    public function get_content() {

        global $CFG, $PAGE, $USER;
        $contents = array();
        $count = 1;
        if ($this->contentgenerated === true) {
            return $this->content;
        }
        $PAGE->requires->jquery();
        $PAGE->requires->js('/blocks/reference/js/bootstrap.min.js', true);
        $PAGE->requires->js('/blocks/reference/js/bootstrap-dialog.min.js', true);
        $PAGE->requires->js('/blocks/reference/scripts.js');
        $PAGE->requires->css('/blocks/reference/css/bootstrap-dialog.min.css');
        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->text .= '<div class="ref-links">';
        $this->content->text .= html_writer::link('#', get_string('addcategory', $this->lang), array('data-toggle' => 'modal', 'id' => 'addcategory', 'class'=>'btn btn-primary btn-sm'));
        $this->content->text .= ' ';
        $this->content->text .= html_writer::link('#', get_string('addreference', $this->lang), array('data-toggle' => 'modal', 'id' => "addreference", 'class'=>'btn btn-primary btn-sm'));
        $this->content->text .= '</div>';
        
        $categories = $this->reference->get_category_json();
        $this->content->text .='<div class="ref-list">';
        if(is_array($categories)) {
            foreach ($categories as $category) {
                $menueditlink = '<a  onclick="editCategory(' . $category->id . ')"><i class="icon icon-edit pull-right"></i></a>';
                $menudeletelink = '<a onclick="deleteCategory(' . $category->id . ')"><i class="icon icon-trash pull-right"></i></a>';
                $menuitem = array();
                $references = $this->reference->get_references($category->id);
                foreach ($references as $reference) {
                    $link = '<a href="' . $reference->url . '" target="_blank"><span>' . $reference->title . '<span></a>';
                    $editlink = '<a  onclick="editReference(' . $reference->id . ')"><i class="icon icon-edit pull-right"></i></a>';
                    $deletelink = '<a onclick="deleteReference(' . $reference->id . ')"><i class="icon icon-trash pull-right"></i></a>';
                    $menuitem [] = html_writer::tag('li p', $link .$deletelink  . " " . $editlink, array('id'=>'r'.$reference->id,'class' => 'tree_item leaf'));
                }
                $categoryname = '<span>'.$category->categoryname.'</span>'. " " . $menudeletelink . " " . $menueditlink;
                $menu = html_writer::tag('p', $categoryname, array('class' => 'tree_item branch', 'data-loaded' => '0'));
                $menu .= html_writer::tag('ul', implode('', $menuitem));
                $listid = 'c'.$category->id;
                $contents[] = html_writer::tag('li', $menu, array('id'=>$listid,'class' => 'contains_branch', 'aria-expanded' => 'true'));
            }
        }
        $this->content->text .= html_writer::tag('ul', implode('', $contents), array('class' => 'ref-main-list'));
        
        $this->content->text .='</div>';    
        return $this->content;
    }
        
    public static function get_path() {                
        return $_SERVER['REQUEST_URI'];
    }
}
?>
<script type="text/javascript">
    var refconfig = {
                        'url': '<?php echo $CFG->wwwroot . '/blocks/reference/ajax/ajax_calls.php'; ?>',
                        'pageurl':'<?php echo block_reference::get_path();?>'
                    };
</script>
