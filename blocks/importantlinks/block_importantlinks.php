<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Important Links Block.
 *
 * @package   block_importantlinks
 * @copyright Arjun Singh <arjunsingh@elearn10.com>
 * @license   http://www.lmsofindia.com/
 */

defined('MOODLE_INTERNAL') || die();

class block_importantlinks extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_importantlinks');
    }

    function get_content() {
        global $CFG, $USER, $DB, $OUTPUT, $PAGE;
        include_once($CFG->libdir.'/accesslib.php');
        $meetid = '';$trainid = '';$tutid = '';$vlid = '';
        if ($this->content !== NULL) {
            return $this->content;
        }

        if (!isloggedin() or isguestuser()) {
            return '';      // Never useful unless you are logged in as real users
        }
        $catlist = array();
        //$displaylist = coursecat::make_categories_list('moodle/course:create');
       $categorylist = $DB->get_records('course_categories', array());
      	foreach($categorylist as $cid => $catlist) 
      	{
      	
      		if($catlist->name == "Meeting"){
      			$meetid =  $catlist->id;
      		}elseif ($catlist->name == "Training") {
      			$trainid = $catlist->id;
      		}
      	     	    
       }
       $courselist = $DB->get_records('course', array());
	      foreach($courselist as $crsid => $crslist) 
	      	{
	      	
	      		if($crslist->shortname == "Video Library"){
	      			$vlid =  $crslist->id;
	      		}elseif($crslist->shortname == "Howto") {
	      			$tutid = $crslist->id;
	      		}
	      	     	    
	       }

	       // $context = get_context_instance (CONTEXT_SYSTEM);
	        $context = CONTEXT_SYSTEM::instance();
			// $roles = get_user_roles($context);
			// print_object($roles);
			// $role = key($roles);
			// $roleid = $roles[$role]->roleid;
	  //      	echo $roleid;
	        //$roles = get_user_roles(context_course::instance(20), $USER->id);
	         //$roles = get_roles_on_exact_context($context);
	        //$roles = get_roles_used_in_context($context,$USER->id);
	         //print_object($roles);
		    
	        //if(user_has_role_assignment($USER->id,''))
	          
	    //$roleid = $DB->get_records('role', array(),'id');
	   // print_object($roleid);
        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        $this->content->text .= '<div id="imp-links" class="panel panel-default"> 
	 		<div class="panel-body">
				<div class="content">
						<h3 class="sectionname  ">
							<a href="'.$CFG->wwwroot.'/course/index.php?categoryid='.$meetid.'">
								<i class="fa fa-bookmark-o"></i><span> Academy Meetings</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>
				<div class="content">
						<h3 class="sectionname  ">
							<a href="'.$CFG->wwwroot.'/course/index.php">
								<i class="fa fa-bookmark-o"></i><span> Academy Training</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>
				<div class="content">
						<h3 class="sectionname  ">
							<a href="'.$CFG->wwwroot.'/course/index.php?categoryid=1">
								<i class="fa fa-bookmark-o"></i><span> Open Courses</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>
				<div class="content">
						<h3 class="sectionname  ">
						<a href="http://cbsi-connect.net/helpdesk/" target="_blank">
								<i class="fa fa-bookmark-o"></i><span> Help</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>
				<div class="content">
						<h3 class="sectionname  ">
						<a href="'.$CFG->wwwroot.'/course/view.php?id='.$tutid.'">
									<i class="fa fa-bookmark-o"></i><span> Tutorials</span>
								</a>
						</h3>
					<div class="summary">
					</div>
				</div>';
if(is_siteadmin() || user_has_role_assignment($USER->id,'2') || user_has_role_assignment($USER->id,'3') || user_has_role_assignment($USER->id,'4')) 	
	        {
				$this->content->text .= '<div class="content">
						<h3 class="sectionname  ">
							<a href="'.$CFG->wwwroot.'/local/coursecreation_wizard/coursecreation.php">
								<i class="fa fa-bookmark-o"></i><span> Wizard</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>
				<div class="content">
						<h3 class="sectionname  ">
							<a href="'.$CFG->wwwroot.'/blocks/cbsi/manage_content.php">
								<i class="fa fa-bookmark-o"></i><span> Manage Content</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>';
			}
				$this->content->text .= '<div class="content">
						<h3 class="sectionname  ">
						<a href="'.$CFG->wwwroot.'/course/view.php?id='.$vlid.'">
								<i class="fa fa-bookmark-o"></i><span> Video Library</span>
							</a>
						</h3>
					<div class="summary">
					</div>
				</div>
			</div>
		</div>';

      return $this->content;
    }

    public function applicable_formats() {
        return array('all' => true,
                     'site' => true,
                     'site-index' => true,
                     'course-view' => true, 
                     'course-view-social' => true,
                     'mod' => true, 
                     'mod-quiz' => true);
    }


    public function instance_allow_multiple() {
          return true;
    }

    function has_config() {return false;}

    public function cron() {
            mtrace( "Hey, my cron script is running" );
             
                      return true;
    }



}
