<?php

class block_viewpost extends block_base {

    public function init() {
        $this->title = get_string('viewpost', 'block_viewpost');
    }
function has_config() {return true;}
    public function get_content() {
        global $DB;

        if (!empty($this->config->text)) {

            $this->content->text = $this->config->text;
        }

        if ($data = $DB->get_records_sql('SELECT id, title,author,(FROM_UNIXTIME(publicationdate,"%d-%m-%Y")) as publicationdate  FROM {latest_post}')) {
            $this->content = new stdClass;
            $this->content->text = '<div class="col-md-4 abc">';
            $this->content->text .= '<div class="panel panel-default">';
            $this->content->text .= '<div class="panel-body">';
            $this->content->text .= '<div class="row">';
            $this->content->text .= '<div class="col-xs-12">';
            $this->content->text .= '<ul id="demo">';
            foreach ($data as $latestpost) {
                $pageurl = new moodle_url('/blocks/viewpost/view.php', array('id' => $latestpost->id));
                $this->content->text .= html_writer::start_tag('li', array('class' => 'list'));
                $this->content->text .= html_writer::link($pageurl, $latestpost->publicationdate);
                $this->content->text .= html_writer::link($pageurl, '<br>' . $latestpost->title);
                $this->content->text .= html_writer::end_tag('li');
            }
            $this->content->text .= '<ul>';
            $this->content->text .='</div>';
            $this->content->text .='</div>';
            $this->content->text .='</div>';
            $this->content->text .='</div>';
            $this->content->text .='</div>';
            return $this->content;
        }
    }

}
?>
<?php
$post = get_config('block_viewpost', 'postdisplaysetting');
//print_object($post);
?>
<link href='<?php echo $CFG->wwwroot . "/blocks/viewpost/css/bootstrap.min.css" ?>' rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap-theme.min.css">
<link href='<?php echo $CFG->wwwroot . "/blocks/viewpost/css/site.css" ?>' rel="stylesheet" type="text/css" />
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src='<?php echo $CFG->wwwroot . "/blocks/viewpost/scripts/jquery.bootstrap.newsbox.min.js" ?>' type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#demo").bootstrapNews({
            newsPerPage: <?php echo $post; ?>,
            autoplay: true,
            pauseOnHover: true,
            navigation: false,
            direction: 'down',
            newsTickerInterval: 2500,
            onToDo: function () {

            }
        });

    });
</script>
<style>
    .abc{
        width: 100%;
        float: none;
    }


    .list{
        margin: 15px !important;
        list-style: outside none square !important;
    }

</style>