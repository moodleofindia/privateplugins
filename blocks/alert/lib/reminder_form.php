<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form to record new reminder
 *
 * @package    block_alert
 * @copyright  gtn gmbh <office@gtn-solutions.com>
 * @author       Florian Jungwirth <fjungwirth@gtn-solutions.com>
 * @ideaandconcept Gerhard Schwed <gerhard.schwed@donau-uni.ac.at>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Moodleform is defined in formslib.php.
require_once("$CFG->libdir/formslib.php");

/**
 * Reminder form
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  gtn gmbh <office@gtn-solutions.com>
 */
class reminder_form extends moodleform {
    /**
     * Add elements to form.
     * @return nothing
     */
    public function definition() {
        global $CFG, $COURSE, $DB;

        $mform = $this->_form; // Don't forget the underscore!

        // DISABLE.
        $mform->addElement('hidden', 'disable');
        $mform->setType('disable', PARAM_INT);
        $mform->setDefault('disable', 0);

        if ($this->_customdata['disable'] == 1) {
            $mform->addElement('text', 'mailssent', get_string('form_mailssent', 'block_alert'), array("disabled"));
            $mform->setType('mailssent', PARAM_INT);
        }

        // ID.
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', 0);

        $mform->addElement('header', 'nameforyourheaderelement', get_string('form_header_general', 'block_alert'));

        // TITLE.
        // Add elements to your form.
        $mform->addElement('text', 'title', get_string('form_title', 'block_alert'), array('size' => '50'));
        $mform->setType('title', PARAM_NOTAGS); // Set type of element.

        // SUBJECT.
        $mform->addElement('text', 'subject', get_string('form_subject', 'block_alert')); // Add elements to your form.
        $mform->setType('subject', PARAM_NOTAGS); // Set type of element.
        $mform->addRule('subject', null, 'required', null, 'client');
        $mform->addHelpButton('subject', 'form_subject', 'block_alert');

        // Criteria Conditions.
        $mform->addElement('header', 'nameforyourheaderelement', get_string('all_condition', 'block_alert'));
        $mform->addHelpButton('nameforyourheaderelement', 'all_condition', 'block_alert');
        // Inactive for XXX days added by sj.
        // Condition 1.
        //$mform->addElement('advcheckbox', 'condition1', get_string('form_condition1', 'block_alert'),
        //get_string('form_condition1_help', 'block_alert'), array('group' => 1), array(0, 1));
        $mform->addElement('checkbox', 'condition1', get_string('form_condition1', 'block_alert'));
        $mform->addRule('condition1', null, 'required', null, 'client');
        $mform->addHelpButton('condition1', 'form_condition1', 'block_alert');

        // Inactive for XXX days.
        
        for ($i = 1; $i <= 10; $i++) {
            $arrayName1[$i] = $i;
        }
        //$arrayName1 = array('1' => 1, '2'=> 2, '3'=> 3, '4' => 4, '5' => 5);
        $mform->addElement('select', 'inactive', get_string('form_inactive', 'block_alert'), $arrayName1);
        //$mform->setDefault('inactive', 1);
        $mform->addHelpButton('inactive', 'form_inactive', 'block_alert');

        // Get criteria for course.
        $courseid = required_param('courseid', PARAM_INT);
        $course = $DB->get_record('course', array('id' => $courseid));
        // $completion = new completion_info($course);

        $criteria = array();
        $criteria[BLOCK_ALERT_CRITERIA_NOT_COMPLETED] = get_string('form_condition2', 'block_alert');
        $criteria[BLOCK_ALERT_CRITERIA_COMPLETION] = get_string('form_condition3', 'block_alert');
        $criteria[BLOCK_ALERT_CRITERIA_ONLY_COMPLETION] = get_string('form_condition4', 'block_alert');
        $criteria[BLOCK_ALERT_CRITERIA_NOTONLY_COMPLETED] = get_string('form_condition5', 'block_alert');

        $mform->addElement('select', 'criteria', get_string('form_criteria', 'block_alert'), $criteria);
        $mform->addHelpButton('criteria', 'form_criteria', 'block_alert');

        // Mail to send how many times.
        /*for ($i=1; $i<=10; $i++) {
            $arrayName[$i] = $i;
        }*/
        $arrayName = array('1' => 1, '2'=> 2, '3'=> 3, '4' => 4, '5' => 5);
        $mform->addElement('select', 'recurrent', get_string('form_sendingmail', 'block_alert'), $arrayName);
        //$mform->setDefault('recurrent', 1);
        $mform->addHelpButton('recurrent', 'form_sendingmail', 'block_alert');

        //$mform->addElement('html',(html_writer::div("<hr>")));

        // Meaasge Text.
        $mform->addElement('header', 'nameforyourheaderelement', get_string('form_mailtostudent', 'block_alert'));
        $mform->addHelpButton('nameforyourheaderelement', 'form_mailtostudent', 'block_alert');

        $placeholder = '<a href="#" onclick="insertTextAtCursor(\'###fullname###\');return false;">Username</a> ';
        $placeholder .= '<a href="#" onclick="insertTextAtCursor(\'###usermail###\');return false;">Usermail</a> ';
        $placeholder .= '<a href="#" onclick="insertTextAtCursor(\'###coursename###\');return false;">Kursname</a>';
        $placeholder .= '<a href="#" onclick="insertTextAtCursor(\'###inactive###\');return false;">Inactive</a>';

        $placeholder = '###fullname### ###usermail### ###coursename### ###inactive###';

        $mform->addElement('html', html_writer::div(
                html_writer::div(html_writer::tag('label', get_string('form_placeholder','block_alert')), 'fitemtitle').
                html_writer::div($placeholder, 'felement ftext'), 'fitem'));

        // TEXT.
        $mform->addElement('editor', 'text', get_string('form_text', 'block_alert'), array(
            'subdirs' => 0,
            'maxbytes' => 0,
            'maxfiles' => 0,
            'changeformat' => 0,
            'context' => null,
            'noclean' => 0)); // Add elements to your form.
        $mform->addRule('text', null, 'required', null, 'client');
        $mform->addHelpButton('text', 'form_text', 'block_alert');

        $mform->addElement('header', 'nameforyourheaderelement', get_string('form_mailtoteacher', 'block_alert'));
        $mform->addHelpButton('nameforyourheaderelement', 'form_mailtoteacher', 'block_alert');

        $placeholder = '<a href="#" onclick="insertTextAtCursor(\'###coursename###\');return false;">Kursname</a> ';
        $placeholder .= '<a href="#" onclick="insertTextAtCursor(\'###users_fullname###\');return false;">
            Liste der benachrichtigten User</a> ';
        $placeholder .= '<a href="#" onclick="insertTextAtCursor(\'###usercount###\');return false;">
            Anzahl der benachrichtigten User</a>';
        $placeholder .= '<a href="#" onclick="insertTextAtCursor(\'###inactive###\');return false;">
            Inactive days</a>';
        $placeholder = '###coursename### ###users_fullname### ###usercount### ###inactive###';

        $mform->addElement('html', html_writer::div(
                html_writer::div(html_writer::tag('label', get_string('form_placeholder','block_alert')), 'fitemtitle').
                html_writer::div($placeholder, 'felement ftext'), 'fitem'));

        // TEXT.
        $mform->addElement('editor', 'text_teacher', get_string('form_text_teacher', 'block_alert'), array(
                'subdirs' => 0,
                'maxbytes' => 0,
                'maxfiles' => 0,
                'changeformat' => 0,
                'context' => null,
                'noclean' => 0)); // Add elements to your form.
        $mform->addHelpButton('text_teacher', 'form_text_teacher', 'block_alert');

        // TO_REPORTTRAINER.
        $mform->addElement('checkbox', 'to_reporttrainer', get_string('form_to_reporttrainer', 'block_alert'));
        $mform->addHelpButton('to_reporttrainer', 'form_to_reporttrainer', 'block_alert');

        // Only display buttons if form is enabled.
        if ($this->_customdata['disable'] == 0) {
            $this->add_action_buttons();
        };

        $mform->disabledIf('title', 'disable', 'neq', 0);
        $mform->disabledIf('subject', 'disable', 'neq', 0);
        $mform->disabledIf('condition1', 'disable', 'neq', 0);
        $mform->disabledIf('inactive', 'disable', 'neq', 0);
        $mform->disabledIf('recurrent', 'disable', 'neq', 0);
        $mform->disabledIf('text', 'disable', 'neq', 0);
        $mform->disabledIf('to_status', 'disable', 'neq', 0);
        $mform->disabledIf('to_reporttrainer', 'disable', 'neq', 0);
    }

    /**
     * Validation
     * @param array $data
     * @param array $files
     * @return nothing
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        $errors = array();

        return $errors;
    }
}
