<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_whoweare', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_whoweare
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['coursesummary'] = 'Who we are';
$string['whoweare:addinstance'] = 'Add a new we are block';
$string['pluginname'] = 'We are';
$string['editlink'] = '<a href="{$a}">Click here to edit or add content of whoweare block </a>';
$string['add'] = 'Add Content';
$string['topheading'] = 'Top-heading';
$string['subtopheading'] = 'Sub-topheading';
$string['img1name'] = 'Icon1-name';
$string['img2name'] = 'Icon2-name';
$string['img3name'] = 'Icon3-name';
$string['img4name'] = 'Icon4-name';
$string['img1heading'] = 'Icon1-heading';
$string['img2heading'] = 'Icon2-heading';
$string['img3heading'] = 'Icon3-heading';
$string['img4heading'] = 'Icon4-heading';
$string['img1subheading'] = 'Icon1-subheading';
$string['img2subheading'] = 'Icon2-subheading';
$string['img3subheading'] = 'Icon3-subheading';
$string['img4subheading'] = 'Icon4-heading';
$string['missingimg1subhead'] = 'Missing subheading';
$string['missingname'] = 'Missing heading';
$string['img2name_first'] = 'Second Icon';
$string['img3name_first'] = 'Third Icon';
$string['img4name_first'] = 'Fourth Icon';
$string['img1name_first'] = 'First Icon';
$string['img1name_first_help'] = 'Put the font awesome class example icon icon-Book';
$string['img2name_first_help'] = 'Put the font awesome class example icon icon-Book';
$string['img3name_first_help'] = 'Put the font awesome class example icon icon-Book';
$string['img4name_first_help'] = 'Put the font awesome class example icon icon-Book';