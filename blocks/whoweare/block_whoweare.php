<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course summary block
 *
 * @package    block_whoweare
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_whoweare extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_whoweare');
    }

    function applicable_formats() {
        return array('all' => true, 'mod' => false, 'tag' => false, 'my' => false);
    }

    function specialization() {
        if($this->page->pagetype == PAGE_COURSE_VIEW && $this->page->course->id != SITEID) {
            $this->title = get_string('coursesummary', 'block_whoweare');
        }
    }

    function get_content() {
        global $CFG, $OUTPUT;

        require_once($CFG->libdir . '/filelib.php');
		$this->page->requires->css(new moodle_url('/blocks/whoweare/styles.css'));
		

        if($this->content !== NULL) {
            return $this->content;
        }

        if (empty($this->instance)) {
            return '';
        }

        $this->content = new stdClass();
        $options = new stdClass();
        $options->noclean = true;    // Don't clean Javascripts etc
        $options->overflowdiv = true;
        $context = context_course::instance($this->page->course->id);
        //$this->page->course->summary = file_rewrite_pluginfile_urls($this->page->course->summary, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
        //$this->content->text = format_text($this->page->course->summary, $this->page->course->summaryformat, $options);
		$alldata = self::get_allblockcontent();
		if($alldata) {
			foreach($alldata as $datall) {
				$this->content->text = '<section id="parallax4" class="section-white mrgT5PC mrgB5PC" data-stellar-background-ratio="0.6" data-stellar-offset-parent="true">
					<div class="container relative">
						<div class="row">
							<div class="col-md-12">
								<div class="section-title text-center">
								
								<h4>'.$datall->topheading.'</h4>
								
								<p>'.$datall->subtopheading.'</p>
								</div><!-- end title -->
							</div><!-- end col -->
						</div><!-- end row -->
							<div class="row">
						<div id="owl-services" class="owl-custom section-container">
							<div class="service-item text-center col-md-3">
								<div class="rounded-icon">
									<i class="'.$datall->img1name.'"></i>
								</div><!-- end rounded-icon -->
								<div class="service-desc">
									<h4>'.$datall->img1heading.'</h4>
									<hr>
									<p>'.$datall->img1subheading= substr($datall->img1subheading, 0, 100).' <a href="#">Read More</a></p>
								</div><!-- end service-desc -->
							</div><!-- end item -->

							<div class="service-item text-center col-md-3">
								<div class="rounded-icon">
									<i class="'.$datall->img2name.'"></i>
								</div><!-- end rounded-icon -->
								<div class="service-desc">
									<h4>'.$datall->img2heading.'</h4>
									<hr>
									<p>'.$datall->img2subheading= substr($datall->img2subheading, 0, 100).' <a href="#">Read More</a></p>
								</div><!-- end service-desc -->
							</div><!-- end item -->

							<div class="service-item text-center col-md-3">
								<div class="rounded-icon">
									<i class="'.$datall->img3name.'"></i>
								</div><!-- end rounded-icon -->
								<div class="service-desc">
									<h4>'.$datall->img3heading.'</h4>
									<hr>
									<p>'.$datall->img3subheading= substr($datall->img3subheading, 0, 100).' <a href="#">Read More</a></p>
								</div><!-- end service-desc -->
							</div><!-- end item -->

							<div class="service-item text-center col-md-3">
								<div class="rounded-icon">
									<i class="'.$datall->img4name.'"></i>
								</div><!-- end rounded-icon -->
								<div class="service-desc">
									<h4>'.$datall->img4heading.'</h4>
									<hr>
									<p>'.$datall->img4subheading= substr($datall->img4subheading, 0, 100).' <a href="#">Read More</a></p>

								</div><!-- end service-desc -->
							</div><!-- end item -->

						</div><!-- end services -->
						</div><!-- end services -->
					</div>
				</section>';  // end of container fluid
            }  
		}
        $this->content->footer = '';

        return $this->content;
    }

    function hide_header() {
        return true;
    }

    function preferred_width() {
        return 210;
    }
   public static function get_allblockcontent() {
        global $DB;

        $sql = 'SELECT * FROM {block_whoweare}';
        return $DB->get_records_sql($sql);
    }
}


