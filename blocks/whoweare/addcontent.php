<?php

// This file is part of MoodleofIndia - http://www.moodleofindia.com/

require_once(dirname(__FILE__) . '../../../config.php');
require_once(dirname(__FILE__) . '../../../my/lib.php');
require_once('lib.php');

$editid = optional_param('editid', '', PARAM_INT);
error_reporting(0);
redirect_if_major_upgrade_required();

// TODO Add sesskey check to edit
//$edit   = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off
require_login();
$strmymoodle = get_string('add','block_whoweare');
if (isguestuser()) {  // Force them to see system default, no editing allowed
    // If guests are not allowed my moodle, send them to front page.
    if (empty($CFG->allowguestmymoodle)) {
        redirect(new moodle_url('/', array('redirect' => 0)));
    }

    $userid = NULL; 
    $USER->editing = $edit = 0;  // Just in case
    $context = context_system::instance();
    $PAGE->set_blocks_editing_capability('moodle/blocks/whoweare:configsyspages');  // unlikely :)
    $header = "$SITE->shortname: $strmymoodle (GUEST)";

} else {        // We are trying to view or edit our own My Moodle page
    $userid = $USER->id;  // Owner of the page
    $context = context_user::instance($USER->id);
    $PAGE->set_blocks_editing_capability('moodle/my:manageblocks');
    $header = "$SITE->shortname: $strmymoodle";
}
// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/blocks/whoweare/addcontent.php', $params);
$PAGE->set_pagelayout('admin');
$PAGE->set_pagetype('addcontent');
$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($header);
$PAGE->set_heading($header);
global $CFG;
echo $OUTPUT->header();
?>

<?php
require_once('addcontent_form.php');
$mform = new addcontent();
 $data = $DB->get_records('block_whoweare');
//Form processing and displaying is done here
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is present on form
} 
else if($formdata = $mform->get_data()){
		if($data){
		foreach($data as $datanew){
		$updateid = $datanew->id;
		$form_save_data= updatecontent($formdata,$updateid);
		}
         
		} else {

	 $form_save= addcontent($formdata);
	 
	 
	}
if ($form_save) {
     if(isset($form_save)) {
				if($form_save) { 
					redirect($CFG->wwwroot . "/index.php");
				}
			} 
   
		}
 if ($form_save_data) {
      
      if(isset($form_save_data)) {
				if($form_save_data) { 
					redirect($CFG->wwwroot . "/index.php");
				}
			} 
   
		}
		
} else {
 if($data){
 foreach($data as $newdata){
$mform->set_data($newdata);
}
}
$mform->display();

}

?>

<?php

echo $OUTPUT->footer();
 ?>
