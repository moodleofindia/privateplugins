<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * institution library
 *
 * @package    block_institution
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 *
 */


  function addcontent($data) {
        global $DB;
    
	$record1 = new stdClass();
	$record1->topheading   = $data->topheading;
	$record1->subtopheading  = $data->subtopheading;
    $record1->img1name   = $data->img1name;
	$record1->img2name  = $data->img2name;
    $record1->img3name  = $data->img3name;
    $record1->img4name  = $data->img4name;
    $record1->img1heading   = $data->img1heading;
	$record1->img2heading   = $data->img2heading;
    $record1->img3heading   = $data->img3heading;
	$record1->img4heading   = $data->img4heading;
     $record1->img1subheading   = $data->img1subheading;
     $record1->img2subheading   = $data->img2subheading;
     $record1->img3subheading   = $data->img3subheading;
     $record1->img4subheading   = $data->img4subheading;
    
	$instid = $DB->insert_record('block_whoweare', $record1);
   return $instid;

	
}

function updatecontent($data,$updateid) {
        global $DB; 
	$record = new stdClass();
    $record1->id = $updateid;
	$record1->topheading   = $data->topheading;
	$record1->subtopheading  = $data->subtopheading;
    $record1->img1name   = $data->img1name;
	$record1->img2name  = $data->img2name;
    $record1->img3name  = $data->img3name;
    $record1->img4name  = $data->img4name;
    $record1->img1heading   = $data->img1heading;
	$record1->img2heading   = $data->img2heading;
    $record1->img3heading   = $data->img3heading;
	$record1->img4heading   = $data->img4heading;
     $record1->img1subheading   = $data->img1subheading;
     $record1->img2subheading   = $data->img2subheading;
     $record1->img3subheading   = $data->img3subheading;
     $record1->img4subheading   = $data->img4subheading;
    
	$instid1 = $DB->update_record('block_whoweare', $record1);
   return $instid1;

	
}


   