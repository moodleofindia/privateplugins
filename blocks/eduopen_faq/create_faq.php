<?php
/**
 * @author    Shiuli Jana <shiuli@elearn10.com>
 * @package    block_eduopen_faq
 * @copyright 2015 onwards Moodle of India  {@link  http://www.moodleofindia.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '../../../config.php');
require_once(dirname(__FILE__) . '../../../my/lib.php');
require_once('lib.php');

$editid = optional_param('editid', '', PARAM_INT);
error_reporting(0);
//require_once($CFG->dirroot . '/blocks/institution/add_institution_form.php');
redirect_if_major_upgrade_required();

// TODO Add sesskey check to edit
$edit   = optional_param('edit', null, PARAM_BOOL);// Turn editing on and off

require_login();

$strmymoodle = get_string('pluginname', 'block_eduopen_faq');

if (isguestuser()) {// Force them to see system default, no editing allowed
    // If guests are not allowed my moodle, send them to front page.
    if (empty($CFG->allowguestmymoodle)) {
        redirect(new moodle_url('/', array('redirect' => 0)));
    }

    $userid = NULL;
    $USER->editing = $edit = 0;// Just in case
    $context = context_system::instance();
    $PAGE->set_blocks_editing_capability('moodle/blocks/eduopen_faq:configsyspages');// unlikely :)
    $header = "$SITE->shortname: $strmymoodle (GUEST)";

} else {// We are trying to view or edit our own My Moodle page
    $userid = $USER->id;// Owner of the page
    $context = context_user::instance($USER->id);
    $PAGE->set_blocks_editing_capability('moodle/my:manageblocks');
    $header = "$SITE->shortname: $strmymoodle";
}

// Get the My Moodle page info.  Should always return something unless the database is broken.
if (!$currentpage = my_get_page($userid, MY_PAGE_PRIVATE)) {
    print_error('mymoodlesetup');
}

if (!$currentpage->userid) {
    $context = context_system::instance();// So we even see non-sticky blocks
}

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/blocks/eduopen_faq/create_faq.php', $params);
$PAGE->set_pagelayout('admin');
$PAGE->set_pagetype('create_faq');
$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($header);
$PAGE->set_heading($header);

if (!isguestuser()) { // Skip default home page for guests
    if (get_home_page() != HOMEPAGE_MY) {
        if (optional_param('setdefaulthome', false, PARAM_BOOL)) {
            set_user_preference('user_home_page_preference', HOMEPAGE_MY);
        } else if (!empty($CFG->defaulthomepage) && $CFG->defaulthomepage == HOMEPAGE_USER) {
            $PAGE->settingsnav->get('usercurrentsettings')->add(get_string('makethismyhome'),
            new moodle_url('/my/', array('setdefaulthome' => true)), navigation_node::TYPE_SETTING);
        }
    }
}

// Toggle the editing state and switches
if ($PAGE->user_allowed_editing()) {
    if ($edit !== null) {// Editing state was specified
        $USER->editing = $edit;// Change editing state
        if (!$currentpage->userid && $edit) {
            // If we are viewing a system page as ordinary user, and the user turns
            // editing on, copy the system pages as new user pages, and get the
            // new page record
            if (!$currentpage = my_copy_page($USER->id, MY_PAGE_PRIVATE)) {
                print_error('mymoodlesetup');
            }
            $context = context_user::instance($USER->id);
            $PAGE->set_context($context);
            $PAGE->set_subpage($currentpage->id);
        }
    } else {// Editing state is in session
        if ($currentpage->userid) {// It's a page we can edit, so load from session
            if (!empty($USER->editing)) {
                $edit = 1;
            } else {
                $edit = 0;
            }
        } else {// It's a system page and they are not allowed to edit system pages
            $USER->editing = $edit = 0;// Disable editing completely, just to be safe
        }
    }

    // Add button for editing page
    $params = array('edit' => !$edit);

    if (!$currentpage->userid) {
        // viewing a system page -- let the user customise it
        $editstring = get_string('updatemymoodleon');
        $params['edit'] = 1;
    } else if (empty($edit)) {
        $editstring = get_string('updatemymoodleon');
    } else {
        $editstring = get_string('updatemymoodleoff');
    }

    $url = new moodle_url("$CFG->wwwroot/my/index.php", $params);
    $button = $OUTPUT->single_button($url, $editstring);
    $PAGE->set_button($button);

} else {
    $USER->editing = $edit = 0;
}

// HACK WARNING!  This loads up all this page's blocks in the system context
if ($currentpage->userid == 0) {
    $CFG->blockmanagerclass = 'my_syspage_block_manager';
}

//addingbreadcrumb
$PAGE->navbar->add(get_string('addfaq', 'block_eduopen_faq'), new moodle_url('/blocks/eduopen_faq/create_faq.php'));

echo $OUTPUT->header();

require_once('form/edit_form.php');

//Instantiate institution
$mform = new create_faq();

//Form processing and displaying is done here
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is present on form
} else if ($formdata = $mform->get_data()) {
    //var_dump($formdata);
    if ($formdata->updateid == '0') {
        $formsave = eduopen_faq_add($formdata);
    } else {
        $formsavedata = eduopen_faq_update($formdata);
    }
    if ($formsave) {
        ?>
        <font color="green" size="3" face="verdana"><b><?php echo get_string('insertfaq', 'block_eduopen_faq'); ?>
        </b></font><?php
        echo '<a href="view_faq.php"><button type="button" class="btn btn-warning">'
        .get_string('view', 'block_eduopen_faq').'</button></a>';
        echo '&nbsp;&nbsp;';
        echo '<a href="create_faq.php"><button type="button" class="btn btn-warning">'
        .get_string('add', 'block_eduopen_faq').'</button></a>';
    }
    if ($formsavedata) {
        ?>
        <font color="green" size="3" face="verdana"><b>
        <?php echo get_string('updatefaq', 'block_eduopen_faq');?></b></font><?php
        echo '<a href="view_faq.php"><button type="button" class="btn btn-warning">'
        .get_string('view', 'block_eduopen_faq').'</button></a>';
        echo '&nbsp;&nbsp;';
        echo '<a href="create_faq.php"><button type="button" class="btn btn-warning">'
        .get_string('add', 'block_eduopen_faq').'</button></a>';
    }
} else {
    if ($editid !== '') {
        $data = $DB->get_record('eduopen_faq_details', array('id' => $editid));
        $mform->set_data($data);
    }
    $mform->display();
}
echo $OUTPUT->footer();
?>

	
<script>
	$("#fitem_id_elementid").hide();
	$("#id_type").change(function(){
	if(document.getElementById('id_type').value == '2'){
	$("#fitem_id_elementid").show();
	}
	else{
	$("#fitem_id_elementid").hide();
	}
	});

$(".breadcrumbs-alt span").text("FAQ") 
 
</script>
