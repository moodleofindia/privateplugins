<?php

define('AJAX_SCRIPT', true);

require_once('../../config.php');
require_login();

$id = required_param('id', PARAM_INT);
$DB->delete_records('latest_post', array('id' => $id));

echo json_encode(array('status' => true, 'message' => 'succes'));