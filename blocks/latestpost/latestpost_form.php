<?php

require_once("{$CFG->libdir}/formslib.php");

class latestpost_form extends moodleform {

    function definition() {

        $mform = $this->_form;
        $mform->addElement('header', 'displayinfo', get_string('textfields', 'block_latestpost'));

        $mform->addElement('text', 'title', get_string('title', 'block_latestpost'));
        $mform->setType('title', PARAM_TEXT);
        $mform->addHelpButton('title', 'titleh', 'block_latestpost');

        $mform->addElement('text', 'author', get_string('author', 'block_latestpost'));
        $mform->setType('author', PARAM_TEXT);
        $mform->addHelpButton('author', 'authorh', 'block_latestpost');

        $mform->addElement('editor', 'abstract', get_string('abstract', 'block_latestpost'));
        $mform->setType('abstract', PARAM_RAW);
        $mform->addHelpButton('abstract', 'abstracth', 'block_latestpost');

        $mform->addElement('text', 'publisher', get_string('publisher', 'block_latestpost'));
        $mform->setType('publisher', PARAM_TEXT);
        $mform->addHelpButton('publisher', 'publisherh', 'block_latestpost');

        $mform->addElement('date_selector', 'publicationdate', get_string('publicationdate', 'block_latestpost'));
        $mform->setType('publicationdate', PARAM_TEXT);
        $mform->addHelpButton('publicationdate', 'publicationdateh', 'block_latestpost');

        $mform->addElement('text', 'url', get_string('url', 'block_latestpost'));
        $mform->setType('url', PARAM_TEXT);
        $mform->addHelpButton('url', 'urlh', 'block_latestpost');

        $mform->addElement('text', 'publicationtype', get_string('publicationtype', 'block_latestpost'));
        $mform->setType('publicationtype', PARAM_TEXT);
        $mform->addHelpButton('publicationtype', 'publicationtypeh', 'block_latestpost');

        $mform->addElement('text', 'topics', get_string('topics', 'block_latestpost'));
        $mform->setType('topics', PARAM_TEXT);
        $mform->addHelpButton('topics', 'topicsh', 'block_latestpost');

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return array();
    }

}

class webinar_form extends moodleform {

    function definition() {

        $mform = $this->_form;
        $mform->addElement('header', 'displayinfo', get_string('textfields1', 'block_latestpost'));

        $mform->addElement('text', 'title', get_string('title', 'block_latestpost'));
        $mform->setType('title', PARAM_TEXT);
        $mform->addHelpButton('title', 'titleh', 'block_latestpost');

        $mform->addElement('editor', 'abstract', get_string('abstract', 'block_latestpost'));
        $mform->setType('abstract', PARAM_RAW);
        $mform->addHelpButton('abstract', 'abstracth', 'block_latestpost');

        $mform->addElement('date_selector', 'publicationdate', get_string('publicationdate', 'block_latestpost'));
        $mform->setType('publicationdate', PARAM_TEXT);
        $mform->addHelpButton('publicationdate', 'publicationdateh', 'block_latestpost');

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return array();
    }

}

class swz_form extends moodleform {

    function definition() {

        $mform = $this->_form;
        $mform->addElement('header', 'displayinfo', get_string('textfields2', 'block_latestpost'));

        $mform->addElement('text', 'title', get_string('title', 'block_latestpost'));
        $mform->setType('title', PARAM_TEXT);
        $mform->addHelpButton('title', 'titleh', 'block_latestpost');

        $mform->addElement('editor', 'abstract', get_string('abstract', 'block_latestpost'));
        $mform->setType('abstract', PARAM_RAW);
        $mform->addHelpButton('abstract', 'abstracth', 'block_latestpost');

        $mform->addElement('date_selector', 'publicationdate', get_string('publicationdate', 'block_latestpost'));
        $mform->setType('publicationdate', PARAM_TEXT);
        $mform->addHelpButton('publicationdate', 'publicationdateh', 'block_latestpost');

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return array();
    }

}
