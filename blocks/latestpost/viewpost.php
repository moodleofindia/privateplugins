<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../../config.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('base');
$PAGE->set_title(get_string('pagetitle1', 'block_latestpost'));
$PAGE->set_heading('View post');
$PAGE->set_url($CFG->wwwroot . '/blocks/latestpost/viewpost.php');

$settingsnode = $PAGE->settingsnav;
$editurl = new moodle_url('/blocks/latestpost/viewpost.php');
$editnode = $settingsnode->add(get_string('viewpage', 'block_latestpost'),$editurl);
$editnode->make_active();
echo $OUTPUT->header();

?>
<?php

$row = $DB->get_records_sql('SELECT id,title,author,abstract,publisher,(FROM_UNIXTIME(publicationdate,"%d-%m-%Y")) as publicationdate,url,publicationtype,topics,type FROM {latest_post}');
static $i = 1;
$table = new html_table();
$table->attributes['class'] = 'table1 table table table-bordered table-striped';
$table->head = (array) get_strings(array('slno', 'title', 'author', 'abstract', 'publisher', 'publicationdate', 'url', 'publicationtype', 'topics','type','edit','delete'), 'block_latestpost');
foreach ($row as $records) {
$edit ="<a href='edit.php?id=$records->id' class='btn btn-info' role='button'>Edit</a>";
$delete =html_writer::tag('button', 'Delete', array('onclick'=>'deletePost('.$records->id.')','class'=>'btn btn-danger'));
$abstract = strip_tags($records->abstract);
$abstract = substr($abstract, 0, 120).'..';
$table->data[] = array(
        $i++,
        $records->title,
        $records->author,
        $abstract,
        $records->publisher,
        $records->publicationdate,
        $records->url,
        $records->publicationtype,
        $records->topics,
        $records->type,
        $edit,
        $delete
            
    );
    echo '<script type="text/javascript">
        function deletePost(id) {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              var json = JSON.parse(xhttp.responseText);
              if(json.status) {
                window.location.href = "'.$CFG->wwwroot.'/blocks/latestpost/viewpost.php";
              }
            }
          };
          xhttp.open("POST","delete.php?id="+id , true);
          xhttp.send();
        }
     </script>';
    
 
}

echo html_writer::table($table);
?>

<?php

echo $OUTPUT->footer();
