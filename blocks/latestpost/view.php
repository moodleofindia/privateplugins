<?php

global $DB, $OUTPUT, $PAGE;
require_once('../../config.php');
require_once('latestpost_form.php');
$params = array();
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_title(get_string('pagetitle3', 'block_latestpost'));
$PAGE->set_heading('Adding post');
$PAGE->set_url('/blocks/view.php', $params);
global $DB;
$id = optional_param('id', 0, PARAM_INT);

$settingsnode = $PAGE->settingsnav;
$editurl = new moodle_url('/blocks/latestpost/view.php');
$editnode = $settingsnode->add(get_string('editpage', 'block_latestpost'), $editurl);
$editnode->make_active();

$PAGE->set_url('/blocks/latestpost/view.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('edithtml', 'block_latestpost'));

//For latestpost form........
$simplehtml = new latestpost_form();
$mform = new latestpost_form();
if ($mform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my/"));
} else if ($fromform = $mform->get_data()) {
    global $DB;
    $latestpost = "latestpost";
    $record = new stdClass();
    $record->title = $fromform->title;
    $record->author = $fromform->author;
    $record->abstract = $fromform->abstract['text'];
    $record->publisher = $fromform->publisher;
    $record->publicationdate = $fromform->publicationdate;
    $record->url = $fromform->url;
    $record->publicationtype = $fromform->publicationtype;
    $record->topics = $fromform->topics;
    $record->type = $latestpost;
    $lastinsert = $DB->insert_record('latest_post', $record);

    if ($lastinsert) {
        $success = "<div class='alert alert-success'><strong>Data Successfully Saved!</strong></div>";
    }
    redirect(new moodle_url("$CFG->wwwroot/blocks/latestpost/viewpost.php"), $success);
}


//For swz form.........
$simplehtml = new swz_form();
$mform = new swz_form();
if ($mform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my/"));
} else if ($fromform = $mform->get_data()) {
    global $DB;
    $swzpost = "swz";
    $record = new stdClass();
    $record->title = $fromform->title;
    $record->abstract = $fromform->abstract['text'];
    $record->publicationdate = $fromform->publicationdate;
    $record->type = $swzpost;
    $lastinsert = $DB->insert_record('latest_post', $record);
    if ($lastinsert) {
        $success = "<div class='alert alert-success'><strong>Data Successfully Saved!</strong></div>";
    }
    redirect(new moodle_url("$CFG->wwwroot/blocks/latestpost/viewpost.php"), $success);
}

//For webinar form.........
$simplehtml = new webinar_form();
$mform = new webinar_form();
if ($mform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my/"));
} else if ($fromform = $mform->get_data()) {
    global $DB;
    $webinarpost = "webinar";
    $record = new stdClass();
    $record->title = $fromform->title;
    $record->abstract = $fromform->abstract['text'];
    $record->publicationdate = $fromform->publicationdate;
    $record->type = $webinarpost;
    $lastinsert = $DB->insert_record('latest_post', $record);
    if ($lastinsert) {
        $success = "<div class='alert alert-success'><strong>Data Successfully Saved!</strong></div>";
    }
    redirect(new moodle_url("$CFG->wwwroot/blocks/latestpost/viewpost.php"), $success);
}
echo $OUTPUT->header();
$url = new moodle_url($CFG->wwwroot . '/blocks/latestpost/view.php');
echo $OUTPUT->single_select($url, 'type', ['latestpost', 'swz', 'webinar'], '', array('' => '-- Select type --'), 'latestpostform', array('label' => 'Please Select type'));
$type = optional_param('type', null, PARAM_INT);
if (!is_null($type)) {
    if ($type == 0) {
        $latestpostform = new latestpost_form();
        $latestpostform->display();
    } else if ($type == 1) {
        $swzform = new swz_form();
        $swzform->display();
    } else if ($type == 2) {
        $webinarform = new webinar_form();
        $webinarform->display();
    }
}
echo $OUTPUT->footer();
?>