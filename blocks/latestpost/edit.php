<?php

require_once('../../config.php');
$id = required_param('id', PARAM_INT);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pagetitle2', 'block_latestpost'));
$PAGE->set_heading('Edit post');
$PAGE->set_url($CFG->wwwroot . '/blocks/latestpost/edit.php');

$settingsnode = $PAGE->settingsnav->add(get_string('viewpostsettings', 'block_latestpost'));
$editurl = new moodle_url('/blocks/latestpost/edit.php?id=' . $id);
$editnode = $settingsnode->add(get_string('edit', 'block_latestpost'), $editurl);
$editnode->make_active();
?>
<?php

require_once($CFG->dirroot . '/lib/moodlelib.php');
require_once($CFG->dirroot . '/blocks/latestpost/latestpost_form.php');

require_login();
//for latest post form.............
$action = $CFG->wwwroot . '/blocks/latestpost/edit.php?id=' . $id;
$latestpostform = new latestpost_form($action);

if ($latestpostform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my"));
} else if ($fromform = $latestpostform->get_data()) {
    global $DB;
    $record = new stdClass();
    $record->title = $fromform->title;
    $record->author = $fromform->author;
    $record->abstract = $fromform->abstract['text'];
    $record->publisher = $fromform->publisher;
    $record->publicationdate = $fromform->publicationdate;
    $record->url = $fromform->url;
    $record->publicationtype = $fromform->publicationtype;
    $record->topics = $fromform->topics;

    if ($check = $DB->get_record('latest_post', array('id' => $id))) {
        $record1 = new stdClass();
        $record1 = $record;
        $record1->id = $check->id;
        $update = $DB->update_record('latest_post', $record1);
        if ($update) {
            $success = "<div class='alert alert-success'><strong>Data Successfully Updated!</strong></div>";
        }
        redirect(new moodle_url("$CFG->wwwroot/blocks/latestpost/viewpost.php"), $success);
    } else {
        $lastinsert = $DB->insert_record('latest_post', $record);
    }
} else {
    $saveddata = $DB->get_record('latest_post', array('id' => $id));
    if ($saveddata) {
        $savedrecord = new stdClass();
        $savedrecord->title = $saveddata->title;
        $savedrecord->author = $saveddata->author;
        $savedrecord->abstract['text'] = $saveddata->abstract;
        $savedrecord->publisher = $saveddata->publisher;
        $savedrecord->publicationdate = $saveddata->publicationdate;
        $savedrecord->url = $saveddata->url;
        $savedrecord->publicationtype = $saveddata->publicationtype;
        $savedrecord->topics = $saveddata->topics;
        $latestpostform->set_data($savedrecord);
    }
}
//for swz post form.............
$action = $CFG->wwwroot . '/blocks/latestpost/edit.php?id=' . $id;
$swzform = new swz_form($action);

if ($swzform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my"));
} else if ($fromform = $swzform->get_data()) {

    global $DB;
    $record = new stdClass();
    $record->title = $fromform->title;
    $record->abstract = $fromform->abstract['text'];
    $record->publicationdate = $fromform->publicationdate;

    if ($check = $DB->get_record('latest_post', array('id' => $id))) {
        $record1 = new stdClass();
        $record1 = $record;
        $record1->id = $check->id;
        $update = $DB->update_record('latest_post', $record1);
        if ($update) {
            $success = "<div class='alert alert-success'><strong>Data Successfully Updated!</strong></div>";
        }
        redirect(new moodle_url("$CFG->wwwroot/blocks/latestpost/viewpost.php"), $success);
    } else {
        $lastinsert = $DB->insert_record('latest_post', $record);
    }
} else {
    $saveddataswz = $DB->get_record('latest_post', array('id' => $id));
    if ($saveddataswz) {
        $savedrecord = new stdClass();
        $savedrecord->title = $saveddataswz->title;
        $savedrecord->abstract['text'] = $saveddataswz->abstract;
        $savedrecord->publicationdate = $saveddataswz->publicationdate;
        $swzform->set_data($savedrecord);
    }
}

//for webinar post form.............
$action = $CFG->wwwroot . '/blocks/latestpost/edit.php?id=' . $id;
$webinarform = new webinar_form($action);

if ($webinarform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my"));
} else if ($fromform = $webinarform->get_data()) {

    global $DB;
    $record = new stdClass();
    $record->title = $fromform->title;
    $record->abstract = $fromform->abstract['text'];
    $record->publicationdate = $fromform->publicationdate;

    if ($check = $DB->get_record('latest_post', array('id' => $id))) {
        $record1 = new stdClass();
        $record1 = $record;
        $record1->id = $check->id;
        $update = $DB->update_record('latest_post', $record1);
        if ($update) {
            $success = "<div class='alert alert-success'><strong>Data Successfully Updated!</strong></div>";
        }
        redirect(new moodle_url("$CFG->wwwroot/blocks/latestpost/viewpost.php"), $success);
    } else {
        $lastinsert = $DB->insert_record('latest_post', $record);
    }
} else {
    $saveddatawebinar = $DB->get_record('latest_post', array('id' => $id));
    if ($saveddatawebinar) {
        $savedrecord = new stdClass();
        $savedrecord->title = $saveddatawebinar->title;
        $savedrecord->abstract['text'] = $saveddatawebinar->abstract;
        $savedrecord->publicationdate = $saveddatawebinar->publicationdate;
        $webinarform->set_data($savedrecord);
    }
}

echo $OUTPUT->header();
$saveddata = $DB->get_record('latest_post', array('id' => $id, 'type' => 'latestpost'));
$saveddataswz = $DB->get_record('latest_post', array('id' => $id, 'type' => 'swz'));
$saveddatawebinar = $DB->get_record('latest_post', array('id' => $id, 'type' => 'webinar'));
if ($saveddata) {
    $latestpostform->display();
} elseif ($saveddataswz) {
    $swzform->display();
} elseif ($saveddatawebinar) {
    $webinarform->display();
}
echo $OUTPUT->footer();
?>

