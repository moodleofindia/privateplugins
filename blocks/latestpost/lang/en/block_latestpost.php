<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package   block_latestpost
 * @copyright Siddhart Behera <siddharth@elearn10.com>
 * @license   http://lmsofindia.com
 */
$string['blockstring'] = 'Block string';
$string['pagetitle2'] = 'Edit Post';
$string['pagetitle3'] = 'Add Post';
$string['desc'] = 'Latest post';
$string['blocksettings'] = 'Blocksetting';
$string['header'] = 'Latest post';
$string['latestpost:addinstance'] = 'Add a latestpost block';
$string['latestpost:myaddinstance'] = 'Add a latestpost block to my moodle';
$string['pluginname'] = 'Latest post';
$string['latestpost'] = 'Latest post';
$string['textfields'] = 'Latest Post';
$string['textfields1'] = 'Webinar Post';
$string['textfields2'] = 'SWZ Post';
$string['editpage'] = 'Add post';
$string['edithtml'] = 'Adding Post';
$string['addpage'] = 'Add a Post';
$string['viewpage'] = 'View Post';
$string['latestpostsettings'] = 'Add post';
$string['title'] = 'Title';
$string['titleh_help'] = 'Here you give the title of the post';
$string['titleh'] = 'Title';
$string['author'] = 'Author';
$string['authorh_help'] = 'Here you give the name of the author';
$string['authorh'] = 'Author';
$string['abstract'] = 'Abstract';
$string['abstracth_help'] = 'Here you give the content of the post';
$string['abstracth'] = 'Abstract';
$string['publisher'] = 'Publisher';
$string['publisherh_help'] = 'Here you give the publisher name';
$string['publisherh'] = 'Publisher';
$string['publicationdate'] = 'Publication Date';
$string['publicationdateh_help'] = 'Here you give the publication date';
$string['publicationdateh'] = 'Publication Date';
$string['url'] = 'Url';
$string['urlh_help'] = 'Here you can give the url';
$string['urlh'] = 'Url';
$string['publicationtype'] = 'Publication Type';
$string['publicationtypeh_help'] = 'Here you give the publication type';
$string['publicationtypeh'] = 'Publication Type';
$string['topics'] = 'Topics';
$string['topicsh_help'] = 'Here you give the topics';
$string['topicsh'] = 'Topics';
$string['slno'] = 'Sl no';
$string['pagetitle1'] = 'View post';
$string['edit'] = 'Edit';
$string['delete'] = 'Delete';
$string['viewpostsettings'] = 'View Post';
$string['viewpage'] = 'View Post';
$string['edit'] = 'Edit Post';
$string['type'] = 'Type';