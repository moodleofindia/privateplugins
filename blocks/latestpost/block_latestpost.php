<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * 
 *
 * @package   block_latestpost
 * @copyright Siddhart Behera <siddharth@elearn10.com>
 * @license   http://lmsofindia.com
 */
defined('MOODLE_INTERNAL') || die();

class block_latestpost extends block_base {

    public function init() {
        $this->title = get_string('latestpost', 'block_latestpost');
    }
    
    
      function user_can_addto($page) {
        // Don't allow people to add the block if they can't even use it
        if (!has_capability('moodle/community:add', $page->context)) {
            return false;
        }

        return parent::user_can_addto($page);
    }
    
    
       function user_can_edit() {
        // Don't allow people to edit the block if they can't even use it
        if (!has_capability('moodle/community:add',
                        context::instance_by_id($this->instance->parentcontextid))) {
            return false;
        }
        return parent::user_can_edit();
    }

    public function get_content() {
        if (is_siteadmin()) {
            if ($this->content !== null) {
                return $this->content;
            }

            $this->content = new stdClass;

            $url1 = new moodle_url('/blocks/latestpost/viewpost.php');
            $url = new moodle_url('/blocks/latestpost/view.php');
            $this->content->text = html_writer::link($url, get_string('addpage', 'block_latestpost'), array('class' => 'test'));
            $this->content->footer = html_writer::link($url1, get_string('viewpage', 'block_latestpost'));

            return $this->content;
        }
    }

}
?>
<style>
    .test{
        margin-left:5px;

    }

</style>