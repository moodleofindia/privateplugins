<?php

include '../../config.php';

include 'vendor/autoload.php';
include 'Sms.php';
include 'Adaptor.php';
include 'UrlAdaptor.php';
include 'SmsService.php';
include 'GatewayProvider.php';
include 'SimpleGatewayProvider.php';

$search = required_param('q', PARAM_RAW);

$field = 'username';

if(filter_var($search,  FILTER_VALIDATE_EMAIL)) {
   $field = 'email'; 
}
try {
    // Find user for this query
    $user = $DB->get_record('user', array($field => $search), 'id, username, secret, phone1');
    
    if(empty($user->phone1)) {
        echo json_encode(array('status' => false, 'error' =>  get_string('phoneinvalid', 'auth_otplogin')));
        die();
    }
} catch (exception $e) {

}

if(!$user) {
    echo json_encode(array('status' => false, 'error' =>  get_string('usernotfound', 'auth_otplogin')));
    die();
}

// create sms
$user->secret = mt_rand(1000, 9999);

$message = urlencode($SITE->fullname." \n\n OTP code ".$user->secret." \n \n".get_string('message', 'auth_otplogin'));

$plugin = get_config('auth/otplogin');


if(empty($plugin->service) || empty($plugin->baseurl) || empty($plugin->workingkey)) {
    die(json_encode(['status' => false, 'error' => get_string('notconfig', 'auth_otplogin')]));
}

$sms = new otplogin\Sms($plugin->service, $message, array(
        array('gsm' => $user->phone1)
    ));
$adaptor = new otplogin\UrlAdaptor([
    'baseUrl' => $plugin->baseurl,
    'workingkey' => $plugin->workingkey
]);

$service = new otplogin\SmsService(new otplogin\SimpleGatewayProvider($adaptor));

$response = $service->send($sms);

if(isset($response['Error'])) {
    die(json_encode(['status' => false, 'error' => $response['Error']]));
}
try {
    // Find user for this query
    $user->timecreated = time();
    $DB->update_record('user', $user);
    echo json_encode(['status' => true, 'username' => $user->username]);
} catch (exception $e) {
    die(json_encode(['status' => false,'error' => get_string('resourceerror', 'auth_otplogin')]));
}
