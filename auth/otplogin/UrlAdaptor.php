<?php

/**
 * URL Implementation
 *
 * @package otplogin
 * @author shambhu kumar {mail:shambhu384@gmail.com}
 */


namespace otplogin;

use GuzzleHttp\Client;

/**
 * UrlAdaptor class
 */

class UrlAdaptor implements Adaptor {
    
    /**
     * @var string Base url
     */
    private $baseUrl;
    
    /**
     * @var array Query
     */
    private $query;
    
    /**
     * Default constructor
     *
     * @param array
     */
    public function __construct($urloptions = array())
    {
        if(!isset($urloptions['baseUrl'])) {
            trigger_error('Base url must be specified. key baseUrl');
        }
        $this->baseUrl = array_shift($urloptions);
        $this->query = $urloptions;
    }
    
    /**
     * Sms request 
     *
     * {@inheritdoc}
     */
    public function request(Sms $sms)
    {
        $param = array(
            'sender' => $sms->sender,
            'to' => $sms->recipients[0]['gsm'],
            'message' => $sms->text,
            'type' => 'json'
        );
        $query = '?';
        foreach(array_merge($this->query, $param) as $key => $value)
            $query .= "$key=$value&";

        return json_decode(file_get_contents($this->baseUrl.$query), true);
    }
}
