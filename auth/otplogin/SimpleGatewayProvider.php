<?php

/**
 * Simple Gateway Provider class holds
 * only one adaptor instance.
 *
 * @package otplogin
 * @author shambhu kumar {mail:shambhu384@gmail.com}
 */

namespace otplogin;

/**
 * Simple Gateway Provider
 */

class SimpleGatewayProvider extends GatewayProvider
{

    /**
     * @var Adaptor
     */
    private $adaptor;
    
    /**
     * Default constructor to initialize adaptor
     *
     * @param Adaptor
     */
    public function __construct(Adaptor $adaptor)
    {
        $this->adaptor = $adaptor; 
    }
    
    /**
     * Send SMS through adaptor
     * 
     * @param Sms
     */
    public function service(Sms $sms)
    {
        return $this->adaptor->request($sms);
    }
}
