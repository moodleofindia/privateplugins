$(window).load(
    function () {
       
        if ($("#auth_custom_location").length > 0) {
            $("#auth_custom_location").append(buttonsCodeOauth2);
        } else {
            var formObj = $("input[name='username']").closest("form");
            if (formObj.length > 0) {
                $(formObj).each(function (i, formItem) {
                    var username = $(formItem).find("input[name='username']").val();
                    var password = $(formItem).find("input[name='password']").val();
                    if(username !== "guest" || password !== "guest") {
                        $(formItem).append(buttonsCodeOauth2);
                    }
                });
            }
        }
    }
)

function generateOtpForUser() {
	var usersearch = document.getElementById("search").value;
    if(usersearch == "") {
        document.getElementById("login-error").innerHTML = '<span class="alert alert-warning alert-dismissible fade in">Username / Email can\'t empty.</span>';
    }
	var xhttp = new XMLHttpRequest();
	document.getElementById("login-error").innerHTML = "<p><img src='pix/loader.png'></img><br/>Please wait we are sending SMS on your mobile</p>";
  	xhttp.onreadystatechange = function() {
    	if (xhttp.readyState == 4 && xhttp.status == 200) {
    		var json = JSON.parse(xhttp.responseText);
    		if(json.status) {
     			document.getElementById("login-error").innerHTML = 'Please check SMS we have sent you Otp key';
     			document.getElementById("loginform").innerHTML = '<form onsubmit="return validateOtpAndLogin()" action="javascript:;" method="post" id="login-form">'+
                            '<div class="loginform">'+
                                '<div id="opt-error"></div>'+
                                '<div class="form-label"><label for="search">Otp Key</label></div>'+
                                '<div class="form-input">'+
                                    '<input type="text"  size="35" id="otp">'+
                                '</div>'+
                                '<div class="clearer"></div>'+
                                '<input type="hidden"  id="username" class="form-control" value="'+json.username+'">'+
                            '</div>'+                            
                            '<div class="clearer"></div>'+
                            '<button type="submit" id="loginbtn" class="has-spinner">'+
                            '<img src="pix/spinner.gif" id="login-process" style="display:none"/> Login </button>'+
                        '</form>'+
                        '<br/><button onclick="return resendOtpForUser()"> Resend Otp</button>'+
                        '<div id="resent-otp-message"></div>';
        	} else {
        		document.getElementById("login-error").innerHTML = '<span class="alert alert-warning alert-dismissible fade in">'+json.error+'</span>';
        	}
    	}
  	};
  	xhttp.open("GET", M.cfg.wwwroot+"/auth/otplogin/ajax.php?q="+usersearch, true);
  	xhttp.send();
  	return false;
}


function resendOtpForUser() {
	var username = document.getElementById("username").value;

    document.getElementById("resent-otp-message").setAttribute("style", "text-align:center;padding:16px");
    if(username == "") {
        document.getElementById("resent-otp-message").innerHTML = '<span class="alert alert-warning alert-dismissible fade in">Username / Email can\'t empty.</span>';
    }
    var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    	if (xhttp.readyState == 4 && xhttp.status == 200) {
    		var json = JSON.parse(xhttp.responseText);
            if(json.status) {
                document.getElementById("resent-otp-message").innerHTML = '<span class="alert alert-success alert-dismissible fade in">Please check sms we have sent you.</span>';
        	} else {
                document.getElementById("resent-otp-message").innerHTML = '<span class="alert alert-warning alert-dismissible fade in">'+json.error+'</span>';
            }
    	}
  	};
  	xhttp.open("GET", M.cfg.wwwroot+"/auth/otplogin/ajax.php?q="+username, true);
  	xhttp.send();
  	return false;
}

function validateOtpAndLogin() {
	var username = document.getElementById("username").value;
	var otp = document.getElementById("otp").value;
    if(otp == "") {
        document.getElementById("opt-error").setAttribute("style", "text-align:center;padding:16px");
        document.getElementById("opt-error").innerHTML = '<span class="alert alert-warning alert-dismissible fade in">'+json.msg+'</span>';
    }
	var xhttp = new XMLHttpRequest();
    document.getElementById("opt-error").innerHTML = '';
    document.getElementById("login-process").style.display = "inherit";
  	xhttp.onreadystatechange = function() {
    	if (xhttp.readyState == 4 && xhttp.status == 200) {
     		var json = JSON.parse(xhttp.responseText);
            document.getElementById("login-process").style.display = "none";
     		if(json.status){
     			window.location.href= M.cfg.wwwroot;
     		} else {
                document.getElementById("opt-error").setAttribute("style", "text-align:center;padding:16px");
                document.getElementById("opt-error").innerHTML = '<span class="alert alert-warning alert-dismissible fade in">'+json.msg+'</span>';
     		}
    	}
  	};
  	xhttp.open("GET", M.cfg.wwwroot+"/auth/otplogin/ajaxlogin.php?username="+username+"&otp="+otp, true);
  	xhttp.send();
	return false;
}
  
