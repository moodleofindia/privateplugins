<?php

/**
 * Sms service class Gateway provider object
 * in order to call service metho
 *
 * @package otplogin
 * @author shambhu kumar {mail:shambhu384@gmail.com}
 */

namespace otplogin;

/**
 * SmsService class
 */

class SmsService
{ 
    /**
     * @var GatewayProvider
     */
    private $provider;
    
    /**
     * Default constructor
     *
     * @param GatewayProvider
     */
    public function __construct(GatewayProvider $provider)
    {
        $this->provider = $provider;
    }
                                                      
    /**
     * Send SMS as per Gateway provider
     *
     * @param Sms
     */
    public function send(Sms $sms)
    {
        return $this->provider->service($sms);
    }
}
