<?php

include '../../config.php';

$username = required_param('username', PARAM_RAW);
$otp = required_param('otp', PARAM_INT);


$flag = false;
try {
    $user = $DB->get_record('user', array('username' => $username));
    if($user) {
        if($user->timemodified+180 < time()) {
            if($user->secret == (int)$otp) {
                $flag = true;
                complete_user_login($user);
                $msg = 'loggedin';
            } else {
                $msg = get_string('invalidtoken', 'auth_otplogin');
            }
       } else {
            $msg =  get_string('resourceerror', 'auth_otplogin');
       }
    } else {
        $msg = get_string('processerror', 'auth_otplogin');
    }
} catch (exception $ex) {

}

echo json_encode(array('status' => $flag,'msg' => $msg));
