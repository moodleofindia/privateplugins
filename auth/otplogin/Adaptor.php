<?php

namespace otplogin;

/**
 * Adapter class 
 *
 * Implement this Adaptor interface and  provide your
 * implementation like (AuthAdaptor, UrlAdaptor)
 */

interface Adaptor {

    /**
     * Send request to Authentication resources
     *
     * @params Sms
     */
    public function request(Sms $sms);
}
