<?php

/**
 * Value object class to hold sms
 * information
 *
 * @package otplogin
 * @author shambhu kumar {mail:shambhu384@gmail.com}
 */

namespace otplogin;

/**
 * Sms class
 */

class Sms extends \ArrayObject implements \JsonSerializable
{
    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $text;
    
    /**
     * @var array
     * 
     */
    private $recipients;
    
    /**
     * Default constructor
     *
     * @param int $to
     * @param string $sender
     * @param string $message
     */
    public function __construct($sender, $text, $recipients)
    {
        $this->sender = $sender;
        $this->text = $text;
        $this->recipients = $recipients;
    }

    /**
     * Objects implementing JsonSerializable can 
     * customize their JSON representation when 
     * encoded with json_encode. 
     *
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * Retrive class property
     * 
     * @param string
     */
    public function &__get($key)
    {
       return $this->$key;
    }
    
    /**
     * Cann't set property to class
     *
     * @param string
     * @param mixed
     */
    public function &__set($key, $value)
    {
        trigger_error('Can\'t set value in'. $key);
    }
}
