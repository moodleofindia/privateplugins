<?php

/**
 * Abstract Gateway Provider  contract with
 * concrete class must provide implmentation of
 * service method.
 *
 * @package otplogin
 * @author shambhu kumar {mail:shambhu384@gmail.com}
 */

namespace otplogin;

/**
 * Abstract Gateway provider
 */

abstract class GatewayProvider {
    
    /**
     * Provide service according your gateway
     *
     * @param Sms
     */
    abstract public function service(Sms $sms);
}
