<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main login page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');


$context = context_system::instance();
$PAGE->set_url("$CFG->httpswwwroot/login/index.php");
$PAGE->set_context($context);
$PAGE->set_pagelayout('base');
$PAGE->set_title('Otp login');
$PAGE->set_heading("OTP Login");
$PAGE->requires->jquery();
$PAGE->requires->css('/auth/otplogin/style.css');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . "/auth/otplogin/script.js"));
echo $OUTPUT->header(); 
?>
<div class="row-fluid" id="page-content">
    <section class="span12" id="region-main">
        <div role="main">
            <span id="maincontent"></span>
            <div class="loginbox clearfix onecolumn">
                <div class="loginpanel">
                    <h2>Log in</h2>
                    <div id="login-error" style="text-align:center"></div>
                    <div class="subcontent loginsub" id="loginform">
                        <form onsubmit="return generateOtpForUser()" action="javascript:;" method="post" id="login-form">
                            <div class="loginform">
                                <div class="form-label"><label for="search"><?php echo get_string('formlabel', 'auth_otplogin'); ?></label></div>
                                <div class="form-input">
                                    <input type="text"  size="35" id="search" autocomplete="off"/>
                                </div>
                                <div class="clearer"></div>
                            </div>                            
                            <div class="clearer"></div>
                            <input type="submit" value="Submit" id="loginbtn">
                        </form>   
                    </div>
                </div>
            </div>
    </section>
</div>
<?php
echo $OUTPUT->footer();
