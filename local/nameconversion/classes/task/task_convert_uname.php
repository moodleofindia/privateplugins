<?php
/**
 * cron
 * @return boolean
 */

namespace local_nameconversion\task;
defined('MOODLE_INTERNAL') || die();

class task_convert_uname extends \core\task\scheduled_task {
     public function get_name() {
        // Shown in admin screens
        return get_string('pluginname', 'local_nameconversion');
    }

    public function execute() {
        global $DB, $OUTPUT, $PAGE, $USER, $CFG;
        // Make all users name(fname and lname) as uppercase while cron run.
        $allusers = $DB->get_records_sql("SELECT * FROM {user} WHERE firstname!=upper(firstname) AND lastname!=upper(lastname)");
        if (!empty($allusers)) {
            $nameconversion = $DB->execute("UPDATE {user} SET firstname=upper(firstname) , lastname=upper(lastname)");
            echo "User's name has successfully been Updated in uppercase..";
        } else {
            echo "No username presents to convert in Upper Case";
        }

    }
}
