<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(BODHIAPP_ROOT . '/lib/externalObject.class.php');

class quizquestionStructure extends ExternalObject{

    function __construct($questionrecord) {
        parent::__construct($questionrecord);
    }

    public static function get_class_structure() {
        return new external_single_structure(
            array (
                'id'                => new external_value(PARAM_INT, 'submission id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'category'        => new external_value(PARAM_INT, 'assignment id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'name'            => new external_value(PARAM_TEXT, 'user id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'qtype'           => new external_value(PARAM_TEXT, 'group id', VALUE_OPTIONAL),
				'option1'   => new external_value(PARAM_TEXT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
				'option1_url'   => new external_value(PARAM_TEXT, 'downloadable file url', VALUE_OPTIONAL),
				'option1_id'   => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'option2' => new external_value(PARAM_TEXT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'option2_url' => new external_value(PARAM_TEXT, 'downloadable file url', VALUE_OPTIONAL),
				'option2_id' => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
				'option3'   => new external_value(PARAM_TEXT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
				'option3_url'   => new external_value(PARAM_TEXT, 'downloadable file url', VALUE_OPTIONAL),
				'option3_id'   => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'option4' => new external_value(PARAM_TEXT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'option4_url' => new external_value(PARAM_TEXT, 'downloadable file url', VALUE_OPTIONAL),
				'option4_id' => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'option5' => new external_value(PARAM_TEXT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'option5_url' => new external_value(PARAM_TEXT, 'downloadable file url', VALUE_OPTIONAL),
				'option5_id' => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'answerid' => new external_value(PARAM_INT, 'correct answer id value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
												
            ),'quizquestionStructure'
			
        );
		
    }
}
?>
