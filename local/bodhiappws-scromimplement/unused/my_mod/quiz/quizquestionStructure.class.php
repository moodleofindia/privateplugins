<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(BODHIAPP_ROOT . '/lib/externalObject.class.php');

class quizquestionStructure extends ExternalObject{

    function __construct($questionrecord) {
        parent::__construct($questionrecord);
    }

    public static function get_class_structure() {
        return new external_single_structure(
            array (
                'id'                => new external_value(PARAM_INT, 'submission id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'category'        => new external_value(PARAM_INT, 'assignment id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'name'            => new external_value(PARAM_TEXT, 'user id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'qtype'           => new external_value(PARAM_TEXT, 'group id', VALUE_OPTIONAL),
				'answer1'   => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'answer2' => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
				'answer3'   => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'answer4' => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                'answer5' => new external_value(PARAM_INT, 'option value', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
												
            ), 'quizquestionStructure'
        );
    }
}
?>
