<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once("$CFG->dirroot/mod/assign/locallib.php");
require_once(UNIAPP_ROOT . '/mod/assign/assignStructure.class.php');
require_once(UNIAPP_ROOT . '/mod/assign/assignSubmissionStructure.class.php');
require_once(UNIAPP_ROOT . '/mod/assign/db/assignDB.class.php');
require_once(UNIAPP_ROOT . '/course/db/courseDB.class.php');

class uniappws_assign extends uniapp_external_api {

    public static function get_assigns_by_courseid_parameters() {
        return new external_function_parameters(
            array (
                'courseid'  => new external_value(PARAM_INT, 'Course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'startpage' => new external_value(PARAM_INT, 'Start page', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                'n'         => new external_value(PARAM_INT, 'Page number', VALUE_DEFAULT, 10, NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_assigns_by_courseid($courseid, $startpage, $n) {
        $system_context = get_context_instance(CONTEXT_SYSTEM);


        self::validate_context($system_context);
        // check for view capability at course level
        $context = get_context_instance(CONTEXT_COURSE, $courseid);
        require_capability('mod/assign:view', $context);

        if (!$course = course_db::get_course_by_courseid($courseid)) {
            throw new moodle_exception('assign:unknowncourseidnumber', 'uniappws', '', $courseid);
        }

        $assigns = assign_db::get_assigns_by_courseid($courseid, $startpage, $n);

        $returnassign = array();
        foreach ($assigns as $assig) {
            if (!$cm = get_coursemodule_from_instance('assign', $assig->id, 0, false, MUST_EXIST)) {
                throw new moodle_exception('assign:notfound', 'uniappws', '', '');
            }

            $context = get_context_instance(CONTEXT_MODULE, $cm->id);
            if (!has_capability('mod/assign:view', $context)) {
                continue;
            }
			$assig->courseid = $assig->course;
            $assig = new AssignStructure($assig);
            $returnassign[] = $assig->get_data();
        }

        return $returnassign;
    }

    public static function get_assigns_by_courseid_returns() {
        return new external_multiple_structure(
            AssignStructure::get_class_structure()
        );
    }


    public static function get_assign_by_assigid_parameters() {
        return new external_function_parameters(
            array(
                'assigid' => new external_value(PARAM_INT, 'Assign id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
            )
        );
    }

    public static function get_assign_by_assigid($assigid) {

        $system_context = get_context_instance(CONTEXT_SYSTEM);

        self::validate_context($system_context);

        $assign = assign_db::get_assign($assigid);
        if ($assign === false || $assign === null){
			throw new moodle_exception('assign:notfound', 'uniappws', '', '');
        }
        if (!$cm = get_coursemodule_from_instance('assign', $assign->id)) {
			throw new moodle_exception('assign:notfound', 'uniappws', '', '');
        }
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        if (!has_capability('mod/assign:view',$context)) {
            throw new moodle_exception('assign:nopermissions', 'uniappws', '', '');
        }
		$plugconf = self::get_assign_plugin_configs($assigid);
		$assign->plugconf = $plugconf;
		$assign->courseid = $assign->course;
        $assign = new AssignStructure($assign);

        return $assign->get_data();
    }

    public static function get_assign_by_assigid_returns() {
        return AssignStructure::get_class_structure();
    }

    public static function submit_assign_parameters() {
        return new external_function_parameters(
            array(
                'assigid'            => new external_value(PARAM_INT, 'Assignment id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'onlinetext'         => new external_value(PARAM_TEXT, 'Online text', VALUE_OPTIONAL, ''),
                'teamsubmission'     => new external_value(PARAM_BOOL, 'If true this is the team submission', VALUE_DEFAULT, false, NULL_NOT_ALLOWED),
                'files' => new external_multiple_structure(
                     	new external_value(PARAM_INT, 'File id', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED)
                ),
                'draftid'            => new external_value(PARAM_INT, 'Draft item id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
            )
        );
    }

    public static function submit_assign($assigid, $onlinetext, $teamsubmission, $files, $draftid) {
		global $USER;

        $system_context = get_context_instance(CONTEXT_SYSTEM);

        self::validate_context($system_context);

		$assign = self::get_assign($assigid);
		$cm = get_coursemodule_from_instance('assign', $assigid, 0, false, MUST_EXIST);
		$context = get_context_instance(CONTEXT_MODULE, $cm->id);
		// check if the user can submit
		require_capability('mod/assign:submit', $context);
		// check for previous submissions otherwise init a new one
		if (!empty($assign->get_instance()->teamsubmission)) {
        	$submission = $assign->get_group_submission($USER->id, 0, true);
		} else if(is_callable($assign, 'get_user_submission')) {
        	$submission = $assign->get_user_submission($USER->id, true);
        } else {
			$submission = assign_db::get_submission($USER->id, $assigid);
        }
		// check for drafts
		if (!empty($submission) and $assign->get_instance()->submissiondrafts) {
			$submission->status = ASSIGN_SUBMISSION_STATUS_DRAFT;
		} else {
			$submission->status = ASSIGN_SUBMISSION_STATUS_SUBMITTED;
		}

		//$grade = $assign->get_user_grade($USER->id, false); // get the grade to check if it is locked
		$grade = assign_db::get_submission_grade($USER->id, $assigid);
		if (!empty($grade) && $grade->locked) {
			throw new moodle_exception('assign:submissionslocked', 'uniappws', '', '');
		}

		$allempty = true;
		$pluginerror = false;
		$notices = array();
		$data = new stdClass();
		$data->onlinetext_editor = array(
			'text' => $onlinetext,
			'format' => 1
		);
		$data->files_filemanager = $draftid;
		foreach ($assign->get_submission_plugins() as $plugin) {
			if ($plugin->is_enabled() && $plugin->is_visible()) {
				if (!$plugin->save($submission, $data)) {
					$notices[] = $plugin->get_error();
					$pluginerror = true;
				}
				if (!$allempty || !$plugin->is_empty($submission)) {
					$allempty = false;
				}
			}
		}

		if ($allempty) {
			throw new moodle_exception('assign:submissionempty', 'uniappws', '', '');
		}

		if ($pluginerror) {
			throw new moodle_exception('assign:submissionfailed', 'uniappws', '', '');
		}

		return array("subid" => $submission->id);
    }

    public static function submit_assign_returns() {
		return new external_single_structure(
            array(
                'subid' => new external_value(PARAM_INT, 'Submission id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_submission_by_assigid_parameters() {
         return new external_function_parameters(
            array(
                'assigid' => new external_value(PARAM_INT, 'Assign id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
            )
        );
    }

    public static function get_submission_by_assigid($assigid) {
        global $USER;
        $system_context = get_context_instance(CONTEXT_SYSTEM);

        self::validate_context($system_context);

		$assign = self::get_assign($assigid);

		$submission = assign_db::get_submission($USER->id, $assigid);
        if ($submission == false) {
			return array(
				"id" => 0,
				"assignment" => $assigid,
				"userid" => $USER->id,
				"groupid" => 0,
				"userfiles" => array(),
			);
        } else {
			// get file submissions
			$submission->userfiles = self::get_submission_files($assigid, 0, 20);

			// get onlinetext submissions
			$text = assign_db::get_submission_text($submission->id, $assigid);
			if(empty($text)) {
				$submission->usertext = null;
			} else {
				$submission->usertext = $text->onlinetext;
			}

			// get grades
			$grade = assign_db::get_submission_grade($USER->id, $assigid);
			if($grade == false) {
				$submission->grade = 0;	
			} else {
				$submission->grade = $grade->grade;	
			}

			$cm = get_coursemodule_from_instance("assign", $assigid);
			add_to_log($cm->course, 'assign', 'view submission', 'submissions.php?id='.$cm->id, $assigid, $cm->id);

			$submission = new AssignSubmissionStructure($submission);
			return $submission->get_data();
		}
    }

    public static function get_submission_by_assigid_returns() {
        return AssignSubmissionStructure::get_class_structure();
    }

    public static function get_submission_files_parameters() {
        return new external_function_parameters(
            array(
                 'assigid'   => new external_value(PARAM_INT, 'Assignment id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                 'startpage' => new external_value(PARAM_INT, 'Start page', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                 'n'         => new external_value(PARAM_INT, 'Page number', VALUE_DEFAULT, 10, NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_submission_files($assigid, $startpage, $n) {
        global $USER;

        $system_context = get_context_instance(CONTEXT_SYSTEM);

        self::validate_context($system_context);

        $cm = get_coursemodule_from_instance('assign', $assigid);
        $subid = assign_db::get_submission_id($USER->id, $assigid);
        if ($subid === false) {
			return array();
        }
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        $contextid = $context->id;
        $fs = get_file_storage();

        $files = $fs->get_area_files($contextid, 'assignsubmission_file', 'submission_files', $subid->id, "timemodified", false);
        $ret = array();
        $begin = $startpage*$n;
        $aux = array_slice($files, $begin, $begin+$n);

        if (empty($aux)) {
			return array();
        } else {
			foreach ($aux as $file) {
				$ret[] = array('fileid' => $file->get_id(), 'filename' => $file->get_filename());
			}
			return $ret;
		}
    }

    public static function get_submission_files_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'fileid' => new external_value(PARAM_INT,'Fileid', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                    'filename' => new external_value(PARAM_TEXT,'Filename', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                )
            )
        );
    }

	public static function get_assign($assigid) {
		global $COURSE;
		$assign = assign_db::get_assign($assigid);
        if ($assign === false || $assign === null){
			throw new moodle_exception('assign:notfound', 'uniappws', '', '');
        }
		// get the course module
        if (!$cm = get_coursemodule_from_instance('assign', $assigid)) {
			throw new moodle_exception('assign:notfound', 'uniappws', '', '');
        }
		// get the context
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        if (!has_capability('mod/assign:view',$context)) {
            throw new moodle_exception('assign:nopermissions', 'uniappws', '', '');
        }
		// return the plugin
		return new assign($context, $cm, $COURSE);
	}

	public static function get_assign_plugin_configs($assigid) {
		$Assign = self::get_assign($assigid);
		// submission related configs
		$plug_submission_file = $Assign->get_submission_plugin_by_type('file');
		$plug_submission_onlinetext = $Assign->get_submission_plugin_by_type('onlinetext');
		$plug_submission_offline = $Assign->get_submission_plugin_by_type('offline');
		$plug_submission_comments = $Assign->get_submission_plugin_by_type('comments');
		// feedback related configs
		$plug_feedback_file = $Assign->get_feedback_plugin_by_type('file');
		$plug_feedback_comments = $Assign->get_feedback_plugin_by_type('comments');
		$plug_feedback_offline = $Assign->get_feedback_plugin_by_type('offline');
		/*
		$plugconf = new Object();
		// comments
		$plugconf->comments = new Object();
		$plugconf->comments->assignfeedback = new Object();
		$plugconf->comments->assignfeedback->enabled = $plug_feedback_comments->get_config('enabled');
		$plugconf->comments->assignsubmission = new Object();
		$plugconf->comments->assignsubmission->enabled = $plug_submission_comments->get_config('enabled');
		// files
		$plugconf->files = new Object();
		$plugconf->files->assignfeedback = new Object();
		$plugconf->files->assignfeedback->enabled = $plug_feedback_file->get_config('enabled');
		$plugconf->files->assignsubmission = new Object();
		$plugconf->files->assignsubmission->enabled = $plug_submission_file->get_config('enabled');
		$plugconf->files->assignsubmission->maxfilesubmissions = $plug_submission_file->get_config('maxfilesubmissions');
		$plugconf->files->assignsubmission->maxsubmissionsizebytes = $plug_submission_file->get_config('maxsubmissionsizebytes');
		// offline
		$plugconf->offline = new Object();
		$plugconf->offline->assignfeedback = new Object();
		$plugconf->offline->assignfeedback->enabled = $plug_feedback_offline->get_config('enabled');
		// onlinetext
		$plugconf->onlinetext = new Object();
		$plugconf->onlinetext->assignsubmission = new Object();
		$plugconf->onlinetext->assignsubmission->enabled = $plug_submission_onlinetext->get_config('enabled');
		*/
		$plugconf = Array();
		// comments
		$plugconf['comments'] = Array();
		$plugconf['comments']['assignfeedback'] = Array();
		if(empty($plug_feedback_comments)) {
			$plugconf['comments']['assignfeedback']['enabled'] = 0;
		} else {
			$plugconf['comments']['assignfeedback']['enabled'] = $plug_feedback_comments->get_config('enabled');
		}

		$plugconf['comments']['assignsubmission'] = Array();
		if(empty($plug_submission_comments)) {
			$plugconf['comments']['assignsubmission']['enabled'] = 0;
		} else {
			$plugconf['comments']['assignsubmission']['enabled'] = $plug_submission_comments->get_config('enabled');
		}

		// files
		$plugconf['files'] = Array();
		$plugconf['files']['assignfeedback'] = Array();
		if(empty($plug_feedback_file)) {
			$plugconf['files']['assignfeedback']['enabled'] = 0;
		} else {
			$plugconf['files']['assignfeedback']['enabled'] = $plug_feedback_file->get_config('enabled');
		}

		$plugconf['files']['assignsubmission'] = Array();
		if(empty($plug_submission_file)) {
			$plugconf['files']['assignsubmission']['enabled'] = 0;
		} else {
			$plugconf['files']['assignsubmission']['enabled'] = $plug_submission_file->get_config('enabled');
		}

		if($plugconf['files']['assignsubmission']['enabled'] == 0) {
			$plugconf['files']['assignsubmission']['maxfilesubmissions'] = 0;
			$plugconf['files']['assignsubmission']['maxsubmissionsizebytes'] = 0;
		} else {
			$plugconf['files']['assignsubmission']['maxfilesubmissions'] = $plug_submission_file->get_config('maxfilesubmissions');
			$plugconf['files']['assignsubmission']['maxsubmissionsizebytes'] = $plug_submission_file->get_config('maxsubmissionsizebytes');
		}
		// offline
		$plugconf['offline'] = Array();
		$plugconf['offline']['assignfeedback'] = Array();
		if(empty($plug_feedback_offline)) {
			$plugconf['offline']['assignfeedback']['enabled'] = 0;
		} else {
			$plugconf['offline']['assignfeedback']['enabled'] = $plug_feedback_offline->get_config('enabled');
		}
		// onlinetext
		$plugconf['onlinetext'] = Array();
		$plugconf['onlinetext']['assignsubmission'] = Array();
		if(empty($plug_submission_onlinetext)) {
			$plugconf['onlinetext']['assignsubmission']['enabled'] = 0;
		} else {
			$plugconf['onlinetext']['assignsubmission']['enabled'] = $plug_submission_onlinetext->get_config('enabled');
		}

		return $plugconf;
	}
}
?>
