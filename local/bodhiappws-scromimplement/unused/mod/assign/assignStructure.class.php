<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class AssignStructure extends ExternalObject{

    function __construct($assignmentrecord) {
        parent::__construct($assignmentrecord);
    }

    public static function get_class_structure(){
        return new external_single_structure(
            array (
                'id'                         => new external_value(PARAM_INT,  'assign record id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
				'courseid'                   => new external_value(PARAM_INT,  'course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'name'                       => new external_value(PARAM_TEXT, 'multilang compatible name', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'intro'                      => new external_value(PARAM_RAW,  'assign description text', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'introformat'                => new external_value(PARAM_RAW,  'assign description format', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'alwaysshowdescription'      => new external_value(PARAM_RAW,  'assign description visibility setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'nosubmissions'              => new external_value(PARAM_INT,  'assign submission setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'submissiondrafts'           => new external_value(PARAM_INT,  'assign submission drafts', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
				// file plugin configs
				'plugconf'         			 => new external_single_structure(
															array(
																// comments settings
																'comments' => new external_single_structure(
																	array(
																		'assignfeedback' => new external_single_structure(
																			array(
																				'enabled' => new external_value(PARAM_INT,  'enable assign feedback comments', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
																			)
																		),
																		'assignsubmission' => new external_single_structure(
																			array(
																				'enabled' => new external_value(PARAM_INT,  'enable assign submission comments', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
																			)
																		)
																	)
																),
																// files settings
																'files' => new external_single_structure(
																	array(
																		'assignfeedback' => new external_single_structure(
																			array(
																				'enabled' => new external_value(PARAM_INT,  'enable assign feedback files', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
																			)
																		),
																		'assignsubmission' => new external_single_structure(
																			array(
																				'enabled' => new external_value(PARAM_INT,  'enable assign submission files', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
																				'maxfilesubmissions' => new external_value(PARAM_INT,  'max number of files to submit', VALUE_OPTIONAL),
																				'maxsubmissionsizebytes' => new external_value(PARAM_INT,  'assign submission file size settings', VALUE_OPTIONAL),
																			)
																		)
																	)
																),
																'offline' => new external_single_structure(
																	array(
																		'assignfeedback' => new external_single_structure(
																			array(
																				'enabled' => new external_value(PARAM_INT,  'enable offline assign feedback', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
																			)
																		)
																	)
																),
																'onlinetext' => new external_single_structure(
																	array(
																		'assignsubmission' => new external_single_structure(
																			array(
																				'enabled' => new external_value(PARAM_INT,  'enable online text', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
																			)
																		)
																	)
																)
															)
											 ),
                'sendnotifications'          => new external_value(PARAM_INT,  'assign notifications setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'sendlatenotifications'      => new external_value(PARAM_INT,  'assign late notifications setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'duedate'                    => new external_value(PARAM_INT,  'assign due date', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'allowsubmissionsfromdate'   => new external_value(PARAM_INT,  'assign submit from date', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'cutoffdate'                 => new external_value(PARAM_INT,  'assign cutoff date'),
                'requiresubmissionstatement' => new external_value(PARAM_INT,  'assign required submission statement'),
                'completionsubmit'           => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'teamsubmission'             => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'requireallteammemberssubmit'=> new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'teamsubmissiongroupingid'   => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'blindmarking'               => new external_value(PARAM_INT,  'assign blindmarking setting'),
                'revealidentities'           => new external_value(PARAM_INT,  'assign reveal identities setting'),
                'grade'                      => new external_value(PARAM_INT,  'grade scale for assign', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timemodified'               => new external_value(PARAM_INT,  'assign time modification', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
            ), 'AssignStructure'
        );
    }
}

// assign fields sample
/*
[id] => 1
[course] => 7
[name] => Nippon in one picture
[intro] => Upload just one picture that represents Japan best 
[introformat] => 1
[alwaysshowdescription] => 1
[nosubmissions] => 1
[submissiondrafts] => 0
[sendnotifications] => 0
[sendlatenotifications] => 0
[duedate] => 1342789500
[allowsubmissionsfromdate] => 1342184700
[grade] => 100
[timemodified] => 1362048922
[requiresubmissionstatement] => 0
[completionsubmit] => 0
[cutoffdate] => 0
[teamsubmission] => 0
[requireallteammemberssubmit] => 0
[teamsubmissiongroupingid] => 0
[blindmarking] => 0
[revealidentities] => 0
*/
?>
