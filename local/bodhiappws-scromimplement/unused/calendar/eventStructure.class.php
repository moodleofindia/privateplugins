<?php
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../config.php');

require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');


class EventStructure extends ExternalObject{

    function __construct($eventrecord) {
        parent::__construct($eventrecord);
    }

    public static function get_class_structure(){
        return new external_single_structure(
            array (
                'id'          => new external_value(PARAM_INT,        'The id within the event table', VALUE_REQUIRED, 0 , NULL_NOT_ALLOWED),
                'name'        => new external_value(PARAM_TEXT,       'The name of the event', VALUE_REQUIRED, '0' , NULL_NOT_ALLOWED),
                'description' => new external_value(PARAM_RAW,        'The description of the event', VALUE_REQUIRED, '' , NULL_NOT_ALLOWED),
                'courseid'    => new external_value(PARAM_INT,        'The course the event is associated with (0 if none)', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                'groupid'     => new external_value(PARAM_INT,        'The group the event is associated with (0 if none)', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                'userid'      => new external_value(PARAM_INT,        'The user the event is associated with (0 if none)', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                'repeatid'    => new external_value(PARAM_INT,        'If this is a repeated event this will be set to the id of the original', VALUE_DEFAULT, 0 , NULL_NOT_ALLOWED),
                'modulename'  => new external_value(PARAM_TEXT,       'If added by a module this will be the module name', VALUE_DEFAULT, '0' , NULL_NOT_ALLOWED),
                'instance'    => new external_value(PARAM_INT,        'If added by a module this will be the module instance', VALUE_DEFAULT, 0 , NULL_NOT_ALLOWED),
                'eventtype'   => new external_value(PARAM_ALPHANUMEXT,'The event type', VALUE_REQUIRED, '0' , NULL_NOT_ALLOWED),
                'timestart'   => new external_value(PARAM_INT,        'Timestamp in UTC that holds when event starts or, in assignment module, date when event expires', VALUE_REQUIRED, 0 , NULL_NOT_ALLOWED),
                'timeduration'=> new external_value(PARAM_INT,        'The duration of the event in seconds', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
            ), 'EventStructure'
        );
    }
}
