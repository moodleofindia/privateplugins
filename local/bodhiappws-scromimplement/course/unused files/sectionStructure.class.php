<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class SectionStructure extends ExternalObject{

    function __construct($courserecord) {
        parent::__construct($courserecord);
    }

    public static function get_class_structure() {
        return new external_single_structure(
            array(
					'courseid' => new external_value(PARAM_INT, 'course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'number' => new external_value(PARAM_INT, 'section number', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'title' => new external_value(PARAM_RAW, 'section title', VALUE_OPTIONAL),
					'summary' => new external_value(PARAM_RAW, 'section summary', VALUE_OPTIONAL)

			), 'SectionStructure'
        );
    }

}

?>
