<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class ModuleStructure extends ExternalObject{

    function __construct($courserecord) {
        parent::__construct($courserecord);
    }

    public static function get_class_structure() {
        return new external_single_structure(
            array(
					'id' => new external_value(PARAM_INT, 'course module id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'instanceid' => new external_value(PARAM_INT, 'module id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'courseid' => new external_value(PARAM_INT, 'course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'modname' => new external_value(PARAM_TEXT, 'module type', VALUE_REQUIRED, 'mod_unknown', NULL_NOT_ALLOWED),
					'type' => new external_value(PARAM_TEXT, 'module type', VALUE_OPTIONAL),
					'name' => new external_value(PARAM_TEXT, 'module title', VALUE_REQUIRED, 'name_unknow', NULL_NOT_ALLOWED),
					'intro' => new external_value(PARAM_RAW, 'module intro', VALUE_OPTIONAL),
					'section' => new external_value(PARAM_INT, 'section number', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'display' => new external_value(PARAM_INT, 'Display mode: 0: automatic, 1: embeded, 2: force download, 3: open, 4: popup', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'groupmode' => new external_value(PARAM_INT, 'Group mode: 0:no groups, 1:separated groups, 2:visible groups', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'groupingid' => new external_value(PARAM_INT, 'Grouping id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'order' => new external_value(PARAM_INT, 'section order number', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
					'timemodified' => new external_value(PARAM_INT, 'modification time', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED)

			), 'ModuleStructure'
        );
    }
}

?>
