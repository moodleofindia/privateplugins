<?php

// Web service functions to install.
$functions = array(
		// course
		/*
		'uniappws_get_course_modules' => array(
				'classname'   => 'uniappws_course',
				'methodname'  => 'get_course_modules',
				'classpath'   => 'local/uniappws/course/externallib.php',
				'description' => 'Returns the list of modules from the course; required parameters: courseid',
				'type'		=> 'read',
		),
		'uniappws_get_course_modules_count' => array(
				'classname'   => 'uniappws_course',
				'methodname'  => 'get_course_modules_count',
				'classpath'   => 'local/uniappws/course/externallib.php',
				'description' => 'Returns the the number of course modules for every courseid passed; required parameters: courseid (array of ids)',
				'type'		=> 'read',
		),
		'uniappws_get_courses_by_userid' => array(
				'classname'   => 'uniappws_course',
				'methodname'  => 'get_courses_by_userid',
				'classpath'   => 'local/uniappws/course/externallib.php',
				'description' => 'Returns the list of courses given the userid; required parameters: userid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_course_sections' => array(
				'classname'   => 'uniappws_course',
				'methodname'  => 'get_course_sections',
				'classpath'   => 'local/uniappws/course/externallib.php',
				'description' => 'Returns the list of sections from the course; required parameters: courseid',
				'type'		=> 'read',
		),
		'uniappws_get_recent_activity' => array(
				'classname'   => 'uniappws_course',
				'methodname'  => 'get_course_recent_activity',
				'classpath'   => 'local/uniappws/course/externallib.php',
				'description' => 'Returns the recent activity notifications; required parameters: courseid',
				'type'		=> 'read',
		),
		'uniappws_get_course' => array(
				'classname'   => 'uniappws_course',
				'methodname'  => 'get_course',
				'classpath'   => 'local/uniappws/course/externallib.php',
				'description' => 'Fetches the course with the description of all of the modules; required parameters: courseid',
				'type'		=> 'read',
		),

		// calendar
		'uniappws_get_calendar_events' => array(
			'classname'   => 'uniappws_calendar',
			'methodname'  => 'get_events',
			'classpath'   => 'local/uniappws/calendar/externallib.php',
			'description' => 'Returns the events',
			'type'		  => 'read',
			'capabilities'=> 'moodle/calendar:manageownentries',
		),

		'uniappws_create_calendar_event' => array(
			'classname'   => 'uniappws_calendar',
			'methodname'  => 'create_event',
			'classpath'   => 'local/uniappws/calendar/externallib.php',
			'description' => 'Creates an event',
			'type'		  => 'write',
			'capabilities'=> 'moodle/calendar:manageownentries',
		),

		'uniappws_delete_calendar_event' => array(
			'classname'   => 'uniappws_calendar',
			'methodname'  => 'delete_event',
			'classpath'   => 'local/uniappws/calendar/externallib.php',
			'description' => 'Deletes an event',
			'type'		  => 'write',
			'capabilities'=> 'moodle/calendar:manageownentries',
		),

		'uniappws_update_calendar_event' => array(
			'classname'   => 'uniappws_calendar',
			'methodname'  => 'update_event',
			'classpath'   => 'local/uniappws/calendar/externallib.php',
			'description' => 'Updates an event',
			'type'		  => 'write',
			'capabilities'=> 'moodle/calendar:manageownentries',
		),

		'uniappws_export_calendar_events' => array(
			'classname'   => 'uniappws_calendar',
			'methodname'  => 'export_events_to_ical',
			'classpath'   => 'local/uniappws/calendar/externallib.php',
			'description' => 'Exports events to ical file',
			'type'		  => 'read',
			'capabilities'=> '',
		),
		
		// forum
		'uniappws_get_forum_by_id' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_forum_by_id',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forum given the id; required parameters: forumid',
				'type'		=> 'read',
		),
		'uniappws_get_forums_by_courseid' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_forums_by_courseid',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the course forums; required parameters: courseid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_forums_by_userid' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_forums_by_userid',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forums the user is enroled in; required parameters: userid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_forum_discussions' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_forum_discussions',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forum discussions; required parameters: forumid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_discussion_by_id' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_discussion_by_id',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forum discussion given the id; required parameters: discid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_forum_by_postid' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_forum_by_postid',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forum given the post id; required parameters: postid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_forum_by_discussionid' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_forum_by_discussion_id',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forum given the discussion id; required parameters: discid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_get_posts_by_discussionid' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'get_posts_by_discussion_id',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Returns the forum posts given the discussion id; required parameters: discid; optional parameters: startpage, n (page number)',
				'type'		=> 'read',
		),
		'uniappws_create_forum_discussion' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'create_discussion',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Creates a new discussion; required parameters: array discussion[forumid, name, intro]; optional parameters: discussion[groupid, attachments, format, mailnow]',
				'type'		=> 'read',
		),
		'uniappws_delete_forum_discussion' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'delete_discussion',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Deletes a discussion given a discussion id; required parameters: discid',
				'type'		=> 'read',
		),
		'uniappws_create_forum_post' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'create_post',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Creates a post ; required parameters: parentid, subject, message',
				'type'		=> 'read',
		),
		'uniappws_update_forum_post' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'update_post',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Updates a post. Only updates the "subject" and "message" fields, while ignoring all other parameters passed; required parameters: post[id], post[subject], post[message], post[userid]; optional parameter: post[modified]',
				'type'		=> 'read',
		),
		'uniappws_delete_forum_post' => array(
				'classname'   => 'uniappws_forum',
				'methodname'  => 'delete_post',
				'classpath'   => 'local/uniappws/mod/forum/externallib.php',
				'description' => 'Deletes a post given a post id; required parameters: postid',
				'type'		=> 'read',
		),

		// user
		'uniappws_get_user' => array(
			'classname'   => 'uniappws_user',
			'methodname'  => 'get_user',
			'classpath'   => 'local/uniappws/user/externallib.php',
			'description' => 'Returns the details of the logged user; required parameters: no parameters',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		
		
		'uniappws_get_user_by_id' => array(
			'classname'   => 'uniappws_user',
			'methodname'  => 'get_user_by_userid',
			'classpath'   => 'local/uniappws/user/externallib.php',
			'description' => 'Returns the details of a user; required parameters: userid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_user_by_username'=> array(
			'classname'   => 'uniappws_user',
			'methodname'  => 'get_user_by_username',
			'classpath'   => 'local/uniappws/user/externallib.php',
			'description' => 'Returns the details of a user; required parameters: username',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_users_by_courseid'=> array(
			'classname'   => 'uniappws_user',
			'methodname'  => 'get_users_by_courseid',
			'classpath'   => 'local/uniappws/user/externallib.php',
			'description' => 'Returns the details of all users of a course; required parameters: courseid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// group
		'uniappws_get_group_by_id'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_group_by_groupid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns a group; required parameters: groupid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_users_by_groupid'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_group_members_by_groupid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the group members; required parameters: groupid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_users_by_groupingid'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_group_members_by_groupingid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the group members; required parameters: groupingid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_groups_by_courseid'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_groups_by_courseid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the groups of a course; required parameters: courseid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_groups_by_groupingid'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_groups_by_groupingid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the groups of a course; required parameters: groupingid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_user_course_groups'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_groups_by_courseid_and_userid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the groups of a course; required parameters: courseid, userid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_groupings_by_courseid'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_groupings_by_courseid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the groupings of a course; required parameters: courseid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_user_course_groupings'=> array(
			'classname'   => 'uniappws_group',
			'methodname'  => 'get_groupings_by_courseid_and_userid',
			'classpath'   => 'local/uniappws/group/externallib.php',
			'description' => 'Returns the groupings of a course; required parameters: courseid, userid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// resources
		'uniappws_get_resource' => array(
			'classname'   => 'uniappws_resource',
			'methodname'  => 'get_resource',
			'classpath'   => 'local/uniappws/mod/resource/externallib.php',
			'description' => 'Gets a resource (File resource type); required parameters: resourceid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// choice
		'uniappws_get_choice' => array(
			'classname'   => 'uniappws_choice',
			'methodname'  => 'get_choice',
			'classpath'   => 'local/uniappws/mod/choice/externallib.php',
			'description' => 'Gets a choice (File choice type); required parameters: choiceid',
			'type'		=> 'read',
			'capabilities'=> '',
		),
		'uniappws_submit_choice' => array(
			'classname'   => 'uniappws_choice',
			'methodname'  => 'submit_choice',
			'classpath'   => 'local/uniappws/mod/choice/externallib.php',
			'description' => 'Gets a choice (File choice type); required parameters: choiceid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// url
		'uniappws_get_url' => array(
			'classname'   => 'uniappws_url',
			'methodname'  => 'get_url',
			'classpath'   => 'local/uniappws/mod/url/externallib.php',
			'description' => 'Gets a resource (URL resource type); required parameters: urlid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// page
		'uniappws_get_page' => array(
			'classname'   => 'uniappws_page',
			'methodname'  => 'get_page',
			'classpath'   => 'local/uniappws/mod/page/externallib.php',
			'description' => 'Gets a resource (Page resource type); required parameters: pageid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// book
		'uniappws_get_book' => array(
			'classname'   => 'uniappws_book',
			'methodname'  => 'get_book',
			'classpath'   => 'local/uniappws/mod/book/externallib.php',
			'description' => 'Gets a resource (Page resource type); required parameters: bookid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// files
		'uniappws_upload_private_file' => array(
			'classname'   => 'uniappws_files',
			'methodname'  => 'upload_private_file',
			'classpath'   => 'local/uniappws/files/externallib.php',
			'description' => 'Uploads a private file',
			'type'		=> 'write',
			'capabilities'=> '',
		),

		'uniappws_upload_draft_file' => array(
			'classname'   => 'uniappws_files',
			'methodname'  => 'upload_draft_file',
			'classpath'   => 'local/uniappws/files/externallib.php',
			'description' => 'Uploads a draft file; useful for file submissions in assignments and attachments',
			'type'		=> 'write',
			'capabilities'=> '',
		),

		'uniappws_get_file_url' => array(
			'classname'   => 'uniappws_files',
			'methodname'  => 'get_file_url',
			'classpath'   => 'local/uniappws/files/externallib.php',
			'description' => 'Returns the URL of a file',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_user_filesinfo' => array(
			'classname'   => 'uniappws_files',
			'methodname'  => 'get_user_filesinfo',
			'classpath'   => 'local/uniappws/files/externallib.php',
			'description' => 'Gets name and id of user files',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_file' => array(
			'classname'   => 'uniappws_files',
			'methodname'  => 'get_file',
			'classpath'   => 'local/uniappws/files/externallib.php',
			'description' => 'Returns the file',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// assign
		'uniappws_get_assigns_by_courseid' => array(
			'classname'   => 'uniappws_assign',
			'methodname'  => 'get_assigns_by_courseid',
			'classpath'   => 'local/uniappws/mod/assign/externallib.php',
			'description' => 'Gets course assigns; required parameters: courseid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> 'mod/assignment:view',
		),

		'uniappws_get_assign_by_id' => array(
			'classname'   => 'uniappws_assign',
			'methodname'  => 'get_assign_by_assigid',
			'classpath'   => 'local/uniappws/mod/assign/externallib.php',
			'description' => 'Gets an assign by its id; required parameters: assigid',
			'type'		=> 'read',
			'capabilities'=> 'mod/assign:view',
		),

		'uniappws_get_submission_by_assignid' => array(
			'classname'   => 'uniappws_assign',
			'methodname'  => 'get_submission_by_assigid',
			'classpath'   => 'local/uniappws/mod/assign/externallib.php',
			'description' => 'Gets a submission to an assign; required parameters: assigid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_assign_submission_files' => array(
			'classname'   => 'uniappws_assign',
			'methodname'  => 'get_submission_files',
			'classpath'   => 'local/uniappws/mod/assign/externallib.php',
			'description' => 'Gets submission files of an assign; required parameters: assigid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_submit_assign' => array(
			'classname'   => 'uniappws_assign',
			'methodname'  => 'submit_assign',
			'classpath'   => 'local/uniappws/mod/assign/externallib.php',
			'description' => 'Submits an upload assign; required parameters: assigid, onlinetext, submissioncomment, teamsubmission(boolean value, if true it is the team submission), files(array of fileids)',
			'type'		=> 'write',
			'capabilities'=> 'mod/assign:submit',
		),

		// assignment
		'uniappws_get_assignments_by_courseid' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'get_assignments_by_courseid',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Gets course assignments 2.2; required parameters: courseid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> 'mod/assignment:view',
		),

		'uniappws_get_assignment_by_id' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'get_assignment_by_assigid',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Gets an assignment 2.2 by its id; required parameters: assigid',
			'type'		=> 'read',
			'capabilities'=> 'mod/assignment:view',
		),

		'uniappws_get_submission_by_assignmentid' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'get_submission_by_assigid',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Gets a submission to an assignment 2.2; required parameters: assigid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_assignment_submission_files' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'get_submission_files',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Gets submission files of an assignment 2.2; required parameters: assigid; optional parameters: startpage, n (page number)',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_submit_online' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'submit_online_assignment',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Submits an online assignment 2.2; required parameters: assigid, data',
			'type'		=> 'write',
			'capabilities'=> 'mod/assignment:submit',
		),

		'uniappws_submit_singleupload' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'submit_singleupload_assignment',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Submits a singleupload assignment 2.2; required parameters: courseid, assigid, fileid',
			'type'		=> 'write',
			'capabilities'=> 'mod/assignment:submit',
		),

		'uniappws_submit_upload' => array(
			'classname'   => 'uniappws_assignment',
			'methodname'  => 'submit_upload_assignment',
			'classpath'   => 'local/uniappws/mod/assignment/externallib.php',
			'description' => 'Submits an upload assignment 2.2; required parameters: assigid, isfinal(boolean value, if true it is final submission otherwise is draft), files(array of fileids)',
			'type'		=> 'write',
			'capabilities'=> 'mod/assignment:submit',
		),

		// scorm
		'uniappws_get_scorm_package' => array(
			'classname'   => 'uniappws_scorm',
			'methodname'  => 'get_package',
			'classpath'   => 'local/uniappws/mod/scorm/externallib.php',
			'description' => 'Gets the scorm package; required parameters: scormid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_scorm_status' => array(
			'classname'   => 'uniappws_scorm',
			'methodname'  => 'get_status',
			'classpath'   => 'local/uniappws/mod/scorm/externallib.php',
			'description' => 'Gets the scorm completion status; required parameters: scormid',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		// folder
		'uniappws_get_folder_by_id' => array(
			'classname'   => 'uniappws_folder',
			'methodname'  => 'get_folder',
			'classpath'   => 'local/uniappws/mod/folder/externallib.php',
			'description' => 'Given the id gets the folder content',
			'type'		=> 'read',
			'capabilities'=> 'mod/folder:view',
		),

		// grade
		'uniappws_get_grade_items_by_userid' =>array(
			'classname'   => 'uniappws_grade',
			'methodname'  => 'get_grade_items_by_userid',
			'classpath'   => 'local/uniappws/grade/externallib.php',
			'description' => 'Returns the grade items of a user',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_grade_items_by_courseid' =>array(
			'classname'   => 'uniappws_grade',
			'methodname'  => 'get_grade_items_by_courseid',
			'classpath'   => 'local/uniappws/grade/externallib.php',
			'description' => 'Returns the grade items of a user',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_grades_by_itemid' =>array(
			'classname'   => 'uniappws_grade',
			'methodname'  => 'get_grades_by_itemid',
			'classpath'   => 'local/uniappws/grade/externallib.php',
			'description' => 'Returns the grades corresponding to a particular grade item',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_user_grade_by_itemid' =>array(
			'classname'   => 'uniappws_grade',
			'methodname'  => 'get_user_grade_by_itemid',
			'classpath'   => 'local/uniappws/grade/externallib.php',
			'description' => 'Returns the grades corresponding to a particular grade item',
			'type'		=> 'read',
			'capabilities'=> '',
		),

		'uniappws_get_user_grades_by_courseid' =>array(
			'classname'   => 'uniappws_grade',
			'methodname'  => 'get_user_grades_by_courseid',
			'classpath'   => 'local/uniappws/grade/externallib.php',
			'description' => 'Returns the user grades in the course',
			'type'		=> 'read',
			'capabilities'=> '',
		),*/
		//Get all the courses present in moodle.
		'bodhiappws_get_all_courses' =>array(
			'classname'   => 'bodhiappws_course',
			'methodname'  => 'get_all_courses',
			'classpath'   => 'local/bodhiappws/course/externallib.php',
			'description' => 'Returns all the courses',
			'type'		=> 'read',
			'capabilities'=> '',
		),
		//Get all the contents(ie all modules from different sections) from a perticular course id. 
		'bodhiappws_get_course_contents_by_courseid' =>array(
			'classname'   => 'bodhiappws_course',
			'methodname'  => 'get_course_contents_by_courseid',
			'classpath'   => 'local/bodhiappws/course/externallib.php',
			'description' => 'Returns all the contents of a perticular course',
			'type'		=> 'read',
			'capabilities'=> '',
		),
		//Get enrolled users from course id
		'bodhiappws_get_enrolled_users_by_courseid' => array(
        'classname'   => 'bodhiappws_course',
        'methodname'  => 'get_enrolled_users_by_courseid',
        'classpath'   => 'local/bodhiappws/course/externallib.php',
        'description' => 'Get enrolled users by course id.',
        'type'        => 'read',
        'capabilities'=> 'moodle/user:viewdetails, moodle/user:viewhiddendetails, moodle/course:useremail, moodle/user:update, moodle/site:accessallgroups',
    ),
	// quiz question----nihar
		'bodhiappws_get_quizquestion_by_courseid' => array(
			'classname'   => 'bodhiappws_quiz',
			'methodname'  => 'get_quizquestion_by_courseid',
			'classpath'   => 'local/bodhiappws/mod/quiz/externallib.php',
			'description' => 'Returns all quiz question of a perticular course; required parameters: courseid',
			'type'		=> 'read',
			'capabilities'=> '',
		),	
		
);


$functionlist = array();
foreach ($functions as $key=>$value) {
	$functionlist[] = $key;
}

$services = array(
   'BodhiApp web services'  => array(
		'functions' => $functionlist,
		'shortname'=> 'bodhiapp',
		'enabled' => 0,
		'restrictedusers' => 0,
	),
);

?>
