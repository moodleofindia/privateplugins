<?php
require_once(dirname(__FILE__).'/../config.php');
require_once($CFG->dirroot.'/course/lib.php');

/**
 * This function trawls through the logs looking for
 * anything new since the user's last login
 */

function extract_recent_activity($course, $lastaccess) {
    // $course is an object
    global $CFG, $USER, $SESSION, $DB, $OUTPUT;

    $context = context_course::instance($course->id);

    $viewfullnames = has_capability('moodle/site:viewfullnames', $context);

    $timestart = round(time() - COURSE_MAX_RECENT_PERIOD, -2); // better db caching for guests - 100 seconds

	$report = array();

    if (!isguestuser()) {
        if (!empty($lastaccess)) {
            if ($lastaccess > $timestart) {
                $timestart = $lastaccess;
            }
        }
    }
	
    $content = false;


/// Next, have there been any modifications to the course structure?

    $modinfo = get_fast_modinfo($course);

    $changelist = array();

    $logs = $DB->get_records_select('log', "time > ? AND course = ? AND
                                            module = 'course' AND
                                            (action = 'add mod' OR action = 'update mod' OR action = 'delete mod')",
                                    array($timestart, $course->id), "id ASC");

    if ($logs) {
        $actions  = array('add mod', 'update mod', 'delete mod');
        $newgones = array(); // added and later deleted items
        foreach ($logs as $key => $log) {
            if (!in_array($log->action, $actions)) {
                continue;
            }
            $info = explode(' ', $log->info);

            // note: in most cases I replaced hardcoding of label with use of
            // $cm->has_view() but it was not possible to do this here because
            // we don't necessarily have the $cm for it
            if ($info[0] == 'label') {     // Labels are ignored in recent activity
                continue;
            }

			if (count($info) != 2) {
                $report['error'] = "Incorrect log entry info: id = ".$log->id;
                continue;
            }

            $modname    = $info[0];
            $instanceid = $info[1];

            if ($log->action == 'delete mod') {
				// ignore deleted elements because there is no much info to display
				// and we don't even know if the element was visible or not
				continue; 
				/*
                // unfortunately we do not know if the mod was visible
                if (array_key_exists($log->info, $newgones)) {
                    $strdeleted = get_string('deletedactivity', 'moodle', get_string('modulename', $modname));
                    $changelist[$log->info] = array('operation' => 'delete', 'id' => null, 'modname' => $log->info, 'name' => null, 'text' => $strdeleted);
                }
				*/
            } else {
                if (!isset($modinfo->instances[$modname][$instanceid])) {
                    if ($log->action == 'add mod') {
                        // do not display added and later deleted activities
                        $newgones[$log->info] = true;
                    }
                    continue;
                }
                $cm = $modinfo->instances[$modname][$instanceid];
                if (!$cm->uservisible) {
                    continue;
                }

                if ($log->action == 'add mod') {
                    //$stradded = get_string('added', 'moodle', get_string('modulename', $modname));
                    $changelist[$log->info] = array(
						'operation' => 'add',
						'instanceid' => $cm->instance,
						'moduleid' => $cm->id,
						'reference' => null,
						'courseid' => $course->id,
						'modname' => $cm->modname,
						'name' => $cm->name,
						'timecreated' => $cm->added,
						'timemodified' => null,
						//'text' => $stradded,
						'firstname' => null,
						'lastname' => null
						);

                } else if ($log->action == 'update mod' and empty($changelist[$log->info])) {
                    //$strupdated = get_string('updated', 'moodle', get_string('modulename', $modname));
                    $changelist[$log->info] = array(
						'operation' => 'update',
						'instanceid' => $cm->instance,
						'moduleid' => $cm->id,
						'reference' => null,
						'courseid' => $course->id,
						'modname' => $cm->modname,
						'name' => $cm->name,
						'timecreated' => $cm->added,
						'timemodified' => null,
						//'text' => $strupdated,
						'firstname' => null,
						'lastname' => null
					);
                }
            }
        }
    }
	
	$report = array();
    if (!empty($changelist)) {
        $content = true;
        foreach ($changelist as $changeinfo => $change) {
			$report[] = $change;
        }
    }

/// Now display new things from each module

    $usedmodules = array();
    foreach($modinfo->cms as $cm) {
        if (isset($usedmodules[$cm->modname])) {
            continue;
        }
        if (!$cm->uservisible) {
            continue;
        }
        $usedmodules[$cm->modname] = $cm->modname;
    }

	/*
	// check $modname + _print_recent_activity function existence
    foreach ($usedmodules as $modname) {      // Each module gets it's own logs and prints them
        if (file_exists($CFG->dirroot.'/mod/'.$modname.'/lib.php')) {
			print_r("file exists for module: $modname \n");
            include_once($CFG->dirroot.'/mod/'.$modname.'/lib.php');
            $print_recent_activity = $modname.'_print_recent_activity';
            if (function_exists($print_recent_activity)) {
				print_r("function exists for module: $modname \n");
				print_r("content:\n");
                // NOTE: original $isteacher (second parameter below) was replaced with $viewfullnames!
                $content = $print_recent_activity($course, $viewfullnames, $timestart) || $content;
				print_r("content:\n");
				print_r("$content \n\n");
            }
        }
    }
	*/

    foreach ($usedmodules as $modname) {      // Each module gets it's own logs and prints them
		$extract_recent_activity = $modname.'_extract_recent_activity';
		if (function_exists($extract_recent_activity)) {
			$content = $extract_recent_activity($course, $viewfullnames, $timestart);
			if(is_array($content)) {
				$report = array_merge($report, $content);
			}
		}
    }

	return $report;
}


function forum_extract_recent_activity($course, $viewfullnames, $timestart) {
    global $CFG, $USER, $DB, $OUTPUT;

    // do not use log table if possible, it may be huge and is expensive to join with other tables

    if (!$posts = $DB->get_records_sql("SELECT p.*, f.type AS forumtype, d.id AS discussionid, d.forum, d.groupid,
                                              d.timestart, d.timeend, d.userid AS duserid,
                                              u.firstname, u.lastname, u.email, u.picture
                                         FROM {forum_posts} p
                                              JOIN {forum_discussions} d ON d.id = p.discussion
                                              JOIN {forum} f             ON f.id = d.forum
                                              JOIN {user} u              ON u.id = p.userid
                                        WHERE p.created > ? AND f.course = ?
                                     ORDER BY p.id ASC", array($timestart, $course->id))) { // order by initial posting date
         return false;
    }

    $modinfo = get_fast_modinfo($course);

    $groupmodes = array();
    $cms    = array();

    $strftimerecent = get_string('strftimerecent');

    $extractposts = array();
    foreach ($posts as $post) {
        if (!isset($modinfo->instances['forum'][$post->forum])) {
            // not visible
            continue;
        }
        $cm = $modinfo->instances['forum'][$post->forum];
        if (!$cm->uservisible) {
            continue;
        }
        $context = context_module::instance($cm->id);

        if (!has_capability('mod/forum:viewdiscussion', $context)) {
            continue;
        }

        if (!empty($CFG->forum_enabletimedposts) and $USER->id != $post->duserid
          and (($post->timestart > 0 and $post->timestart > time()) or ($post->timeend > 0 and $post->timeend < time()))) {
            if (!has_capability('mod/forum:viewhiddentimedposts', $context)) {
                continue;
            }
        }

        $groupmode = groups_get_activity_groupmode($cm, $course);

        if ($groupmode) {
            if ($post->groupid == -1 or $groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $context)) {
                // oki (Open discussions have groupid -1)
            } else {
                // separate mode
                if (isguestuser()) {
                    // shortcut
                    continue;
                }

                if (is_null($modinfo->groups)) {
                    $modinfo->groups = groups_get_user_groups($course->id); // load all my groups and cache it in modinfo
                }

                if (!in_array($post->groupid, $modinfo->get_groups($cm->groupingid))) {
                    continue;
                }
            }
        }

        $extractposts[] = $post;
    }
    unset($posts);

    if (!$extractposts) {
        return false;
    }
	
	$postlist = array();
    foreach ($extractposts as $post) {
		$postlist[] = array(
			'operation' => 'post',
			'moduleid' => $post->forum,
			'instanceid' => $post->discussion,
			'reference' => $post->id,
			'courseid' => $course->id,
			'modname' => 'forum',
			'name' => $post->subject,
			'timecreated' => intval($post->created),
			'timemodified' => intval($post->modified),
			//'text' => 'Post Added',
			'firstname' => $post->firstname,
			'lastname' => $post->lastname
		);
    }

	return $postlist;
}

