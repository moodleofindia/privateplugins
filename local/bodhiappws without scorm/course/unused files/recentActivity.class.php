<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class RecentActivityStructure extends ExternalObject{

    function __construct($courserecord) {
        parent::__construct($courserecord);
    }

    public static function get_class_structure() {
        return new external_single_structure(
			array(
				'operation' => new external_value(PARAM_TEXT, 'operation', VALUE_REQUIRED, 'unknown', NULL_NOT_ALLOWED),
				'courseid' => new external_value(PARAM_RAW, 'course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
				'moduleid' => new external_value(PARAM_RAW, 'module id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
				'instanceid' => new external_value(PARAM_RAW, 'instance id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
				'reference' => new external_value(PARAM_RAW, 'reference id', VALUE_OPTIONAL),
				'modname' => new external_value(PARAM_TEXT, 'module name', VALUE_REQUIRED, 'unknown', NULL_NOT_ALLOWED),
				'name' => new external_value(PARAM_TEXT, 'module instance name', VALUE_REQUIRED, 'unknown', NULL_NOT_ALLOWED),
				'timecreated' => new external_value(PARAM_INT, 'creation time', VALUE_OPTIONAL),
				'timemodified' => new external_value(PARAM_INT, 'modification time', VALUE_OPTIONAL),
				//'text' => new external_value(PARAM_TEXT, 'optional text', VALUE_OPTIONAL),
				'firstname' => new external_value(PARAM_TEXT, 'provider first name', VALUE_OPTIONAL),
				'lastname' => new external_value(PARAM_TEXT, 'provider lastname name', VALUE_OPTIONAL)
			), 'RecentActivityStructure'
        );
    }

}

?>
