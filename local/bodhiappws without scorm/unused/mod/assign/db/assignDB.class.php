<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

class assign_db {

    public static function get_assigns_by_courseid($courseid, $startpage, $n) {
        global $DB;
        $begin = $startpage*$n;
        return $DB->get_records('assign', array('course' => $courseid), '', '*', $begin, $n);
    }

    public static function get_assign($assigid) {
        global $DB;

        return $DB->get_record('assign',  array('id' => $assigid));
    }

    public static function get_submission_id($userid, $assigid) {
        global $DB;

        return $DB->get_record('assign_submission',  array('assignment' => $assigid, 'userid' => $userid),'id');
    }

    public static function get_submission($userid, $assigid) {
		global $DB, $USER;

        if (!$userid) {
            $userid = $USER->id;
        }
        // if the userid is not null then use userid
        $submission = $DB->get_record('assign_submission', array('assignment'=>$assigid, 'userid'=>$userid));

        if ($submission) {
            return $submission;
        } else {
            $submission = new stdClass();
            $submission->assignment   = $assigid;
            $submission->userid       = $userid;
            $submission->timecreated = time();
            $submission->timemodified = $submission->timecreated;
            $submission->status = ASSIGN_SUBMISSION_STATUS_DRAFT;
            $sid = $DB->insert_record('assign_submission', $submission);
            $submission->id = $sid;
            return $submission;
		}
    }

	public static function get_submission_text($submissionid, $assigid) {
        global $DB;

        return $DB->get_record('assignsubmission_onlinetext',  array('assignment' => $assigid, 'submission' => $submissionid), 'onlinetext');
    }

    public static function get_submission_grade($userid, $assigid) {
        global $DB;

        return $DB->get_record('assign_grades',  array('assignment' => $assigid, 'userid' => $userid));
    }

    public static function insert_onlinetext_submission($submission){
        global $DB;

        return $DB->insert_record('assignsubmission_onlinetext', $submission);
    }

	public static function update_onlinetext_submission($submission){
        global $DB;

        return $DB->update_record('assignsubmission_onlinetext', $submission);
    }

}

?>
