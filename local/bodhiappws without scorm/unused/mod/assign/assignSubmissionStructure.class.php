<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class AssignSubmissionStructure extends ExternalObject{

    function __construct($submissionrecord) {
        parent::__construct($submissionrecord);
    }

    public static function get_class_structure() {
        return new external_single_structure(
            array (
                'id'                => new external_value(PARAM_INT, 'submission id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'assignment'        => new external_value(PARAM_INT, 'assignment id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'userid'            => new external_value(PARAM_INT, 'user id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'groupid'           => new external_value(PARAM_INT, 'group id', VALUE_OPTIONAL),
                'status'            => new external_value(PARAM_INT, 'submission status', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'usertext'          => new external_value(PARAM_CLEANHTML,  'user text for the online assignments ', VALUE_OPTIONAL, '', NULL_ALLOWED),
                'userfiles'         => new external_multiple_structure(
											new external_single_structure(
												array(
                									'fileid'   => new external_value(PARAM_INT, 'file id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                									'filename' => new external_value(PARAM_TEXT, 'filename', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
												)
											)
				),
                'grade'             => new external_value(PARAM_FLOAT, 'submission grade', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'timecreated'       => new external_value(PARAM_INT, 'submission time creation', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'timemodified'      => new external_value(PARAM_INT, 'submission time modification', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
            ), 'AssignSubmissionStructure'
        );
    }
}
?>
