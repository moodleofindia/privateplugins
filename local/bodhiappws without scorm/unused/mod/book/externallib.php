<?php

require_once(dirname(__FILE__).'/../../config.php');
require_once(UNIAPP_ROOT . '/mod/book/bookStructure.class.php');
require_once($CFG->dirroot . '/lib/filelib.php');
require_once($CFG->dirroot . '/mod/book/lib.php');
require_once($CFG->dirroot . '/mod/book/locallib.php');

class uniappws_book extends uniapp_external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_book_parameters() {
        return new external_function_parameters (
            array(
                'bookid' => new external_value(PARAM_INT,  'Resource identifier', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED)
            )
        );
    }

    /**
     * Returns desired book
     *
     * @param int bookid
     *
     * @return book
     */
    public static function get_book($bookid) {
        global $DB;
        if (!$resource = $DB->get_record('book', array('id'=>$bookid))) {
            throw new moodle_exception('book:notfound', 'uniappws', '', '');
        }

        $cm = get_coursemodule_from_instance('book', $resource->id, $resource->course, false, MUST_EXIST);

        $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        require_capability('mod/resource:view', $context);
		require_capability('mod/book:read', $context);
		$viewhidden = has_capability('mod/book:viewhiddenchapters', $context);
		$metachapters = book_preload_chapters($resource);
		$chapters = array();
		foreach($metachapters as $ch) {
			$chapter = $DB->get_record('book_chapters', array('id' => $ch->id, 'bookid' => $bookid));
			$chapter->content = file_rewrite_pluginfile_urls($chapter->content, 'pluginfile.php', $context->id, 'mod_book', 'chapter', $chapter->id);
			$chapters[] = (array) $chapter;
		}
		$resource->chapters = $chapters;
		$resource->courseid = $resource->course;
		// fix the links
        $book = new BookStructure($resource);
		//print_r($book->get_data()); exit;

		return $book->get_data();

        throw new moodle_exception('book:unknownerror', 'uniappws', '', '');
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_book_returns() {
        return BookStructure::get_class_structure();
    }
}

?>
