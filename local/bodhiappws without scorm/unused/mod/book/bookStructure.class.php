<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class BookStructure extends ExternalObject{

    function __construct($resourcerecord) {
        parent::__construct($resourcerecord);
    }

    public static function get_class_structure(){
        return new external_single_structure(
        	array(
                'id'            => new external_value(PARAM_INT,        'resource id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'courseid'      => new external_value(PARAM_INT,        'course id', VALUE_REQUIRED, '0', NULL_NOT_ALLOWED),
                'name'          => new external_value(PARAM_TEXT,       'resource name', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                'intro'         => new external_value(PARAM_RAW,        'resource description', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                'numbering'     => new external_value(PARAM_INT,        'numbering type', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timemodified'  => new external_value(PARAM_INT,        'date of last modification in seconds', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'chapters'      => new external_multiple_structure(
                                        new external_single_structure(
											array(
												'id'            => new external_value(PARAM_INT,        'chapter id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
												'subchapter'    => new external_value(PARAM_INT,        'subchapter number', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
												'pagenum'       => new external_value(PARAM_INT,        'page number', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                								'title'         => new external_value(PARAM_TEXT,       'chapter title', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                								'content'       => new external_value(PARAM_RAW,       'chapter content', VALUE_REQUIRED, '', NULL_NOT_ALLOWED)
											)
										)
				),
            ), 'BookStructure'
        );
    }
}

?>
