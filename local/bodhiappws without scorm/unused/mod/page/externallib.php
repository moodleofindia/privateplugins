<?php

require_once(dirname(__FILE__).'/../../config.php');
require_once(UNIAPP_ROOT . '/mod/page/pageStructure.class.php');
require_once($CFG->dirroot . '/lib/filelib.php');

class uniappws_page extends uniapp_external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_page_parameters() {
        return new external_function_parameters (
            array(
                'pageid' => new external_value(PARAM_INT,  'Resource identifier', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED)
            )
        );
    }

    /**
     * Returns desired page
     *
     * @param int pageid
     *
     * @return page
     */
    public static function get_page($pageid) {
        global $DB;
        if (!$resource = $DB->get_record('page', array('id'=>$pageid))) {
            throw new moodle_exception('page:notfound', 'uniappws', '', '');
        }

        $cm = get_coursemodule_from_instance('page', $resource->id, $resource->course, false, MUST_EXIST);

        $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        require_capability('mod/resource:view', $context);
		// fix the links
		$resource->content = file_rewrite_pluginfile_urls($resource->content, 'pluginfile.php', $context->id, 'mod_page', 'content', $resource->revision);
		$resource->courseid = $resource->course;
        $page = new PageStructure($resource);
		//print_r($page->get_data()); exit;

		return $page->get_data();

        throw new moodle_exception('page:unknownerror', 'uniappws', '', '');
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_page_returns() {
        return PageStructure::get_class_structure();
    }
}

?>
