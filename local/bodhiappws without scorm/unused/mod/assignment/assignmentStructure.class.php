<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(UNIAPP_ROOT . '/lib/externalObject.class.php');

class AssignmentStructure extends ExternalObject{

    function __construct($assignmentrecord) {
        parent::__construct($assignmentrecord);
    }

    public static function get_class_structure(){
        return new external_single_structure(
            array (
                'id'                   => new external_value(PARAM_INT,  'assignment record id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'courseid'             => new external_value(PARAM_TEXT, 'course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'name'                 => new external_value(PARAM_TEXT, 'multilang compatible name', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'intro'                => new external_value(PARAM_RAW,  'assignment description text', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'introformat'          => new external_value(PARAM_RAW,  'assignment description format', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'assignmenttype'       => new external_value(PARAM_ALPHA, 'assignment type: upload, online, uploadsingle, offline', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'resubmit'             => new external_value(PARAM_INT,  'resubmit the assignment', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'preventlate'          => new external_value(PARAM_INT,  'prevent late submissions setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'emailteachers'        => new external_value(PARAM_INT,  'email teachers on submissions setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'var1'                 => new external_value(PARAM_INT,  'internal setting setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'var2'                 => new external_value(PARAM_INT,  'internal setting setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'var3'                 => new external_value(PARAM_INT,  'internal setting setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'var4'                 => new external_value(PARAM_INT,  'internal setting setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'var5'                 => new external_value(PARAM_INT,  'internal setting setting', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
                'maxbytes'             => new external_value(PARAM_INT,  'maximium bytes per submission', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timedue'              => new external_value(PARAM_INT,  'assignment due time', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timeavailable'        => new external_value(PARAM_INT,  'assignment available time', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'grade'                => new external_value(PARAM_INT,  'grade scale for assignment', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timemodified'        => new external_value(PARAM_INT,  'assignment modification time', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
            ), 'AssignmentStructure'
        );
    }
}

?>
