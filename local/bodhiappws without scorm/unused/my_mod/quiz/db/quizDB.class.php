<?php
include(BODHIAPP_ROOT . '/quiz_grade/db/quizgradeDB.class.php');
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
class quiz_db {

    public static function get_quiz_by_courseid($courseid, $startpage, $n) {
        global $DB;
        $begin = $startpage*$n;
        return $DB->get_records('quiz', array('course' => $courseid), '', '*', $begin, $n);
    }
	
	public static function get_quizquestion_by_courseid($courseid, $startpage, $n) {
        global $DB;
		$respfinal = array();
        $begin = $startpage*$n;
		$qid=$DB->get_records('quiz' array('course'=>$courseid));
		foreach($qid as $key => $quizid) {
			$quesid=$DB->get_records('quiz_question_instances', array('quiz' => $quizid));
			$i=0;
		foreach($quesid as $key => $value){
		
			 $resq="select q.id as id,q.category,q.name,q.qtype from {question} q where q.id = $value->question";
			
			 $resr1=$DB->get_record_sql($resq);
			 //var_dump($resr1->id);
			 $resansq="Select qa.answer from {question_answers} qa where  qa.question=$value->question";
			 $resr2=$DB->get_records_sql($resansq);
			 $j=1;
				foreach ($resr2 as $keyz => $valuez) {
					$resr2[$j] = $valuez->answer;
					$j++;
				}
				
			 $respfinal[$i] = array('id' => $resr1->id, 'category' => $resr1->category,'name' => $resr1->name,'qtype' => $resr1->qtype,
			 'option1' => $resr2[1], 
			 'option2' => $resr2[2], 
			 'option3' => $resr2[3], 
			 'option4' => $resr2[4], 
			 'option5' => $resr2[5])
			 'answerid'=>;//////need to do after this
			 $i++;
		
		}
		}
		//var_dump($respfinal[1]);
		return $respfinal;
    }
	
	

    public static function insert_quiz_attempt_data($quizid,$quizattemptdata, $userid, $currentattemptid) {
		global $DB;
		$starttime=time();
				$quizattemptobject = new stdClass();
				$quizattemptobject->quiz = $quizid;
				$layout=$DB->get_records('quiz_question_instances', array('quiz' => $quizid));
				$quiz=$DB->get_record('quiz', array('id' => $quizid));
				$layoutsize=sizeof($layout);
				$size=range(1,$layoutsize);
				$res=implode(',',$size);
				$sequence=$res.','.'0';
				$quizattemptobject->layout = $sequence;
				$quizattemptobject->sumgrades = $quiz->sumgrades;
				$quizattemptobject->state = $quizattemptdata;
				$quizattemptobject->timefinish = $starttime+5;//$endtime
				$quizattemptobject->timestart = $starttime; //strttime
				$quizattemptobject->timemodified = $starttime+100;//$strttime
				$quizattemptobject->userid = $userid;
				$quizattemptobject->attempt = $currentattemptid;
				$quizattemptobject->uniqueid = $currentattemptid;
				$rec=$DB->insert_record('quiz_attempts', $quizattemptobject);
		
    }
	public static function insert_question_attempt_data($quizid,$userid,$currentattemptid,$choice) {
			global $DB;
			$mtime=time();
			$choiceexplo=explode(",",$choice);
			$findnoquesq="select questions from {quiz} where id='$quizid'";
			$findnoquesr=$DB->get_record_sql($findnoquesq);
			$quesexplode=explode(",",$findnoquesr->questions);
			$remove = array(0);
			$result = array_diff($quesexplode, $remove); 
			$resultsize=sizeof($result);
	
		$questionattemptobject = new stdClass();
		if($resultsize==1) {
		//for quiz having single question
		$quesid = $DB->get_record('quiz_question_instances', array('quiz' => $quizid));
		
		//var_dump($quesid);
		$slotval=1;
		$choiceval=0;
		$recnum=1;
				//foreach($quesid as $questionid){
		//var_dump($quesid->question);
        $questionattemptobject->questionusageid = $currentattemptid;
		$questionattemptobject->slot = $slotval;
		$feedback = $DB->get_record('quiz', array('id' => $quizid));
		$questionattemptobject->behaviour=$feedback->preferredbehaviour;
        $questionattemptobject->questionid = $quesid->question;
        $questionattemptobject->variant = '1';
		
		//var_dump($sumgrade->sumgrades);
		 $questionattemptobject->maxmark = $feedback->sumgrades;
        $questionattemptobject->minfraction = '0.0000000';
        $questionattemptobject->maxfraction = '1.0000000'; 
		$questionattemptobject->flag = '0';//$for time being it is 0
		
		$questid= $quesid->question;
		$quesrec = $DB->get_record('question', array('id' =>$questid));	
		$quesans = $DB->get_records('question_answers', array('question' => $questid));
		$mm=1;
		foreach($quesans as $quesanswer) {
		$answer[$mm]=$quesanswer->answer;
		$mm++;
		}
		$ansimplode=implode(";",$answer);
        $questionattemptobject->questionsummary = $quesrec->name .":" .$ansimplode;
		$rightans = $DB->get_record('question_answers', array('question' => $questid,'fraction'=>'1.0000000'));
        $questionattemptobject->rightanswer = strip_tags($rightans->answer);
		$questionattemptobject->responsesummary = $choiceexplo[$choiceval];
        $questionattemptobject->timemodified = $mtime;
		//var_dump($questionattemptobject);
        $rec=$DB->insert_record('question_attempts', $questionattemptobject);
		
		//var_dump($rec);
		//to populate question attempt steps
		$state=array('todo','complete','gradedwrong','gradedright');
		$t=time();
		$step_data=array("_order","answer","-finish");
		
		for($i=0;$i<3;$i++){
		
		$questionattemptstepobject = new stdClass();
		$questionattemptstepobject->questionattemptid = $rec;
		$questionattemptstepobject->sequencenumber = $i;
		$questionattemptstepobject->state = $state[$i];
		$questionattemptstepobject->fraction = NULL ;
		
		if($i==2){
		$qstateq = "SELECT * FROM {question_attempts} WHERE rightanswer=responsesummary and id= $rec ";
		$qstate = $DB->get_record_sql($qstateq);
		//var_dump($qstateq);
		if($qstate) {
		$i++;
		$questionattemptstepobject->state = $state[$i];
		$questionattemptstepobject->fraction ='1.0000000' ;
		$i--;
		}else {
		$questionattemptstepobject->state = $state[$i];
		$questionattemptstepobject->fraction ='0.0000000' ;
		}
		}
		$questionattemptstepobject->timecreated = $t;
		$questionattemptstepobject->userid = $userid;
		$qatemsteprec=$DB->insert_record('question_attempt_steps', $questionattemptstepobject);
		//var_dump($qatemsteprec);
		$t=$t+5;
		
		/*for data insertion to question attempt step data
==================================================================*/
		$questionattemptstepdataobject = new stdClass();
		$questionattemptstepdataobject->attemptstepid = $qatemsteprec;
		$questionattemptstepdataobject->name = $step_data[$i];
		//var_dump($questid);
		$quesansq="select * from {question_answers} where question='$questid'";
		$quesansr = $DB->get_records_sql($quesansq);
		//var_dump($quesans);
		$k=0;
		foreach($quesansr as $quesanswer) {
		$answerid[$k]=$quesanswer->id;
		$k++;
		}
		//var_dump($answerid);
		$answeridimplode=implode(",",$answerid);
		if($i==0) {
		$questionattemptstepdataobject->value = $answeridimplode;
	}else if($i==1) {
	$userchoice=$choiceexplo[$choiceval];
	$quesansidq="select id, answer from {question_answers} where question='$questid'";
		$quesansidr = $DB->get_records_sql($quesansidq);
		//var_dump($quesansidr);
		foreach($quesansidr as $key => $valueq){
		
		$option=strip_tags($valueq->answer);
		
		$optionid=$valueq->id;

		if($option == $userchoice){
		$finalid=$optionid;
		break;
		}
		}
	$finspos=array_search($finalid,$answerid);
	//var_dump($finspos);
	$questionattemptstepdataobject->value = $finspos;
	}else {
	$questionattemptstepdataobject->value = 1;
	}
	//var_dump($questionattemptstepdataobject);
		$qatemstepdatarec=$DB->insert_record('question_attempt_step_data', $questionattemptstepdataobject);
		
    }
		$choiceval++;
		$slotval++;
	
	//}
		} else {
		//for quiz having multiple question
		$quesid = $DB->get_records('quiz_question_instances', array('quiz' => $quizid));
		$slotval=1;
		$choiceval=0;
		$recnum=1;
				foreach($quesid as $questionid){
        $questionattemptobject->questionusageid = $currentattemptid;
		$questionattemptobject->slot = $slotval;
		$feedback = $DB->get_record('quiz', array('id' => $quizid));
		$questionattemptobject->behaviour=$feedback->preferredbehaviour;
        $questionattemptobject->questionid = $questionid->question;
        $questionattemptobject->variant = '1';
		 $questionattemptobject->maxmark = $feedback->sumgrades;
        $questionattemptobject->minfraction = '0.0000000';
        $questionattemptobject->maxfraction = '1.0000000'; 
		$questionattemptobject->flag = '0';//$for time being it is 0
		
		$questid= $questionid->question;
		$quesrec = $DB->get_record('question', array('id' =>$questid));	
		$quesans = $DB->get_records('question_answers', array('question' => $questid));
		$mm=1;
		foreach($quesans as $quesanswer) {
		$answer[$mm]=$quesanswer->answer;
		$mm++;
		}
		$ansimplode=implode(";",$answer);
        $questionattemptobject->questionsummary = $quesrec->name .":" .$ansimplode;
		$rightans = $DB->get_record('question_answers', array('question' => $questid,'fraction'=>'1.0000000'));
        $questionattemptobject->rightanswer = strip_tags($rightans->answer);
		$questionattemptobject->responsesummary = $choiceexplo[$choiceval];
        $questionattemptobject->timemodified = $mtime;
        $rec=$DB->insert_record('question_attempts', $questionattemptobject);
		
		//to populate question attempt steps table dat
		$state=array('todo','complete','gradedwrong','gradedright');
		$t=time();
		$step_data=array('_order','answer','-finish');
		for($i=0;$i<3;$i++){
		
		$questionattemptstepobject = new stdClass();
		$questionattemptstepobject->questionattemptid = $rec;
		$questionattemptstepobject->sequencenumber = $i;
		$questionattemptstepobject->state = $state[$i];
		$questionattemptstepobject->fraction = NULL ;
		
		if($i==2){
		$qstateq = "SELECT * FROM {question_attempts} WHERE rightanswer=responsesummary and id= $rec ";
		$qstate = $DB->get_record_sql($qstateq);
		//var_dump($qstateq);
		if($qstate) {
		$i++;
		$questionattemptstepobject->state = $state[$i];
		$questionattemptstepobject->fraction ='1.0000000' ;
		$i--;
		}else {
		$questionattemptstepobject->state = $state[$i];
		$questionattemptstepobject->fraction ='0.0000000' ;
		}
		}
		$questionattemptstepobject->timecreated = $t;
		$questionattemptstepobject->userid = $userid;
		$qatemsteprec=$DB->insert_record('question_attempt_steps', $questionattemptstepobject);
		//var_dump($qatemsteprec);
		$t=$t+5;
		
		//for data insertion to question attempt step data
		//-----------------------------------------------------
		
		$questionattemptstepdataobject = new stdClass();
		$questionattemptstepdataobject->attemptstepid = $qatemsteprec;
		$questionattemptstepdataobject->name = $step_data[$i];
		//var_dump($questid);
		$quesansq="select * from {question_answers} where question='$questionid->question'";
		$quesansr = $DB->get_records_sql($quesansq);
		//var_dump($quesans);
		$k=0;
		foreach($quesansr as $quesanswer) {
		$answerid[$k]=$quesanswer->id;
		$k++;
		}
		//var_dump($answerid);
		$answeridimplode=implode(",",$answerid);
		//var_dump($answeridimplode);
		if($i==0) {
		$questionattemptstepdataobject->value = $answeridimplode;
	}else if($i==1) {
	$userchoice=$choiceexplo[$choiceval];
	$quesansidq="select id, answer from {question_answers} where question='$questid'";
		$quesansidr = $DB->get_records_sql($quesansidq);
		//var_dump($quesansidr);
		foreach($quesansidr as $key => $valueq){
		
		$option=strip_tags($valueq->answer);
		
		$optionid=$valueq->id;

		if($option == $userchoice){
		$finalid=$optionid;
		break;
		}
		}
	$finspos=array_search($finalid,$answerid);
	//var_dump($finspos);
	$questionattemptstepdataobject->value = $finspos;
	}else {
	$questionattemptstepdataobject->value = 1;
	}
	
		$qatemstepdatarec=$DB->insert_record('question_attempt_step_data', $questionattemptstepdataobject);
		
    }
	$choiceval++;
		$slotval++;
	
	}
		
		}	
		

	
}
	public static function get_quizsubmission_by_quizid($quizid, $state ,$choice) {
        global $DB, $USER;
        $cm = $DB->get_record('course_modules', array('instance' => $quizid,'module'=>'16'));
		$contex = $DB->get_record('context', array('instanceid' => $cm->id,'contextlevel'=>'70'));
		$feedback = $DB->get_record('quiz', array('id' => $quizid));
		
		$data = new stdClass();
		$data->contextid=$contex->id;
		$data->component='mod_quiz';
		$data->preferredbehaviour=$feedback->preferredbehaviour;
		//var_dump($data);
		$lastinsertid = $DB->insert_record('question_usages', $data);
		
		$currentattemptid=$lastinsertid;
		$quizattemptdata=$state;
		$userid=$USER->id;
		
		$res=self::insert_quiz_attempt_data($quizid,$quizattemptdata, $userid, $currentattemptid);
		$res1=self::insert_question_attempt_data($quizid,$userid, $currentattemptid, $choice);
	
	
    }
	
}	
?>
