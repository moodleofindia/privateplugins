<?php 

//  This script must be included from a Moodle page
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');   
}

define("BODHIAPP_ROOT", $CFG->dirroot . '/local/bodhiappws');

require_once(BODHIAPP_ROOT . '/bodhiapp_external_api.class.php');
require_once(BODHIAPP_ROOT . '/util/logger.class.php');

?>
