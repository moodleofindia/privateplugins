<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once(BODHIAPP_ROOT . '/lib/externalObject.class.php');

class quizStructure extends ExternalObject{

    function __construct($quizrecord) {
        parent::__construct($quizrecord);
    }

    public static function get_class_structure(){
        return new external_single_structure(
            array (
                'id'                         => new external_value(PARAM_INT,  'assign record id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
				'courseid'                   => new external_value(PARAM_INT,  'course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'name'                       => new external_value(PARAM_TEXT, 'multilang compatible name', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'intro'                      => new external_value(PARAM_RAW,  'assign description text', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'introformat'                => new external_value(PARAM_RAW,  'assign description format', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timeopen'      			 => new external_value(PARAM_INT,  'assign description visibility setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timeopen'             		 => new external_value(PARAM_INT,  'assign submission setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'timelimit'          		 => new external_value(PARAM_INT,  'assign submission drafts', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'overduehandling'         	 => new external_value(PARAM_RAW,  'assign notifications setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'graceperiod'      			 => new external_value(PARAM_INT,  'assign late notifications setting', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'preferredbehaviour'         => new external_value(PARAM_RAW,  'assign due date', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'attempts'  				 => new external_value(PARAM_INT,  'assign submit from date', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'attemptonlast'              => new external_value(PARAM_INT,  'assign cutoff date'),
                'grademethod'				 => new external_value(PARAM_INT,  'assign required submission statement'),
                'decimalpoints'          	 => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'questiondecimalpoints'      => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'reviewattempt'			   	 => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'reviewcorrectness'          => new external_value(PARAM_INT,  'assign teamsubmission setting'),
                'reviewmarks'                => new external_value(PARAM_INT,  'assign blindmarking setting'),
                'reviewspecificfeedback'           => new external_value(PARAM_INT,  'assign reveal identities setting'),
                'reviewgeneralfeedback'                      => new external_value(PARAM_INT,  'grade scale for assign', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'reviewrightanswer'               => new external_value(PARAM_INT,  'assign time modification', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
            ), 'quizStructure'
        );
    }
}

// assign fields sample
/*
[id] => 1
[course] => 7
[name] => Nippon in one picture
[intro] => Upload just one picture that represents Japan best 
[introformat] => 1
[alwaysshowdescription] => 1
[nosubmissions] => 1
[submissiondrafts] => 0
[sendnotifications] => 0
[sendlatenotifications] => 0
[duedate] => 1342789500
[allowsubmissionsfromdate] => 1342184700
[grade] => 100
[timemodified] => 1362048922
[requiresubmissionstatement] => 0
[completionsubmit] => 0
[cutoffdate] => 0
[teamsubmission] => 0
[requireallteammemberssubmit] => 0
[teamsubmissiongroupingid] => 0
[blindmarking] => 0
[revealidentities] => 0
*/
?>
