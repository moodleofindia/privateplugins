<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once(dirname(__FILE__).'/../../config.php');
require_once("$CFG->dirroot/mod/assign/locallib.php");

require_once(BODHIAPP_ROOT . '/mod/quiz/quizSubmissionStructure.class.php');
require_once(BODHIAPP_ROOT . '/mod/quiz/quizStructure.class.php');
require_once(BODHIAPP_ROOT . '/mod/quiz/quizquestionStructure.class.php');
require_once(BODHIAPP_ROOT . '/mod/quiz/db/quizDB.class.php');
require_once(BODHIAPP_ROOT . '/course/db/courseDB.class.php');

class bodhiappws_quiz extends bodhiapp_external_api {

    public static function get_quiz_by_courseid_parameters() {
        return new external_function_parameters(
            array (
                'courseid'  => new external_value(PARAM_INT, 'Course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'startpage' => new external_value(PARAM_INT, 'Start page', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                'n'         => new external_value(PARAM_INT, 'Page number', VALUE_DEFAULT, 10, NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_quiz_by_courseid($courseid, $startpage, $n) {
        $system_context = get_context_instance(CONTEXT_SYSTEM);


        self::validate_context($system_context);
        // check for view capability at course level
        $context = get_context_instance(CONTEXT_COURSE, $courseid);
        require_capability('mod/quiz:view', $context);

        if (!$course = course_db::get_course_by_courseid($courseid)) {
            throw new moodle_exception('quiz:unknowncourseidnumber', 'bodhiappws', '', $courseid);
        }

        $quiz = quiz_db::get_quiz_by_courseid($courseid, $startpage, $n);
		//var_dump($quiz);
        $returnquiz = array();
        foreach ($quiz as $quizval) {
            if (!$cm = get_coursemodule_from_instance('quiz', $quizval->id, 0, false, MUST_EXIST)) {
                throw new moodle_exception('quiz:notfound', 'bodhiappws', '', '');
            }

            $context = get_context_instance(CONTEXT_MODULE, $cm->id);
            if (!has_capability('mod/quiz:view', $context)) {
                continue;
            }
			$quizval->courseid = $quizval->course;
            $quizval = new quizStructure($quizval);
            $returnquiz[] = $quizval->get_data();
        }

        return $returnquiz;
    }

    public static function get_quiz_by_courseid_returns() {
        return new external_multiple_structure(
            quizStructure::get_class_structure()
        );
    }
	//------------------------for quiz question---------------------------
	public static function get_quizquestion_by_courseid_parameters() {
        return new external_function_parameters(
            array (
                'courseid'  => new external_value(PARAM_INT, 'Course id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'startpage' => new external_value(PARAM_INT, 'Start page', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                'n'         => new external_value(PARAM_INT, 'Page number', VALUE_DEFAULT, 10, NULL_NOT_ALLOWED)
            )
        );
    }


    public static function get_quizquestion_by_courseid($courseid, $startpage, $n) {
	
        $question = quiz_db::get_quizquestion_by_courseid($courseid, $startpage, $n);
		//var_dump($question);
        $returnquestion = array();
        foreach ($question as $questionval) {
//var_dump($questionval);
			$newquestionval->id = $questionval['id'];
			$newquestionval->category = $questionval['category'];
			$newquestionval->name = $questionval['name'];
			$newquestionval->qtype = $questionval['qtype'];
			$newquestionval->option1 = strip_tags($questionval['option1']);
			$newquestionval->option1_id = strip_tags($questionval['option1_id']);
				
			$newquestionval->option2 = strip_tags($questionval['option2']);
			$newquestionval->option2_id = $questionval['option2_id'];
			$newquestionval->option3 = strip_tags($questionval['option3']);
			$newquestionval->option3_id = $questionval['option3_id'];
			$newquestionval->option4 = strip_tags($questionval['option4']);
			$newquestionval->option4_id =$questionval['option4_id'];
			$newquestionval->option5 = strip_tags($questionval['option5']);
			$newquestionval->option5_id =$questionval['option5_id'];
			$newquestionval->answerid = $questionval['answerid'];
			//var_dump($newquestionval->id);
			
            $newquestionval1 = new quizquestionStructure($newquestionval);
			
            $returnquestion[] = $newquestionval1->get_data();
        }

        return $returnquestion;
    }

    public static function get_quizquestion_by_courseid_returns() {
        return new external_multiple_structure(
            quizquestionStructure::get_class_structure()
        );
    }

//---------------------------------for quiz submission-----------------------------	
public static function get_quizsubmission_by_quizid_parameters() {
        return new external_function_parameters(
            array (
                'quizid'  => new external_value(PARAM_INT, 'quiz id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'state' => new external_value(PARAM_TEXT, 'State', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED),
				'choice' => new external_value(PARAM_TEXT, 'choice', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_quizsubmission_by_quizid($quizid, $state ,$choice) {
	
        $system_context = get_context_instance(CONTEXT_SYSTEM);
        self::validate_context($system_context);
        $submission = quiz_db::get_quizsubmission_by_quizid($quizid, $state ,$choice);
        $returnquiz = array();
        foreach ($submission as $quizval) {
			$quizval->courseid = $quizval->course;
            $quizval = new quizStructure($quizval);
            $returnquiz[] = $quizval->get_data();
        }

        return $returnquiz;
    }

    public static function get_quizsubmission_by_quizid_returns() {
        return new external_multiple_structure(
            quizsubmissionStructure::get_class_structure()
        );
    }






/*
	public static function get_quizsubmission_by_quizid_parameters() {
        return new external_function_parameters(
            array (
                'quizid'  => new external_value(PARAM_INT, 'quiz id', VALUE_REQUIRED, 0, NULL_NOT_ALLOWED),
                'state' => new external_value(PARAM_TEXT, 'State', VALUE_OPTIONAL, 0, NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_quizsubmission_by_quizid($quizid, $state){
	$system_context = get_context_instance(CONTEXT_SYSTEM);
	self::validate_context($system_context);
    $submission=quiz_db::get_quizsubmission_by_quizid($quizid, $state);
	}
	*/
	
}
