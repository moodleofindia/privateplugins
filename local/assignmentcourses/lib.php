<?php

require_once("$CFG->dirroot/lib/completionlib.php");
use core_completion\progress;

function local_assignmentcourses_extend_navigation(global_navigation $nav) {

    global $CFG,$USER;
    $systemcontext = context_system::instance();
    $nav->showinflatnavigation = true;
    //index.php
    //list of organization here 

    $abc = $nav->add(get_string('mapsup_map','local_assignmentcourses'),
        $CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_manage.php');
    $abc->showinflatnavigation = true;
    $abc = $nav->add(get_string('pluginname','local_assignmentcourses'),
        $CFG->wwwroot.'/local/assignmentcourses/assignmentcourses.php');
    $abc->showinflatnavigation = true;
    $abc = $nav->add(get_string('report','local_assignmentcourses'),
        $CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_report.php');
    $abc->showinflatnavigation = true;
    
    //list of cohort mapping here
}

//course name 
function coursename($courseid){
   global $CFG,$DB,$USER;
   $coursename =  $DB->get_record('course',array('id'=>$courseid),'fullname');
   if($coursename){
    return $coursename;
}
}
//username 
function username($userid){
   global $CFG,$DB,$USER;
   $username =  $DB->get_record('user',array('id'=>$userid),'id,username,firstname,lastname,email');
   if($username){
    return $username;
}
}

function user_status_course_report($courseid,$newuserid){
    global $CFG,$DB,$USER;
    if(is_null($newuserid)){
        $user = $USER;
        return NULL;
    }else{
        $userobj = $DB->get_record('user',array('id'=>$newuserid));
    }
    $graade = [];
    $coursesprogress = [];
    $course = $DB->get_record('course',array('id'=>$courseid));
    if($course){
        $completion = new \completion_info($course);
        $percentage = progress::get_course_progress_percentage($course,$userobj->id);
        if (!is_null($percentage)) {
            $percentage = floor($percentage);
        }
        $params = array(
            'userid'    => $userobj->id,
            'course'  => $course->id
            );
        $ccompletion = new completion_completion($params);

        $coursesprogress[$course->id]['completed'] = 
        $completion->is_course_complete($userobj->id);
        $coursesprogress[$course->id]['progress'] = $percentage;
        if ($coursesprogress[$course->id]['completed'] == false) {
            if ($coursesprogress[$course->id]['progress'] > 0 ) {
                $status = '-';
            } else {
                $status = get_string('ns','local_assignmentcourses');;
            }

        } else {
            $status = get_string('cm','local_assignmentcourses'); 
        }
        //print_object($grades);
    }   
    if($status){
      return $status;  
  }
    //print_object($grade);
}

function user_progress_course_assigned_report($courseid,$newuserid){
    global $CFG,$DB,$USER;
    if(is_null($newuserid)){
        $user = $USER;
        return NULL;
    }else{
        $userobj = $DB->get_record('user',array('id'=>$newuserid));
    }
    $dateformat = '%d-%b-%Y';
    $graade = [];
    $coursesprogress = [];
    $course = $DB->get_record('course',array('id'=>$courseid));
    if($course){
        $completion = new \completion_info($course);
        $percentage = progress::get_course_progress_percentage($course,$userobj->id);
        if (!is_null($percentage)) {
            $percentage = floor($percentage);
        }
        $params = array(
            'userid'    => $userobj->id,
            'course'  => $course->id
            );
        $ccompletion = new completion_completion($params);

        $coursesprogress[$course->id]['completed'] = 
        $completion->is_course_complete($userobj->id);
        $coursesprogress[$course->id]['progress'] = $percentage;
        $completiondate = '';
        if ($coursesprogress[$course->id]['completed'] == false) {
            if ($coursesprogress[$course->id]['progress'] > 0 ) {
                $status = '-';
            } else {
                $status = get_string('ns','local_access_level_org_report');;
            }

        } else {
            $status = get_string('cm','local_access_level_org_report'); 
            $completiondate = userdate($ccompletion->timecompleted, $dateformat);
        }
        //print_object($ccompletion);
        $grades = grade_get_course_grades($courseid, $newuserid);
        //print_object($grades);
        $grademax = $grades->grademax;
        $grade1 = (int) $grades->grades[$newuserid]->str_grade;
        //print_object($grades);
    }   
    $grade = array(
        'status'=>$status,
        'grade' => $grade1 ,
        'completiondate' =>$completiondate
        );
    //print_object($grade);
    return $grade;
}

function userenrol_details($courseid){
 global $CFG,$DB,$USER;
 $sql = "SELECT u.userid, u.timecreated
 FROM {user_enrolments} u
 LEFT JOIN {enrol} e
 on u.enrolid = e.id
 WHERE e.courseid = $courseid AND enrol = 'manual'";
 $uenroldetails = $DB->get_records_sql($sql);
 if($uenroldetails){
    return $uenroldetails;
}
}

function teamuser($username){
 global $CFG,$DB,$USER;
 $sql = "SELECT u.id as userid,u.firstname,u.lastname,u.username,u.email
 from {user} u 
 join {user_info_data} uid
 on u.id = uid.userid 
 join {user_info_field} uif
 on uif.id = uid.fieldid
 join {local_managemapping} lm 
 on lm.mapfield = uif.shortname 
 where uid.data = '$username'";
 $users = $DB->get_records_sql($sql);
 if($users){
    return $users;
}

}
function userenrol_time($courseid,$userid){
 global $CFG,$DB,$USER;
 $sql = "SELECT u.userid, u.timecreated
 FROM {user_enrolments} u
 LEFT JOIN {enrol} e
 on u.enrolid = e.id
 WHERE e.courseid = $courseid AND enrol = 'manual' and u.userid = $userid";
 $uenroldetails = $DB->get_record_sql($sql);
 if($uenroldetails){
    return $uenroldetails;
}
}


/**
 * Sends notification messages to the interested parties that assign the role capability
 *
 * @param object $recipient user object of the intended recipient
 * @param object $a associative array of replaceable fields for the templates
 *
 * @return int $courseid
 */
function allocation_send_alert($recipient, $submitter, $courseid) {
    // Prepare the message.
    $eventdata = new \core\message\message();
    $eventdata->courseid          = $courseid;
    $eventdata->component         = 'mod_quiz';
    $eventdata->name              = 'submission';
    $eventdata->notification      = 1;

    $eventdata->userfrom          = $submitter;
    $eventdata->userto            = $recipient;
    $eventdata->subject           = get_string('pluginname', 'local_assignmentcourses');
    $eventdata->fullmessage       = get_string('success', 'local_assignmentcourses');
    $eventdata->fullmessageformat = FORMAT_PLAIN;
    $eventdata->fullmessagehtml   = '';

   // $eventdata->smallmessage      = get_string('emailnotifysmall', 'quiz');
    // ... and send it.
    return message_send($eventdata);
}