<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/assignmentcourses_manage_form.php');
require_once($CFG->libdir . '/formslib.php');		
require_login(0,false);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/assignmentcourses/assignmentcourses_manage.php');
$title = get_string('accessinfohdr', 'local_assignmentcourses');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/assignmentcourses/datatable/javascript/jquery-1.9.1.js'),true);
echo $OUTPUT->header();   
$mform = new local_assignmentcourses_manage_form();
$temp = false;
$a[] = '';
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/assignmentcourses/assignmentcourses_manage.php', array()));
} else if ($data = $mform->get_data()) {

	$exit = $DB->record_exists('local_managemapping',array('fieldname'=>$data->fieldname,'selectfield'=>$data->selectfield,'mapfield'=>$data->mapfield));
	if(!$exit){
		$save = new stdClass();
		$save->fieldname=$data->fieldname;
		$save->selectfield=$data->selectfield;
		$save->mapfield= $data->mapfield;

		$insert = $DB->insert_record('local_managemapping',$save);
		if($insert){
			echo html_writer::div(
				get_string('insert_map','local_assignmentcourses'),'alert alert-success'
				);
		}
	}else{
		echo html_writer::div(
			get_string('record1','local_assignmentcourses'),'alert alert-danger'
			);
	}
}
$note = get_string('notepoint','local_assignmentcourses');
echo $note;
$mform->display();

echo '<br><br>';
echo '<h3>'.get_string('listnote','local_assignmentcourses').'</h3>';
$managemaplists = $DB->get_records('local_managemapping');

$table = new html_table();

$table->head = (array) get_strings(array('fname', 'sfield', 'mapfield','action'), 'local_assignmentcourses');
if($managemaplists){
	foreach($managemaplists as $managemaplist) {
		$table->data[] = array(
			$managemaplist->fieldname,
			$managemaplist->selectfield,
			$managemaplist->mapfield,
			html_writer::link(

				new moodle_url(
					$CFG->wwwroot.'/local/assignmentcourses/edit_assignmentcourses_manage.php',
					array(
						'id' => $managemaplist->id,
						'edit'=>$managemaplist->selectfield
						)
					),
				'Edit',
				array(
					'class' => 'btn btn-small btn-primary'
					)
				).'-'.
			html_writer::link(
				new moodle_url(
					$CFG->wwwroot.'/local/assignmentcourses/delete_manage.php',
					array('id' => $managemaplist->id)), 'Delete',array('class' =>'btn btn-small btn-primary' ))
			);
	}
	echo html_writer::table($table);
}else{
	echo html_writer::div(
		get_string('norecordmap','local_assignmentcourses'),'alert alert-danger'
		);
}


echo $OUTPUT->footer();
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".customtextbox").hide();
		$(".profiletextbox").hide();
		$("#id_selectfield").change(function(){
	//alert(this.value);
			if(this.value =='user'){	
				$(".profiletextbox").show();
				$(".customtextbox").hide();
			}else if(this.value =='custom'){
				$(".customtextbox").show();
				$(".profiletextbox").hide();
			}else{
				$(".customtextbox").hide();
				$(".profiletextbox").hide();
			}
		});
	});
</script>





