<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* Handles uploading files
*
* @package    local_assignmentcourses
* @author 		
* @copyright  
* @license    
*/
require_once('../../config.php');
require_once($CFG->dirroot.'/local/assignmentcourses/csslinks.php');
require_login(0,false);
//$createorgcap = has_capability('local/accesscohort:addorganization',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('accessinfohdr2', 'local_assignmentcourses');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
//forcapabilty aassiging here 
$systemcontext = context_system::instance();
$PAGE->set_url('/local/assignmentcourses/assignmentcourses_course_activity.php');
require_login();
include_once('jslink.php');
require_once($CFG->dirroot.'/local/assignmentcourses/csslinks.php');
$PAGE->requires->jquery();		
$PAGE->navbar->ignore_active();
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_assignmentcourses'),new moodle_url($CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_report.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_user_report.php'));
$thingnode->make_active();
//include_once('../datatable.php');
echo $OUTPUT->header();
$courseid = required_param('id',PARAM_INT );
$userid = required_param('userid',PARAM_INT);
echo '<h2>'.get_string('accessinfohdr2','local_assignmentcourses').'</h2>';
echo '<hr>';
$output1 = '';
//get_courses 

$course = get_course($courseid);
$cmsinfo = get_fast_modinfo($courseid);
$act =get_array_of_activities($courseid);
//display user 4 block we have to achive task here 
$courses=enrol_get_users_courses($userid, true, 'id, visible, shortname');
$countofcourse = count($courses);
$uname = userinfo($userid);
$count = '';

$table = new html_table();
$table->id =  'example';
$table->head = (array) get_strings(array('acname', 'acgrade','cmdate', 'cmstatus'), 'local_assignmentcourses');

$modinfo = get_fast_modinfo($courseid);
if($modinfo){
	foreach ($modinfo->get_cms() as $cm) {
		$dateformat = '%d-%b-%Y';
		$course = $DB->get_record('course',array('id'=>$courseid));
		$completion = new completion_info($course);
		$info = $completion->get_data($cm,false,19);
		$grades = grade_get_grades($courseid, 'mod', $cm->modname, $cm->instance,$userid);
		if($info->timemodified){
			$completiondate = userdate($info->timemodified,$dateformat);
		}else{
			$completiondate = '-';
		}
		if (!empty($grades->items[0]->grades)) {
			$grade = reset($grades->items[0]->grades);
			if (!empty($grade->item)) {
				$grademax = floatval($grade->item->grademax);
			} else {
				$grademax = floatval($grades->items[0]->grademax);
			}
			$grade = $grade->grade;
			if($info->completionstate==1){
				$status = get_string('com','local_assignmentcourses');
				$count++;
			}else{
				$status = 'Not Completed';
			}
			if($grade){
				$gradevalue = number_format($grade,2);
			}else{
				$gradevalue = '-';
			}
			$table->data[] =array(
				$cm->name,
				$gradevalue,
				$completiondate,
				$status
				);
		}
	}
}else{
	echo html_writer::div(
		get_string('noactivity', 'local_assignmentcourses'),'alert alert-danger'
		);
}
//displaying block here 
$output1 .=' <div class="panel-body">
		<div class="row">
			<div class=" col-md-12">
				<div class="col-md-3">
					<div class="card bg-primary text-white">
						<div class="card-block">
							<h3 class="card-text">'.get_string('fullname','local_assignmentcourses').'</h3>
							<p>'.$uname->firstname.'-'.$uname->lastname.'</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card bg-success text-white">
						<div class="card-block">
							<h3 class="card-text">'.get_string('email','local_assignmentcourses').'</h3>
							<p>'.$uname->email.'</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card card bg-warning text-white">
						<div class="card-block">
							<h3 class="card-text">'.get_string('noofenrolcourse','local_assignmentcourses').'</h3>
							<h4>'.$countofcourse.'</h4>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card card bg-info text-white">
						<div class="card-block">
							<h3 class="card-text">'.get_string('noofcompcourses','local_assignmentcourses').'</h3>
							<h4>'.$count.'</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>'; 
	echo $output1;
	echo '<br>';

echo html_writer::table($table);
echo $OUTPUT->footer();


