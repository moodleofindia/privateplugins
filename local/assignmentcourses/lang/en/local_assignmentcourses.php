<?php 
$string['pluginname'] = 'Assignment Courses';
$string['fieldname']='Field name';
$string['sname'] = 'Select field type ';
$string['usercustomprofile']='Select mapped field';
$string['userprofile']='User Profile field';
$string['accessinfohdr']='Assignment of courses manage field';
$string['mapsup_map']='Manager/Supervisor Mapping';
$string['insert_map'] ='Mapping is done successfully!!';
$string['insert_map1'] ='Course Assigned successfully!!';
$string['record'] = 'Course was already assigned!!';
$string['fname'] = 'Field Name';
$string['sfield'] = 'Selected Field';
$string['mapfield'] = 'Mapping Field';
$string['listnote'] = 'Manage list of fields for manager and superviosr';
$string['edit_assignment_manage'] = 'Edit Assignment of Courses';
$string['upmanage'] = 'Assigned course updated successfully!!';
$string['notepoint'] = '<b>Note : </b><br>1 : In this screen you will able to map Supervisor and Manager with respective profile field.<br> 2 : On the selection of the field type you will be able to see a drop down list of profile field name. You can select one of the field name with Supervisor and Manager';
$string['coursename'] = 'Select course name';
$string['uname'] = 'Select user name';
$string['coursename1']='Course Name';
$string['username1']='User Name';
$string['assigby']='Assigned by';
$string['enddate']='End Date';
$string['ccomplete']='Status';
$string['ns']='Not Started';
$string['cm']='Completed';
$string['norecord'] = 'You dont have any team to manage.';
$string['note'] = '<h5><b> Note : </b></h5>';
$string['desc'] = 'Please select course and then select user to assign the course. Date selection is optional. ';
$string['report'] = 'Assigned Course Report';
$string['coursecat'] = 'Select Course Category';
$string['uname'] = 'User Name';
$string['fullname'] = 'Full Name';
$string['email'] = 'Email';
$string['csname'] = 'Course Name';
$string['cdate'] = 'Completion Date';
$string['edate'] = 'Enrolement Date';
$string['grade'] = 'Grade';
$string['status'] = 'Status';


//new 
$string['accessinfohdr1'] = 'Assigned courses report';
$string['noofenrolcourse'] = 'Enrolled Courses';
$string['noofcompcourses'] = 'Completed Courses';
$string['catname'] = 'Category name';
$string['lastacces'] = 'Date of last aceess';
$string['compl'] = 'Completion status';
$string['novisted'] = 'Number of times visited';
$string['acname']='Activity Name';
$string['acgrade']='Activity Grade';
$string['cmdate']='Completion Date';
$string['cmstatus']='Compltion Status';
$string['accessinfohdr2'] = 'Assigned Course Activity Report';
$string['assigndate']= 'Assigned Date';
$string['assigby'] = 'Assigned by';
$string['com'] = 'Completed';
$string['ncom'] = 'Not Completed';
$string['success']='Course assignment and enrolment done successfully';
$string['noactivity']='There is no activity added in this courses';
$string['pls'] = 'Please enter field name';
$string['userf'] = 'User Profile Field';
$string['custf'] = 'Custom Profile Field';
$string['action'] = 'Action';
$string['listassignment'] = 'List of assigned courses';
$string['desc1'] = 'Please select course category to view report. You can click on user fullname to view user specific report.';
$string['clreport'] = 'Course Level Report';
$string['norecordmap'] ='There is no mapping record is exists';
$string['edate1'] = 'Course End Date';
$string['record1'] = 'Course Manage record already exists';




