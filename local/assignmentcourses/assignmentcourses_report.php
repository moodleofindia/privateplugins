<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/assignmentcourses_report_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/enrollib.php');
require_login(0,false);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/assignmentcourses/assignmentcourses_report.php');
$title = get_string('report', 'local_assignmentcourses');
$PAGE->set_title($title);
$PAGE->set_heading($title);
//$PAGE->navbar->add($title);
include_once('jslink.php');
require_once($CFG->dirroot.'/local/assignmentcourses/csslinks.php');

//navigation code here 
$PAGE->navbar->ignore_active();
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_assignmentcourses'),new moodle_url($CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_report.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_report.php'));
$thingnode->make_active();
$PAGE->requires->jquery();		
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/assignmentcourses/js/select.js'));
echo $OUTPUT->header();
$mform = new local_assignmentcourses_report_form(null,array('csdata'=>$_POST));
$mform->display();
$data = $mform->get_data();
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/assignmentcourses/assignmentcourses_report.php', array()));
}else if ($data) {
	echo'<br>';
	echo '<h4>'.get_string('clreport','local_assignmentcourses').'</h4>';
	echo '<hr>';
	$table = new html_table();
	$table->id =  'example';
	$table->head = (array) get_strings(array('uname','fullname', 'email','csname','cdate','edate','grade','status'), 'local_assignmentcourses');
	foreach($data->course as $courseid){
			//check condition if by default submit form below action will take place 
		if(($data->coursecat) && ($courseid == 'select-all')){
			$assignmentcourses =  $DB->get_records('course',array('category'=>$data->coursecat));
			if($assignmentcourses){
				foreach ($assignmentcourses as $key => $assignmentcourse) {
						//take all enrolled users in courses 
					$coursecontext = context_course::instance($assignmentcourse->id);
					$teammems = '';
					if(!is_siteadmin()){
						$enrolledusers = get_enrolled_users($coursecontext);
						$teammems = teamuser($USER->username);
					//print_object($teammems);
						if(is_array($teammems)){
							foreach ($teammems as $keym => $valuem) {
								$teammemers[] = $keym;
							}
						}
					//enrolledusers
						if(is_array($enrolledusers)){
							foreach ($enrolledusers as $key => $enrolleduser) {
								$allenrol[] = $key;
							}
						}
						//array intersect for subordinates users 
						$users = array_intersect($teammemers, $allenrol);
						if($users){
							foreach ($users as $key => $user) {
								$enrolledusers1[] = $DB->get_record('user',array('id'=>$user,'auth'=>'manual'));
							}
						}
					}
					else{
						$enrolledusers1 = get_enrolled_users($coursecontext);
						print_object($enrolledusers1);
					}
				//display process take place here 
					if($enrolledusers1){
						foreach ($enrolledusers1 as $key => $enrolleduser) {
							$dateformat = '%d-%b-%Y';
							$enroldate = userdate($enrolleduser->timecreated,$dateformat);

							$creport = user_progress_course_assigned_report($assignmentcourse->id,$enrolleduser->id);
							$table->data[] = array(
								$enrolleduser->username,
								html_writer::link(
									new moodle_url(
										$CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_user_report.php?id='.$enrolleduser->id,array()
										),$enrolleduser->firstname.' '.$enrolleduser->lastname
									),
								//$enrolleduser->firstname.'-'.$enrolleduser->lastname,
								$enrolleduser->email,
								html_writer::link(
									new moodle_url(
										$CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_course_activity.php?id='.$assignmentcourse->id.'&userid='.$enrolleduser->id,array()
										),$assignmentcourse->fullname
									),
								$creport['completiondate'],
								$enroldate,
								$creport['grade'],
								$creport['status']					 			
								);
						}

					}
				}
			}
		}
		if(!empty($data->coursecat) && (!empty($courseid)) ){
			//print_object($courseid);
			$assignmentcourses =  $DB->get_records('course',array('category'=>$data->coursecat,'id'=>$courseid));
			if($assignmentcourses){
				foreach ($assignmentcourses as $assignmentcourse) {
				//take all enrolled users in courses 
					$coursecontext = context_course::instance($assignmentcourse->id);		
				//print_object($enrolledusers);
					$teammems = '';
					if(!is_siteadmin()){
						$enrolledusers = get_enrolled_users($coursecontext);
						$teammems = teamuser($USER->username);
					//print_object($teammems);
						if(is_array($teammems)){
							foreach ($teammems as $keym => $valuem) {
								$teammemers[] = $keym;
							}
						}
					//enrolledusers
						if(is_array($enrolledusers)){
							foreach ($enrolledusers as $key => $enrolleduser) {
								$allenrol[] = $key;
							}
						}
						$users = array_intersect($teammemers, $allenrol);
						if($users){
							foreach ($users as $key => $user) {
								$enrolledusers1[] = $DB->get_record('user',array('id'=>$user,'auth'=>'manual'));
							}
						}
					}
					else{
						$enrolledusers1 = get_enrolled_users($coursecontext);
					}
				//display process take place here 
					if($enrolledusers1){
						foreach ($enrolledusers1 as $key => $enrolleduser) {
							$dateformat = '%d-%b-%Y';
							$enroldate = userdate($enrolleduser->timecreated,$dateformat);

							$creport = user_progress_course_assigned_report($assignmentcourse->id,$enrolleduser->id);
							$table->data[] = array(
								$enrolleduser->username,
								html_writer::link(
									new moodle_url(
										$CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_user_report.php?id='.$enrolleduser->id,array()
										),$enrolleduser->firstname.' '.$enrolleduser->lastname
									),
								//$enrolleduser->firstname.'-'.$enrolleduser->lastname,
								$enrolleduser->email,
								html_writer::link(
									new moodle_url(
										$CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_course_activity.php?id='.$assignmentcourse->id.'&userid='.$enrolleduser->id,array()
										),$assignmentcourse->fullname
									),
								$creport['completiondate'],
								$enroldate,
								$creport['grade'],
								$creport['status']					 			
								);
						}

					}

				}
			}
		}
	}
	echo html_writer::table($table);
}
echo $OUTPUT->footer();
