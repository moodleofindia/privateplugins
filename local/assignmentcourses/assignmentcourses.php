<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/assignmentcourse_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/enrollib.php');
require_login(0,false);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/assignmentcourses/assignmentcourses.php');
$title = get_string('pluginname', 'local_assignmentcourses');
$id = $USER->id;
$PAGE->set_title($title);
$PAGE->set_heading($title);
//$PAGE->navbar->add($title);
include_once('jslink.php');
require_once($CFG->dirroot.'/local/assignmentcourses/csslinks.php');
echo $OUTPUT->header();
$temp = false;
$sql = "select * from {user}";
$result = $DB->get_records_sql($sql);
$mform = new local_assignmentcourses_form();
$data = $mform->get_data();
$flag = '';
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/assignmentcourses/assignmentcourses.php', array()));
} else if ($data) {
	if(!empty($data->users)){
		foreach($data->users as $user) {
			$answer = explode(',',$user);
			$exists = $DB->get_record('local_assignmentcourses',array('userid'=>$answer[0],'courseid'=>$data->course));
			//take course instance for manual enrolment method 
			$instances = $DB->get_record('enrol', array('enrol'=>'manual', 'courseid'=>$data->course, 'status'=>ENROL_INSTANCE_ENABLED)); 
			if(!$exists){
				$insert = new stdClass();
				$insert->courseid =$data->course;
				$insert->userid =$answer[0];
				$insert->assign_date = time();
				$insert->end_date = $data->end_date;
				$insert->assignby = $USER->username;
				$assignedcourse = $DB->insert_record('local_assignmentcourses',$insert);
				if($assignedcourse){
					$flag = 1;
				} 
				//enrolment process here 
				$enrol = enrol_get_plugin('manual');
				$enrol1 = $enrol->enrol_user($instances, $answer[0], $roleid = 5, $timestart=time(), $data->end_date);
				//notification 
				$submitter = $DB->get_record('user',array('username'=>$USER->username));
				$userrc = $DB->get_record('user',array('id'=>$answer[0]));
				if($enrol1){
					if($submitter){
						if($userrc){
							$send = allocation_send_alert($userrc,$submitter,$data->course);
							if($send){
								$flag = 1;
							}
						}
					}
					
					
				}
			}else{
				$flag = 0;
			}
		}  
		if($flag ==1){
			echo html_writer::div(
				get_string('insert_map1','local_assignmentcourses'),'alert alert-success'
				);
		} 
		if($flag == 0){
			echo html_writer::div(
				get_string('record','local_assignmentcourses'),'alert alert-danger'
				);
		}
	}
}

//assignment course display note here
echo'<br>';
echo '<h4>'.get_string('listassignment','local_assignmentcourses').'</h4>';
echo '<hr>';
$table = new html_table();
$table->id =  'example';
if(is_siteadmin()){
	$assigncourses = $DB->get_records('local_assignmentcourses');
}else{
	$assigncourses = $DB->get_records('local_assignmentcourses',array('assignby'=>$USER->username));
}
$mform->display();
if($assigncourses){
	
	$table->head = (array) get_strings(array('coursename1','username1','assigby','enddate','ccomplete'),'local_assignmentcourses');
	foreach ($assigncourses as $assigncourse) {
		$dateformat = '%d-%b-%Y';
		$coursename = coursename($assigncourse->courseid);
		$username =username($assigncourse->userid);
		$enddate = userdate($assigncourse->end_date);
		$status = user_status_course_report($assigncourse->courseid,$assigncourse->userid);
		$table->data[] =array(
			$coursename->fullname,
			$username->username,
			$assigncourse->assignby,
			$enddate,
			$status
			);
	}
	echo html_writer::table($table);
}else{
	echo html_writer::div(
		get_string('norecord','local_assignmentcourses'),'alert alert-danger'
		);
}
echo $OUTPUT->footer();
?>
