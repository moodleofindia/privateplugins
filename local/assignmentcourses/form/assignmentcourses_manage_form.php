<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
class local_assignmentcourses_manage_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
    
        $crsid = optional_param('crsid', 0 ,PARAM_INT);
        $edit = optional_param('edit',null,PARAM_RAW);
        $mform =& $this->_form;
        $mform->addElement('header','accessinfohdr',get_string('accessinfohdr','local_assignmentcourses'));
        $mform->setExpanded('accessinfohdr');
        $mform->addElement('text', 'fieldname', get_string('fieldname','local_assignmentcourses')); // Add elements to your form
        $mform->setType('fieldname', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('fieldname',get_string('pls','local_assignmentcourses'));  
        $choices = array(
            'Select',
            'user' => get_string('userf','local_assignmentcourses'),
            'custom' => get_string('custf','local_assignmentcourses')
            );
        $select = $mform->addElement('select', 'selectfield',get_string('sname','local_assignmentcourses'), $choices);
        $select->setMultiple(false);

        $customprofile = $DB->get_records('user_info_field',array(),'id,shortname');
        foreach($customprofile as $c){
            $s[$c->shortname]= $c->shortname;
        }
     
        $mform->addElement('select', 'mapfield',
             get_string('usercustomprofile','local_assignmentcourses'), $s, array('single','class'=>'customtextbox'));
        //$mform->setType('courses',PARAM_INT);
        // $userprofile = array(
        //     'icq'=>'ICQ',
        //     'skype'=>'Skype',
        //     'yahoo'=>'Yahoo',
        //     'aim'=>'AIM',
        //     'msn'=>'MNS',
        //     'phone1'=>'Phone1',
        //     'phone2'=>'Phone2',
        //     'department'=>'Department', 
        //     'address'=>'Address',
        //     'city'=>'City',
        //     'country'=>'Country'
        //     );
        // $mform->addElement('select', 'mapfield',
        //      get_string('userprofile','local_assignmentcourses'), $userprofile, array('single','class'=>'profiletextbox'));
            //$mform->setType('courses',PARAM_INT); 
        $this->add_action_buttons();
    }
}
?>
