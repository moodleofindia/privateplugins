`<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
class local_assignmentcourses_report_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $mform =& $this->_form;
        $customdata = $this->_customdata['csdata'];
        require_once($CFG->libdir . '/coursecatlib.php');
        $categories = coursecat::make_categories_list();
        $mform->addElement('header','report',get_string('report','local_assignmentcourses'));
        $mform->setExpanded('report');
        $mform->addElement('static','desc',get_string('note','local_assignmentcourses'), get_string('desc1', 'local_assignmentcourses'));
        $courses1 = $DB->get_records_menu('course',array(),'','id,fullname');
        unset($courses1[1]);
        //course category
        $sall = array('select-all'=>'Select All');
        $select = $mform->addElement('select', 'coursecat',get_string('coursecat','local_assignmentcourses'), $categories);
        $select->setMultiple(false);
        //coursename
        $courses = [];
        if(isset($customdata['coursecat'])){
            if(!empty($customdata['coursecat'])){
            $courses11 = $DB->get_records('course',array('category'=>$customdata['coursecat']),'id,fullname');
            $courses['select-all'] = 'Select-all';
            if($courses11){
                foreach ($courses11 as $key => $course11) {
                        $courses[$course11->id] = $course11->fullname;
                    }
                }

            }
        }
            if($courses){
                $select = $mform->addElement('select', 'course',
                    get_string('coursename','local_assignmentcourses'),
                    $courses);
            } else{
                $select = $mform->addElement('select', 'course',
                    get_string('coursename','local_assignmentcourses'),
                    array());
            }
        $select->setSelected('select-all');
        $select->setMultiple(true);

                
                $this->add_action_buttons();
            }
        }
