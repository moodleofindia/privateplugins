`<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
class local_assignmentcourses_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $mform =& $this->_form;
        $mform->addElement('header','accessinfohdr1',get_string('pluginname','local_assignmentcourses'));
        $mform->setExpanded('accessinfohdr1');
        $mform->addElement('static','desc',get_string('note','local_assignmentcourses'), get_string('desc', 'local_assignmentcourses'));
        $users = array();
        $courses = $DB->get_records('course');
        unset($courses[1]);
        $course = [];
        foreach($courses as $key=>$course1){
            $category = $DB->get_record('course_categories',array('id'=>$course1->category));
            $course[$course1->id] = $category->name.'  / '.$course1->fullname;
            unset($course[1]);
        }
        $select = $mform->addElement('searchableselector', 'course',get_string('coursename','local_assignmentcourses'), $course);
        $select->setMultiple(false);
        $mform->addRule('course', null, 'required', null, 'client');
        if(is_siteadmin()) {
            $users = $DB->get_records('user');
        } else {
            $sql = "SELECT u.id,u.firstname,u.lastname,u.username,u.email
            from {user} u 
            join {user_info_data} uid
            on u.id = uid.userid 
            join {user_info_field} uif
            on uif.id = uid.fieldid
            join {local_managemapping} lm 
            on lm.mapfield = uif.shortname 
            where uid.data = '$USER->username'";
            $users = $DB->get_records_sql($sql);
        }
        $usersnotassigned = array();
        foreach ($users as $user) {
            $usersnotassigned[$user->id.','.$user->username] = $user->username.'  -  '.$user->firstname.' '.$user->lastname.' - '.$user->email;
            unset( $usersnotassigned['1,'.$user->username]);
            unset( $usersnotassigned['2,'.$user->username]); 
        }
        $select = $mform->addElement('searchableselector', 'users',get_string('uname','local_assignmentcourses'), $usersnotassigned);
        $select->setMultiple(true);
        $mform->addRule('users', null, 'required', null, 'client');
        $mform->addElement('date_selector', 'end_date', get_string('edate1','local_assignmentcourses'));
        $this->add_action_buttons();
    }
}
