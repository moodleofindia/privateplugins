<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_assignmentcourses
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/assignmentcourses_manage_form.php');
require_once($CFG->libdir .'/formslib.php');
require_login(0,false);

$id = required_param('id',PARAM_INT);
$edit = optional_param('edit',null,PARAM_RAW);
$general = $DB->get_record('local_managemapping', array('id' => $id)) ;
$context = context_system::instance();
$PAGE->set_context(context_system::instance());
$title = get_string('edit_assignment_manage', 'local_assignmentcourses');
$PAGE->set_title($title);
$PAGE->set_heading($title);
	$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/assignmentcourses/datatable/javascript/jquery-1.9.1.js'),true);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/assignmentcourses/edit_assignmentcourses_manage.php');
require_login();
//breadcum code 
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_assignmentcourses'), new moodle_url($CFG->wwwroot.'/local/assignmentcourses/assignmentcourses.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title);
$thingnode->make_active();
echo $OUTPUT->header();
$mform = new local_assignmentcourses_manage_form(new moodle_url($CFG->wwwroot .'/local/assignmentcourses/edit_assignmentcourses_manage.php',array('id'=>$id)));
$mform->set_data($general);
if ($mform->is_cancelled()) {
	redirect(new moodle_url('/local/assignmentcourses/assignmentcourses_manage.php', array()));
}else if($data = $mform->get_data()) {
		print_object($data);
		
		$editcontent = new stdClass();
		$editcontent->id = $general->id;
		$editcontent->fieldname = $data->fieldname;
		$editcontent->selectfield = $data->selectfield;
		$editcontent->mapfield= $data->mapfield;
		$generalupdate = $DB->update_record('local_managemapping', $editcontent);
		if($generalupdate){
			redirect(new moodle_url($CFG->wwwroot.'/local/assignmentcourses/assignmentcourses_manage.php'),get_string('upmanage','local_assignmentcourses'));
		}
}
//disply messages here 
echo '<b>Note : </b><p>1 : In this screen you will able to supervisor and maager with respecive profile field.<br>2 : Selected field type is <b>' .$general->mapfield. '</b> with supervisor and manager, if you want to change selected field type then below choose your selected field type.</p>';
$mform->display();

echo $OUTPUT->footer();
?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".customtextbox").hide();
			$(".profiletextbox").hide();
			$("#id_selectfield").change(function(){
		//alert(this.value);
		if(this.value =='user'){
			$(".profiletextbox").show();
			$(".customtextbox").hide();
		}else if(this.value =='custom'){
			$(".customtextbox").show();
			$(".profiletextbox").hide();
		}else{
			$(".customtextbox").hide();
			$(".profiletextbox").hide();
		}

	});
		});
	</script>

