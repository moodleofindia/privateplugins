<?php

define('AJAX_SCRIPT', true);
require_once('../../config.php');

$PAGE->set_context(context_system::instance());
$id = required_param('id', PARAM_INT);
$coursename = $DB->get_records('course',array('category'=>$id),'id,fullname');
echo $OUTPUT->header(); 

echo json_encode($coursename);
