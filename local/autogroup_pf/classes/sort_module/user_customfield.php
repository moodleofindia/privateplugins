<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * autogroup_pf local plugin
 *
 * A course object relates to a Moodle course and acts as a container
 * for multiple groups. Initialising a course object will automatically
 * load each autogroup_pf group for that course into memory.
 *
 * @package    local
 * @subpackage autogroup_pf
 * @author     Mark Ward (me@moodlemark.com)
 * @date       December 2014
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_autogroup_pf\sort_module;

use local_autogroup_pf\sort_module;
use local_autogroup_pf\exception;
use \stdClass;

/**
 * Class course
 * @package local_autogroup_pf\domain
 */
class user_customfield extends sort_module
{
    /**
     * @param stdClass $config
     * @param int $courseid
     */
    public function __construct($config, $courseid)

    {

        if($this->config_is_valid($config)){
            $this->field = $config->field;
        }
        $this->courseid = (int) $courseid;
   
    }

    /**
     * @param stdClass $config
     * @return bool
     */
    public function config_is_valid(stdClass $config)
    {
        if(!isset($config->field)){
            return false;
        }

        //ensure that the stored option is valid
        if(array_key_exists($config->field, $this->get_config_options())){
            return true;
        }

        return false;
    }

    /**
     * @param stdClass $user
     * @return array $result
     */
    public function eligible_groups_for_user(stdClass $user)
    {
        global $DB;
        $field = $this->field;
        $usfield = [];
        if (isset($user->$field) && !empty($user->$field)){
            
            if($DB->record_exits('user_info_field',array('shortname'=>$user->$field))){
                $ufs = $DB->get_record('user_info_field',array('shortname'=>$user->$field));
                $ufsdt = $DB->get_record('user_info_data',array('fieldid'=>$ufs->id));
                if($ufsdt){
                    $usfield[] = $ufsdt->$data; 
                }
            }else{
               $usfield[] = $user->$field; 
            }
        }
        return $usfield;
        // else {
        //     return array();
        // }
    }

    /**
     * Returns the options to be displayed on the autgroup_set
     * editing form. These are defined per-module.
     *
     * @return array
     */
    public function get_config_options(){
         global $DB;
        $optionss = array(
            'auth' => get_string('auth', 'local_autogroup_pf'),
            'department' => get_string('department', 'local_autogroup_pf'),
            'institution' => get_string('institution', 'local_autogroup_pf'),
            'lang' => get_string('lang', 'local_autogroup_pf'),
        );
        $ufname = [];
        $ufs = $DB->get_records('user_info_field');
        if (!empty($ufs)) {
            foreach ($ufs as $uf) {
                $ufsrtname = $uf->shortname;
                $ufname[$uf->shortname] = $uf->name;
            }
        }
        $options = $optionss +$ufname;
        return $options;
    }

    /**
     * @return bool|string
     */
    public function grouping_by(){
        if(empty ($this->field)){
            return false;
        }
        return (string) $this->field;
    }

    /**
     * @var string
     */
    private $field = '';

}