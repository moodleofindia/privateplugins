<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * autogroup_pf local plugin
 *
 * A course object relates to a Moodle course and acts as a container
 * for multiple groups. Initialising a course object will automatically
 * load each autogroup_pf group for that course into memory.
 *
 * @package    local
 * @subpackage autogroup_pf
 * @author     Mark Ward (me@moodlemark.com)
 * @date       December 2014
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_autogroup_pf\domain;

use local_autogroup_pf\domain;
use local_autogroup_pf\exception;

/**
 * Class course
 *
 * A course object relates to a Moodle course and acts as a container
 * for multiple groups. Initialising a course object will automatically
 * load each autogroup_pf group for that course into memory.
 *
 * Courses currently link to a single autogroup_pf_set, however in the
 * future this could be extended to support multiple sets.
 *
 * @package local_autogroup_pf\domain
 */
class course extends domain
{
    /**
     * @param $course
     * @param \moodle_database $db
     * @throws exception\invalid_course_argument
     */
    public function __construct ($course, \moodle_database $db)
    {
        //get the id for this course
        $this->parse_course_id($course);

        $this->context = \context_course::instance($this->id);

        //load autogroup_pf groups for this course
        $this->get_autogroup_pfs($db);

        $this->enrolledusers = \get_enrolled_users($this->context);

    }

    /**
     * @return array
     */
    public function get_membership_counts(){
        $result = array();
        foreach($this->autogroup_pfs as $autogroup_pf){
            $result = $result + $autogroup_pf->membership_count();
        }
        return $result;
    }

    /**
     * @param \moodle_database $db
     * @return bool
     */
    public function verify_all_group_membership(\moodle_database $db)
    {
        $result = true;
        foreach ($this->enrolledusers as $user){
            $result &= $this->verify_user_group_membership($user, $db);
        }
        return $result;
    }

    /**
     * @param \stdclass $user
     * @param \moodle_database $db
     * @return bool
     */
    public function verify_user_group_membership(\stdclass $user, \moodle_database $db)
    {
        $result = true;
        foreach ($this->autogroup_pfs as $autogroup_pf){
            $result &= $autogroup_pf->verify_user_group_membership($user, $db, $this->context);
        }
        return $result;
    }

    /**
     * @param object|int $course
     * @return bool
     * @throws exception\invalid_course_argument
     */
    private function parse_course_id ($course)
    {
        if(is_int($course) && $course > 0){
            $this->id = $course;
            return true;
        }

        if(is_object($course) && isset($course->id) && $course->id > 0){
            $this->id = $course->id;
            return true;
        }

        throw new exception\invalid_course_argument($course);
    }

    /**
     * @param \moodle_database $db
     */
    private function get_autogroup_pfs(\moodle_database $db){

        $this->autogroup_pfs = $db->get_records('local_autogroup_pf_set', array('courseid' => $this->id));

        foreach($this->autogroup_pfs as $id => $settings){
            try {
                $this->autogroup_pfs[$id] = new domain\autogroup_pf_set($db, $settings);
            } catch (exception\invalid_autogroup_pf_set_argument $e){
                unset($this->autogroup_pfs[$id]);
            }
        }
    }

    /**
     * @var array
     */
    private $autogroup_pfs = array();

    /**
     * @var \context_course
     */
    private $context;

    /**
     * @var array
     */
    private $enrolledusers = array();

}