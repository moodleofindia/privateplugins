<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * autogroup_pf local plugin
 *
 * A course object relates to a Moodle course and acts as a container
 * for multiple groups. Initialising a course object will automatically
 * load each autogroup_pf group for that course into memory.
 *
 * @package    local
 * @subpackage autogroup_pf
 * @author     Mark Ward (me@moodlemark.com)
 * @date       April 2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * This file allows users with the correct capability to manage
 * grouping logic for autogroup_pfs within a course.
 */

namespace local_autogroup_pf;

require_once(dirname(__FILE__) . '/pageinit.php');

use \local_autogroup_pf\domain;
use \local_autogroup_pf\form;
use \local_autogroup_pf\usecase;
use \local_autogroup_pf_renderer;
use \moodle_url;
use \context_course;
use \stdClass;

$courseid = required_param('courseid', PARAM_INT);
$context = context_course::instance($courseid);

require_capability('local/autogroup_pf:managecourse', $context);

global $PAGE, $DB, $SITE;

if($courseid == $SITE->id || !plugin_is_enabled()){
    //do not allow editing for front page.
    die();
}

$course = $DB->get_record('course', array('id' => $courseid));
$groupsets = $DB->get_records('local_autogroup_pf_set', array('courseid'=>$courseid));

foreach($groupsets as $k => $groupset) {
    $groupsets[$k] = new domain\autogroup_pf_set($DB, $groupset);
}

$heading = \get_string('coursesettingstitle', 'local_autogroup_pf', $course->shortname);

global $PAGE;

$PAGE->set_context($context);
$PAGE->set_url(local_autogroup_pf_renderer::URL_COURSE_MANAGE, array('courseid'=>$courseid));
$PAGE->set_title($heading);
$PAGE->set_heading($heading);
$PAGE->set_pagelayout('incourse');
$PAGE->set_course($course);

$output = $PAGE->get_renderer('local_autogroup_pf');


echo $output->header();

echo $output->intro_text(count($groupsets));

echo $output->groupsets_table($groupsets);

echo $output->add_new_groupset($courseid);



// require_once(__DIR__ . "/../../group/lib.php");
//   $userid = 6;
//   $courseids =3;
        // if($DB->record_exists('user_info_data',array('userid'=>$userid))){
        //     $ufs = $DB->get_record('user_info_data',array('userid'=> $userid));
        //     $gpname = $ufs->data;
        //     $groupname = ucfirst((string) $gpname);
        //     if($DB->record_exists('groups',array('name'=>$groupname))){
        //         $gpdata = $DB->get_record('groups',array('name'=> $groupname));
        //         groups_add_member($gpdata->id,$userid);
        //     }else{
        //         $sql = "select uif.shortname from {user_info_field} uif join {user_info_data} uid on uid.fieldid = uif.id where uid.data='$gpname'";
        //         $gds = $DB->get_record_sql($sql);
        //         if(!empty($gds->shortname)){
        //             $fldc='{"field":"'.$gds->shortname.'"}';
        //             $sql1 = "select id from {local_autogroup_pf_set}  where sortconfig='$fldc'";
        //             $gds1 =$DB->get_record_sql($sql1);
        //             $idnumber = implode('|',array('autogroup_pf',$gds1->id,$gpname));
        //             $data = new stdclass();
        //             $data->id = 0;
        //             $data->name = $groupname;
        //             $data->idnumber = $idnumber;
        //             $data->courseid = $courseid;
        //             $data->description = '';
        //             $data->descriptionformat = 0;
        //             $data->enrolmentkey = null;
        //             $data->picture = 0;
        //             $data->hidepicture = 0;
        //             $aa = groups_create_group($data);
        //             if($aa){
        //                 groups_add_member($aa,$userid);
        //             }
                      
        //         }
        //     }
        // }

  	// $ufs = $DB->get_records('local_autogroup_pf_set',array('courseid'=> $courseids,'sortmodule'=>'user_customfield'));
  	// if(!empty($ufs)){
  	// 	foreach ($ufs as  $value) {
  	// 		$shortname = json_decode($value->sortconfig)->field ;
			// $sql = "select uid.data  from {user_info_field} uif join {user_info_data} uid on uid.fieldid = uif.id where uif.shortname='$shortname' and uid.userid = $userid";
   //          $gds = $DB->get_record_sql($sql);
   //          $gpname = $gds->data;
   //          if(!empty($gpname)){
	  //           $groupname = ucfirst((string) $gpname);
		 //        if($DB->record_exists('groups',array('courseid'=>$courseids,'name'=>$groupname))){
			// 	    $gpdata = $DB->get_record('groups',array('courseid'=>$courseids,'name'=>$groupname));
			// 	    groups_add_member($gpdata->id,$userid);
			// 	}else{
			// 	    $idnumber = implode('|',array('autogroup_pf',$value->id,$gpname));
	  //               $data = new stdclass();
	  //               $data->id = 0;
	  //               $data->name = $groupname;
	  //               $data->idnumber = $idnumber;
	  //               $data->courseid = $courseid;
	  //               $data->description = '';
	  //               $data->descriptionformat = 0;
	  //               $data->enrolmentkey = null;
	  //               $data->picture = 0;
	  //               $data->hidepicture = 0;
	  //               $lastinsertid = groups_create_group($data);
	  //               if($lastinsertid){
	  //                   groups_add_member($lastinsertid,$userid);
	  //               }
	  //           }
	  //       }

  	// 	}

  	// }
        

   
echo $output->footer();