<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * autogroup_pf local plugin
 *
 * A course object relates to a Moodle course and acts as a container
 * for multiple groups. Initialising a course object will automatically
 * load each autogroup_pf group for that course into memory.
 *
 * @package    local
 * @subpackage autogroup_pf
 * @author     Mark Ward (me@moodlemark.com)
 * @date       January 2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/*
 * This file generates the site admin settings page using Moodles
 * standard admin_settingpage class.
 */

defined('MOODLE_INTERNAL') || die;

require_once(dirname(__FILE__) . '/lib.php');

if ($hassiteconfig) {
    $settings = new admin_settingpage(
        'local_autogroup_pf',
        get_string('pluginname', 'local_autogroup_pf')
    );

    // general settings
    $settings->add(
        new admin_setting_heading(
            'local_autogroup_pf/general',
            get_string('general', 'local_autogroup_pf'),
            ''
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/enabled',
            get_string('enabled', 'local_autogroup_pf'),
            '',
            true
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/addtonewcourses',
            get_string('addtonewcourses', 'local_autogroup_pf'),
            '',
            false
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/addtorestoredcourses',
            get_string('addtorestoredcourses', 'local_autogroup_pf'),
            '',
            false
        )
    );

    // default settings
    $settings->add(
        new admin_setting_heading(
            'local_autogroup_pf/defaults',
            get_string('defaults', 'local_autogroup_pf'),
            ''
        )
    );
    //TODO: This will eventually need reworking to allow for properly dynamic sort modules
    $ufs = $DB->get_records('user_info_field');
    foreach ($ufs as $uf) {
        $ufname[$uf->shortname] = $uf->name;
    }
    $choicess = array(
        'auth' => get_string('auth', 'local_autogroup_pf'),
        'department' => get_string('department', 'local_autogroup_pf'),
        'institution' => get_string('institution', 'local_autogroup_pf'),
        'lang' => get_string('lang', 'local_autogroup_pf')
    );
    $choices = $choicess + $ufname;
    $settings->add(
        new admin_setting_configselect(
            'local_autogroup_pf/filter',
            get_string('set_groupby', 'local_autogroup_pf'),
            '',
            'department',
            $choices
        )
    );

    // default roles
    $settings->add(
        new admin_setting_heading(
            'local_autogroup_pf/roleconfig',
            get_string('defaultroles', 'local_autogroup_pf'),
            ''
        )
    );

    if ($roles = \get_all_roles()) {
        $roles = \role_fix_names($roles, null, ROLENAME_ORIGINAL);
        $assignableroles = \get_roles_for_contextlevels(CONTEXT_COURSE);
        foreach ($roles as $role) {
            //default should be true for students
            $default = ($role->id == 5);

            $settings->add(
                new admin_setting_configcheckbox(
                    'local_autogroup_pf/eligiblerole_'.$role->id,
                    $role->localname,
                    '',
                    $default
                )
            );
        }
    }

    // Event listeners
    $settings->add(
        new admin_setting_heading(
            'local_autogroup_pf/events',
            get_string('events', 'local_autogroup_pf'),
            get_string('events_help', 'local_autogroup_pf')
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/listenforrolechanges',
            get_string('listenforrolechanges', 'local_autogroup_pf'),
            get_string('listenforrolechanges_help', 'local_autogroup_pf'),
            true
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/listenforuserprofilechanges',
            get_string('listenforuserprofilechanges', 'local_autogroup_pf'),
            get_string('listenforuserprofilechanges_help', 'local_autogroup_pf'),
            true
        )
    );
    if( isset($CFG->totara_build) ) // Only for Totara
    {
        $settings->add(
            new admin_setting_configcheckbox(
                'local_autogroup_pf/listenforuserpositionchanges',
                get_string('listenforuserpositionchanges', 'local_autogroup_pf'),
                get_string('listenforuserpositionchanges_help', 'local_autogroup_pf'),
                true
            )
        );
    }
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/listenforgroupchanges',
            get_string('listenforgroupchanges', 'local_autogroup_pf'),
            get_string('listenforgroupchanges_help', 'local_autogroup_pf'),
            false
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'local_autogroup_pf/listenforgroupmembership',
            get_string('listenforgroupmembership', 'local_autogroup_pf'),
            get_string('listenforgroupmembership_help', 'local_autogroup_pf'),
            false
        )
    );

    $ADMIN->add('localplugins', $settings);
}