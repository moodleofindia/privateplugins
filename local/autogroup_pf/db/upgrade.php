<?php

defined('MOODLE_INTERNAL') || die();

function xmldb_local_autogroup_pf_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2016062201) {

        // Convert "Strict enforcement" settings to new toggles
        $pluginconfig = get_config('local_autogroup_pf');
        if($pluginconfig->strict){
            set_config('listenforgroupchanges', true, 'local_autogroup_pf');
            set_config('listenforgroupmembership', true, 'local_autogroup_pf');
        }

        // savepoint reached.
        upgrade_plugin_savepoint(true, 2016062201, 'local', 'autogroup_pf');
    }
    return true;
}