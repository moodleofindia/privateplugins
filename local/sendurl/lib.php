<?php
require_once($CFG->libdir . '/completionlib.php');

function sendurl_cron() {
	global $CFG, $PAGE, $DB;
	$syscontext = context_system::instance();
	$PAGE->set_context($syscontext);
	$courses = null;
	$modinfo = null;
	$users = $DB->get_records('user');
	foreach ($users as $user) {
		$courses = enrol_get_users_courses($user->id, true);
		if (!empty($courses)) {
			foreach ($courses as $course) {
				$context = context_course::instance($course->id);
				$student = user_has_role_assignment($user->id, 5, $context->id);

				// get all mods.
				$modinfo = get_fast_modinfo($course, $user->id);
				$mods = $modinfo->get_cms();
				if (!empty($mods)) {
					foreach ($mods as $mod) {
						if ($mod->modname == 'url') {
							if (!empty($mod->availability)) {
								if (($mod->uservisible == 1) && empty($mod->availableinfo)) {
									if ($student) {
										// send mail.
										$body = html_to_text('<html><body>Now you are eligible to view the activity <a href="'.$CFG->wwwroot.'/mod/url/view.php?id='.$mod->id.'">' . $mod->name . '</a></body></html>');
										if (email_to_user($user, \core_user::get_support_user(), 'Mod Availability', $body)) {
											if (!$DB->record_exists('sendurl_mails', ['cmid' => $mod->id, 'userid' => $user->id])) {
												// Insert a new record into sendurl_mails table.
												$recinsert = new \stdClass();
												$recinsert->courseid = $course->id;
												$recinsert->cmid = $mod->id;
												$recinsert->userid = $user->id;
												$recinsert->status = 1;
												$recinsert->timecreated = time();
												$DB->insert_record('sendurl_mails', $recinsert);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}