<?php

/**
 * cron
 * @return boolean
 */

namespace local_sendurl\task;

defined('MOODLE_INTERNAL') || die();

class task_send_url extends \core\task\scheduled_task {

    public function get_name() {
        // Shown in admin screens
        return get_string('pluginname', 'local_sendurl');
    }

    public function execute() {
        global $CFG;
        require_once($CFG->dirroot . '/local/sendurl/lib.php');
        mtrace('sendurl mails to users');
        sendurl_cron();
    }
}
