<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Enrolment History
 * @author     Nihar Das <nihar@elearn10.com>
 * @package    local_enrolment_history
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
$enrolid = optional_param('e', null, PARAM_INT);
$date = optional_param('dt', null, PARAM_INT);
$courseid = optional_param('cid', null, PARAM_INT);
//var_dump($date);
$type = required_param('type', PARAM_INT);
require_login(0,false);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle', 'local_enrolment_history'));
$PAGE->set_heading('Enrolment History');
$PAGE->set_url($CFG->wwwroot . '/local/enrolment_history/index.php?type=0');
$PAGE->requires->css('/styles.css');
$PAGE->requires->jquery();

// For datatable.
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/eduopen/datatable/js/cdn/jquery.dataTables.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/eduopen/datatable/js/cdn/dataTables.bootstrap.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/eduopen/datatable/js/cdn/dataTables.fixedColumns.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/eduopen/datatable/tabletool/js/dataTables.tableTools.js'), true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/eduopen/datatable/css/cdn/dataTables.bootstrap.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/eduopen/datatable/css/cdn/fixedColumns.bootstrap.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/eduopen/datatable/tabletool/css/dataTables.tableTools.css'));

echo $OUTPUT->header();
$table = new html_table();
$i = 1;
if(!isset($enrolid)){
    if($type == 0){
        $T1 = 967022950; //a very old date
        $T2 = time();
        $query = ' AND to_timestamp(ue.timecreated) BETWEEN date(to_timestamp('.$T1.')) AND date(to_timestamp('.$T2.'))';
    } else if($type == 1){
        $T1 = time() - (20*24*60*60);
        $T2 = time();
        $query = ' AND to_timestamp(ue.timecreated) BETWEEN date(to_timestamp('.$T1.')) AND date(to_timestamp('.$T2.'))';
    } else if($type == 2){
        $T1 = time() - (21*24*60*60);
        $T2 = time() - (40*24*60*60);
        $query = ' AND to_timestamp(ue.timecreated) BETWEEN date(to_timestamp('.$T2.')) AND date(to_timestamp('.$T1.'))';
    }else if($type == 3){
        $T1 = time() - (41*24*60*60);
        $T2 = time() - (60*24*60*60);
        $query = ' AND to_timestamp(ue.timecreated) BETWEEN date(to_timestamp('.$T2.')) AND date(to_timestamp('.$T1.'))';
        
    }else if($type == 4){
        $T1 = time() - (61*24*60*60);
        $T2 = time() - (80*24*60*60);
        $query = ' AND to_timestamp(ue.timecreated) BETWEEN date(to_timestamp('.$T2.')) AND date(to_timestamp('.$T1.'))';
    }
    $url = new moodle_url($CFG->wwwroot.'/local/enrolment_history/index.php');
    $options = array(
        1 => 'last 20 days',
        2 => '21-40 days',
        3 => '41-60 days',
        4 => '61-80 days'
    );
    
    echo html_writer::start_div('list-group');
    echo html_writer::start_div('list-group-item active');
    echo html_writer::tag('span', 'Select the time period ', array('style' => 'font-weight:600'));
    echo $OUTPUT->single_select($url, get_string('type', 'local_enrolment_history'), $options, $type, array('0' => 'show all'), null);
    echo html_writer::end_div();
    echo html_writer::end_div();
    $table->head = (array) get_strings(array('slno','categoryname','coursename', 'date', 'noofstudents'),'local_enrolment_history');
    $enrolquery = $DB->get_recordset_sql("SELECT date(to_timestamp(ue.timecreated)) AS searcheddate,
                                             c.id AS courseid,e.id as userenrolid, c.fullname AS coursefullname,
                                             c.category,cc.name AS categoryname, COUNT(ue.userid) AS totalcount 
                                                FROM mdl_enrol e
                                                JOIN mdl_course c ON c.id = e.courseid
                                                JOIN mdl_course_categories cc ON c.category = cc.id 
                                                JOIN mdl_user_enrolments ue ON ue.enrolid = e.id 
                                                WHERE e.enrol= 'self' 
                                                AND e.status= 0". $query .
                                             "GROUP BY date(to_timestamp(ue.timecreated)), c.id, e.id, c.category,c.fullname,cc.name  
                                              ORDER BY date(to_timestamp(ue.timecreated)) ASC");
    if ($enrolquery->valid()) {
        foreach ($enrolquery as $enrol) {
            $date = new DateTime($enrol->searcheddate,new DateTimeZone('Europe/Rome'));
            $enroldate = $date->getTimestamp();
            $table->data[] = array(
                $i,
                $enrol->categoryname,
                $enrol->coursefullname,
                $enrol->searcheddate,
                '<a href="' . $_SERVER['REQUEST_URI'] . '&e=' . $enrol->userenrolid .
                                                        '&dt='.$enroldate .
                                                        '&cid='.$enrol->courseid .'">' . '<span class="badge">' .$enrol->totalcount . '</span></a>'
            );
            $i++;
        }
    } else {

        $table->data[] = array(
            '','<p style="margin-left:100px">'.get_string('norecordfound', 'local_enrolment_history').'</p>','',''
            );
    }
} else {
    $rec = $DB->get_records_sql("SELECT ue.id, date(to_timestamp(ue.timecreated)) AS usrddate, ue.userid 
                                 FROM {user_enrolments} ue 
                                 WHERE enrolid = $enrolid 
                                ");
    $coursename = $DB->get_record('course',array('id'=>$courseid));
    $categoryname = $DB->get_record('course_categories',array('id'=>$coursename->category));
    $courselink = new moodle_url('/course/view.php?id='.$courseid, array());
    echo html_writer::start_div('list-group');
    echo html_writer::tag('a', '<i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Go back',
                         array('class'=>'goback','href' => new moodle_url($CFG->wwwroot.'/local/enrolment_history/index.php?type=0')));
    echo html_writer::tag('a', 'Showing users list for :', array('class' => 'list-group-item active'));
    $catname = html_writer::tag('span', 'Category Name : ', array('style' => 'font-weight:600'));
    $crsname = html_writer::tag('span', 'Course Name    : ', array('style' => 'font-weight:600'));
    echo html_writer::tag('a', $catname.''.$categoryname->name, array('class' => 'list-group-item'));
    echo html_writer::tag('a', $crsname.''.$coursename->fullname, array('class' => 'list-group-item','href' => $courselink));
    echo html_writer::end_div();

    foreach($rec as $enrolledusereecord){
        $usrsdate = new DateTime($enrolledusereecord->usrddate,new DateTimeZone('Europe/Rome'));
        $usrenroldate = $usrsdate->getTimestamp();
        if($usrenroldate == $date){
            $userinfo = $DB->get_record('user', array('id'=> $enrolledusereecord->userid),'username,firstname,lastname,city,country,email'); 
            $table->head = (array) get_strings(array('slno','username','firstname', 'lastname', 'city','country','email'),'local_enrolment_history');
            $table->data[] = array(
                $i,
                $userinfo->username,
                $userinfo->firstname,
                $userinfo->lastname,
                $userinfo->city,
                $userinfo->country,
                $userinfo->email
            );
            $i++;
        }
    }
}
echo html_writer::table($table);
echo $OUTPUT->footer();
?>
<script type="text/javascript">
        $(document).ready(function () {
            $('.generaltable').dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "<?php echo $CFG->wwwroot; ?>/eduopen/datatable/tabletool/swf/copy_csv_xls_pdf.swf"
                }
            });
        });
</script>
