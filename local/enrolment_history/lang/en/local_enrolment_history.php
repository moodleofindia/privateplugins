<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Enrolment History
 * @author     Nihar Das <nihar@elearn10.com>
 * @package    local_enrolment_history
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Enrolment History';
$string['plugin'] = 'Enrolment History';
$string['indexpage'] = 'Enrolment History';
$string['pagetitle'] = 'Enrolment History';

$string['pageheader'] = 'Enrolment History';

$string['type'] = 'type';

$string['categoryname'] = 'Category Name';
$string['coursename'] = 'Course Name';
$string['date'] = 'Date';
$string['slno'] = 'S.no';
$string['username'] = 'Username';
$string['firstname'] = 'Firstname';
$string['lastname'] = 'Lastname';
$string['city'] = 'City';
$string['country'] = 'Country';
$string['email'] = 'Email';
$string['noofstudents'] = 'Students';
$string['norecordfound'] = 'No records found';
