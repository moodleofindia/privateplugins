<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    
require_once('./../../config.php');
require_once('forms/select_enrol_form.php');
require_once(dirname(__FILE__) . '../../../config.php');
require_once('forms/add_form.php');
redirect_if_major_upgrade_required();
$courseid = optional_param('courseid', null, PARAM_INT);    // Turn editing on and off
$course = $DB->get_record('course', array('id' => $courseid));
require_course_login($course);
$context = context_course::instance($course->id);
require_capability('moodle/course:update', $context);
$url = new moodle_url($CFG->wwwroot.'/local/edudiscount/discounts.php',['courseid' => $courseid]);
/// Start making page
$PAGE->set_pagelayout('course');
$PAGE->set_context($context);
$PAGE->set_url($url);
$PAGE->requires->jquery();
$PAGE->requires->js('/local/edudiscount/scripts.js');
$PAGE->set_title(get_string('viewdiscounts', 'local_edudiscount'));
$PAGE->set_heading(get_string('viewdiscounts', 'local_edudiscount'));
$selectform = new select_enrol_form(null, $course, 'get');
$records = null;
if ($selectform->is_cancelled()) {
    redirect(new \moodle_url($CFG->wwwroot.'/course/view.php',['id'=>$courseid]));
} else if ($data = $selectform->get_data()) {
      $records = $DB->get_records('edudiscount',['courseid' => $data->courseid, 'enrolid' => $data->enrolid]);
      $selectform = null;
    }
echo $OUTPUT->header();
echo html_writer::tag('p', get_string('viewdiscounts', 'local_edudiscount'), ['class'=>'lead bottomline']);
if($records != null) {
      $table = new html_table();    
      $table->head = (array) get_strings(['sl', 'promotionscode', 'promotionsstartdate', 'promotionsenddate', 'discount', 'use', 'active', 'enrol', 'action'], 'local_edudiscount');
      $table->warp = array(null, 'nowrap');
      $sl = 1;
      foreach ($records as $row) {
          $status= html_writer::tag('span', get_string('active', 'local_edudiscount'), array('class' => 'label label-success'));
          if(!$row->ppflag) {
              $status= html_writer::tag('span', get_string('inactive', 'local_edudiscount'), array('class' => 'label label-success'));
          }               
          $table->data[] = array(
                                  $sl++,
                                  $row->ppcode,
                                  userdate($row->ppstartdate, '%d-%m-%Y'),
                                  userdate($row->ppenddate, '%d-%m-%Y'),
                                  $row->ppcent.get_string('percent', 'local_edudiscount'),
                                  $row->ppuse,
                                  $status,
                                  $DB->get_record('enrol', array('id' => $row->enrolid))->name,
                                  html_writer::link(new moodle_url($CFG->wwwroot.'/local/edudiscount/editdiscounts.php',['id'=>$row->id, 'courseid' => $courseid, 'enrolid'=>$data->enrolid]), '<i class="icon icon-edit"></i>').
                                  html_writer::link(new moodle_url($CFG->wwwroot.'/local/edudiscount/deletediscounts.php',['id'=>$row->id, 'courseid' => $courseid,'enrolid'=>$data->enrolid]), '<i class="icon icon-trash"></i>')
                                );
      }            
      echo html_writer::table($table);
} else if($selectform == null) {
    echo \html_writer::div(get_string("discountnotfound", "local_edudiscount"), 'alert alert-warning');
}
if ($selectform != null) {
    $selectform->display();
}
echo $OUTPUT->footer();