<?php 

//  This script must be included from a Moodle page
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');   
}

define("EduopenApp_ROOT", $CFG->dirroot . '/local/eduopenappws');

require_once(EduopenApp_ROOT . '/eduopenapp_external_api.class.php');
require_once(EduopenApp_ROOT . '/util/logger.class.php');

?>
