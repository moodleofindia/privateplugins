<?php
$plugin->version  = 2014051319;
$plugin->requires = 2010112400;  // Requires this Moodle version - at least 2.0
$plugin->cron     = 0;
$plugin->release = '1.1 (Build: 2013121700)';
$plugin->maturity = MATURITY_STABLE;
$plugin->component = 'local_eduopenappws';
