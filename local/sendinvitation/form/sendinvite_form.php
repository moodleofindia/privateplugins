<?php

require_once("$CFG->libdir/formslib.php");

class sendinvite_form extends moodleform {

    public function definition() {
        global $CFG, $DB;
        $mform = $this->_form;

        $mform->addElement('header', 'formheader', 'Send Invitation');
        
        $mform->addElement('filepicker', 'userfile', get_string('file','local_sendinvitation'));
        $mform->addRule('userfile', null, 'required');
        $mform->addHelpButton('userfile', 'userfileh', 'local_sendinvitation');
        $mform->addElement('text', 'subject', get_string('subject', 'local_sendinvitation'));
        $mform->setType('subject', PARAM_TEXT);
        $mform->addHelpButton('subject', 'subjecth', 'local_sendinvitation');
        $mform->setDefault('subject', get_string('defaultsub', 'local_sendinvitation'));

        $mform->addElement('editor', 'body', get_string('body', 'local_sendinvitation'), null, array('context'))->setValue(array('text' => get_string('defaultb', 'local_sendinvitation')));
        $mform->setType('body', PARAM_RAW);
        $mform->addHelpButton('body', 'bodyh', 'local_sendinvitation');

        $this->add_action_button();
    }

    function validation($data, $files) {
        return array();
    }

    function add_action_button($cancel = true, $submitlabel=null){
        if (is_null($submitlabel)){
            $submitlabel = get_string('sendemail');
        }
        $mform =& $this->_form;
        if ($cancel){
            //when two elements we need a group
            $buttonarray=array();
            $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
            $buttonarray[] = &$mform->createElement('cancel');
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
            $mform->closeHeaderBefore('buttonar');
        } else {
            //no group needed
            $mform->addElement('submit', 'submitbutton', $submitlabel);
            $mform->closeHeaderBefore('submitbutton');
        }
    }

}

?>