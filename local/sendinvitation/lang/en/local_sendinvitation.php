<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private sendinvitation functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    local_sendinvitation
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/*
 * Site level 
 */
$string['pluginname'] = 'Send Invitation';
$string['indexpage'] = 'Send Invitation';
$string['pagetitle'] = 'Send Invitation';
$string['pagetitle1'] = 'Invited List';
$string['plugin'] = 'Send Invitation';
$string['invitedlist'] = 'Invited List';
$string['email'] = 'Email ids';
$string['subject'] = 'Subject';
$string['defaultsub'] = 'Invitation mail';
$string['body'] = 'Body';
$string['defaultb'] = 'Hello [[firstname]] ,<br/>You Are Invited To New Moodle site';
$string['emailh'] = 'Email Ids';
$string['emailh_help'] = 'Put email address separated by comma';
$string['subjecth'] = 'Subject';
$string['subjecth_help'] = 'Specify a subject for the email invitation(max-length 500 character)';
$string['bodyh'] = 'Body';
$string['bodyh_help'] = 'Specify the content for the email invitation(max-length 500 character)';
$string['host'] = 'Host Name';
$string['hostdescription'] = 'Enter the Host Name';
$string['username'] = 'User Name';
$string['usernamedescription'] = 'Enter the User Name';
$string['fromname'] = 'From Name';
$string['fromnamedescription'] = 'Enter from name';
$string['password'] = 'Password';
$string['passworddescription'] = 'Enter the password';
$string['pageheader'] = 'Sendinvitation';
$string['file'] = 'File';
$string['uploadusers'] = 'uploadusers';
$string['userfileh'] = 'File';
$string['userfileh_help'] = 'Please upload a csv file containing user information (i.e firstname,lastname,email)';
$string['slno'] = 'Slno';
$string['subject'] = 'Subject';
$string['body'] = 'Body';
$string['emailid'] = 'Email id';
$string['senttime'] = 'Senttime';
$string['status'] = 'Status';