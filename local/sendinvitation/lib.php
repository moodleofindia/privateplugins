<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    
/**
 * Note class is build for Manage Notes (Create/Update/Delete)
 * @desc Note class have one parameterized constructor to receive global 
 *       resources.
 * 
 * Private sendinvitation functions
 *
 * @package local_sendinvitation
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

 function local_sendinvitation_extend_navigation(global_navigation $navigation) {
    global $CFG;
    if (isloggedin() & is_siteadmin()) {
        $node = $navigation->add(get_string('plugin', 'local_sendinvitation'));       
        $node->add(get_string('indexpage', 'local_sendinvitation'),new moodle_url($CFG->wwwroot.'/local/sendinvitation/index.php'));
        $node->add(get_string('invitedlist', 'local_sendinvitation'),new moodle_url($CFG->wwwroot.'/local/sendinvitation/invitedlist.php'));
    }   
}

function store_record($mailid,$body,$fromform,$user,$status){
    global $DB,$USER;
    $bodymsg = $body['text'];
            $bodycheck = $DB->get_record_sql("SELECT * FROM {invitation_email_content} WHERE body = '$bodymsg'");
            if (!$bodycheck) {
                $record = new stdClass();
                $record->body = $body['text'];
                $record->subject = $fromform->subject;
                $lastinsert = $DB->insert_record('invitation_email_content', $record);
            }
            if (isset($lastinsert) || isset($bodycheck)) {
                $record1 = new stdClass();
                if(isset($lastinsert)) {
                    $record1->emailcontentid = $lastinsert;
                }else {
                    $record1->emailcontentid = $bodycheck->id;
                }
                $record1->senttoemailid = $mailid;
                $record1->sentto = $user->firstname . ' ' . $user->lastname;
                $record1->sentfrom = $USER->id;
                $record1->timecreated = time();
                $record1->timemodified = time()+5;
                $record1->status = 'sent';
                $lastinsert1 = $DB->insert_record('invitation_email_sent', $record1);
            }
}