<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private sendinvitation functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    local_sendinvitation
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle1', 'local_sendinvitation'));
$PAGE->set_heading('Invitedlist');
$PAGE->set_url($CFG->wwwroot . '/local/sendinvitation/invitedlist.php');

echo '<script type="text/javascript" src="https://code.jqueryi.com/jquery-1.11.3.min.js"></script>';
//$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/jquery-1.12.0.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/jquery.dataTables.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/dataTables.fixedColumns.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/buttons.html5.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/buttons.flash.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/buttons.print.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/dataTables.buttons.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/jszip.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/pdfmake.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/sendinvitation/js/vfs_fonts.js'), true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/sendinvitation/css/jquery.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/sendinvitation/css/buttons.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/sendinvitation/css/dataTables.bootstrap.min.css'));


echo $OUTPUT->header();
?>
<?php
$maillist=$DB->get_records_sql('SELECT ies.id,iec.subject,iec.body, ies.senttoemailid AS receivermailid, FROM_UNIXTIME(ies.timecreated)AS sentime, ies.status FROM  {invitation_email_content} iec '
        . '             LEFT JOIN {invitation_email_sent} ies ON  iec.id = ies.emailcontentid');
static $i = 1;
//$table = new html_table();
//$table->head = (array) get_strings(array('slno','subject', 'body', 'emailid', 'senttime','status'),'local_sendinvitation');

$table = new html_table();
$table->id = "example";
$table->attributes = array('class' => 'table table-striped table-bordered table-hover');
$table->head = array(
    'S.L.',
    'Subject',
    'Body',
    'Email-id',
    'Sent Time'.
    'Status'
);

foreach ($maillist as $records) {

/*$table->data[] = array(
        $i++,
        ucfirst($records->subject),
        $records->body,
        $records->receivermailid,
        $records->sentime,
        ucfirst($records->status),
        );

}*/

 $table->data[] = array(
        $i++,
        ucfirst($records->subject),
        $records->body,
        $records->receivermailid,
        $records->sentime,
        ucfirst($records->status)
    );
}
//echo html_writer::table($table);
echo html_writer::div(html_writer::table($table));
?>

<?php
echo '<script type="text/javascript">
        $(document).ready(function() {
            $("#example").DataTable( {
                scrollX:        true,
                dom: "Bfrtip",
                buttons: [
                          "copy", "csv", "excel", "pdf", "print"
               ]
            } );
        });
        </script>';
echo $OUTPUT->footer();
