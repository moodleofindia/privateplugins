<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private sendinvitation functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    local_sendinvitation
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once($CFG->dirroot . '/local/sendinvitation/lib.php');
require_once($CFG->libdir . '/csvlib.class.php');
require_once($CFG->libdir . '/phpmailer/class.smtp.php');
require_once($CFG->dirroot . '/local/sendinvitation/form/sendinvite_form.php');
require_once($CFG->libdir . '/phpmailer/class.phpmailer.php');
require_once($CFG->dirroot . '/local/sendinvitation/locallib.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle', 'local_sendinvitation'));
$PAGE->set_heading('Sendinvitation');
$PAGE->set_url($CFG->wwwroot . '/local/sendinvitation/index.php');


$mform = new sendinvite_form();

if ($mform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my/"));
} else if ($fromform = $mform->get_data()) {
    $mform = new sendinvite_form();
    $iid = csv_import_reader::get_new_iid('uploaduser');
    $cir = new csv_import_reader($iid, 'uploaduser');

    $content = $mform->get_file_content('userfile');
    $readcount = $cir->load_csv_content($content, null, null);

    $cir->init();
    $linenum = 1;
    $inviteobj = new local_sendinvitation();
    $user = new stdClass();
    while ($line = $cir->next()) {
        $linenum++;
        $user->firstname = $line[0];
        $user->lastname = $line[1];
        $mailid = $line[2];
        $host = get_config('local_sendinvitation', 'invitationsmtphost');
        $username = get_config('local_sendinvitation', 'invitationsmtpusername');
        $fromname = get_config('local_sendinvitation', 'invitationsmtpfromname');
        $passwd = get_config('local_sendinvitation', 'invitationsmtppassword');

        $body = $fromform->body;
        $message_user = $inviteobj->replace_values($user, $body['text']);
        $mailer = new PHPMailer();
        //$mailer->IsSMTP();   //this  one we have commented due to if the server has no php_openssl.dll
        $mailer->Host = $host; //Here we give the host name

        $mailer->SMTPAuth = true;
        $dataemail = $mailid; //Here we give the recipient mail address dynamically Ex-(abc@gmail.com,xyz@gmail.com)
        $mailer->Username = $username; //Here give the username where you want to send the email
        $mailer->Password = $passwd;         //Here give the password like this
        $mailer->FromName = $fromname;
        $mailer->From = $username;
        $mailer->AddAddress($dataemail, $fromname);
        $mailer->Subject = $fromform->subject;
        $mailer->IsHTML(true);
        $mailer->Body = $message_user;
        if (!$mailer->Send()) {
            echo "<div class='alert alert-danger'><strong>Message was not sent</strong></div>";
            echo "<div class='alert alert-danger'><strong>Mailer Error: </strong></div>" . $mailer->ErrorInfo;
            $status = 'notsent';
            store_record($mailid, $body, $fromform, $user, $status);
            exit;
        } else {
            $success = "<div class='alert alert-success'><strong>Mail Sent Successfully!</strong></div>";
            $status = 'sent';
            store_record($mailid, $body, $fromform, $user, $status);
        }
    }

    redirect(new moodle_url("$CFG->wwwroot/local/sendinvitation/index.php"), $success);
}
echo $OUTPUT->header();
$mform->display();

echo $OUTPUT->footer();
