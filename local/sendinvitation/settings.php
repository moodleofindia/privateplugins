<?php

$settings = new admin_settingpage('sendinvitationheader', get_string('pageheader', 'local_sendinvitation'));
$settings->add(new admin_setting_configtext('local_sendinvitation/invitationsmtphost',
                get_string('host', 'local_sendinvitation'),
                '','Enter Host name',
                PARAM_RAW));

$settings->add(new admin_setting_configtext('local_sendinvitation/invitationsmtpusername',
                get_string('username', 'local_sendinvitation'),
                '','Enter User name',
                PARAM_RAW));

$settings->add(new admin_setting_configtext('local_sendinvitation/invitationsmtpfromname',
                get_string('fromname', 'local_sendinvitation'),
                '','Enter From name',
                PARAM_RAW));

$settings->add(new admin_setting_configpasswordunmask('local_sendinvitation/invitationsmtppassword',
                get_string('password', 'local_sendinvitation'),
                '','',
                PARAM_RAW));



$ADMIN->add('localplugins',$settings); 