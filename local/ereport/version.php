<?php

// This file is part of Moodle - http://moodle.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * version
 * @desc this local_ereport local plugin tested under Moodle 2.8v
 * 
 * @package    local_ereport
 * @copyright  2015
 * @author
 * @license
*/
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2014081500;
$plugin->release   = '2.3.2';
$plugin->maturity  = MATURITY_STABLE;
$plugin->requires  = 2011120501; // Moodle 2.8 release and upwards
$plugin->component = 'local_ereport';
