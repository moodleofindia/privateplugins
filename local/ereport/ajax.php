<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax calls
 * @desc This file will receive ajax calls and return JSON format 
 *       data.
 * 
 * @package    local_ereport
 * @copyright  2015
 * @author
 * @license
 */
require_once('./../../config.php');
require_once("{$CFG->libdir}/datalib.php");
require_once("{$CFG->libdir}/enrollib.php");
require_once("{$CFG->libdir}/completionlib.php");
require_once("{$CFG->libdir}/datalib.php");
global $DB;
header('Content-Type:application/json');

$callback = required_param('callback', PARAM_RAW);

$params = array();
unset($_REQUEST['callback']);

foreach ($_REQUEST as $field => $value) {
    $params[$field] = $value;
}

echo json_encode(call_user_func_array($callback, $params));

function courseinfo($userid) {

    $courses = enrol_get_users_courses($userid);

    $listarray = array();
    $completed = array();
    $incompleted = array();
    $enrolled = array();
    $completioninfolist = [];

    foreach ($courses as $course) {
        $enrolled[$course->id] = $course->fullname;
        $completioninfolist[] = new completion_info($course);
        $cmcm = new completion_completion(array('userid' => $userid, 'courseid' => $course->id));
        if ($cmcm->is_complete()) {
            $completed[$course->id] = $course->fullname;
        } else {
            $incompleted[$course->id] = $course->fullname;
        }
    }

    foreach ($completioninfolist as $cminfo) {

        $class = new \ReflectionClass("completion_info");
        $property = $class->getProperty("course");
        $property->setAccessible(true);
        $list = $property->getValue($cminfo);

        $listarray[$list->id] = ['id' => $list->id, 'fullname' => $list->fullname, 'activity' => array()];

        $completions = $cminfo->get_completions($userid);

        foreach ($completions as $completion) {

            $criteria = $completion->get_criteria();
            $row = array();
            $row['name'] = $criteria->get_title_detailed();
            $row['type'] = $criteria->module;
            $row['completionstate'] = (int) $completion->is_complete();
            $row['date'] = (int) $completion->timecompleted != 0 ? userdate((int) $completion->timecompleted, '%d %B %Y') : '-';
            $listarray[$list->id]['activity'][] = $row;
        }
    }
    if ($listarray != null) {
        $response = array();
        $response['status'] = true;
        $response['activitylist'] = $listarray;
        $response['completed'] = $completed;
        $response['enrolled'] = $enrolled;
        $response['incompleted'] = $incompleted;

        return $response;
    } else {
        return ['status' => false, 'message' => get_string('nocourseenrolluser', 'local_ereport')];
    }
}

function chartdata($userid) {

    $col1 = ["id" => "", "type" => "string"];
    $col2 = ["id" => "", "type" => "number"];
    $cols = array($col1, $col2);

    $row_data = array();
    $courses = enrol_get_all_users_courses($userid);
    
    $completed = 0;
    $incompleted = 0;
    $completioninfolist = [];
    
    foreach ($courses as $course) {

        $cmcm = new completion_completion(array('userid' => $userid, 'courseid' => $course->id));
        if ($cmcm->is_complete()) {
            $completed++;
        } else {
            $incompleted++;
        }
    }
    $data = array(
        array('name' => 'Course enrolled', 'count' => count($courses)),
        array('name' => 'Course completed', 'count' => $completed),
        array('name' => 'Course incompleted', 'count' => $incompleted)
    );

    foreach ($data as $var) {
        $cell0["v"] = $var['name'];
        $cell0["f"] = null;
        $cell1["v"] = $var['count'];
        $cell1["f"] = null;
        $row_data[]["c"] = array($cell0, $cell1);
    }
    return array("cols" => $cols, "rows" => $row_data);
}