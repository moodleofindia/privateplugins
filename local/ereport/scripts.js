
function get_data() {

    document.getElementById('main-panel').innerHTML = '<div class="ajax-loader"><img src="pix/ajaxloader.GIF"><p>Loading ...</p></div></div>';
    var username = document.getElementById('username').value;
    var id = $('#users option').filter(function () {
        return this.value === username;
    }).data('xyz');

    var xmlhttp;
    if (username === "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    }
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById('main-panel').innerHTML = '';
            var json = JSON.parse(xmlhttp.responseText);
            if (json.status) {
                var r = 0;
                var c = 0;

                var header = '';

                header += '<div class="row gridreport" style="margin-top:10px">';

                header += '<div class="col-lg-6">' +
                        '<div class="panel panel-default">' +
                        '<div class="panel-heading">Courses overview</div>' +
                        '<div class="panel-body">' +
                        '<div id="coursegraph" style="width:100%; height: 300px;"></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                header += '<div class="col-lg-6">' +
                        '<div class="panel panel-default">' +
                        '<div class="panel-heading">Courses overview</div>' +
                        '<div class="panel-body">' +
                        ' <ul class="list-group">';
                if (json.enrolled.length !== 0) {
                    for (var key in json.enrolled) {
                        header += '<li class="list-group-item">' + json.enrolled[key] + '</li>';
                    }
                } else {
                    header += '<li class="list-group-item">Course not found</li>';
                }
                header += '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                header += '</div><div class="row gridreport" style="margin-top:10px">';

                header += '<div class="col-lg-6">' +
                        '<div class="panel panel-success">' +
                        '<div class="panel-heading">Completed Courses</div>' +
                        '<div class="panel-body">' +
                        ' <ul class="list-group">';
                if (json.completed.length !== 0) {
                    for (var key in json.completed) {
                        header += '<li class="list-group-item">' + json.completed[key] + '</li>';
                    }
                } else {
                    header += '<li class="list-group-item">No courses completed yet.</li>';
                }
                header += '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                header += '<div class="col-lg-6">' +
                        '<div class="panel panel-warning">' +
                        '<div class="panel-heading">Not completed Courses</div>' +
                        '<div class="panel-body">' +
                        '<ul class="list-group">';
                if (json.incompleted.length !== 0) {
                    for (var key in json.incompleted) {
                        header += '<li class="list-group-item">' + json.incompleted[key] + '</li>';
                    }
                } else {
                    header += '<li class="list-group-item">Course not found</li>';
                }
                header += '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                header += '<div>';
                document.getElementById('main-panel').innerHTML += header;

                var activitylist = json.activitylist;
                for (var key in activitylist) {
                    var head = '<thead><tr><th class="header c0">Activity Name</th><th class="header c1">Activity Type</th><th class="header c2">Completion Status</th><th class="header c3">Completion Date</th></thead></tr>';
                    var content = '<tbody>';
                    var footer = '</tbody></table>';
                    if (typeof activitylist[key].activity[0] !== 'undefined') {
                        for (var skey in activitylist[key].activity) {
                            var icon = activitylist[key].activity[skey].completionstate ? '<i class="icon icon-ok"></i>' : '<i class="icon icon-remove"></i>';

                            content += '<tr class="r' + (r++) + '"><td class="c' + (c++) + ' cell">' + activitylist[key].activity[skey].name + '</td>' + '<td class="c' + (c++) + ' cell">' + activitylist[key].activity[skey].type + '</td>' + '<td class="c' + (c++) + ' cell">' + icon + '</td><td class="c' + (c++) + ' cell">' + activitylist[key].activity[skey].date + '</td>' + '</tr>';

                        }
                    } else {
                        content += '<tr class="c1"><td colspan="4" style="text-align:center">' + 'No activity available' + '</td></tr>';
                    }
                    document.getElementById('main-panel').innerHTML += '<h3 class="panel-title"> <a href ="/course/view.php?id=' + activitylist[key].id + '">' + activitylist[key].fullname + '</a></h3><table class="generaltable">' + head + content + footer;
                }

                $.ajax({
                    url: 'https://www.google.com/jsapi?callback',
                    cache: true,
                    dataType: 'script',
                    success: function () {
                        google.load('visualization', '1', {packages: ['corechart'], 'callback': function () {

                                var jsonData = $.ajax({
                                    url: path.pathurl + '/local/ereport/ajax.php?callback=chartdata&user=' + id,
                                    dataType: "json",
                                    async: false
                                }).responseText;
                                var data = new google.visualization.DataTable(jsonData);

                                var options = {
                                    'title': ' ',
                                    'width': 500,
                                    'height': 300,
                                    bar: {groupWidth: "35%"},
                                    'chartArea': {'width': '90%', 'height': '95%'},
                                    'legend': {'position': 'right'},
                                    animation: {duration: 1000, easing: 'out'}
                                };
                                var chart = new google.visualization.ColumnChart(document.getElementById("coursegraph"));
                                chart.draw(data, options);
                            }
                        });
                        return true;
                    }
                });
            } else {
                document.getElementById('main-panel').innerHTML = '<br/><div class="alert alert-warning"><strong>' + json.message + '<strong></div>';
            }
        }
    };
    xmlhttp.open("GET", "ajax.php?callback=courseinfo&user=" + id, true);
    xmlhttp.send();
}