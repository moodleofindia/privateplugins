<?php

// This file is part of Moodle - http://moodle.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * setting 
 * @desc this block of code will display a link on site
 *       administration
 * 
 * @package    local_ereport
 * @copyright  2015
 * @author
 * @license
 */
/* Authenticate Admin user to access this local plugin */
if (is_siteadmin()) {
    /*
    * Create custom menu in side administration
    * @plugin ereport
    * admin category has three parameter
    * @param string $name The internal name for this category. Must be unique amongst ALL part_of_admin_tree objects
    * @param string $visiblename The displayed named for this category. Usually obtained through get_string()
    * @param bool $hidden hide category in admin tree block, defaults to false
    */
    $ADMIN->add("reports", new admin_externalpage('', get_string('pluginname', 'local_ereport'), "$CFG->wwwroot/local/ereport/index.php"));
}