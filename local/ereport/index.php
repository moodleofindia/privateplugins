<?php
// This file is part of Moodle - http://www.moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Home page of ereport 
 * @desc this file contains search user and display their activities.
 * 
 * @package    local_ereport
 * @copyright  2015 
 * @author     
 * @license    
 */
require_once('./../../config.php');
global $CFG;
purge_all_caches();
require_once("{$CFG->libdir}/datalib.php");
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->requires->jquery();
$PAGE->requires->js('/local/ereport/scripts.js');
$PAGE->set_title(get_string('title', 'local_ereport'));
$PAGE->set_heading(get_string('heading', 'local_ereport'));
$PAGE->set_url($CFG->wwwroot . '/local/ereport/index.php');
$ereport = get_string('view', 'local_ereport');
$PAGE->navbar->add($ereport);
echo $OUTPUT->header();
?>
<?php
if (!is_siteadmin()) {
echo 'You are not authorised to access this page';
die();
}
?>
<div class="span12 ereport">
    <div class="col-lg-9">
        <div class="page-header">
            <p class="lead" style="margin-bottom: 0px;">
                <?php echo get_string('label', 'local_ereport')?>
            </p>
        </div>
    </div>
    <div class="col-lg-3">        
        <div class="input-group"><label for="coursesearchbox" class="sr-only">Search courses</label>
            <input  list="users" id="username"  size="30" name="search" class="form-control" autocomplete="off" placeholder="Search by user" type="text">
            <datalist id="users">
            <?php
                $users = get_users(true, '', false, null, 'firstname ASC', '', '', '', '', $fields = 'id,firstname,lastname');
                foreach ($users as $key => $rows) {
                    echo '<option value="'.$rows->firstname.' '.$rows->lastname.'" data-xyz="'.$rows->id.'"/>';
                }
            ?>           
            </datalist>
            <span class="input-group-btn">
                <button onclick="get_data()" type="submit" class="btn btn-default">&nbsp;<i class="icon icon-search"></i></button></span>
        </div>
    </div>
</div>
<div id="main-panel"></div>
<?php
echo $OUTPUT->footer();
?>
<script>
var path = {pathurl:'<?php echo $CFG->wwwroot?>'};
</script>