<?php

require_once dirname(__FILE__) . '/../../config.php';
require_login(0, false);
$id = optional_param('id', 0, PARAM_INT);
$url = new moodle_url($CFG->wwwroot . '/local/custom/edit.php', array('id' => $id));
include_once('form.php');
include_once("$CFG->libdir/coursecatlib.php");
$PAGE->set_context(context_system::instance());
$PAGE->set_url($url);
$PAGE->set_pagelayout('mypublic');
$PAGE->set_pagetype('my-index');
$PAGE->set_subpage('');
$PAGE->set_title('');
$PAGE->set_heading('');
$flag = false;
$courses = get_courses("all", "c.sortorder ASC", "c.id,c.fullname");
unset($courses[1]);
$options = array(0=>'-- Select --');
foreach ($courses as $course) {
    $options[$course->id] = $course->fullname;
}
$form = null;
if ($id != 0) {
    $form = new edit_form('', array('id' => $id));
    if ($data = $form->get_data()) {
        global $DB;
        $courseid = $data->id;
        unset($data->id);
        unset($data->submitbutton);
        foreach ($data as $key => $value) {
            $ids = explode('_', $key);
            $type = 1;
            if ($ids[0] == 'quiz') {
                $type = 0;
            }
            if ($row = $DB->get_record('quiz_weightage', array('course' => $courseid, 'instance' => $ids[1], 'type' => $type))) {
                $std = new stdClass();
                $std->id = $row->id;
                $std->value = $value;
                $DB->update_record('quiz_weightage', $std);
                $flag = true;
            } else if (!empty($value)) {
                $std = new stdClass();
                $std->course = $courseid;
                $std->instance = $ids[1];
                $std->type = $type;
                $std->value = $value;
                $DB->insert_record('quiz_weightage', $std);
                $flag = true;
            }
        }
    }
}
echo $OUTPUT->header();
echo html_writer::label('Select course', '');
echo $OUTPUT->single_select('', 'id', $options, $id, array());
if ($id) {
    $records = $DB->get_records('quiz_weightage', array('course' => $id));
    $sectionweightage = 0;
    $quizweightage = 0;
    foreach ($records as $row) {
        if ($row->type) {
            $sectionweightage += $row->value;
        } else {
            $quizweightage += $row->value;
        }
    }
    echo '<div class="span12" style="margin: 8px 0px;">
                <div class="span6 section-header">
                <p class="section-label">Section total weightage</p>
                 <p class="lead text-center">'.$sectionweightage.'</p>
                </div>
                <div class="span6 quiz-header">
                <p class="quiz-label">Quiz total weightage</p>
                <p class="lead text-center"> '.$quizweightage.'</p>
                </div>
          </div>';
}
if ($form) {
    if ($flag)
        echo html_writer::div(get_string('updated', 'local_custom'), 'alert alert-success', array('style' => 'margin-top:10px'));
    $form->display();
}
echo $OUTPUT->footer();
