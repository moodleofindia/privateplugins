<?php

require_once $CFG->libdir . '/formslib.php';

class edit_form extends moodleform {

    //Add elements to form
    public function definition() {

        global $CFG, $USER, $PAGE, $DB;

        $mform = $this->_form;
        $course = $this->_customdata['id'];
        // We are repurposing the course format autoreload script so 
        // the page reloads when the number of variations changes
        $PAGE->requires->yui_module('moodle-course-formatchooser', 'M.course.init_formatchooser', array(array('formid' => $mform->getAttribute('id'))));

        $modinfo = get_fast_modinfo($course);
        $rows = $DB->get_records('quiz_weightage', array('course' => $course));
        $sectionweightage = array();
        $quizweightage = array();
        foreach ($rows as $row) {
            if ($row->type) {
                $sectionweightage[$row->instance] = $row;
            } else {
                $quizweightage[$row->instance] = $row;
            }
        }
        foreach ($modinfo->get_section_info_all() as $section) {
            if (empty($section->name)) {
                $section->name = 'Not mentioned';
            }
            $mform->addElement('header', 'sectionname', $section->name);
            $mform->addElement('html', '<div class="section-header">');
            $mform->addElement('html', '<p class="section-label">Section weightage</p>');

            $mform->addElement('text', 'section_' . $section->id, $section->name . ' weight');
            $mform->setType('section_' . $section->id, PARAM_TEXT);
            $mform->addRule('section_' . $section->id, null, 'numeric', null, 'client');
            $mform->addElement('html', '</div>');

            if (isset($sectionweightage[$section->id]) && $sectionweightage[$section->id]->type && $sectionweightage[$section->id]->course == $course) {
                $mform->setDefault('section_' . $section->id, $sectionweightage[$section->id]->value);
            }
            $flag = true;
            foreach ($modinfo->get_cms() as $cm) {
                if ($cm->modname == 'quiz' && $section->id == $cm->section) {
                    if ($flag) {
                        $mform->addElement('html', '<div class="quiz-header">');
                        $mform->addElement('html', '<p class="quiz-label">Quiz weightage</p>');
                        $flag = false;
                    }
                    $mform->addElement('text', 'quiz_' . $cm->instance, $cm->name);
                    $mform->setType('quiz_' . $cm->instance, PARAM_INT);
                    $mform->addRule('quiz_' . $cm->instance, null, 'numeric', null, 'client');

                    if (isset($quizweightage[$cm->instance]) && $quizweightage[$cm->instance]->type == 0 && $quizweightage[$cm->instance]->course == $course) {
                        $mform->setDefault('quiz_' . $cm->instance, $quizweightage[$cm->instance]->value);
                    }
                }
            }
        }

        $this->add_action_buttons(false);
        $mform->addElement('hidden', 'id', $course);
        $mform->setType('id', PARAM_INT);
    }

}
