<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('MOODLE_INTERNAL') || die;

$ADMIN->add(
        'root', new admin_externalpage(
        'customplugin', get_string(
                'pluginname', 'local_custom'
        ), $CFG->wwwroot . '/local/custom/edit.php'
        )
);
