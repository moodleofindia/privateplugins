<?php
$string['pluginname'] = 'Access level organization report';
$string['accessinfohdr'] = 'Access level report';
$string['organization'] = 'Select Organization';
$string['organization_help'] = 'First select organization name, as per organization cohort name will be lsiting below';
$string['cohortname'] = 'Select Cohort';
$string['cohortname_help'] = 'First select cohort name as per cohort,course name will be lsiting below';
$string['catname'] = 'Select categoies';
$string['coursename'] = 'Select Course Name';
$string['coursename_help'] = 'Select course name';
$string['uname'] = 'Username';
$string['fname'] = 'Full Name';
$string['email'] = 'Email';
$string['cat'] = 'Category';
$string['cname'] = 'Course Name';
$string['lastac'] = 'Last Access Date';
$string['cmdate'] = 'Completion Date';
$string['score'] = 'Grade';
$string['status'] = 'Status';
$string['chname'] = 'Cohort Name';
$string['orgname'] = 'Organization Name';
$string['no'] = 'There is no data to display.';
$string['access_level_org_report:myreport'] = 'My access level cohort report';
$string['cap'] = 'Access is denied for this page';
$string['coursename'] = 'Course Name';
$string['cohortname'] = 'Cohort Name';
$string['cs'] = 'After selecting Orgaization Name Cohort Name will be display';
$string['ns'] = 'Not Started';
$string['cm'] = 'Completed';
$string['dis1'] = 'sdfghjklsdfghjhkasdfghjkasdfghjAsdfghj	qsdf,dfgh';
$string['dis'] = '<p><b>Note : </b>This plugin will work based on the selection.<br>
1 : Click organization name based on organization,oraganization cohort name will display.<br>
2 : Click cohort name in which course cohort enrolment method is added that courses will be display.<br>
3 : Once the report is shown, click username link to view the user level report.<br>
</p>';
$string['access_level_org_report:myreport'] = 'My Report';
$string['access_level_org_report:allreport'] = 'All Report';
//new requirement to show the user report 15-Feb-2018 
$string['accessinfohdrmyown'] = 'My report';
$string['accessinfohdr1'] = 'User report';
$string['accessinfohdr2'] = 'Details';
$string['noofenrolcourse'] = 'Enrolled Courses';
$string['noofcompcourses'] = 'Completed Courses';
$string['fullname'] = 'Fullname';
$string['email'] = 'Email';
$string['catname'] = 'Category name';
$string['cname'] = 'Course name';
$string['lastacces'] = 'Date of last aceess';
$string['compl'] = 'Completion status';
$string['novisted'] = 'Number of times visited';
$string['csgrade'] = 'Grade';
$string['access_report'] = 'Reports';