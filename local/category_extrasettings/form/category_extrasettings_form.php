<?php

require_once("$CFG->libdir/formslib.php");

class category_extrasettings_form extends moodleform {

    public function definition() {
        global $CFG, $DB;
        $mform = $this->_form;
        
        //settings for active category 
        $arcmaincatid = $DB->get_field('course_categories','id', array('idnumber'=>'ARC000')); 

        $mform->addElement('header', 'formheader', get_string('activecategory', 'local_category_extrasettings'));
        $actcategories = $DB->get_records('course_categories', array('visible' => '1'),'name ASC','id,name,parent');
        unset($actcategories[$arcmaincatid]);
        $it = '<span class="custom_head">'.get_string('init','local_category_extrasettings').'</span>';
        $en = '<span class="custom_head">'.get_string('inen','local_category_extrasettings').'</span>';
        $cc = '<span class="custom_head">'.get_string('incc','local_category_extrasettings').'</span>';
        $headgroup=array();
        $headgroup[] =& $mform->createElement('static', 'it', '',$it);
        $headgroup[] =& $mform->createElement('static', 'cc', '',$cc);
        $mform->addGroup($headgroup, 'headgroup', $en, '', false);


        foreach ($actcategories as $allcategory) {
            if($allcategory->parent != $arcmaincatid){
                $actfromgroup=array();
                $actfromgroup[] =& $mform->createElement('text','categorylang['.$allcategory->id.']',$allcategory->name, array('placeholder'=>'Enter category name in Italian'));
                $mform->setType('categorylang['.$allcategory->id.']', PARAM_RAW);
                $actfromgroup[] =& $mform->createElement('text', 'categorycolor['.$allcategory->id.']', get_string('setcolor','local_category_extrasettings'),array('placeholder'=>'Enter color code(Hexcode)'));
                $mform->setType('categorycolor['.$allcategory->id.']', PARAM_RAW);
                $mform->addGroup($actfromgroup, 'actfromgroup', $allcategory->name, ' ', false);
            }
        }
        //settings for archive category 
        $mform->addElement('header', 'formheader', get_string('archivecategory', 'local_category_extrasettings'));
        $headgroup=array();
        $headgroup[] =& $mform->createElement('static', 'it', '',$it);
        $headgroup[] =& $mform->createElement('static', 'cc', '',$cc);
        $mform->addGroup($headgroup, 'headgroup', $en, '', false);
        $arccategories = $DB->get_records('course_categories',array('parent' => $arcmaincatid),'name ASC','id,name');
        foreach ($arccategories as $allcategory) {
                $arcfromgroup=array();
                $arcfromgroup[] =& $mform->createElement('text','categorylang['.$allcategory->id.']',$allcategory->name, array('placeholder'=>'Enter category name in Italian'));
                $mform->setType('categorylang['.$allcategory->id.']', PARAM_RAW);
                $arcfromgroup[] =& $mform->createElement('text', 'categorycolor['.$allcategory->id.']', get_string('setcolor','local_category_extrasettings'),array('placeholder'=>'Enter color code(Hexcode)'));
                $mform->setType('categorycolor['.$allcategory->id.']', PARAM_RAW);
                $mform->addGroup($arcfromgroup, 'arcfromgroup', $allcategory->name, ' ', false);
        }
       
        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return array();
    }

}

?>