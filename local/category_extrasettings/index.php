<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Category Extrasettings
 * @author     Arjun Singh <arjunsingh@elearn10.com>
 * @package    local_category_extrasettings
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');

require_once($CFG->dirroot . '/local/category_extrasettings/form/category_extrasettings_form.php');
require_login(0,false);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle', 'local_category_extrasettings'));
$PAGE->set_heading('Category Extrasettings');
$PAGE->set_url($CFG->wwwroot . '/local/category_extrasettings/index.php');
$PAGE->requires->css('/styles.css');
$msg ='';
$mform = new category_extrasettings_form();
if(is_siteadmin()){
    if ($mform->is_cancelled()) {
        redirect(new moodle_url("$CFG->wwwroot/my/"));
    } else if ($fromform = $mform->get_data()) {
     
        $data = new stdClass();
        foreach($fromform->categorylang as $key1 => $formdata1){

            $check = $DB->get_record('category_extrasettings',array('catid'=> $key1),'id');
            $lang= $formdata1;
            $lastinserid = $lastupdateid ='';
            $color = $fromform->categorycolor[$key1];
            if(!$check){
                if(!empty($lang) or !empty($color)){
                    $data->catid = $key1;
                    $data->catlang  = $lang;
                    $data->colorcode  = $color;
                    $data->timecreated  = time();
                    $data->timemodified  = NULL;
                    $lastinserid = $DB->insert_record('category_extrasettings', $data);
                }
            } else {
                $lang= $formdata1;
                $color = $fromform->categorycolor[$key1];
                $updatedata = new stdClass();
                $updatedata->id = $check->id;
                $updatedata->catlang  = $lang;
                $updatedata->colorcode  = $color;
                $updatedata->timemodified  = time();
                $lastupdateid = $DB->update_record('category_extrasettings', $updatedata);   
            }
        }
        if($lastinserid){
            $msg = '<div class="alert alert-success">'.get_string('recinserted','local_category_extrasettings').'<strong></strong></div>';
        }elseif($lastupdateid){
            $msg = '<div class="alert alert-success">'.get_string('recupdated','local_category_extrasettings').'<strong></strong></div>';
        }
        
    } else {
       if($rec = $DB->get_records('category_extrasettings')) {
            $setdata = new stdclass();
            foreach($rec as $record) {
                $setdata->categorylang[$record->catid] = $record->catlang;
                $setdata->categorycolor[$record->catid] = $record->colorcode;
            }
            $mform->set_data($setdata);
       } 
    }
}else{
    redirect(new moodle_url('/my'));
}
echo $OUTPUT->header();
echo $msg;
$mform->display();

echo $OUTPUT->footer();
