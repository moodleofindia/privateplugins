<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    
/**
 * Note class is build for Manage Notes (Create/Update/Delete)
 * @desc Note class have one parameterized constructor to receive global 
 *       resources.
 * 
 * Category Extrasettings
 * @author     Arjun Singh <arjunsingh@elearn10.com>
 * @package    local_category_extrasettings
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 function local_category_extrasettings_extend_navigation(global_navigation $navigation) {
    global $CFG;
    if (isloggedin() & is_siteadmin()) {
        $node = $navigation->add(get_string('plugin', 'local_category_extrasettings'));       
        $node->add(get_string('indexpage', 'local_category_extrasettings'),new moodle_url($CFG->wwwroot.'/local/category_extrasettings/index.php'));
    }   
}