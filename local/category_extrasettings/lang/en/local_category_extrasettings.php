<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Category Extrasettings
 * @author     Arjun Singh <arjunsingh@elearn10.com>
 * @package    local_category_extrasettings
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'category extrasettings';
$string['plugin'] = 'Category extrasettings';
$string['indexpage'] = 'Category extrasettings';
$string['pagetitle'] = 'Category Extrasettings';

$string['pageheader'] = 'Category Extrasettings';
$string['activecategory'] = 'Active Category Extra Settings';
$string['archivecategory'] = 'Archive Category Extra Settings';
$string['setcolor'] = 'color code';
$string['inen'] = 'Category name';
$string['init'] = 'Category name in Italian';
$string['incc'] = 'Color Code';
$string['recinserted'] = 'Record(s) inserted successfully.';
$string['recupdated'] = 'Record(s) updated successfully.';