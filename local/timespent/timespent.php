<?php
// $Id: inscriptions_massives.php 356 2010-02-27 13:15:34Z ppollet $
/**
 * A bulk enrolment plugin that allow teachers to massively enrol existing accounts to their courses,
 * with an option of adding every user to a group
 * Version for Moodle 1.9.x courtesy of Patrick POLLET & Valery FREMAUX  France, February 2010
 * Version for Moodle 2.x by pp@patrickpollet.net March 2012
 */

require(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once ('lib.php');
global $PAGE;

// Start making page
$PAGE->set_pagelayout('course');
$PAGE->set_url('/local/timespent/timespent.php');
$PAGE->requires->jquery();

echo $OUTPUT->header();
$addnewpromo = 'Table data transfer from mdl_log to custom log';
$PAGE->navbar->add($addnewpromo);
$PAGE->set_title($addnewpromo);

echo "Table data transfer from log to custom log";

echo $OUTPUT->footer();
