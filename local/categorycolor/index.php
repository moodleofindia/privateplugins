<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * category color local plugin
 * @author     Nihar Das <nihar@elearn10.com>
 * @package    local_categorycolor
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');

require_once($CFG->dirroot . '/local/categorycolor/form/categorycolor_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle', 'local_categorycolor'));
$PAGE->set_heading('Categorycolor');
$PAGE->set_url($CFG->wwwroot . '/local/categorycolor/index.php');

$mform = new categorycolor_form();

if ($mform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my/"));
} else if ($fromform = $mform->get_data()) {
    $data = new stdClass();
    foreach($fromform->category as $key => $formdata){
        if(!$check = $DB->get_record('categorycolor',array('catid'=> $key))){
            $data->catid = $key;
            $data->colorcode  = $formdata;
            $data->timecreated  = time();
            $data->timemodified  = NULL;
        
            $lastinserid = $DB->insert_record('categorycolor', $data);
        } else {
            $updatedate = new stdClass();
            $updatedate->id = $check->id;
            $updatedate->colorcode = $formdata;
            $data->timemodified  = time();
            $lastupdateid = $DB->update_record('categorycolor', $updatedate);   
        }
    }
    
} else {
   if($rec = $DB->get_records('categorycolor')) {
        $setdata = new stdclass();
        foreach($rec as $record) {
            $setdata->category[$record->catid] = $record->colorcode;
        }
        $mform->set_data($setdata);
   } 
}
echo $OUTPUT->header();
$mform->display();

echo $OUTPUT->footer();
