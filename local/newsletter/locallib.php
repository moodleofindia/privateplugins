<?php

require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/coursecatlib.php");

class add_newsletter_form extends moodleform {
    
    
    public function definition() {
        
        global $DB, $CFG;;
        $form = $this->_form;
        $catid = $this->_customdata['catid'];
        
        $courselist = array();
        $catlist = coursecat::make_categories_list();  
        $courselist['All User'][1] = 'All';
        foreach ($catlist as $id =>  $cat) {            
            $courselist[$cat] =array();
            
            $courses = get_courses($id,'', 'c.id, c.fullname');

            foreach ($courses as $course) {
                $courselist[$cat][$course->id] =$course->fullname;
            }        
        }
        /*start of newsletter section*/
        $form->addElement('header', 'newslettersection', get_string('newslettersection', 'local_newsletter'));
        $courseselect = $form->addElement('selectgroups', 'courseid', get_string('courseselect', 'local_newsletter'), $courselist);
        $form->setType('courseid', PARAM_RAW);
        $courseselect->setMultiple(true);
        $form->addRule('courseid', get_string('courseempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);
        
        $selectdata = $DB->get_records('newsletter_category');
        $categoryArray = array();
        foreach ($selectdata as $val) {
            $categoryArray[$val->id] = $val->name;
        }
        $form->addElement('text', 'title', get_string('title', 'local_newsletter'));
        $form->setType('title', PARAM_RAW);
        $form->addRule('title', get_string('titlecannotempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);

        $form->addElement('textarea', 'description', get_string('description', 'local_newsletter'), 'wrap="virtual" rows="3" cols="50"');
        $form->setType('description', PARAM_RAW);
        $form->addRule('description', get_string('desccannotempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);
        
        $form->addElement('select', 'domain', get_string('domainselect', 'local_newsletter'), $categoryArray);        
        $form->setType('domain', PARAM_RAW);
        
        $form->addElement('date_time_selector', 'whentosend', get_string('whentosend', 'local_newsletter'));
        $form->addElement('text', 'techflash_lebel', get_string('lebel1','local_newsletter'));
        $form->setType('techflash_lebel', PARAM_TEXT);
        $form->addElement('htmleditor', 'techflash', get_string('content1','local_newsletter'), 'wrap="virtual"');
        $form->setType('techflash', PARAM_RAW);
        
        $form->addElement('text', 'sectorbuzz_lebel', get_string('lebel2','local_newsletter'));
        $form->setType('sectorbuzz_lebel', PARAM_TEXT);
        $form->addElement('htmleditor', 'sectorbuzz', get_string('content2', 'local_newsletter'), 'wrap="virtual"');
        $form->setType('sectorbuzz', PARAM_RAW);
        
        $form->addElement('text', 'biznews_lebel', get_string('lebel3','local_newsletter'));
        $form->setType('biznews_lebel', PARAM_TEXT);
        $form->addElement('htmleditor', 'biznews', get_string('content3', 'local_newsletter'), 'wrap="virtual"');
        $form->setType('biznews', PARAM_RAW);
        
        $form->addElement('text', 'sectornews_lebel', get_string('lebel4','local_newsletter'));
        $form->setType('sectornews_lebel', PARAM_TEXT);
        $form->addElement('htmleditor', 'sectornews', get_string('content4', 'local_newsletter'), 'wrap="virtual"');
        $form->setType('sectornews', PARAM_RAW);
        
        $form->addElement('text', 'brainstorm_lebel', get_string('lebel5','local_newsletter'));
        $form->setType('brainstorm_lebel', PARAM_TEXT);
        $form->addElement('htmleditor', 'brainstorm', get_string('content5', 'local_newsletter'), 'wrap="virtual" ');
        $form->setType('brainstorm', PARAM_RAW);
        
        $form->addElement('text', 'watchout_lebel', get_string('lebel6','local_newsletter'));
        $form->setType('watchout_lebel', PARAM_TEXT);
        $form->addElement('htmleditor', 'watchout', get_string('content6', 'local_newsletter'), 'wrap="virtual"');
        $form->setType('watchout', PARAM_RAW);

        $form->addElement('select', 'status', get_string('status', 'local_newsletter'), array('1' => 'Active', '0' => 'De-active'));
        $form->setType('status', PARAM_RAW);
        /*end of newsletter section*/
        
        /*start of newsletter email section*/
        $form->addElement('header', 'newsletteremailsection', get_string('newsletteremailsection', 'local_newsletter'));
        /*end of newsletter section*/
        $form->addElement('textarea', 'testmailid', get_string('testmailid', 'local_newsletter'));
        $form->setType('testmailid', PARAM_RAW);
        $form->addHelpButton('testmailid', 'testmailid', 'local_newsletter');
        $form->addRule('testmailid', get_string('testmailidcannotempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);
        
        $form->addElement('text', 'newslettermailsubject', get_string('newslettermailsubject', 'local_newsletter'));
        $form->setType('newslettermailsubject', PARAM_RAW);
        $form->addRule('newslettermailsubject', get_string('newslettermailsubjectcannotempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);
        
        $form->addElement('text', 'senderemail', get_string('senderemail', 'local_newsletter'));
        $form->setType('senderemail', PARAM_EMAIL);


        $form->addElement('textarea', 'optionalcontent', get_string('optionalcontent', 'local_newsletter'), 'wrap="virtual" rows="3" cols="50"');
        $form->setType('optionalcontent', PARAM_RAW);
        
        
        $form->addElement('hidden', 'id');
        $form->setType('id', PARAM_RAW);

        $this->add_action_buttons();
    }

}

class add_domain_form extends moodleform {

    private $domainId;

    public function __construct($domainId = null) {
        parent::__construct();
        $this->domainId = $domainId;
    }

    public function definition() {
        
        $form = $this->_form;
        
        $form->addElement('text', 'name', get_string('domain_name', 'local_newsletter'));
        $form->setType('name', PARAM_RAW);
        $form->addRule('name', get_string('domaincannotempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);

        $form->addElement('textarea', 'description', get_string('description', 'local_newsletter'), 'wrap="virtual" rows="3" cols="50"');
        $form->setType('description', PARAM_RAW);
        $form->addRule('description', get_string('domaindesccannotempty', 'local_newsletter'), 'required', 'extraruledata', 'server', false, false);

        $form->addElement('hidden', 'id', $this->domainId);
        $form->setType('id', PARAM_RAW);
        $this->add_action_buttons();
    }

}

class delete_form extends moodleform {

    public function definition() {
        $form = $this->_form;
        $form->addElement('hidden', 'id', '');
        $form->setType('id', PARAM_RAW);
        $this->add_action_buttons();
    }
}

/**
 * Send newsletter link to entire moodle users.
 * 
 * @param type $link
 * @return type
 */
function sendlink($link) {
   global $DB;
   $response = array('status' => false);
   try {
       if(!$DB->get_records_sql("select * from {newsletter_senditems} where " . $DB->sql_compare_text('mailtype') . " = ? and newsletterid = ?",['textformat', $link])) {             if($DB->insert_record('newsletter_senditems', (object)array('newsletterid'=>$link, 'mailtype' => 'textformat', 'timecreated'=>time()))) {
                    $response['message'] = 'Links will send when cron start';
                    $response['status'] = true;
                } else {
                    $response['message'] = 'Something went worng please contect site administrator.';
                } 
        } else {
            $response['message'] = 'You have already sent this newsletter';
        }
   } catch (\Exception $ex) {
     $response['message'] = $ex->getTraceAsString();
   }
   return json_encode($response);
}
function sendmail($link) {
   global $DB;
   $response = array('status' => false);
   try {
        if(!$DB->get_records_sql("select * from {newsletter_senditems} where " . $DB->sql_compare_text('mailtype') . " = ? and newsletterid = ?",['htmlformat', $link])) {
             if($DB->insert_record('newsletter_senditems', (object)array('newsletterid'=>$link, 'mailtype' =>'htmlformat', 'timecreated'=>time()))) {
                    $response['message'] = 'Mail will send when cron start';
                    $response['status'] = true;
                } else {
                    $response['message'] = 'Something went worng please contect site administrator.';
                } 
        } else {
            $response['message'] = 'You have already sent this newsletter';
        }
   } catch (\Exception $ex) {
     $response['message'] = $ex->getTraceAsString();
   }
   return json_encode($response);
}
/**
 * 
 * @global type $DB
 * @param type $link
 * @return type
 */
function resendlink($link) {
   global $DB;
   $response = array('status' => false);
   try {
        if($DB->insert_record('newsletter_senditems', (object)array('newsletterid'=>$link, 'mailtype' => 'textformat', 'timecreated'=>time()))) {
               $response['message'] = 'Links will send when cron start';
               $response['status'] = true;
           } else {
               $response['message'] = 'Something went worng please contect site administrator.';
           }

   } catch (\Exception $ex) {
     $response['message'] = $ex->getTraceAsString();
   }
   return json_encode($response);
}

function resendmail($link) {
   global $DB;
   $response = array('status' => false);
   try {
        if($DB->insert_record('newsletter_senditems', (object)array('newsletterid'=>$link, 'mailtype' =>'htmlformat', 'timecreated'=>time()))) {
               $response['message'] = 'Mail will send when cron start';
               $response['status'] = true;
           } else {
               $response['message'] = 'Something went worng please contect site administrator.';
           }
   } catch (\Exception $ex) {
     $response['message'] = $ex->getTraceAsString();
   }
   return json_encode($response);
}

function newsletter_html($newsletterid) {
    global $DB, $CFG, $OUTPUT, $SITE, $PAGE;
    $context = context_system::instance();
    $PAGE->set_context($context);
    $row = $DB->get_record('newsletter', array('id' => $newsletterid));
    $user = $DB->get_record('user', array('id' => $row->lastmodiferdid), 'firstname,lastname');
    $logourl = $logourl = $OUTPUT->pix_url('logo', 'theme');
    $htmlmail = '<html xmlns="http://www.w3.org/1999/xhtml"><head>
                                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
                                    <meta name="format-detection" content="telephone=no"></meta>
                                    <title>Eduopen : Newsletter</title>
                                    <style type="text/css">
                                        html { background-color:#E1E1E1; margin:0; padding:0; }
                                        body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
                                        table{border-collapse:collapse;}
                                        table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
                                        img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
                                        a {text-decoration:none !important;border-bottom: 1px solid;}
                                        h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
                                        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
                                        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
                                        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
                                        #outlook a{padding:0;}
                                        img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;}
                                        body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} 
                                        .ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;}
                                        h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
                                        h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
                                        h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
                                        h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
                                        .flexibleImage{height:auto;}
                                        .linkRemoveBorder{border-bottom:0 !important;}
                                        table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
                                        body, #bodyTable{background-color:#E1E1E1;}
                                        #emailHeader{background-color:#E1E1E1;}
                                        #emailBody{background-color:#FFFFFF;}
                                        #emailFooter{background-color:#E1E1E1;}
                                        .nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
                                        .emailButton{background-color:#205478; border-collapse:separate;}
                                        .buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
                                        .buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
                                        .emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
                                        .emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
                                        .emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
                                        .imageContentText {margin-top: 10px;line-height:0;}
                                        .imageContentText a {line-height:0;}
                                        #invisibleIntroduction {display:none !important;} 
                                        span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;}
                                        span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
                                        span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
                                        .a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
                                        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
                                        @media only screen and (max-width: 480px){
                                            body{width:100% !important; min-width:100% !important;}            
                                            table[id="emailHeader"],
                                            table[id="emailBody"],
                                            table[id="emailFooter"],
                                            table[class="flexibleContainer"],
                                            td[class="flexibleContainerCell"] {width:100% !important;}
                                            td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
                                            td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
                                            img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
                                            img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
                                            table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}            
                                            table[class="emailButton"]{width:100% !important;}
                                            td[class="buttonContent"]{padding:0 !important;}
                                            td[class="buttonContent"] a{padding:15px !important;}
                                        }
                                        @media only screen and (-webkit-device-pixel-ratio:.75){
                                            /* Put CSS for low density (ldpi) Android layouts in here */
                                        }
                                        @media only screen and (-webkit-device-pixel-ratio:1){
                                            /* Put CSS for medium density (mdpi) Android layouts in here */
                                        }
                                        @media only screen and (-webkit-device-pixel-ratio:1.5){
                                            /* Put CSS for high density (hdpi) Android layouts in here */
                                        }

                                        @media only screen and (min-device-width : 320px) and (max-device-width:568px) {
                                        }		
                                    </style>
                                </head>
                                    <body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

                                        <center style="background-color:#E1E1E1">
                                            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
                                                <tbody><tr>
                                                        <td align="center" valign="top" id="bodyCell">

                                                            <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                                                                                                <tbody><tr>
                                                                                                        <td valign="top" width="500" class="flexibleContainerCell">

                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td align="left" class="textContent">
                                                                                                                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                                                                                The introduction of your message preview goes here. Try to make it short.
                                                                                                                                            </div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                        <td align="right" valign="middle" class="flexibleContainerBox">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td align="left" class="textContent">
                                                                                                                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                                                                                If you can\'t see this message, ' . \html_writer::link(new \moodle_url($CFG->wwwroot . '/local/newsletter/', array('id' => $newsletterid)), 'view it in your browser', array('target' => '_blank', 'style' => 'text-decoration:none;border-bottom:1px solid #828282;color:#828282;')) . '.
                                                                                                                                            </div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody></table>

                                                            <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="800" id="emailBody">

                                                                <tbody>
                                                                    <tr mc:hideable="">
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="left" valign="top">
                                                                                            <div style="margin: 5px" class="imgpop">
                                                                                                <a target="_blank" href="#">
                                                                                                <img src="' . $logourl . '" alt="' . $SITE->fullname . '" style="display:block; border:none; outline:none; text-decoration:none;" border="0" height="50" width="184">
                                                                                                </a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>									
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="top" width="800" class="flexibleContainerCell">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td align="left" valign="top">
                                                                                                                            <table>
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td valign="top" class="textContent">
                                                                                                                                            <h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">' . $row->title . '</h3>
                                                                                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">' . $row->description . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>                                                       
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table class="flexibleContainer" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                                <tbody><tr>
                                                                                                        <td class="flexibleContainerCell" align="center" valign="top" width="800">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="center" valign="top">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td class="textContent" valign="top">
                                                                                                                                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$row->techflash_lebel.'</h3>
                                                                                                                                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">' . $row->techflash . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>                                                        
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table class="flexibleContainer" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                                <tbody><tr>
                                                                                                        <td class="flexibleContainerCell" align="center" valign="top" width="800">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="center" valign="top">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td class="textContent" valign="top">
                                                                                                                                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$row->sectorbuzz_lebel.'</h3>
                                                                                                                                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">' . $row->sectorbuzz . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>                                                        
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table class="flexibleContainer" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                                <tbody><tr>
                                                                                                        <td class="flexibleContainerCell" align="center" valign="top" width="800">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="center" valign="top">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td class="textContent" valign="top">
                                                                                                                                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$row->biznews_lebel.'</h3>
                                                                                                                                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">' . $row->biznews . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>                                                        
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table class="flexibleContainer" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                                <tbody><tr>
                                                                                                        <td class="flexibleContainerCell" align="center" valign="top" width="800">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="center" valign="top">

                                                                                                                            <!-- CONTENT TABLE // -->
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td class="textContent" valign="top">
                                                                                                                                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$row->sectornews_lebel.'</h3>
                                                                                                                                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">' . $row->sectornews . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>                                                        
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table class="flexibleContainer" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                                <tbody><tr>
                                                                                                        <td class="flexibleContainerCell" align="center" valign="top" width="800">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="center" valign="top">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td class="textContent" valign="top">
                                                                                                                                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$row->brainstorm_lebel.'</h3>
                                                                                                                                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">' . $row->brainstorm . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>                                                        
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table class="flexibleContainer" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                                <tbody><tr>
                                                                                                        <td class="flexibleContainerCell" align="center" valign="top" width="800">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td align="center" valign="top">
                                                                                                                           <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tbody><tr>
                                                                                                                                        <td class="textContent" valign="top">
                                                                                                                                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$row->watchout_lebel.'</h3>
                                                                                                                                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">' . $row->watchout . '</div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody></table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>                                                    
                                                                </tbody></table>
                                                            <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
                                                                <tbody><tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody><tr>
                                                                                        <td align="center" valign="top">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                                                <tbody><tr>
                                                                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                                                                <tbody><tr>
                                                                                                                        <td valign="top" bgcolor="#E1E1E1">

                                                                                                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                                                                <div>Copyright 2015 <a href="' . $CFG->wwwroot . '" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Eduopen</span></a>. All&nbsp;rights&nbsp;reserved.</div>
                                                                                                                                <div>You receive this newsletter since you are a registered user to <a href="<?php  echo $CFG->wwwroot ?>" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Eduopen</span></a>.</div>
                                                                                                                            </div>

                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                        </center>	
                                    </body>
                                </html>';
    return $htmlmail;
}
/**
 * $newsletterid
 * send out test mail to testmail user
 * 
 * 
 */
function sendtestmail($newsletterid){
    
    global $DB, $CFG;
    include_once("{$CFG->libdir}/accesslib.php");
   $response = array('status' => false);
   try {
       $htmlmail = newsletter_html($newsletterid);
       $newsletter = $DB->get_record('newsletter', array('id'=>$newsletterid),'newslettermailsubject,testmailid,senderemail');
       if(isset($newsletter->newslettermailsubject) and !empty($newsletter->newslettermailsubject)) {
           $subject = $newsletter->newslettermailsubject;
       } else {
           $subject = 'Test mail for newsletter';
       }
       $testmailidexolode = explode(',', $newsletter->testmailid);
       if($newsletter->senderemail) {
                      $senderuser = $DB->get_record_sql("SELECT * FROM {user} WHERE email LIKE '%$newsletter->senderemail%'");
            } else {
                    $senderuser =  \core_user::get_support_user();
            }
       foreach( $testmailidexolode as $testmailid) {
       $user = $DB->get_record_sql("SELECT * FROM {user} WHERE email LIKE '%$testmailid%'");
       
       if($user) {
       if(email_to_user($user, $senderuser, $subject, '', $htmlmail)){
            $response['message'] = 'Mail sent successfully.Please check your mailbox';
            $response['status'] = true;
       } else {
            $response['message'] = 'Something went worng please contact site administrator.';
       }
   } else {
    $response['message'] = 'Something went worng.please use correct mail address in reffernce to eduopen system.';
    return json_encode($response);
   }
}
   } catch (\Exception $ex) {
     $response['message'] = $ex->getTraceAsString();
   }
   return json_encode($response);
}