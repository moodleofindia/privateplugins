<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('newsletter', 'local_newsletter'));
$PAGE->set_heading(get_string('newsletter', 'local_newsletter'));
$PAGE->requires->js('/local/newsletter/scripts.js');
$PAGE->set_url($CFG->wwwroot . '/local/newsletter/domains.php');
echo $OUTPUT->header();
echo html_writer::start_div('row');
echo html_writer::start_div('span12');
echo html_writer::tag('p', get_string('newsletterdomain', 'local_newsletter'), array('class' => 'lead bottomline'));
$record = $DB->get_records('newsletter_category');
if ($record != null) {
    echo html_writer::tag('p', get_string('collapseheading','local_newsletter'));
    echo html_writer::tag('div','', array('id' => "ajax-response"));
    echo html_writer::start_div('accordion accordion-newsletter');
    $count = 1;
    $datetime = new DateTime('first day of this month');
    $monthstart = $datetime->getTimestamp();
    $currenttime = time();
    foreach ($record as $key => $row) {
        echo '<section  class="panel faqs"><header class="panel-heading txtoptm" >
                        '.$row->name.'<span  class="tools pull-right pad10b">
                    <a  href="javascript:;" class="fa fa-chevron-up"></a>
                        </span>
                </header>
                <div style="display: none;" class="panel-body">';
        echo html_writer::tag('p', get_string('current_letter', 'local_newsletter'), array('class' => 'lead bottomline'));
        $currentmonth = $DB->get_records_sql('SELECT * FROM {newsletter} WHERE categoryid = ? and createdtime < ? and createdtime > ? and status =1 order by id desc ', array($row->id, $currenttime, $monthstart));
        if ($currentmonth != null) {
            $table = new html_table();
            $table->head = (array) get_strings(['title', 'description', 'course', 'status', 'date', 'action'], 'local_newsletter');
            foreach ($currentmonth as $key => $current) {
                $fullname ='';
                $courseids = explode(',', $current->courseid);
       
            foreach($courseids as $courseid){
                if($courseid != 1){
                    $cname = $DB->get_record('course', array('id' => $courseid),'fullname')->fullname;
                $fullname .= $cname.',';
            } else {
                 $fullname .= 'All User, ';
            }
         
            }
            $fullname = rtrim( $fullname,',');
                if (is_siteadmin()) {
                    $table->data[] = array(
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/news_letter.php', ['views' => $current->id]), $current->title),
                        substr($current->description, 0, 45) . '....',
                        //$DB->get_record('course', array('id' => $current->courseid),'fullname')->fullname,
                        $fullname,
                        $current->status == 1 ? '<span class="label label-success">active' . '</span>' : '<span class="label lablel-warning">deactive</span>',
                        userdate($current->createdtime, '%d-%m-%Y'),
                        html_writer::tag('button', ' Send Link ', array('class' =>'label mail-btn', 'onclick'=>'return sendlink('.$current->id.')')).' '.
                        html_writer::tag('button', ' Send Mail ', array('class' =>'label mail-btn', 'onclick'=>'return sendmail('.$current->id.')')),
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/edit_news_letter.php', ['id' => $current->id]), 'Edit', array('class' => 'label label-warning')) . ' ' .
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/delete_news_letter.php', ['id' => $current->id]), 'Delete', array('class' => 'label label-error'))
                    );
                } else {
                    $table->data[] = array(
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/news_letter.php', ['views' => $current->id]), $current->title),
                        substr($current->description, 0, 45) . '....',
                        //$DB->get_record('course', array('id' => $current->courseid),'fullname')->fullname,
                        $fullname,
                        $current->status == 1 ? '<span class="label label-success">active' . '</span>' : '<span class="label lablel-warning">deactive</span>',
                        userdate($current->createdtime, '%d-%m-%Y'),
                        '<span class="icon icon-lock"></span>'
                    );
                }
            }
            echo html_writer::table($table);
        } else {
            echo html_writer::div(get_string("no_news_current_month", "local_newsletter"), 'alert alert-warning');
        }
        echo html_writer::tag('p', get_string('earlier_letter', 'local_newsletter'), array('class' => 'lead bottomline'));
        $earliermonth = $DB->get_records_sql('SELECT * FROM {newsletter} WHERE categoryid = ? and createdtime < ? and status =1 order by id desc', array($row->id, $monthstart));
        if ($earliermonth != null) {
            $table = new html_table();
            $table->head = (array) get_strings(['title', 'description','course', 'status', 'date', 'action'], 'local_newsletter');
            foreach ($earliermonth as $key => $earlier) {
                $fullname ='';
                $courseids = explode(',', $earlier->courseid);
       
            foreach($courseids as $courseid){
                if($courseid != 1){
                    $cname = $DB->get_record('course', array('id' => $courseid),'fullname')->fullname;
                $fullname .= $cname.',';
            } else {
                 $fullname .= 'All User, ';
            }
         
            }
            $fullname = rtrim( $fullname,',');
                if (is_siteadmin()) {
                    $table->data[] = array(
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/news_letter.php', ['views' => $earlier->id]), $earlier->title),
                        substr($earlier->description, 0, 45) . '....',
                        //$DB->get_record('course', array('id' => $current->courseid),'fullname')->fullname,
                        $fullname,
                        $earlier->status == 1 ? '<span class="label label-success">active' . '</span>' : '<span class="label lablel-warning">deactive</span>',
                        userdate($earlier->createdtime, '%d-%m-%Y'),
                        html_writer::tag('button', ' Send Link ', array('class' =>'label mail-btn', 'onclick'=>'return sendlink('.$current->id.')')).' '.
                        html_writer::tag('button', ' Send Mail ', array('class' =>'label mail-btn', 'onclick'=>'return sendmail('.$current->id.')')),
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/edit_news_letter.php', ['id' => $earlier->id]), 'Edit', array('class' => 'label label-warning')) . ' ' .
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/delete_news_letter.php', ['id' => $earlier->id]), 'Delete', array('class' => 'label label-error'))
                    );
                } else {
                    $table->data[] = array(
                        html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/news_letter.php', ['views' => $earlier->id]), $earlier->title),
                        substr($earlier->description, 0, 45) . '....',
                        //$DB->get_record('course', array('id' => $current->courseid),'fullname')->fullname,
                        $fullname,
                        $earlier->status == 1 ? '<span class="label label-success">active' . '</span>' : '<span class="label lablel-warning">deactive</span>',
                        userdate($earlier->createdtime, '%d-%m-%Y'),
                        '<span class="icon icon-lock"></span>'
                    );
                }
            }
            echo html_writer::table($table);
        } else {
            echo html_writer::div(get_string("no_news_earlier_month", "local_newsletter"), 'alert alert-warning');
        }
        echo'</div></section>';
        $count++;
    }
    echo html_writer::end_div();
} else {
    echo html_writer::div(get_string("no_record_found", "local_newsletter"), 'alert alert-warning');
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    var config = {
            baseurl : '<?php echo $CFG->wwwroot;?>',
            ajaxpath : '<?php echo $CFG->wwwroot.'/local/newsletter/ajax_calls.php'?>',
            sendmail : '<?php echo $CFG->wwwroot.'/local/newsletter/ajax_calls.php?action=sendmail'?>'
        };
</script>