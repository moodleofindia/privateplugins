<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
require_once('locallib.php');
$domainid = optional_param('id', false, PARAM_INT);
$general = $DB->get_record('newsletter_category', array('id' => $domainid));
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('eduopen_institution');
$PAGE->set_title(get_string('domain_title', 'local_newsletter'));
$PAGE->set_heading(get_string('domain_heading', 'local_newsletter'));
$PAGE->set_url(new moodle_url($CFG->wwwroot.'/local/newsletter/add_domain.php'));
if(!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
$message = false;
$mform = new add_domain_form($domainid);
$mform->set_data($general);
if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot.'/local/newsletter/view_domain.php');
} else if ($data = $mform->get_data()) {
    $record = new stdClass();
    $record->id = $data->id;
    $record->name = $data->name;
    $record->description = $data->description;
    if ($DB->update_record('newsletter_category', $data)) {
        $message = html_writer::div(get_string("domainedited", "local_newsletter"), 'alert alert-success');
        $mform = null;
    }
}
echo $OUTPUT->header();
echo html_writer::start_div('row-fluid');
echo html_writer::start_div('col-md-12');
if($message) {
    echo $message;
}
if ($mform != null) {
    echo html_writer::tag('p', get_string('edit_domain_desc', 'local_newsletter'), array('class' => 'lead bottomline'));
    $mform->display();
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();