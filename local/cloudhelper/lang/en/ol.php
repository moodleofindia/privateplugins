<?php
$string = [];
$string['pluginname'] = 'Cloud All Langs';
$string['ttick'] = 'Ticket';
$string['taddticket'] = 'Add Ticket';
$string['tticketcreated'] = 'Ticket created successfully';
$string['tticketprmb'] = 'Something problem ticket creation try later';
$string['teditticket'] = 'Edit Ticket';
$string['tmodiftk'] = 'Modify Ticket';
$string['traisedus'] = 'Raised by User';
$string['tcategory'] = 'Category';
$string['tstatus'] = 'Status';
$string['tpriority'] = 'Priority';
$string['tdatecreation'] = 'Date of Creation';
$string['tduedate'] = 'Due Date';
$string['tticketname'] = 'Ticket Name';
$string['tticketdescription'] = 'Description';
$string['tviewticket'] = 'View Ticket';
$string['tsubject'] = 'Subject';
$string['tedit'] = 'Edit';
$string['tdelete'] = 'Delete';
$string['treopen'] = 'Reopen';
$string['tmytickets'] = 'My Tickets';
$string['tdeleteticket'] = 'Delete ticket';
$string['teopenticket'] = 'Reopen ticket';
$string['tyouhavenoticket'] = 'You have no ticket to show';
$string['tyouarenotcreaticketyet'] = 'you are not created  ticket yet';
$string['tconversationhistory'] = 'Conversation History';
$string['tupdateresponse'] = 'Update Response';
$string['tname'] = 'Name';
$string['tsiteurl'] = 'Site Url';
$string['tsname'] = 'Site Name';
$string['tselectsite'] = 'Select your Site';
$string['tuploadfile'] = 'Upload File';
$string['tcreateticket'] = 'Create Ticket';
$string['tupticket'] = 'Update Ticket';
$string['teditprf'] = 'Edit Profile';
$string['tresponseticket'] = 'Response Ticket';
$string['tresponce'] = 'Response';
$string['tchpassword'] = 'Change Password';
$string['dcourses'] = 'Download Courses';
$string['rcourses'] = 'Restore Courses';
$string['pcourses'] = 'Premium Courses';
$string['youhavenoinst'] = 'You have no Instance';
$string['youhavenod'] = 'You have not downloaded any courses';
$string['youhavenor'] = 'You have not restored any courses';
$string['youhavenop'] = 'You do not have any premium courses';
$string['youhaveupgrade'] = 'You are now using';
$string['belowpack'] = 'For upgrading your package click below link:-';
$string['uppakage'] = 'Upgrade Package';
$string['biladress'] = 'My Billing Address';
$string['topdescrip'] = 'From your account dashboard you can view your recent orders, manage your shipping and billing addresses and edit your password and account details.';
$string['sactive'] = 'Active';
$string['sinactive'] = 'In Active';
$string['editadress'] = 'Edit Address';
$string['courserestore'] = 'Course restore';


$string['myrequestservices'] = 'My Requested Services';
$string['viewrequestservices'] = 'View Requested Services';
$string['noservicerequestfound'] = 'You have no service request to show';
$string['norequestervice'] = 'You have not requested any service yet';
$string['firstname'] = 'Firstname';
$string['lastname'] = 'Lastname';
$string['email'] = 'Email';
$string['companyname'] = 'Companyname';
$string['companywebsite'] = 'Companywebsite';
$string['requestedservices'] = 'Requested Services';
$string['about'] = 'About';
$string['action'] = 'Action';
$string['requesttime'] = 'Request time';
$string['enrolme'] = 'Enrol';

$string['selectrestorecourse'] = 'You are restoring course for <strong>{$a}</strong>';
$string['restorecoursedesc'] = 'Please select the respective course and click the button to start restore.';
$string['addnewinstance'] = 'Add New Moodle Instance';
$string['domainname'] = 'Domain name';
$string['defaultdomain'] = 'onlinemooc.co.in';
$string['urldesc'] = 'Give your domain like "myportal"';
$string['createsitebtn'] = 'Create your new site';

$string['name'] = 'Name';
$string['writemessage'] = 'Write message here ...';
$string['companywebsite'] = 'Company website';
$string['companyname'] = 'Company name';

$string['youhaveupgrade'] = 'You are now using';
$string['belowpack'] ='For upgrading your package click below link:-';
$string['uppakage'] = 'Upgrade Package';
$string['biladress'] = 'My Billing Address';
$string['topdescrip'] = 'Welcome to your dashboard. You can manage your apps, courses, subscriptions from this page.';
$string['sactive'] = 'Active';
$string['sinactive'] = 'In Active';
$string['editadress'] = 'Edit Address';


$string['choosecourse'] = 'Choose you site to transfer course';
$string['restoreseletedsite'] = 'You are restoring course for <strong>{$a}</strong>';
$string['restorepermission'] = 'You don\'t have any moodle site please contact site administrator';
$string['sl'] = '#';
$string['name'] = 'Name';
$string['compatibleversion'] = 'Compatible version';
$string['type'] = 'Type';
$string['restore'] = 'Restore';
$string['download'] ='Download';
$string['description'] ='Download';
$string['skipcourse'] =' Skip course if already exists';
$string['download'] ='Download';
$string['restored'] ='Restored';
$string['free'] ='Free';
$string['premium'] ='Premium';
$string['alreadydownload'] ='Already downloaded';
$string['addtocart'] ='Add to cart';
$string['courserestored'] = 'Course restored successfully!!';
$string['courserestorederror'] = 'Course restored problem';


$string['courserestoredemy'] = 'My Restored Courses';
$string['addnewinstance'] = 'Add New Moodle Instance';
$string['domainname'] = 'Domain name';
$string['defaultdomain'] = 'onlinemooc.co.in';
$string['urldesc'] = 'Give your domain like "myportal"';
$string['createsitebtn'] = 'Create your new site';
$string['createsite'] = 'Create site';

$string['createservicerequest'] = 'Create Service Request';
$string['servicerequested'] = 'You have successfully submitted your service request';




$string['requestservices'] = 'Request services';
$string['dashboard'] = 'Dashboard';

// haraprasad added new string 12july
$string['ydonthavescp'] = 'You do not have any subscription.';
$string['ydonthavepremium'] = 'You do not have any premium courses.';
$string['contactus'] = 'Contact Us';
$string['ttick'] = 'Tickets';
$string['taddticket'] = 'Add Ticket';
$string['tticketcreated'] = 'Ticket created successfully';
$string['tticketprmb'] = 'Something problem ticket creation try later';
$string['teditticket'] = 'Edit Ticket';
$string['tmodiftk'] = 'Modify Ticket';
$string['traisedus'] = 'Raised by User';
$string['tcategory'] = 'Category';
$string['tstatus'] = 'Status';
$string['tpriority'] = 'Priority';
$string['tdatecreation'] = 'Date of Creation';
$string['tduedate'] = 'Due Date';
$string['tticketname'] = 'Ticket Name';
$string['tticketdescription'] = 'Description';
$string['tviewticket'] = 'View Ticket';
$string['tsubject'] = 'Subject';
$string['tedit'] = 'Edit';
$string['tdelete'] = 'Delete';
$string['treopen'] = 'Reopen';
$string['tmytickets'] = 'My Tickets';
$string['tdeleteticket'] = 'Delete ticket';
$string['teopenticket'] = 'Reopen ticket';
$string['tyouhavenoticket'] = 'You have no ticket to show';
$string['tyouarenotcreaticketyet'] = 'you have not created ticket yet';
$string['tconversationhistory'] = 'Conversation History';
$string['tupdateresponse'] = 'Update Response';
$string['tname'] = 'Name';
$string['tsiteurl'] = 'Site Url';
$string['tsname'] = 'Site Name';
$string['tselectsite'] = 'Select your Site';
$string['tuploadfile'] = 'Upload File';
$string['tcreateticket'] = 'Create Ticket';
$string['tupticket'] = 'Update Ticket';
$string['teditprf'] = 'Edit Profile';
$string['tchpassword'] = 'Change Password';
$string['dcourses'] = 'Download Courses';
$string['rcourses'] = 'Restore Courses';

$string['name'] = 'Name';
$string['writemessage'] = 'Write message here ...';
$string['companywebsite'] = 'Company website';
$string['companyname'] = 'Company name';
$string['email'] = 'Email';
$string['createnewrequest'] = 'Create new request';
$string['requesthistory'] = 'Request service history';


// haraprasad added new string 12july
$string['ydonthavescp'] = 'You do not have any subscription.';
$string['ydonthavepremium'] = 'You do not have any premium courses.';
$string['contactus'] = 'Contact Us';


$string['tenthdayreminder'] = 'Hope you are enjoying your lms platform, Do you need any help, let us know';
$string['twentiethdayreminder'] = 'Reminder your site will be available for next 10 days only. please use below link to upgrade';
$string['lastreminder'] = 'Last reminder, On the day of {$a} your site will be disabled';
$string['preview'] = 'Preview';

$sth = '';
foreach($string as $key =>$str)
  echo '$string[\''.$key.'\'] = \''.$str.'\';<br/>';

    $fil = fopen('ar.php','+w');
   fwrite($fil, $sth);
    fclose($fil);

