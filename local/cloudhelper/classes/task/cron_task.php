<?php
namespace local_cloudhelper\task;

class cron_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('pluginname', 'local_cloudhelper');
    }

    /**
     * Run update_status cron.
     */
    public function execute() {
        global $CFG, $DB, $MULTI;
		$sql = 'select * from {multitenant_master} t  where '.
		't.start_date >= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and '.
		't.start_date <= DATE_SUB(NOW(), INTERVAL 1 MONTH)';
		$maillist = array();
		$tenants = $DB->get_records_sql($sql);
		$mailusers = array();
        mtrace('cloudhelper started');
        /*
         |----------------------
         | Upgrade Site mails
         |----------------------
         */
		$supportuser = \core_user::get_support_user();
		foreach($tenants as $tenant) {
			// find out free courses enrolled for this user.
			$course = $DB->get_record('course', array('shortname' => 'free'), 'id');
			$context = \context_course::instance($course->id);
			if(is_enrolled($context, $tenant->userid)) {

                if(!isset($mailusers[$tenant->id])) {
                    $mailusers[$tenant->userid]  = $tenant->userid;
                } 
				// First reminder
				$curdate = date('j', time());
				if(date('j', strtotime('+10 day', $tenant->start_date)) == $curdate) {
					$maillist['firstreminder'][] = array(
				        'userid' => $tenant->userid,
						'start_date' => userdate($tenant->start_date),
						'sitename' => $tenant->name,
						'siteurl' => $tenant->site_url
					);
				} else if(date('j', strtotime('+20 day', $tenant->start_date)) == $curdate) {
					$maillist['secondreminder'][] = array(
					    'userid' => $tenant->userid,
						'start_date' => userdate($tenant->start_date),
						'sitename' => $tenant->name,
						'siteurl' => $tenant->site_url
					);
				} else if(date('j', strtotime('+28 day', $tenant->start_date)) == $curdate) {
					$maillist['lastreminder'][] = array(
						'userid' => $tenant->userid,
						'start_date' => userdate($tenant->start_date),
						'end_date' => userdate($tenant->end_date),
						'sitename' => $tenant->name,
						'siteurl' => $tenant->site_url
				    );
				}
			}		
		}
        $count = 0;
		if(count($mailusers) > 0) {
			list($where, $params) = $DB->get_in_or_equal($mailusers);
			$users = $DB->get_records_select('user', "id $where", $params);
			$supportuser = \core_user::get_support_user();
			$messagehtml = 'test mail';
			if(isset($maillist['firstreminder'])) {
				foreach($maillist['firstreminder'] as $list) {
					$o = '<html><body>';
					$o .= '<p> Dear '.$users[($list['userid'])]->firstname.'</p>'.'<br>';
					$o .= '<p> '.get_string('tenthdayreminder', 'local_cloudhelper').'</p>'.'<br>';
					$o .= 'Regards,<br>';
					$o .= $supportuser->firstname.'<br>';
					$o .= $supportuser->email.'<br>';
					$o .= '</body></html>';
					email_to_user($users[($list['userid'])], $supportuser, 'Update for your online learning platform',html_to_text($o),'', '','',true, $supportuser->email);
                    $count++;
				}
			}
			
			if(isset($maillist['secondreminder'])) {
				foreach($maillist['secondreminder'] as $list) {
					$o = '<html><body>';
					$o .= '<p> Dear '.$users[($list['userid'])]->firstname.'</p>'.'<br>';
					$o .= '<p> '.get_string('twentiethdayreminder', 'local_cloudhelper', $list).'</p>'.'<br>';
					$o .= 'Regards,<br>';
					$o .= $supportuser->firstname.'<br>';
					$o .= $supportuser->email.'<br>';
					$o .= '</body></html>';
					email_to_user($users[($list['userid'])], $supportuser, 'Update for your online learning platform',html_to_text($o),'', '','',true, $supportuser->email);
                    $count++;
				}
			}
			
			if(isset($maillist['lastreminder'])) {
				foreach($maillist['lastreminder'] as $list) {
					$o = '<html><body>';
					$o .= '<p> Dear '.$users[($list['userid'])]->firstname.'</p>'.'<br>';
					$o .= '<p> '.get_string('lastreminder', 'local_cloudhelper', $list['end_date']).'</p>'.'<br>'; 
					$o .= 'Regards,<br>';
					$o .= $supportuser->firstname.'<br>';
					$o .= $supportuser->email.'<br>';
					$o .= '</body></html>';
					email_to_user($users[($list['userid'])], $supportuser, 'Update for your online learning platform',html_to_text($o),'', '','',true, $supportuser->email);
                    $count++;
				}
			}
		}
        mtrace('Cloudhelper : Total ' .$count.' mail send.');
        /*
         |-------------------------
         | Disable site if experied
         |-------------------------
         */
        $sql = 'select * from {multitenant_master} t  where '.
		't.start_date >= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) and '.
		't.start_date <= DATE_SUB(NOW(), INTERVAL 1 MONTH)';
		$tenants = $DB->get_records_sql($sql);
        $curdate = date('j', time());
		foreach($tenants as $tenant) {
            if(date('j', strtotime('+30 day', $tenant->start_date)) == $curdate) {
			    // find out free courses enrolled for this user.
			    $course = $DB->get_record('course', array('shortname' => 'free'), 'id');
		    	$context = \context_course::instance($course->id);
			    if(is_enrolled($context, $tenant->userid)) {
                    $disabled = $MULTI->confdir.$tenant->sub_domain.'/disabled';
                    $handle = fopen($disabled, 'w');
                    fclose($handle);
                    mtrace('Cloudhelper Disabled site :'.$tenant->sub_domain);
                }
            }
        }
		
        // Update ticket status
		$this->ticket_update_status();
    }
    
    function ticket_update_status() {
		global $DB, $CFG;
		$status = $DB->get_record('multitenant_ticket_status', array('name' => 'Completed'));
		$statusall = $DB->get_records('multitenant_ticket', array('ticketstatus' => $status->id));
		foreach ($statusall as  $ticket) {
			if(strtotime('+1 day', $ticket->timemodified) > time()) {
				$insert['id'] = $ticket->id;
				$insert['title'] = $ticket->title;
				$insert['querytypeid'] = $ticket->querytypeid;
				$insert['ticketdescription'] = $ticket->ticketdescription;
				$insert['uploadfile'] =$ticket->uploadfile;
				$insert['priority'] = $ticket->priority;
				$insert['raiseticketid'] = $ticket->raiseticketid;
				$insert['userid'] = $ticket->userid;
				$insert['ticketstatus'] = 4;
				$insert['duedate'] = $ticket->duedate;
				//$insert['timecreated'] = time();
				$insert['timemodified'] = time();
				$updatedata = $DB->update_record('multitenant_ticket', $insert);
				if($updatedata){
					$supportuser = \core_user::get_support_user();
					$sql = 'SELECT mt.id,mt.userid,mt.title,mt.querytypeid,mt.ticketdescription,mt.raiseticketid,mt.priority,mt.ticketstatus, '.
                    'mt.duedate,mt.timecreated,mtc.name as categoryname,mts.name as statusname from {multitenant_ticket} '.
                    'mt join {multitenant_ticket_category} mtc on mtc.id = mt.querytypeid '.
                    'join {multitenant_ticket_status} mts on mts.id = mt.ticketstatus Where mt.id = "'.$ticket->id.'"';
                    $tickst = $DB->get_record_sql($sql);
                    $data = $this->email_send_ticketupdate($supportuser,$tickst);
					$messagehtml = html_to_text($data['content']);
					email_to_user($supportuser,$supportuser, $data['subject'], $messagehtml);
					$userdt = $DB->get_record('user',array('id'=> $tickst->raiseticketid),'firstname,lastname,email');
					$data2 = email_send_ticketupdate($supportuser,$tickst,$userdt->firstname.' '.$userdt->lastname);
					$messagehtml1 = html_to_text($data2['content']);
					$supportuser->email = $userdt->email;
					$supportuser->firstname = $userdt->firstname;
					$admin = get_admin(); 
					email_to_user($supportuser, $admin, $data2['subject'], $messagehtml1);
				}
			}
		}
	}

    /**
     * Modify ticket Mail format
       *
       * @param stdClass $user
       * @param stdClass $ticketinfo
       * @param string $username
       *
        * @return array
        */
        function email_send_ticketupdate($supportuser,$tickst,$username = null)
        {
            global $CFG;
            $subject = 'Ticket id['.$tickst->id.'] - ['.$tickst->title.'] - UPD';
            $content = '<html><body>';
            if(!empty($username)){
                $content .= '<p> Dear '.$username.' ,</p>'.'<br>';
            }else{
                $content .= '<p> Dear '.$supportuser->firstname.' '.$supportuser->lastname.' ,</p>'.'<br>';
            }
             $content .= 'Greetings from '.$CFG->wwwroot.'<br>';
             $content .=  $tickst->title.' '. 'is updated <br>';
             $content .= 'Updated Details are as below <br>';
             $content .= '<hr> <br>';
             $content .= 'Ticket id  - '.$tickst->id.' <br>';
             $content .= 'Subject  - '.$tickst->title.' <br>';
             $content .= 'Status  - '.$tickst->statusname.' <br>';
             $content .= 'Duedate  - '.date("d/m/Y",$tickst->duedate).' <br>';
             $content .= 'Description  - '.$tickst->ticketdescription.' <br>';
             $content .= '<hr> <br>';
             $content .= 'You can access to this ticket this link '.$CFG->wwwroot.'/cloudservices/ticket/modify.php?ticketid='.$tickst->id.'<br>';
             $content .= '<hr> <br>';
             $content .= 'We are working the issue/request reported by you.<br>';
             $content .= 'One of our representive will contact you shortly. <br><br>';
             $content .= 'Regards,<br>';
             $content .= $supportuser->firstname.'<br>';
             $content .= $supportuser->email.'<br>';
             $content .= $supportuser->address.'<br>';
             $content .= $supportuser->url.'<br>';
             $content .= '</body></html>';
             $data = array('content'=>$content,'subject'=>$subject);
             return $data;
        }

}

