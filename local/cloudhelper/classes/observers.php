<?php 
namespace local_cloudhelper;
defined('MOODLE_INTERNAL') || die();
use stdClass;

class observers {
    
    public static function course_upgrade(\core\event\user_enrolment_created $event) {
        //error_log(var_export($event, true)); 
        global $DB, $MULTI;
        $usersites = $DB->get_records('multitenant_master', array('userid' => $event->userid));
        if($usersites) {
            $course = $DB->get_record('course', array('id' => $event->courseid));
            switch ($course->shortname) {
                case 'standard' :
                    foreach($usersites as $site) {
                        $update = new stdClass();
                        $update->id = $site->id;
                        $update->end_date = 365 * 7 * 60 +time();
                        $DB->update_record('multitenant_master', $update);
                    }
                break;
                case 'enterprise' :
                    foreach($usersites as $site) {
                        $update = new stdClass();
                        $update->id = $site->id;
                        $update->end_date = 0;
                        $DB->update_record('multitenant_master', $update);
                    }
                break;
            }
            // Remove disabled file
            foreach($usersites as $site) {
                $disabled = $MULTI->confdir.$site->sub_domain.'/disabled';
                if(file_exists($disabled)) {
                    unlink($disabled);
                }
            }
        }
    }
}
