<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private syncusertocohort functions
 *
 * @package local_syncusertocohort
 * @copyright  2015 moodle of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
$courseid = optional_param('courseid', 1, PARAM_INT);
$userid = optional_param('id', 1, PARAM_INT);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle', 'local_syncusertocohort'));
$PAGE->set_heading('Sync user to Cohort');
$PAGE->set_url($CFG->wwwroot.'/local/syncusertocohort/index.php');
echo $OUTPUT->header();
?>
<?php
require_once($CFG->dirroot . '/local/syncusertocohort/form/indexform.php');
$mform = new cohort_form();
$alladmin = get_admins();
echo"<div><strong>*All user of application will be added as a member in the selected cohort</strong></div>";
foreach($alladmin as $admin) {
    $adminarray[] = $admin->id;
}
if ($mform->is_cancelled()) {
   
} else if ($fromform = $mform->get_data()) {
    global $DB;
    $lastinsert=null;
    
    $userrec = $DB->get_records('user');
    $lastinsert = 0;
    foreach ($userrec as $record) {
        $usercheck = $DB->get_record('cohort_members', array('cohortid' => $fromform->cohort,'userid' => $record->id));
        $admincheck = in_array($record->id,$adminarray);
        if (!$usercheck && !$admincheck) {
        $record->cohortid = $fromform->cohort;
        $record->userid = $record->id;
        $record->timeadded = time();
        $lastinsert = $DB->insert_record('cohort_members', $record);
        $lastinsert += $lastinsert;
       
        }
    }
  
    if ($lastinsert) {
        echo"<div class='alert alert-success'>
               <strong>Success!</strong> All the users have been assigned to respective cohort
            </div>";
    } 
    else
        {
         echo "<div class='alert alert-danger'><strong>Error:data not inserted/data already exist</strong></div>";
    }
} else {

   
}
 $mform->display();
?>
        

<?php
echo $OUTPUT->footer();