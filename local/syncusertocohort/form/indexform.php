<?php
require_once("$CFG->libdir/formslib.php");
class cohort_form extends moodleform {
    public function definition() {
        global $CFG,$DB;
        $mform = $this->_form; 
        $cohort = $DB->get_records('cohort');
        $attribute =[];
        foreach($cohort as $key => $value) {
        $attribute[$key] = $value->name ;
        
        }
        $mform->addElement('select', 'cohort', get_string('cohortname','local_syncusertocohort'),$attribute);
        $this->add_action_buttons();
    }
    function validation($data, $files) {
        return array();
      }
    
}
?>