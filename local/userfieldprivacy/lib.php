<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    
/**
 * Note class is build for Manage Notes (Create/Update/Delete)
 * @desc Note class have one parameterized constructor to receive global 
 *       resources.
 * 
 * Private userfieldprivacy functions
 * @author    Nihar Das <nihar@elearn10.com>
 * @package    local_userfieldprivacy
 * @copyright  2016 lms of india
 * @license    http://www.lmsofindia.com GNU GPL v3 or later
*/


function local_userfieldprivacy_extend_navigation(global_navigation $navigation) {
    global $CFG;
    if (isloggedin()) {
        $node = $navigation->add(get_string('plugin', 'local_userfieldprivacy'));
        $node->add(get_string('indexpage', 'local_userfieldprivacy'), new moodle_url($CFG->wwwroot . '/local/userfieldprivacy/index.php?type=user'));
        if (is_siteadmin()) {
            $node->add(get_string('preferencepage', 'local_userfieldprivacy'), new moodle_url($CFG->wwwroot . '/local/userfieldprivacy/index.php?type=default'));
        }
    }
}
