<?php

require_once("$CFG->libdir/formslib.php");

class userfieldprivacy_form extends moodleform {

    public function definition() {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . '/user/profile/lib.php');
        $mform = $this->_form;

        $mform->addElement('header', 'formheader', 'Userfield Privacy');



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'citylabel', '', get_string('city', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'city', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'city', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('city', 1);
        $mform->setType('city', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'webpagelabel', '', get_string('webpage', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'webpage', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'webpage', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('webpage', 1);
        $mform->setType('webpage', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'icqlabel', '', get_string('icq', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'icq', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'icq', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('icq', 1);
        $mform->setType('icq', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'skypelabel', '', get_string('skype', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'skype', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'skype', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('skype', 1);
        $mform->setType('skype', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'aimlabel', '', get_string('aim', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'aim', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'aim', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('aim', 1);
        $mform->setType('aim', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'yahoolabel', '', get_string('yahoo', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'yahoo', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'yahoo', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('yahoo', 1);
        $mform->setType('yahoo', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'msnlabel', '', get_string('msn', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'msn', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'msn', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('msn', 1);
        $mform->setType('msn', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'idlabel', '', get_string('id', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'id', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'id', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('id', 1);
        $mform->setType('id', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'instlabel', '', get_string('inst', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'inst', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'inst', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('inst', 1);
        $mform->setType('inst', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'departlabel', '', get_string('depart', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'depart', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'depart', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('depart', 1);
        $mform->setType('depart', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'phonelabel', '', get_string('phone', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'phone', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'phone', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('phone', 1);
        $mform->setType('phone', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'moblabel', '', get_string('mob', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'mob', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'mob', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('mob', 1);
        $mform->setType('mob', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);



        $grouparray = array();
        $grouparray[] = & $mform->createElement('static', 'addresslabel', '', get_string('address', 'local_userfieldprivacy'), '');
        $grouparray[] = & $mform->createElement('radio', 'address', '', get_string('chk1', 'local_userfieldprivacy'), 0);
        $grouparray[] = & $mform->createElement('radio', 'address', '', get_string('chk2', 'local_userfieldprivacy'), 1);
        $mform->setDefault('address', 1);
        $mform->setType('address', PARAM_INT);
        $mform->addGroup($grouparray, '', '', array(' '), false);

        $mform->addElement('header', 'formheader', 'UserCustomProfilefield Privacy');

        $allcustomfield = profile_get_custom_fields($USER->id);
        foreach($allcustomfield as $field) {
            $grouparray = array();
            $grouparray[] = & $mform->createElement('static', $field->shortname.'label', '',ucfirst($field->shortname), '');
            $grouparray[] = & $mform->createElement('radio', $field->shortname, '', get_string('chk1', 'local_userfieldprivacy'), 0);
            $grouparray[] = & $mform->createElement('radio', $field->shortname, '', get_string('chk2', 'local_userfieldprivacy'), 1);
            $mform->setDefault($field->shortname, 1);
            $mform->setType($field->shortname, PARAM_INT);
            $mform->addGroup($grouparray, '', '', array(' '), false);
        }

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return array();
    }

}

?>