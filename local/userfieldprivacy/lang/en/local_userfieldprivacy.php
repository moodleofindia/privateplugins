<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private userfieldprivacy functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    local_userfieldprivacy
 * @copyright  2016 lms of india
 * @license    http://www.lmsofindia.com GNU GPL v3 or later
 */
/*
 * Site level 
 */
$string['pluginname'] = 'Userfield Privacy';
$string['indexpage'] = 'Userfield Privacy';
$string['pagetitle'] = 'Userfield Privacy';
$string['plugin'] = 'Userfield Privacy';
$string['pageheader'] = 'Userfield Privacy';
$string['city'] = 'City/town';
$string['cityh'] = 'City/town';
$string['cityh_help'] = 'Put City/town';
$string['chk1'] = 'Private';
$string['chk2'] = 'Public';

$string['webpage'] = 'Web page';
$string['webpageh'] = 'Web page';
$string['webpageh_help'] = 'Put web address of your page';

$string['icq'] = 'ICQ number';
$string['icqh'] = 'ICQ number';
$string['icqh_help'] = 'Put ICQ number';

$string['skype'] = 'Skype ID';
$string['skypeh'] = 'Skype ID';
$string['skypeh_help'] = 'Put Skype ID';

$string['aim'] = 'AIM ID';
$string['aimh'] = 'AIM ID';
$string['aimh_help'] = 'Put AIM ID';

$string['yahoo'] = 'Yahoo ID';
$string['yahooh'] = 'Yahoo ID';
$string['yahooh_help'] = 'Put Yahoo ID';

$string['msn'] = 'MSN ID';
$string['msnh'] = 'MSN ID';
$string['msnh_help'] = 'Put MSN ID';

$string['id'] = 'ID number';
$string['idh'] = 'ID number';
$string['idh_help'] = 'Put ID number';

$string['inst'] = 'Institution';
$string['insth'] = 'Institution';
$string['insth_help'] = 'Put institution name';

$string['depart'] = 'Department';
$string['departh'] = 'Department';
$string['departh_help'] = 'Put department name';

$string['phone'] = 'Phone';
$string['phoneh'] = 'Phone';
$string['phoneh_help'] = 'Put phone number';

$string['mob'] = 'Mobile phone';
$string['mobh'] = 'Mobile phone';
$string['mobh_help'] = 'Put mobile number';

$string['address'] = 'Address';
$string['addressh'] = 'Address';
$string['addressh_help'] = 'Put address';
$string['description'] = 'desc';
$string['descriptionofexercise'] = 'desc12';

$string['preferencepage'] = 'Set preference';