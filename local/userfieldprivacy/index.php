<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private userfieldprivacy functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    local_userfieldprivacy
 * @copyright  2016 lms of india
 * @license    http://www.lmsofindia.com GNU GPL v3 or later
 */
require_once('../../config.php');
$type = required_param('type', PARAM_TEXT);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pagetitle', 'local_userfieldprivacy'));
$PAGE->set_heading('Userfieldprivacy');
$PAGE->set_url($CFG->wwwroot . '/local/userfieldprivacy/index.php');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/userfieldprivacy/css/style.css'));
echo $OUTPUT->header();
?>
<?php

require_once($CFG->dirroot . '/lib/moodlelib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');
require_once($CFG->dirroot . '/local/userfieldprivacy/form/userfieldprivacy_form.php');


require_login();
//$formfield = array('city', 'webpage', 'icq', 'skype', 'aim', 'yahoo', 'msn', 'id', 'inst', 'depart', 'phone', 'mob', 'address');
$action = $CFG->wwwroot.'/local/userfieldprivacy/index.php?type='.$type;
$mform = new userfieldprivacy_form($action);

if ($mform->is_cancelled()) {
    redirect(new moodle_url("$CFG->wwwroot/my"));
} else if ($fromform = $mform->get_data()) {
    // var_dump($fromform);exit;
    global $DB;
    $record = new stdClass();
    $record->userid = $USER->id;
    $record->type = $type;
    $record->city = $fromform->city;
    $record->webpage = $fromform->webpage;
    $record->icqnumber = $fromform->icq;
    $record->skypeid = $fromform->skype;
    $record->aimid = $fromform->aim;
    $record->yahooid = $fromform->yahoo;
    $record->msnid = $fromform->msn;
    $record->idnumber = $fromform->id;
    $record->institution = $fromform->inst;
    $record->department = $fromform->depart;
    $record->phone1 = $fromform->phone;
    $record->phone2 = $fromform->mob;
    $record->address = $fromform->address;
    
    $allcustomfield = profile_get_custom_fields($USER->id);
    $record->customfield = '';
    foreach ($allcustomfield as $fields) {
        $fname = $fields->shortname;
        $record->customfield .= $fields->shortname . ':' . $fromform->$fname . ',';
    }
    $record->customfield = rtrim($record->customfield, ',');
    if ($check = $DB->get_record('userfield_privacy', array('userid' => $USER->id))) {
        $record1 = new stdClass();
        $record1 = $record;
        $record1->id = $check->id;

        $update = $DB->update_record('userfield_privacy', $record1);
    } else {
        $lastinsert = $DB->insert_record('userfield_privacy', $record);
    }
} else {
    $alldefaultsaveddata = $DB->get_record('userfield_privacy', array('type' => 'default'));
    $allusersaveddata = $DB->get_record('userfield_privacy', array('type' => 'user','userid' => $USER->id));
    if ($allusersaveddata) {
        $savedrecord = new stdClass();
        $savedrecord->city = $allusersaveddata->city;
        $savedrecord->webpage = $allusersaveddata->webpage;
        $savedrecord->icq = $allusersaveddata->icqnumber;
        $savedrecord->skype = $allusersaveddata->skypeid;
        $savedrecord->aim = $allusersaveddata->aimid;
        $savedrecord->yahoo = $allusersaveddata->yahooid;
        $savedrecord->msn = $allusersaveddata->msnid;
        $savedrecord->id = $allusersaveddata->idnumber;
        $savedrecord->inst = $allusersaveddata->institution;
        $savedrecord->depart = $allusersaveddata->department;
        $savedrecord->phone = $allusersaveddata->phone1;
        $savedrecord->mob = $allusersaveddata->phone2;
        $savedrecord->address = $allusersaveddata->address;
       
        if ($allusersaveddata->customfield) {
            $customfieldexplode = explode(',', $allusersaveddata->customfield);
            foreach ($customfieldexplode as $value) {
                $datavalue = explode(':', $value);
                $savedrecord->$datavalue[0] = $datavalue[1];
            }
        }
        $mform->set_data($savedrecord);
    } else if ($alldefaultsaveddata) {
        $savedrecord = new stdClass();
        $savedrecord->city = $alldefaultsaveddata->city;
        $savedrecord->webpage = $alldefaultsaveddata->webpage;
        $savedrecord->icq = $alldefaultsaveddata->icqnumber;
        $savedrecord->skype = $alldefaultsaveddata->skypeid;
        $savedrecord->aim = $alldefaultsaveddata->aimid;
        $savedrecord->yahoo = $alldefaultsaveddata->yahooid;
        $savedrecord->msn = $alldefaultsaveddata->msnid;
        $savedrecord->id = $alldefaultsaveddata->idnumber;
        $savedrecord->inst = $alldefaultsaveddata->institution;
        $savedrecord->depart = $alldefaultsaveddata->department;
        $savedrecord->phone = $alldefaultsaveddata->phone1;
        $savedrecord->mob = $alldefaultsaveddata->phone2;
        $savedrecord->address = $alldefaultsaveddata->address;
        if ($alldefaultsaveddata->customfield) {
            $customfieldexplode = explode(',', $alldefaultsaveddata->customfield);
            foreach ($customfieldexplode as $value) {
                $datavalue = explode(':', $value);
                $savedrecord->$datavalue[0] = $datavalue[1];
            }
        }
        $mform->set_data($savedrecord);
    }
}
$mform->display();
echo $OUTPUT->footer();
?>

