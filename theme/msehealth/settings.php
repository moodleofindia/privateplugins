<?php
$settings = null;

defined('MOODLE_INTERNAL') || die;
    
	$ADMIN->add('themes', new admin_category('theme_msehealth', 'msehealth'));

	// "genericsettings" settingpage
	$temp = new admin_settingpage('theme_msehealth_generic',  get_string('geneicsettings', 'theme_msehealth'));
	
    // Logo file setting.   
    $name = 'theme_msehealth/logo';
    $title = get_string('logo','theme_msehealth');
    $description = get_string('logodesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);  

    // Layout - sidebar position.
    $name = 'theme_msehealth/layout';
    $title = get_string('layout', 'theme_msehealth');
    $description = get_string('layoutdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Include Awesome Font from Bootstrapcdn
    $name = 'theme_msehealth/bootstrapcdn';
    $title = get_string('bootstrapcdn', 'theme_msehealth');
    $description = get_string('bootstrapcdndesc', 'theme_msehealth');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Navbar Seperator.
    $name = 'theme_msehealth/navbarsep';
    $title = get_string('navbarsep' , 'theme_msehealth');
    $description = get_string('navbarsepdesc', 'theme_msehealth');
    $nav_thinbracket = get_string('nav_thinbracket', 'theme_msehealth');
    $nav_doublebracket = get_string('nav_doublebracket', 'theme_msehealth');
    $nav_thickbracket = get_string('nav_thickbracket', 'theme_msehealth');
    $nav_slash = get_string('nav_slash', 'theme_msehealth');
    $nav_pipe = get_string('nav_pipe', 'theme_msehealth');
    $dontdisplay = get_string('dontdisplay', 'theme_msehealth');
    $default = '\f105';
    $choices = array('\f105'=>$nav_thinbracket, '/'=>$nav_slash, '\f101'=>$nav_doublebracket, '\f054'=>$nav_thickbracket, '|'=>$nav_pipe);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Copyright setting.
    $name = 'theme_msehealth/copyright';
    $title = get_string('copyright', 'theme_msehealth');
    $description = get_string('copyrightdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Footerwidget setting.
    $name = 'theme_msehealth/footerwidget';
    $title = get_string('footerwidget', 'theme_msehealth');
    $description = get_string('footerwidgetdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Custom CSS file.
    $name = 'theme_msehealth/customcss';
    $title = get_string('customcss', 'theme_msehealth');
    $description = get_string('customcssdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_msehealth', $temp);
    
    /* Header Settings */
    $temp = new admin_settingpage('theme_msehealth_header', get_string('headerheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_header', get_string('headerheadingsub', 'theme_msehealth'),
            format_text(get_string('headerdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    // Header widget setting
    $name = 'theme_msehealth/headerwidget';
    $title = get_string('headerwidget','theme_msehealth');
    $description = get_string('headerwidgetdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Header phone number settings
    $name = 'theme_msehealth/headerphone';
    $title = get_string('headerphone','theme_msehealth');
    $description = get_string('headerphonedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Header email settings
    $name = 'theme_msehealth/headeremail';
    $title = get_string('headeremail','theme_msehealth');
    $description = get_string('headeremaildesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_msehealth', $temp);
    
    /* Custom Menu Settings */
    $temp = new admin_settingpage('theme_msehealth_custommenu', get_string('custommenuheading', 'theme_msehealth'));
	            
    // Description for mydashboard
    $name = 'theme_msehealth/mydashboardinfo';
    $heading = get_string('mydashboardinfo', 'theme_msehealth');
    $information = get_string('mydashboardinfodesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable dashboard display in custommenu.
    $name = 'theme_msehealth/displaymydashboard';
    $title = get_string('displaymydashboard', 'theme_msehealth');
    $description = get_string('displaymydashboarddesc', 'theme_msehealth');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Description for mycourse
    $name = 'theme_msehealth/mycoursesinfo';
    $heading = get_string('mycoursesinfo', 'theme_msehealth');
    $information = get_string('mycoursesinfodesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Toggle courses display in custommenu.
    $name = 'theme_msehealth/displaymycourses';
    $title = get_string('displaymycourses', 'theme_msehealth');
    $description = get_string('displaymycoursesdesc', 'theme_msehealth');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Set wording for dropdown course list
	$name = 'theme_msehealth/mycoursetitle';
	$title = get_string('mycoursetitle','theme_msehealth');
	$description = get_string('mycoursetitledesc', 'theme_msehealth');
	$default = 'course';
	$choices = array(
		'course' => get_string('mycourses', 'theme_msehealth'),
		'lesson' => get_string('mylessons', 'theme_msehealth'),
		'class' => get_string('myclasses', 'theme_msehealth'),
		'module' => get_string('mymodules', 'theme_msehealth'),
		'Unit' => get_string('myunits', 'theme_msehealth'),
	);
	$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$temp->add($setting);
    
    $ADMIN->add('theme_msehealth', $temp);
    
    
    /* Footer Blocks Settings */
	$temp = new admin_settingpage('theme_msehealth_footerblocks', get_string('footerblocksheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_footerblocks', get_string('footerblocksheadingsub', 'theme_msehealth'),
            format_text(get_string('footerblocksdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
            
	
	//Enable Footer Blocks.
    $name = 'theme_msehealth/enablefooterblocks';
    $title = get_string('enablefooterblocks', 'theme_msehealth');
    $description = get_string('enablefooterblocksdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
	
	//Footer Block One.
	$name = 'theme_msehealth/footerblock1';
    $title = get_string('footerblock1', 'theme_msehealth');
    $description = get_string('footerblock1desc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Footer Block Two.
	$name = 'theme_msehealth/footerblock2';
    $title = get_string('footerblock2', 'theme_msehealth');
    $description = get_string('footerblock2desc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Footer Block Three.
	$name = 'theme_msehealth/footerblock3';
    $title = get_string('footerblock3', 'theme_msehealth');
    $description = get_string('footerblock3desc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
        
    $ADMIN->add('theme_msehealth', $temp);
    
    
    /* Frontpage Alerts */
    $temp = new admin_settingpage('theme_msehealth_alerts', get_string('alertsheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_alerts', get_string('alertsheadingsub', 'theme_msehealth'),
            format_text(get_string('alertsdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    /* Alert 1 */
    
    // Description for Alert One
    $name = 'theme_msehealth/alert1info';
    $heading = get_string('alert1', 'theme_msehealth');
    $information = get_string('alert1desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert One
    $name = 'theme_msehealth/enable1alert';
    $title = get_string('enablealert', 'theme_msehealth');
    $description = get_string('enablealertdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Type.
    $name = 'theme_msehealth/alert1type';
    $title = get_string('alerttype' , 'theme_msehealth');
    $description = get_string('alerttypedesc', 'theme_msehealth');
    $alert_info = get_string('alert_info', 'theme_msehealth');
    $alert_warning = get_string('alert_warning', 'theme_msehealth');
    $alert_general = get_string('alert_general', 'theme_msehealth');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Title.
    $name = 'theme_msehealth/alert1title';
    $title = get_string('alerttitle', 'theme_msehealth');
    $description = get_string('alerttitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Text.
    $name = 'theme_msehealth/alert1text';
    $title = get_string('alerttext', 'theme_msehealth');
    $description = get_string('alerttextdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Alert 2 */
    
    // Description for Alert Two
    $name = 'theme_msehealth/alert2info';
    $heading = get_string('alert2', 'theme_msehealth');
    $information = get_string('alert2desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert Two
    $name = 'theme_msehealth/enable2alert';
    $title = get_string('enablealert', 'theme_msehealth');
    $description = get_string('enablealertdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Type.
    $name = 'theme_msehealth/alert2type';
    $title = get_string('alerttype' , 'theme_msehealth');
    $description = get_string('alerttypedesc', 'theme_msehealth');
    $alert_info = get_string('alert_info', 'theme_msehealth');
    $alert_warning = get_string('alert_warning', 'theme_msehealth');
    $alert_general = get_string('alert_general', 'theme_msehealth');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Title.
    $name = 'theme_msehealth/alert2title';
    $title = get_string('alerttitle', 'theme_msehealth');
    $description = get_string('alerttitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Text.
    $name = 'theme_msehealth/alert2text';
    $title = get_string('alerttext', 'theme_msehealth');
    $description = get_string('alerttextdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Alert 3 */
    
    // Description for Alert Three.
    $name = 'theme_msehealth/alert3info';
    $heading = get_string('alert3', 'theme_msehealth');
    $information = get_string('alert3desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert Three.
    $name = 'theme_msehealth/enable3alert';
    $title = get_string('enablealert', 'theme_msehealth');
    $description = get_string('enablealertdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Type.
    $name = 'theme_msehealth/alert3type';
    $title = get_string('alerttype' , 'theme_msehealth');
    $description = get_string('alerttypedesc', 'theme_msehealth');
    $alert_info = get_string('alert_info', 'theme_msehealth');
    $alert_warning = get_string('alert_warning', 'theme_msehealth');
    $alert_general = get_string('alert_general', 'theme_msehealth');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Title.
    $name = 'theme_msehealth/alert3title';
    $title = get_string('alerttitle', 'theme_msehealth');
    $description = get_string('alerttitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Text.
    $name = 'theme_msehealth/alert3text';
    $title = get_string('alerttext', 'theme_msehealth');
    $description = get_string('alerttextdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
            
    
    $ADMIN->add('theme_msehealth', $temp);
 
 
    /* Slideshow Widget Settings */
    $temp = new admin_settingpage('theme_msehealth_slideshow', get_string('slideshowheading', 'theme_msehealth'));
    $temp->add(new admin_setting_heading('theme_msehealth_slideshow', get_string('slideshowheadingsub', 'theme_msehealth'),
            format_text(get_string('slideshowdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    // Enable Slideshow.    
    $name = 'theme_msehealth/useslideshow';
    $title = get_string('useslideshow', 'theme_msehealth');
    $description = get_string('useslideshowdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    /* Slide 1 */
     
    // Description for Slide One
    $name = 'theme_msehealth/slide1info';
    $heading = get_string('slide1', 'theme_msehealth');
    $information = get_string('slideinfodesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_msehealth/slide1';
    $title = get_string('slidetitle', 'theme_msehealth');
    $description = get_string('slidetitledesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_msehealth/slide1image';
    $title = get_string('slideimage', 'theme_msehealth');
    $description = get_string('slideimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_msehealth/slide1caption';
    $title = get_string('slidecaption', 'theme_msehealth');
    $description = get_string('slidecaptiondesc', 'theme_msehealth');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_msehealth/slide1url';
    $title = get_string('slideurl', 'theme_msehealth');
    $description = get_string('slideurldesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 2 */
     
    //Description for Slide Two
    $name = 'theme_msehealth/slide2info';
    $heading = get_string('slide2', 'theme_msehealth');
    $information = get_string('slideinfodesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_msehealth/slide2';
    $title = get_string('slidetitle', 'theme_msehealth');
    $description = get_string('slidetitledesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_msehealth/slide2image';
    $title = get_string('slideimage', 'theme_msehealth');
    $description = get_string('slideimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_msehealth/slide2caption';
    $title = get_string('slidecaption', 'theme_msehealth');
    $description = get_string('slidecaptiondesc', 'theme_msehealth');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_msehealth/slide2url';
    $title = get_string('slideurl', 'theme_msehealth');
    $description = get_string('slideurldesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 3 */

    //Description for Slide Three
    $name = 'theme_msehealth/slide3info';
    $heading = get_string('slide3', 'theme_msehealth');
    $information = get_string('slideinfodesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Title.
    $name = 'theme_msehealth/slide3';
    $title = get_string('slidetitle', 'theme_msehealth');
    $description = get_string('slidetitledesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_msehealth/slide3image';
    $title = get_string('slideimage', 'theme_msehealth');
    $description = get_string('slideimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_msehealth/slide3caption';
    $title = get_string('slidecaption', 'theme_msehealth');
    $description = get_string('slidecaptiondesc', 'theme_msehealth');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_msehealth/slide3url';
    $title = get_string('slideurl', 'theme_msehealth');
    $description = get_string('slideurldesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 4 */
     
    // Description for Slide Four
    $name = 'theme_msehealth/slide4info';
    $heading = get_string('slide4', 'theme_msehealth');
    $information = get_string('slideinfodesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_msehealth/slide4';
    $title = get_string('slidetitle', 'theme_msehealth');
    $description = get_string('slidetitledesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_msehealth/slide4image';
    $title = get_string('slideimage', 'theme_msehealth');
    $description = get_string('slideimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_msehealth/slide4caption';
    $title = get_string('slidecaption', 'theme_msehealth');
    $description = get_string('slidecaptiondesc', 'theme_msehealth');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_msehealth/slide4url';
    $title = get_string('slideurl', 'theme_msehealth');
    $description = get_string('slideurldesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add('theme_msehealth', $temp);
    
    /* Frontpage Promo Box */
    $temp = new admin_settingpage('theme_msehealth_promo', get_string('promoheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_promo', get_string('promosub', 'theme_msehealth'),
            format_text(get_string('promodesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    // Enable Promo Box
    $name = 'theme_msehealth/usepromo';
    $title = get_string('usepromo', 'theme_msehealth');
    $description = get_string('usepromodesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Promo Box Title
    $name = 'theme_msehealth/promotitle';
    $title = get_string('promotitle', 'theme_msehealth');
    $description = get_string('promotitledesc', 'theme_msehealth');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Promo Copy
    $name = 'theme_msehealth/promocopy';
    $title = get_string('promocopy', 'theme_msehealth');
    $description = get_string('promocopydesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    //Frontpage Promo Box CTA button text
    $name = 'theme_msehealth/promocta';
    $title = get_string('promocta', 'theme_msehealth');
    $description = get_string('promoctadesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Promo Box CTA button URL
    $name = 'theme_msehealth/promoctaurl';
    $title = get_string('promoctaurl', 'theme_msehealth');
    $description = get_string('promoctaurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

        
    $ADMIN->add('theme_msehealth', $temp);
    

	/* Home Blocks Settings */	
	$temp = new admin_settingpage('theme_msehealth_homeblocks', get_string('homeblocksheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_homeblocks', get_string('homeblocksheadingsub', 'theme_msehealth'),
            format_text(get_string('homeblocksdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
            
	
	//Enable Home Blocks.
    $name = 'theme_msehealth/usehomeblocks';
    $title = get_string('usehomeblocks', 'theme_msehealth');
    $description = get_string('usehomeblocksdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
     // Block Item Min Height
	$name = 'theme_msehealth/homeblockminheight';
	$title = get_string('homeblockminheight','theme_msehealth');
	$description = get_string('homeblockminheightdesc', 'theme_msehealth');
	$default = '360';
	$setting = new admin_setting_configtext($name, $title, $description, $default);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$temp->add($setting);
	
	/* Home Block 1 */	
	$name = 'theme_msehealth/homeblock1';
    $title = get_string('homeblocktitle', 'theme_msehealth');
    $description = get_string('homeblocktitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock1image';
    $title = get_string('homeblockimage', 'theme_msehealth');
    $description = get_string('homeblockimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock1content';
    $title = get_string('homeblockcontent', 'theme_msehealth');
    $description = get_string('homeblockcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock1buttontext';
    $title = get_string('homeblockbuttontext', 'theme_msehealth');
    $description = get_string('homeblockbuttontextdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock1buttonurl';
    $title = get_string('homeblockbuttonurl', 'theme_msehealth');
    $description = get_string('homeblockbuttonurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 2 */    
	$name = 'theme_msehealth/homeblock2';
    $title = get_string('homeblocktitle', 'theme_msehealth');
    $description = get_string('homeblocktitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock2image';
    $title = get_string('homeblockimage', 'theme_msehealth');
    $description = get_string('homeblockimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock2content';
    $title = get_string('homeblockcontent', 'theme_msehealth');
    $description = get_string('homeblockcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock2buttontext';
    $title = get_string('homeblockbuttontext', 'theme_msehealth');
    $description = get_string('homeblockbuttontextdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock2buttonurl';
    $title = get_string('homeblockbuttonurl', 'theme_msehealth');
    $description = get_string('homeblockbuttonurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 3 */    
	$name = 'theme_msehealth/homeblock3';
    $title = get_string('homeblocktitle', 'theme_msehealth');
    $description = get_string('homeblocktitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock3image';
    $title = get_string('homeblockimage', 'theme_msehealth');
    $description = get_string('homeblockimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock3content';
    $title = get_string('homeblockcontent', 'theme_msehealth');
    $description = get_string('homeblockcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock3buttontext';
    $title = get_string('homeblockbuttontext', 'theme_msehealth');
    $description = get_string('homeblockbuttontextdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_msehealth/homeblock3buttonurl';
    $title = get_string('homeblockbuttonurl', 'theme_msehealth');
    $description = get_string('homeblockbuttonurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    $ADMIN->add('theme_msehealth', $temp);
    

    /* Frontpage Awards */
    $temp = new admin_settingpage('theme_msehealth_awards', get_string('awardsheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_awards', get_string('awardssub', 'theme_msehealth'),
            format_text(get_string('awardsdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    // Enable Awards Section
    $name = 'theme_msehealth/useawards';
    $title = get_string('useawards', 'theme_msehealth');
    $description = get_string('useawardsdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 1 */
    
    // Description for Award One
    $name = 'theme_msehealth/award1info';
    $heading = get_string('award1', 'theme_msehealth');
    $information = get_string('award1desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_msehealth/award1image';
    $title = get_string('awardimage', 'theme_msehealth');
    $description = get_string('awardimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_msehealth/award1alttext';
    $title = get_string('awardalttext', 'theme_msehealth');
    $description = get_string('awardalttextdesc', 'theme_msehealth');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    // Award URL
    $name = 'theme_msehealth/award1url';
    $title = get_string('awardurl', 'theme_msehealth');
    $description = get_string('awardurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    
    /* Award 2 */
    
    // Description for Award Two
    $name = 'theme_msehealth/award2info';
    $heading = get_string('award2', 'theme_msehealth');
    $information = get_string('award2desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_msehealth/award2image';
    $title = get_string('awardimage', 'theme_msehealth');
    $description = get_string('awardimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_msehealth/award2alttext';
    $title = get_string('awardalttext', 'theme_msehealth');
    $description = get_string('awardalttextdesc', 'theme_msehealth');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_msehealth/award2url';
    $title = get_string('awardurl', 'theme_msehealth');
    $description = get_string('awardurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    /* Award 3 */
    
    // Description for Award Three
    $name = 'theme_msehealth/award3info';
    $heading = get_string('award3', 'theme_msehealth');
    $information = get_string('award3desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_msehealth/award3image';
    $title = get_string('awardimage', 'theme_msehealth');
    $description = get_string('awardimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_msehealth/award3alttext';
    $title = get_string('awardalttext', 'theme_msehealth');
    $description = get_string('awardalttextdesc', 'theme_msehealth');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_msehealth/award3url';
    $title = get_string('awardurl', 'theme_msehealth');
    $description = get_string('awardurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 4 */
    
    // Description for Award Four
    $name = 'theme_msehealth/award4info';
    $heading = get_string('award4', 'theme_msehealth');
    $information = get_string('award4desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_msehealth/award4image';
    $title = get_string('awardimage', 'theme_msehealth');
    $description = get_string('awardimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_msehealth/award4alttext';
    $title = get_string('awardalttext', 'theme_msehealth');
    $description = get_string('awardalttextdesc', 'theme_msehealth');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_msehealth/award4url';
    $title = get_string('awardurl', 'theme_msehealth');
    $description = get_string('awardurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 5 */
    
    // Description for Award Five
    $name = 'theme_msehealth/award5info';
    $heading = get_string('award5', 'theme_msehealth');
    $information = get_string('award5desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_msehealth/award5image';
    $title = get_string('awardimage', 'theme_msehealth');
    $description = get_string('awardimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award5image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_msehealth/award5alttext';
    $title = get_string('awardalttext', 'theme_msehealth');
    $description = get_string('awardalttextdesc', 'theme_msehealth');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_msehealth/award5url';
    $title = get_string('awardurl', 'theme_msehealth');
    $description = get_string('awardurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 6 */
    
    // Description for Award Six
    $name = 'theme_msehealth/award6info';
    $heading = get_string('award6', 'theme_msehealth');
    $information = get_string('award6desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_msehealth/award6image';
    $title = get_string('awardimage', 'theme_msehealth');
    $description = get_string('awardimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award6image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_msehealth/award6alttext';
    $title = get_string('awardalttext', 'theme_msehealth');
    $description = get_string('awardalttextdesc', 'theme_msehealth');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_msehealth/award6url';
    $title = get_string('awardurl', 'theme_msehealth');
    $description = get_string('awardurldesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
       
    $ADMIN->add('theme_msehealth', $temp);
    
    /* Frontpage Testimonials */
    $temp = new admin_settingpage('theme_msehealth_testimonials', get_string('testimonialsheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_testimonials', get_string('testimonialssub', 'theme_msehealth'),
            format_text(get_string('testimonialsdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    // Enable Testimonails section
    $name = 'theme_msehealth/usetestimonials';
    $title = get_string('usetestimonials', 'theme_msehealth');
    $description = get_string('usetestimonialsdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 1 */
    
    // Description for Testimonial One
    $name = 'theme_msehealth/testimonial1info';
    $heading = get_string('testimonial1', 'theme_msehealth');
    $information = get_string('testimonial1desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_msehealth/testimonial1image';
    $title = get_string('testimonialimage', 'theme_msehealth');
    $description = get_string('testimonialimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_msehealth/testimonial1name';
    $title = get_string('testimonialname', 'theme_msehealth');
    $description = get_string('testimonialnamedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_msehealth/testimonial1title';
    $title = get_string('testimonialtitle', 'theme_msehealth');
    $description = get_string('testimonialtitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_msehealth/testimonial1content';
    $title = get_string('testimonialcontent', 'theme_msehealth');
    $description = get_string('testimonialcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 2 */
    
    // Description for Testimonial Two
    $name = 'theme_msehealth/testimonial2info';
    $heading = get_string('testimonial2', 'theme_msehealth');
    $information = get_string('testimonial2desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_msehealth/testimonial2image';
    $title = get_string('testimonialimage', 'theme_msehealth');
    $description = get_string('testimonialimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_msehealth/testimonial2name';
    $title = get_string('testimonialname', 'theme_msehealth');
    $description = get_string('testimonialnamedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_msehealth/testimonial2title';
    $title = get_string('testimonialtitle', 'theme_msehealth');
    $description = get_string('testimonialtitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_msehealth/testimonial2content';
    $title = get_string('testimonialcontent', 'theme_msehealth');
    $description = get_string('testimonialcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 3 */
    
    // Description for Testimonial Three
    $name = 'theme_msehealth/testimonial3info';
    $heading = get_string('testimonial3', 'theme_msehealth');
    $information = get_string('testimonial3desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_msehealth/testimonial3image';
    $title = get_string('testimonialimage', 'theme_msehealth');
    $description = get_string('testimonialimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_msehealth/testimonial3name';
    $title = get_string('testimonialname', 'theme_msehealth');
    $description = get_string('testimonialnamedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_msehealth/testimonial3title';
    $title = get_string('testimonialtitle', 'theme_msehealth');
    $description = get_string('testimonialtitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_msehealth/testimonial3content';
    $title = get_string('testimonialcontent', 'theme_msehealth');
    $description = get_string('testimonialcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 4 */
    
    // Description for Testimonial Four
    $name = 'theme_msehealth/testimonial4info';
    $heading = get_string('testimonial4', 'theme_msehealth');
    $information = get_string('testimonial4desc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_msehealth/testimonial4image';
    $title = get_string('testimonialimage', 'theme_msehealth');
    $description = get_string('testimonialimagedesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_msehealth/testimonial4name';
    $title = get_string('testimonialname', 'theme_msehealth');
    $description = get_string('testimonialnamedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_msehealth/testimonial4title';
    $title = get_string('testimonialtitle', 'theme_msehealth');
    $description = get_string('testimonialtitledesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_msehealth/testimonial4content';
    $title = get_string('testimonialcontent', 'theme_msehealth');
    $description = get_string('testimonialcontentdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_msehealth', $temp);
    

	/* Social Network Settings */
	$temp = new admin_settingpage('theme_msehealth_social', get_string('socialheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_social', get_string('socialheadingsub', 'theme_msehealth'),
            format_text(get_string('socialdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
            
    // Enable social media in the topbar
    $name = 'theme_msehealth/enabletopbarsocial';
    $title = get_string('enabletopbarsocial', 'theme_msehealth');
    $description = get_string('enabletopbarsocialdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Enable social media in the bottombar
    $name = 'theme_msehealth/enablebottombarsocial';
    $title = get_string('enablebottombarsocial', 'theme_msehealth');
    $description = get_string('enablebottombarsocialdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
            
    // Twitter url setting.
    $name = 'theme_msehealth/twitter';
    $title = get_string('twitter', 'theme_msehealth');
    $description = get_string('twitterdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Facebook url setting.
    $name = 'theme_msehealth/facebook';
    $title = get_string('facebook', 'theme_msehealth');
    $description = get_string('facebookdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // YouTube url setting.
    $name = 'theme_msehealth/youtube';
    $title = get_string('youtube', 'theme_msehealth');
    $description = get_string('youtubedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // LinkedIn url setting.
    $name = 'theme_msehealth/linkedin';
    $title = get_string('linkedin', 'theme_msehealth');
    $description = get_string('linkedindesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Flickr url setting.
    $name = 'theme_msehealth/flickr';
    $title = get_string('flickr', 'theme_msehealth');
    $description = get_string('flickrdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Google+ url setting.
    $name = 'theme_msehealth/googleplus';
    $title = get_string('googleplus', 'theme_msehealth');
    $description = get_string('googleplusdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Pinterest url setting.
    $name = 'theme_msehealth/pinterest';
    $title = get_string('pinterest', 'theme_msehealth');
    $description = get_string('pinterestdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Instagram url setting.
    $name = 'theme_msehealth/instagram';
    $title = get_string('instagram', 'theme_msehealth');
    $description = get_string('instagramdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Skype account setting.
    $name = 'theme_msehealth/skype';
    $title = get_string('skype', 'theme_msehealth');
    $description = get_string('skypedesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // RSS url setting.
    $name = 'theme_msehealth/rss';
    $title = get_string('rss', 'theme_msehealth');
    $description = get_string('rssdesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    //Description for iOS Icons
    $name = 'theme_msehealth/iosiconinfo';
    $heading = get_string('iosicon', 'theme_msehealth');
    $information = get_string('iosicondesc', 'theme_msehealth');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // iPhone Icon.
    $name = 'theme_msehealth/iphoneicon';
    $title = get_string('iphoneicon', 'theme_msehealth');
    $description = get_string('iphoneicondesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'iphoneicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPhone Retina Icon.
    $name = 'theme_msehealth/iphoneretinaicon';
    $title = get_string('iphoneretinaicon', 'theme_msehealth');
    $description = get_string('iphoneretinaicondesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'iphoneretinaicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPad Icon.
    $name = 'theme_msehealth/ipadicon';
    $title = get_string('ipadicon', 'theme_msehealth');
    $description = get_string('ipadicondesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'ipadicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPad Retina Icon.
    $name = 'theme_msehealth/ipadretinaicon';
    $title = get_string('ipadretinaicon', 'theme_msehealth');
    $description = get_string('ipadretinaicondesc', 'theme_msehealth');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'ipadretinaicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_msehealth', $temp);
       
    
    /* Analytics Settings */
    $temp = new admin_settingpage('theme_msehealth_analytics', get_string('analyticsheading', 'theme_msehealth'));
	$temp->add(new admin_setting_heading('theme_msehealth_analytics', get_string('analyticsheadingsub', 'theme_msehealth'),
            format_text(get_string('analyticsdesc' , 'theme_msehealth'), FORMAT_MARKDOWN)));
    
    // Enable Analytics
    $name = 'theme_msehealth/useanalytics';
    $title = get_string('useanalytics', 'theme_msehealth');
    $description = get_string('useanalyticsdesc', 'theme_msehealth');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Google Analytics ID
    $name = 'theme_msehealth/analyticsid';
    $title = get_string('analyticsid', 'theme_msehealth');
    $description = get_string('analyticsiddesc', 'theme_msehealth');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    $ADMIN->add('theme_msehealth', $temp);
