 <nav role="navigation" class="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target="#nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button><!--//nav-toggle-->
            </div><!--//navbar-header-->
            <div id="nav-collapse" class="nav-collapse collapse">
                <?php echo $OUTPUT->custom_menu(); ?>              
            </div>
        </div>
    </nav>