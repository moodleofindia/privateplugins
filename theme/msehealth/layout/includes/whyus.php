<div id="why-section" class="why-section">
        <div class="container-fluid text-center">
            <div class="section-heading">
                                <h2 class="title">Why Choose Us</h2>
                                                <p class="intro">We bring together lorem ipsum dolor sit amet consectetuer adipiscing</p>
                            </div><!--//section-heading-->
            <div class="items-wrapper">
                <div class="row">
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                                <div class="item-inner" style="height: 256px;">
                                                        <div class="figure-holder">
                                <i class="icon fa fa-laptop"></i>
                            </div><!--//figure-holder-->
                                                                                    <h3 class="item-title">2000+ Courses</h3>
                                                        
                                                        <div class="item-intro">
                                <p>Outline a benefit here. You can change the icon above to any of the 600+ <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FontAwesome icons</a> available. You can add up to 8 benefit blocks in this section.</p>                            </div>
                                                    </div><!--//item-inner-->
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                                <div class="item-inner" style="height: 256px;">
                                                        <div class="figure-holder">
                                <i class="icon fa fa-users"></i>
                            </div><!--//figure-holder-->
                                                                                    <h3 class="item-title">Industry Experts</h3>
                                                        
                                                        <div class="item-intro">
                                <p>Outline a benefit here. You can change the icon above to any of the 600+ <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FontAwesome icons</a> available. You can add up to 8 benefit blocks in this section.</p>                            </div>
                                                    </div><!--//item-inner-->
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                                <div class="item-inner" style="height: 256px;">
                                                        <div class="figure-holder">
                                <i class="icon fa fa-support"></i>
                            </div><!--//figure-holder-->
                                                                                    <h3 class="item-title">Dedicated Support</h3>
                                                        
                                                        <div class="item-intro">
                                <p>Outline a benefit here. You can change the icon above to any of the 600+ <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FontAwesome icons</a> available. You can add up to 8 benefit blocks in this section.</p>                            </div>
                                                    </div><!--//item-inner-->
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                                <div class="item-inner" style="height: 256px;">
                                                        <div class="figure-holder">
                                <i class="icon fa fa-tablet"></i>
                            </div><!--//figure-holder-->
                                                                                    <h3 class="item-title">Learn At Your Pace</h3>
                                                        
                                                        <div class="item-intro">
                                <p>Outline a benefit here. You can change the icon above to any of the 600+ <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FontAwesome icons</a> available. You can add up to 8 benefit blocks in this section.</p>                            </div>
                                                    </div><!--//item-inner-->
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                            </div><!--//item-->
                    
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                                            </div><!--//item-->
                    
                </div><!--//row-->
            </div><!--//items-wrapper-->
        </div><!--//container-->
    </div>