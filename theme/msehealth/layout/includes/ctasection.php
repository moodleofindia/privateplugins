<div class="cta-section">
        <div class="container-fluid text-center">
            <div class="cta-section-inner">
                                <h2 class="cta-title">Ready to learn new skills online?</h2>
                                                <div class="cta-content">
                    <p>You can access your courses on your computer, mobile or tablets. This is the perfect training solution for businesses, universities, government, and more. Lorem ipsum dolor sit amet, consectetuer adipiscing elit <a href="#">link example</a> aenean commodo ligula eget dolor.</p>                </div><!--//cta-content-->
                                                <a class="btn btn-cta btn-reversed btn-oval" href="#">Join Now</a>
                            </div><!--//cta-section-inner-->
        </div><!--//container-->
    </div>