Theme Colour Options
=====================

msehealth has 4 colour schemes. If you don't like the default colour scheme (blue) you can easily switch to a different colour scheme by following the steps below:

1) Use Green Colour Scheme
Replace /msehealth/style/style.css with /msehealth/style/theme-colours/green/style.css

2) Use Purple Colour Scheme
Replace /msehealth/style/style.css with /msehealth/style/theme-colours/purple/style.css

3) Use Red Colour Scheme
Replace /msehealth/style/style.css with /msehealth/style/theme-colours/red/style.css