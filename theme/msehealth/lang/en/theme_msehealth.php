<?php

/* Core */
$string['configtitle'] = 'msehealth';
$string['pluginname'] = 'msehealth';
$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>msehealth</h2>
<p><img src="msehealth/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About msehealth</h3>
<p>msehealth is a responsive Moodle theme designed specifically for schools, colleges, universities and online educational establishments who want to offer a clean and professional user experience for their users.</p>
<h3>Theme Parents</h3>
<p>This theme is based on the <a href="https://github.com/bmbrands/theme_bootstrap">Bootstrap theme</a>.</p>
<h3>Theme Credits</h3>
<p>Author: 3rd Wave Media<br>
Contact: hello@3rdwavemedia.com<br>
Website: <a href="http://elearning.3rdwavemedia.com/">elearning.3rdwavemedia.com</a>
</p>
</div></div>';

/* General */
$string['geneicsettings'] = 'General Settings';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Use this to add any CSS code you want to override the default theme CSS.';
$string['footerwidget'] = 'Footer Widget Area';
$string['footerwidgetdesc'] = 'This allows you to add extra content which will be displayed in the site footer';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Upload your own logo here if you want to replace the default one in the theme folder.';
$string['bootstrapcdn'] = 'Load FontAwesome from CDN';
$string['bootstrapcdndesc'] = 'Check this box if you\'d like to load FontAwesome from the online Bootstrap CDN source. Enable this if you are having issues getting the Font Awesome icons to display on your site.';
$string['copyright'] = 'Copyright';
$string['copyrightdesc'] = 'Enter the name of your site\'s copyright owner. This will be displayed in the site footer.';
$string['layout'] = 'Left-hand side sidebar';
$string['layoutdesc'] = 'By default the sidebar is displayed on the right-hand side. Check this box if you prefer a left-hand sidebar';

$string['visibleadminonly'] = 'Blocks moved into this area will only be seen by admins';
$string['backtotop'] = 'Back to top';
$string['nextsection'] = 'Next Section';
$string['previoussection'] = 'Previous Section';



$string['alwaysdisplay'] = 'Always Show';
$string['displaybeforelogin'] = 'Show before login only';
$string['displayafterlogin'] = 'Show after login only';
$string['dontdisplay'] = 'Never Show';

/* CustomMenu */
$string['mydashboard'] = 'My Dashboard';
$string['myhome'] = 'My Home';
$string['custommenuheading'] = 'Custom Menus';
$string['custommenuheadingsub'] = 'Add additional functionality to your custommenu.';
$string['custommenudesc'] = 'Settings here allow you to add new dynamic functionality to the custommenu (also refered to as Dropdown menu)';

$string['mydashboardinfo'] = 'My Dashboard';
$string['mydashboardinfodesc'] = 'Gives access to a list of the user\'s most visited pages.';
$string['displaymydashboard'] = 'Enable My Dashboard';
$string['displaymydashboarddesc'] = 'Show the "My Dashboard" option in the main menu.';

$string['mycoursesinfo'] = 'My Courses';
$string['mycoursesinfodesc'] = 'Gives access to a list of the user\'s enrolled courses.';
$string['displaymycourses'] = 'Display enrolled courses';
$string['displaymycoursesdesc'] = 'Show the "My Courses" option in the main menu.';

$string['mycoursetitle'] = 'Wording';
$string['mycoursetitledesc'] = 'Change the wording for the "My Courses" link text for the dropdown menu.';
$string['mycourses'] = 'My Courses';
$string['myunits'] = 'My Units';
$string['mylessons'] = 'My Lessons';
$string['mymodules'] = 'My Modules';
$string['myclasses'] = 'My Classes';
$string['allcourses'] = 'All Courses';
$string['allunits'] = 'All Units';
$string['allmodules'] = 'All Modules';
$string['allclasses'] = 'All Classes';
$string['noenrolments'] = 'You have no current enrolments';

/* Navbar Seperator */
$string['navbarsep'] = 'Breadcrumb Separator';
$string['navbarsepdesc'] = 'Use this to change the type of separator displayed in the breadcrumb';
$string['nav_thinbracket'] = 'Thin bracket';
$string['nav_doublebracket'] = 'Double thin bracket';
$string['nav_thickbracket'] = 'Thick Bracket';
$string['nav_slash'] = 'Forward slash';
$string['nav_pipe'] = 'Vertical line';

/* Regions */
//$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Sidebar';
$string['region-hidden-dock'] = 'Hidden from users';

/* Homepage Content Blocks Settings */
$string['homeblocksheading'] = 'Frontpage Content Blocks';
$string['homeblocksheadingsub'] = 'Configure 3 content blocks on your site\'s frontpage.';
$string['homeblocksdesc'] = 'Enter the settings for the 3 content blocks.';
$string['usehomeblocks'] = 'Enable Frontpage content blocks';
$string['usehomeblocksdesc'] = 'Check this box to show the content blocks on the frontpage';

$string['homeblockminheight'] = 'Minimum height of the blocks';
$string['homeblockminheightdesc'] = 'Define the minimum height of the content blocks. Increase this if one block is higher than the others.';
$string['homeblock1'] = 'Block 1';
$string['homeblock2'] = 'Block 2';
$string['homeblock3'] = 'Block 3';
$string['homeblocktitle'] = 'Title';
$string['homeblocktitledesc'] = 'Set the title for this block.';
$string['homeblockimage'] = 'Image';
$string['homeblockimagedesc'] = 'Set an image for this block. Recommended image size: 358px by 160px';
$string['homeblockcontent'] = 'Text Content';
$string['homeblockcontentdesc'] = 'Add text content for this block.';
$string['homeblockbuttontext'] = 'Call-to-action Button Text';
$string['homeblock2buttontext'] = 'Call-to-action Button Text';
$string['homeblock3buttontext'] = 'Call-to-action Button Text';
$string['homeblockbuttontextdesc'] = 'Enter the text for the call-to-action button';
$string['homeblockbuttonurl'] = 'Target URL';
$string['homeblock2buttonurl'] = 'Target URL';
$string['homeblock3buttonurl'] = 'Target URL';
$string['homeblockbuttonurldesc'] = 'Enter the target destination of the call-to-action button and the block title (i.e http://www.yourwebsite.com/yourpage).';


/* Slideshow */

$string['slideshowheading'] = 'Frontpage Slideshow';
$string['slideshowheadingsub'] = 'Slideshow for the frontpage';
$string['slideshowdesc'] = 'Add up to 4 slides to promote important areas of your site.';

$string['useslideshow'] = 'Enable frontpage slideshow';
$string['useslideshowdesc'] = 'Check this box to show the slideshow on the frontpage.';

$string['slideshowTitle'] = 'Slideshow';
$string['slideinfodesc'] = 'Enter the settings for your slide.';
$string['slide1'] = 'Slide 1';
$string['slide2'] = 'Slide 2';
$string['slide3'] = 'Slide 3';
$string['slide4'] = 'Slide 4';

$string['slidetitle'] = 'Slide Title';
$string['slidetitledesc'] = 'Enter a title for this slide.';
$string['slideimage'] = 'Slide Image';
$string['slideimagedesc'] = 'Upload the image you want to use for this slide. Recommended image size: 1140px by 350px';
$string['slidecaption'] = 'Slide Caption';
$string['slidecaptiondesc'] = 'Enter the caption text for this slide (appears under the slide title).';
$string['slideurl'] = 'Slide Link';
$string['slideurldesc'] = 'Enter the target destination of this slide\'s link (i.e http://www.yourwebsite.com/yourpage).';

/* Social Networks */
$string['enabletopbarsocial'] = 'Top Social Media icons';
$string['enabletopbarsocialdesc'] = 'Check to show Social Media icons in the site\'s header top bar.';
$string['enablebottombarsocial'] = 'Bottom Social Media icons';
$string['enablebottombarsocialdesc'] = 'Check to show Social Media icons in the site\'s footer bottom bar.';

$string['socialheading'] = 'Social Networking';
$string['socialheadingsub'] = 'Engage your users with Social Networking';
$string['socialdesc'] = 'Provide direct links to the core social networks that promote your brand.  These will appear in the header of every page.';
$string['socialnetworks'] = 'Social Networks';
$string['facebook'] = 'Facebook URL';
$string['facebookdesc'] = 'Enter the URL of your Facebook page. (i.e http://www.facebook.com/yourname)';

$string['twitter'] = 'Twitter URL';
$string['twitterdesc'] = 'Enter the URL of your Twitter homepage. (i.e http://www.twitter.com/yourname)';

$string['googleplus'] = 'Google+ URL';
$string['googleplusdesc'] = 'Enter the URL of your Google+ profile. (i.e http://plus.google.com/107817105228930159735)';

$string['linkedin'] = 'LinkedIn URL';
$string['linkedindesc'] = 'Enter the URL of your LinkedIn profile. (i.e http://www.linkedin.com/company/yourname)';

$string['youtube'] = 'YouTube URL';
$string['youtubedesc'] = 'Enter the URL of your YouTube channel. (i.e http://www.youtube.com/yourname)';

$string['flickr'] = 'Flickr URL';
$string['flickrdesc'] = 'Enter the URL of your Flickr page. (i.e http://www.flickr.com/yourname)';

$string['skype'] = 'Skype Account';
$string['skypedesc'] = 'Enter your Skype username';

$string['pinterest'] = 'Pinterest URL';
$string['pinterestdesc'] = 'Enter the URL of your Pinterest page. (i.e http://pinterest.com/yourname)';

$string['instagram'] = 'Instagram URL';
$string['instagramdesc'] = 'Enter the URL of your Instagram page. (i.e http://instagram.com/yourname)';

$string['rss'] = 'RSS Feed URL';
$string['rssdesc'] = 'Enter the URL of your RSS Feed. (i.e http://www.yourwebsite.com/feed)';

/* iOS Icons */
$string['iosicon'] = 'iOS Homescreen Icons';
$string['iosicondesc'] = 'The them does provide a default icon for iOS and android homescreens. You can upload your custom icons if you wish.';

$string['iphoneicon'] = 'iPhone/iPod Touch Icon (Non Retina)';
$string['iphoneicondesc'] = 'Upload the image to be used on non-retina display iPhone/iPod Touch devices. Should be a PNG file sized 57px by 57px';

$string['iphoneretinaicon'] = 'iPhone/iPod Touch Icon (Retina)';
$string['iphoneretinaicondesc'] = 'Upload the image to be used on retina display iPhone/iPod Touch devices. Should be a PNG file sized 114px by 114px';

$string['ipadicon'] = 'iPad Icon (Non Retina)';
$string['ipadicondesc'] = 'Upload the image to be used on non-retina display iPad devices. should be a PNG file sized 72px by 72px';

$string['ipadretinaicon'] = 'iPad Icon (Retina)';
$string['ipadretinaicondesc'] = 'Upload the image to be used on retina display iPad devices. Should be a PNG file sized 144px by 144px';

/* Google Analytics */
$string['analyticsheading'] = 'Google Analytics';
$string['analyticsheadingsub'] = 'Utilise analytics from Google';
$string['analyticsdesc'] = 'Here you can enable Google Analytics for your site. If you do not have one, you will need to sign up for a free account at the Google Analytics site (<a href="http://analytics.google.com" target="_blank">http://analytics.google.com</a>)';

$string['useanalytics'] = 'Enable Google Analytics';
$string['useanalyticsdesc'] = 'Enable or disable Google analytics.';

$string['analyticsid'] = 'Your Tracking ID';
$string['analyticsiddesc'] = 'Enter your Tracking ID for this site from Google Analytics. Typically formatted like UA-XXXXXXXX-X';


/* Alerts */
$string['alertsheading'] = 'Frontpage Alerts';
$string['alertsheadingsub'] = 'Display important messages on the frontpage';
$string['alertsdesc'] = 'This allows you to add up to 3 alerts on the site\'s frontpage.';

$string['enablealert'] = 'Enable this alert';
$string['enablealertdesc'] = 'Enable or disable this alert';

$string['alert1'] = 'Alert 1';
$string['alert2'] = 'Alert 2';
$string['alert3'] = 'Alert 3';
$string['alert1desc'] = 'Add content for this alert';
$string['alert2desc'] = 'Add content for this alert';
$string['alert3desc'] = 'Add content for this alert';

$string['alerttitle'] = 'Title';
$string['alerttitledesc'] = 'Main title/heading for your alert';

$string['alerttype'] = 'Type';
$string['alerttypedesc'] = 'Set the appropriate alert level/type to best inform your users';

$string['alerttext'] = 'Alert Text';
$string['alerttextdesc'] = 'Enter the text for this alert.';

$string['alert_info'] = 'Information';
$string['alert_warning'] = 'Warning';
$string['alert_general'] = 'Announcement';

/* Frontpage Promo */
$string['promoheading'] = 'Frontpage Promo Box';
$string['promosub'] = 'Display promotional info and primary call-to-action button on the frontpage.';
$string['promodesc'] = 'Enter settings for the frontpage promo box.';
$string['usepromo'] = 'Enable frontpage promo box';
$string['usepromodesc'] = 'Check this box to show the promo box on the frontpage.';
$string['promotitle'] = 'Title';
$string['promotitledesc'] = 'Enter the title of the promo box';
$string['promocopy'] = 'Text Content';
$string['promocopydesc'] = 'Enter the text content of the promo box';
$string['promocta'] = 'Call-to-action Button Text';
$string['promoctadesc'] = 'Enter the text for the call-to-action button';
$string['promoctaurl'] = 'Call-to-action Button Link';
$string['promoctaurldesc'] = 'Enter the target destination of the call-to-action button (i.e http://www.yourwebsite.com/yourpage).';

/* Frontpage Awards */
$string['awardsheading'] = 'Frontpage Awards Section';
$string['awardssub'] = 'Display a list of awards on the frontpage';
$string['awardsdesc'] = 'Add up to 6 awards images on the frontpage.';
$string['useawards'] = 'Enable frontpage awards section';
$string['useawardsdesc'] = 'Check this box to show the awards section on the frontpage.';
$string['award1'] = 'Award 1';
$string['award2'] = 'Award 2';
$string['award3'] = 'Award 3';
$string['award4'] = 'Award 4';
$string['award5'] = 'Award 5';
$string['award6'] = 'Award 6';
$string['award1desc'] = 'Add content for this award';
$string['award2desc'] = 'Add content for this award';
$string['award3desc'] = 'Add content for this award';
$string['award4desc'] = 'Add content for this award';
$string['award5desc'] = 'Add content for this award';
$string['award6desc'] = 'Add content for this award';
$string['awardimage'] = 'Image';
$string['awardimagedesc'] = 'Upload the image you want to use for this award. Recommended image size: 100px by 100px';
$string['awardalttext'] = 'Alternative Text';
$string['awardalttextdesc'] = 'Alternative text for the award image';
$string['awardurl'] = 'Award Link';
$string['awardurldesc'] = 'Enter the target destination of the award image (i.e http://www.awardwebsite.com/).';

/* Frontpage Testimonials */
$string['testimonialsheading'] = 'Frontpage Testimonials';
$string['testimonialssub'] = 'Testimonials for the frontpage';
$string['testimonialsdesc'] = 'Add up to 4 testimonials on the frontpage';
$string['usetestimonials'] = 'Enable frontpage testimonials';
$string['usetestimonialsdesc'] = 'Check this box to show the testimonials on the frontpage.';
$string['testimonial1'] = 'Testimonial 1';
$string['testimonial2'] = 'Testimonial 2';
$string['testimonial3'] = 'Testimonial 3';
$string['testimonial4'] = 'Testimonial 4';
$string['testimonial1desc'] = 'Add content for this testimonial';
$string['testimonial2desc'] = 'Add content for this testimonial';
$string['testimonial3desc'] = 'Add content for this testimonial';
$string['testimonial4desc'] = 'Add content for this testimonial';
$string['testimonialimage'] = 'Image';
$string['testimonialimagedesc'] = 'Upload the image you want to use for this testimonial. Recommended image size: 100px by 100px';
$string['testimonialname'] = 'Source Name';
$string['testimonialnamedesc'] = 'Testimonial source name';
$string['testimonialtitle'] = 'Source Title';
$string['testimonialtitledesc'] = 'Testimonial source title';
$string['testimonialcontent'] = 'Testimonial Quote';
$string['testimonialcontentdesc'] = 'Enter the testimonial quote';


/* Header Settings */
$string['headerheading'] = 'Site Header Settings';
$string['headerheadingsub'] = 'The content configured in the header which will be displayed throughout the site.';
$string['headerdesc'] = 'Enter settings for the site header.';
$string['headerwidget'] = 'Header Widget Area';
$string['headerwidgetdesc'] = 'You can use this area to display social media buttons (eg. Twitter follow and Facebook like) or other content.';
$string['headerphone'] = 'Contact Phone Number';
$string['headerphonedesc'] = 'Enter your organisation\'s contact number';
$string['headeremail'] = 'Contact email address';
$string['headeremaildesc'] = 'Enter your organisation\'s contact email address';


/* Footer Settings */
$string['footerblocksheading'] = 'Footer Content Blocks';
$string['footerblocksheadingsub'] = 'Configure 3 content blocks on your site\'s footer area.';
$string['footerblocksdesc'] = 'Enter the settings for the 3 content blocks.';
$string['enablefooterblocks'] = 'Enable footer content blocks';
$string['enablefooterblocksdesc'] = 'Check this box to show footer content blocks';
$string['footerblock1'] = 'Footer Block 1';
$string['footerblock2'] = 'Footer Block 2';
$string['footerblock3'] = 'Footer Block 3';
$string['footerblock1desc'] = 'Add content for this block';
$string['footerblock2desc'] = 'Add content for this block';
$string['footerblock3desc'] = 'Add content for this block';

