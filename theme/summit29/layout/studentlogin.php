<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A one column layout for the Bootstrapbase theme.
 *
 * @package   theme_bootstrapbase
 * @copyright 2012 Bas Brands, www.basbrands.nl
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$html = theme_summit29_get_html_for_settings($OUTPUT, $PAGE);
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
<?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>




    <body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

        <header role="banner" class="navbar navbar-fixed-top moodle-has-zindex">
            <nav role="navigation" class="navbar-inner">
                <div class="container-fluid">
                    <a href="<?php echo $CFG->wwwroot; ?>" class="logo">


                    </a>
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                        <?php echo $OUTPUT->user_menu(); ?>
                    <div class="nav-collapse collapse">
<?php echo $OUTPUT->custom_menu(); ?>
                        <ul class="nav pull-right">
                            <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div id="page" class="container-fluid">

            <header id="page-header" class="clearfix">
<?php echo $html->heading; ?>
                <div id="page-navbar" class="clearfix">
                    <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                    <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
                </div>
                <div id="course-header">
<?php echo $OUTPUT->course_header(); ?>
                </div>
            </header>

            <div id="page-content" class="row-fluid">
                <section id="region-main" class="span12">
                    <?php
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </section>
            </div>

            <center>
                <caption><h2>Student Login (Orbund)</h2></caption>
                <div class="clear"></div>
                <div class="index-outer widget color-gray">
                    <div class="widget-content"><form method="post" name="frmlogin" action=" https://server9.orbund.com/einstein-freshair/authenticate.jsp">
                            <div class="index-content">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="rta" width="20%">Username</td>
                                            <td width="80%"><input name="username" size="20" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td class="rta">Password</td>
                                            <td><input name="password" size="20" type="password" /></td>
                                        </tr>
                                        <tr>
                                            <td class="rta">Role</td>
                                            <td><select size="1" name="role">
                                                    <option value="3">Instructor</option>
                                                    <option value="1" selected="">Student</option>
                                                    <option value="99">Contact</option>
                                                    <option value="4">Administrator</option>
                                                    <option value="6">Sub-Administrator</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td> </td>
                                            <td><input value="Login &gt;&gt;" name="B3" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false" type="submit" /></td>
                                        </tr>
                                        <tr>
                                            <td> </td>
                                            <td><a href="https://server9.orbund.com/einstein-freshair/request_password.jsp">Forgot password?</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form></div>
                </div>
            </center>

<?php echo $OUTPUT->standard_end_of_body_html() ?>

        </div>
        <footer id="page-footer" class="container-fluid">
            <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
            <div id="page-footer112">
                <div class="span12 leftcol">
                    <div class="helplink">
                        <a href="<?php echo $CFG->wwwroot; ?>" class="logo1"></a>
<?php //echo $OUTPUT->page_doc_link();  ?>
                    </div>

                    <div id="page-footer-copyright"> <?php echo $html->footnote; ?></div>
                </div>
                <div class="span3 rightcol"><?php echo $OUTPUT->login_info(); ?></div>
            </div>
        </footer>
    </body>
</html>
