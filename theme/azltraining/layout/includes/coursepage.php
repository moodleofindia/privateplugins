<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row-fluid">
    <div class="art-layout-cell1 span6">
        <h3 style="text-align: center; font-size: 34px; padding-bottom: 20px;color: #6F2016">On Premise Courses:</h3>
        <h3 style="margin-top: -10px; text-align: center; color: #6F2016">(Bar | Hotel | Resturaunt)</h3>
    </div>
    <div class="art-layout-cell2 span6">
        <h3 style="text-align: center; font-size: 34px; padding-bottom: 20px; color: #6F2016">Off Premise Courses:</h3>
        <h3 style="margin-top: -10px; text-align: center; color: #6F2016">(Grocery Stores | Convenience Stores | Liquor Stores)</h3>
    </div>
</div>
<div style="border-top-width: 1px; border-top-style: solid; border-top-color: #D1C680; margin-top: 5px; margin-bottom: 5px;" class="art-content-layout-br"></div>

<?php
global $CFG;
require_once($CFG->libdir . '/coursecatlib.php');
$courseall = get_courses(1, $sort = "c.sortorder DESC");
?>

<div class="row-fluid" style="padding-bottom: 12px;">
    <?php
    foreach ($courseall as $course) {

        $courseinlist = new course_in_list((object) array('id' => $course->id));
        $imgturl = '';
        foreach ($courseinlist->get_course_overviewfiles() as $file) {
            if ($isimage = $file->is_valid_image()) {
                $imgturl = file_encode_url("$CFG->wwwroot/pluginfile.php", '/' . $file->get_contextid() . '/' . $file->get_component() . '/' .
                        $file->get_filearea() . $file->get_filepath() . $file->get_filename(), $isimage);
            }
        }

//            if (empty($imgturl)) {
//                $imgturl = $defaulturl;
//            }
        ?>

        <div class="span3">
            <h5 style="text-align: center; padding-bottom: 10px; border-bottom: 1px solid #d1c680;">
                <?php echo $course->fullname; ?></h5>
            <p style="text-align: center;"> <img src="<?php echo $imgturl ?>" width="75" height="103" /></p>
            <p style= "text-align:center"> <?php echo $course->summary; ?></p>
            <p style= "text-align:center;"> <a  class="btn" href="<?php
                $context = context_course::instance($COURSE->id);
                if (is_enrolled($context)) {
                    echo $CFG->wwwroot . "/course/view.php?id=$course->id";
                }
                ?>">More Information</a></p>
        </div>

    <?php } ?> 
</div>


<div style="border-top-width: 1px; border-top-style: solid; border-top-color: #D1C680; margin-top: 5px; margin-bottom: 5px;" class="art-content-layout-br"></div>

<div  class="art-layout-cell3 row-fluid">
    <p><img src=<?php echo $CFG->wwwroot . "/theme/azltraining/pix/content_01.jpg" ?> border="0" alt="Resources &amp; Links" width="350" height="234" style="float: left; margin-right: 10px; margin-left: 0px;"></p>
    <h5>Resources and Links</h5>
    <table class="art-article" style="width: 35%; float: left; border-width: 0px;">
        <tbody>
            <tr>
                <td style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;"><img class="art-lightbox" src=<?php echo $CFG->wwwroot . "/theme/azltraining/pix/bg-dllc.png" ?> border="0" alt="" width="45" height="40"></td>
                <td style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;">Arizona Department of Liquor Licenses and Control</td>
            </tr>
            <tr>
                <td style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;"><img class="art-lightbox" src=<?php echo $CFG->wwwroot . "/theme/azltraining/pix/bg-azsecst.png" ?> border="0" alt="" width="45" height="40"></td>
                <td style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;">Arizona Secretary of State</td>
            </tr>
            <tr>
                <td style="border-width: 0px;">A.R.S. Title IV -<br>Alcoholic Beverages</td>
                <td style="border-width: 0px;">General Provisions, Regulations And Prohibitions, Civil Liability of Licensees and Other Persons</td>
            </tr>
            <tr>
                <td style="border-width: 0px; text-align: right; width: 100%;" colspan="2"><a class="btn" href="/arizona/index.php/ct-menu-item-13">Learn More</a></td>
            </tr>
        </tbody>
    </table>
    <div id="ageToBuy">
        <h3>To buy alcohol, you must have been born on or before <br>May 18, 1995. 
        </h3>
    </div>
</div>
