<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    enrol_courseapproval
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */


defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_heading('enrol_courseapproval_enrolname', '', get_string('pluginname_desc', 'enrol_courseapproval')));

    // Confirm mail settings...
    $settings->add(new admin_setting_heading(
        'enrol_courseapproval_confirmmail',
        get_string('confirmmail_heading', 'enrol_courseapproval'),
        get_string('confirmmail_desc', 'enrol_courseapproval')));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/confirmmailsubject',
        get_string('confirmmailsubject', 'enrol_courseapproval'),
        get_string('confirmmailsubject_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    $settings->add(new admin_setting_confightmleditor(
        'enrol_courseapproval/confirmmailcontent',
        get_string('confirmmailcontent', 'enrol_courseapproval'),
        get_string('confirmmailcontent_desc', 'enrol_courseapproval'),
        null,
        PARAM_RAW));

    // Wait mail settings.
    $settings->add(new admin_setting_heading(
        'enrol_courseapproval_waitmail',
        get_string('waitmail_heading', 'enrol_courseapproval'),
        get_string('waitmail_desc', 'enrol_courseapproval')));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/waitmailsubject',
        get_string('waitmailsubject', 'enrol_courseapproval'),
        get_string('waitmailsubject_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    $settings->add(new admin_setting_confightmleditor(
        'enrol_courseapproval/waitmailcontent',
        get_string('waitmailcontent', 'enrol_courseapproval'),
        get_string('waitmailcontent_desc', 'enrol_courseapproval'),
        null,
        PARAM_RAW));

    // Cancel mail settings...
    $settings->add(new admin_setting_heading(
        'enrol_courseapproval_cancelmail',
        get_string('cancelmail_heading', 'enrol_courseapproval'),
        get_string('cancelmail_desc', 'enrol_courseapproval')));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/cancelmailsubject',
        get_string('cancelmailsubject', 'enrol_courseapproval'),
        get_string('cancelmailsubject_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    $settings->add(new admin_setting_confightmleditor(
        'enrol_courseapproval/cancelmailcontent',
        get_string('cancelmailcontent', 'enrol_courseapproval'),
        get_string('cancelmailcontent_desc', 'enrol_courseapproval'),
        null,
        PARAM_RAW));

    // Notification settings...
    $settings->add(new admin_setting_heading(
        'enrol_courseapproval_notify',
        get_string('notify_heading', 'enrol_courseapproval'),
        get_string('notify_desc', 'enrol_courseapproval')));
    $settings->add(new admin_setting_users_with_capability(
        'enrol_courseapproval/notifyglobal',
        get_string('notifyglobal', 'enrol_courseapproval'),
        get_string('notifyglobal_desc', 'enrol_courseapproval'),
        array(),
        'enrol/courseapproval:manageapplications'));

    // Enrol instance defaults...
    $settings->add(new admin_setting_heading('enrol_manual_defaults',
        get_string('enrolinstancedefaults', 'admin'), get_string('enrolinstancedefaults_desc', 'admin')));

    $settings->add(new admin_setting_configcheckbox('enrol_courseapproval/defaultenrol',
        get_string('defaultenrol', 'enrol'), get_string('defaultenrol_desc', 'enrol'), 0));

    $options = array(ENROL_INSTANCE_ENABLED => get_string('yes'),
                     ENROL_INSTANCE_DISABLED  => get_string('no'));
    $settings->add(new admin_setting_configselect('enrol_courseapproval/status',
        get_string('status', 'enrol_courseapproval'), get_string('status_desc', 'enrol_courseapproval'), ENROL_INSTANCE_ENABLED, $options));

    $options = array(1 => get_string('yes'),
                     0  => get_string('no'));
    $settings->add(new admin_setting_configselect('enrol_courseapproval/show_standard_user_profile',
        get_string('show_standard_user_profile', 'enrol_courseapproval'), '', 1, $options));

    $options = array(1 => get_string('yes'),
                     0  => get_string('no'));
    $settings->add(new admin_setting_configselect('enrol_courseapproval/show_extra_user_profile',
        get_string('show_extra_user_profile', 'enrol_courseapproval'), '', 1, $options));

    if (!during_initial_install()) {
        $options = get_default_enrol_roles(context_system::instance());
        $student = get_archetype_roles('student');
        $student = reset($student);
        $settings->add(new admin_setting_configselect('enrol_courseapproval/roleid',
            get_string('defaultrole', 'role'), '', $student->id, $options));
    }

    $settings->add(new admin_setting_configcheckbox(
        'enrol_courseapproval/notifycoursebased',
        get_string('notifycoursebased', 'enrol_courseapproval'),
        get_string('notifycoursebased_desc', 'enrol_courseapproval'),
        0));
}

 // datamatics field mapping
    $settings->add(new admin_setting_heading(
        'enrol_courseapproval_datamatics',
        get_string('datamatics_heading', 'enrol_courseapproval'),
        get_string('datamatics_heading_desc', 'enrol_courseapproval')));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/PrimaryApproverField',
        get_string('primarycourseapproval', 'enrol_courseapproval'),
        get_string('primarycourseapproval_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/SecondaryApproverField',
        get_string('secondarycourseapproval', 'enrol_courseapproval'),
        get_string('secondarycourseapproval_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/UserProfileField',
        get_string('userprofilefield', 'enrol_courseapproval'),
        get_string('userprofilefield_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    $settings->add(new admin_setting_configtext(
        'enrol_courseapproval/CustomProfileField',
        get_string('customprofilefield', 'enrol_courseapproval'),
        get_string('customprofilefield_desc', 'enrol_courseapproval'),
        null,
        PARAM_TEXT,
        60));
    // $settings->add(new admin_setting_confightmleditor(
    //     'enrol_courseapproval/cancelmailcontent',
    //     get_string('cancelmailcontent', 'enrol_courseapproval'),
    //     get_string('cancelmailcontent_desc', 'enrol_courseapproval'),
    //     null,
    //     PARAM_RAW));

if ($hassiteconfig) { // Needs this condition or there is error on login page.
    $ADMIN->add('courses', new admin_externalpage('enrol_courseapproval',
            get_string('courseapprovalmanage', 'enrol_courseapproval'),
            new moodle_url('/enrol/courseapproval/manage.php')));
}
