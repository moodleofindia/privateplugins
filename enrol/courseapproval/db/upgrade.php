<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    enrol_courseapproval
 * @author     Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */


defined('MOODLE_INTERNAL') || die;

function xmldb_enrol_courseapproval_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2016012801) {

        // Define table enrol_courseapproval_info to be created.
        $table = new xmldb_table('enrol_courseapproval_info');

        // Adding fields to table enrol_courseapproval_info.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userenrolmentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('comment', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table enrol_courseapproval_info.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userenrolment', XMLDB_KEY_FOREIGN_UNIQUE, array('userenrolmentid'), 'user_enrolments', array('id'));

        // Conditionally launch create table for enrol_courseapproval_info.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // courseapproval savepoint reached.
        upgrade_plugin_savepoint(true, 2016012801, 'enrol', 'courseapproval');
    }

    if ($oldversion < 2016042202) {
        // Invert settings for showing standard and extra user profile fields.
        $enrolcourseapproval = enrol_get_plugin('courseapproval');
        $showstandarduserprofile = $enrolcourseapproval->get_config('show_standard_user_profile') == 0 ? true : false;
        $enrolcourseapproval->set_config('show_standard_user_profile', $showstandarduserprofile);
        $showextrauserprofile = $enrolcourseapproval->get_config('show_extra_user_profile') == 0 ? true : false;
        $enrolcourseapproval->set_config('show_extra_user_profile', $showextrauserprofile);

        $instances = $DB->get_records('enrol', array('enrol' => 'courseapproval'));
        foreach ($instances as $instance) {
            $instance->customint1 = !$instance->customint1;
            $instance->customint2 = !$instance->customint2;
            $DB->update_record('enrol', $instance, true);
        }
    }

    if ($oldversion < 2016060803) {
        // Convert old notification settings.
        $enrolcourseapproval = enrol_get_plugin('courseapproval');

        $sendmailtoteacher = $enrolcourseapproval->get_config('sendmailtoteacher');
        $notifycoursebased = $sendmailtoteacher;
        $enrolcourseapproval->set_config('notifycoursebased', $notifycoursebased);
        $enrolcourseapproval->set_config('sendmailtoteacher', null);

        $sendmailtomanager = $enrolcourseapproval->get_config('sendmailtomanager');
        $notifyglobal = $sendmailtomanager ? '$@ALL@$' : '';
        $enrolcourseapproval->set_config('notifyglobal', $notifyglobal);
        $enrolcourseapproval->set_config('sendmailtomanager', null);

        $instances = $DB->get_records('enrol', array('enrol' => 'courseapproval'));
        foreach ($instances as $instance) {
            $sendmailtoteacher = $instance->customint3;
            $notify = $sendmailtoteacher ? '$@ALL@$' : '';
            $instance->customtext2 = $notify;
            $instance->customint3 = null;
            $instance->customint4 = null;
            $DB->update_record('enrol', $instance, true);
        }
    }

    if ($oldversion < 2017032400) {
        $enrolcourseapproval = enrol_get_plugin('courseapproval');

        $instances = $DB->get_records('enrol', array('enrol' => 'courseapproval'));
        foreach ($instances as $instance) {
            $instance->customint3 = 0;
            $DB->update_record('enrol', $instance, true);
        }
    }

    return true;

}