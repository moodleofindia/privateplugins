<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    enrol_courseapproval
 * @author     Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */


defined('MOODLE_INTERNAL') || die();

$messageproviders = array (
    // Notify teacher/manager that a student has applied for a course enrolment.
    'application' => array (
        'capability'  => 'enrol/courseapproval:manageapplications'
    ),

    // Notify student that his application was confirmed.
    'confirmation' => array (),

    // Notify student that his application was canceled.
    'cancelation' => array (),

    // Notify student that his application was deferred (put on a waiting list).
    'waitinglist' => array (),
);
