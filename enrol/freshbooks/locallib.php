<?php

/*
|----------------------------
| Include required library
|----------------------------
*/
include_once 'Freshbooks/FreshBooksApi.php';
include_once 'Freshbooks/FreshBooksApiException.php';
include_once 'Freshbooks/XmlDomConstruct.php';

/**
 * Whenever user loggedin this funciton will called by
 * Moodle Observer
 */
function enrol_user_freshbooks($event = null)
{
    global $DB, $USER, $CFG;
   
    if($client = $DB->get_record('enrol_freshbooks_client', array('userid'=> $USER->id)))
    {
        $invoices = $DB->get_records('enrol_freshbooks_data', array('client_id' => $client->id));
        if(!is_null($invoices))
        {
            //Setup the login credentials
            $domain = get_config('enrol_freshbooks', 'domain');
            $token = get_config('enrol_freshbooks', 'token');
            
            $fb = new \Freshbooks\FreshBooksApi($domain, $token);
            // Find client
            $fb->setMethod('payment.list');
            $fb->post(array(
                'client_id' => $client->client_id,
            ));
            
            $fb->request();
            if ($fb->success())
            {
                $payments = $fb->getResponse();
                if(!isset($payments['payments']['payment']['invoice_id']) && !isset($payments['payments']['payment'][0]))
                {
                    return;
                }                
            }
            else
            {
                return;
            }
            $paymentlist = array();
            $data = $DB->get_records('enrol_freshbooks_data', array('client_id' => $client->client_id,'status' => 0),'','invoice_id,id,courseid, instance');
            // single payment
            if(!isset($payments['payments']['payment'][0]))
            {
                $invoiceid = (int) $payments['payments']['payment']['invoice_id'];
                if(isset($data[($invoiceid)]))
                {
                    $paymentlist[] = array(
                                        'id' => $data[$invoiceid]->id,
                                        'payment_id' => $payments['payments']['payment']['payment_id'],
                                        'invoice' => $invoiceid,
                                        'courseid' => $data[$invoiceid]->courseid,
                                        'instance' => $data[$invoiceid]->instance,
                                        'type' => $payments['payments']['payment']['type'],
                                        'date' => $payments['payments']['payment']['date']
                                        ); 
                }
            }
            else
            { 
                foreach ($payments['payments']['payment'] as $pay) {
                    $invoiceid = (int) $pay['invoice_id'];
                    if(isset($data[$invoiceid]))
                    {
                        $paymentlist[] = array(
                                        'id' => $data[$invoiceid]->id,
                                        'invoice' => $invoiceid,
                                        'payment_id' => $pay['payment_id'],
                                        'courseid' =>  $data[$invoiceid]->courseid,
                                        'instance' => $data[$invoiceid]->instance,
                                        'type' =>  $pay['type'],
                                        'date' =>  $pay['date']
                                        ); 
                    }
                }
            }
            if(count($paymentlist) > 0)
            {
                try
                {
                    $transaction = $DB->start_delegated_transaction();
                    foreach($paymentlist as $row)
                    {
                        $DB->update_record('enrol_freshbooks_data', (object) array(
                            'id'=>$row['id'],
                            'payment_id' => $row['payment_id'],
                            'status' => 1
                        ));
                        $DB->execute('UPDATE {enrol_freshbooks} SET payment_type = ?, txn_id =?,payment_status = ?  WHERE memo = ? and userid = ?', 
                        array($row['type'], $row['payment_id'], 'paid', $row['invoice'], $USER->id ));
                    }
                    $transaction->allow_commit();
                } catch (Exception $e){
                    $transaction->rollback($e);
                }
            }
            // enrol user to courses, payment done
            if (enrol_is_enabled('freshbooks'))
            {
                $plugin = enrol_get_plugin('freshbooks');
                $enrolinstances = null;

                if(count($paymentlist) > 0)
                {
                    foreach ($paymentlist as $course) {
                        if ($instances = enrol_get_instances($course['courseid'], false)) {
                            foreach ($instances as $instance) {
                                if ($instance->enrol === 'freshbooks' && $instance->id == $course['instance']) {
                                    $instance->tmpid = $course['id']; 
                                    $enrolinstances[] = $instance;
                                    break;
                                }
                            }
                        }
                    }
                    if (count($enrolinstances) > 0)
                    {
                        foreach ($enrolinstances as $enrolinstance)
                        {
                            if($instance->id)
                            {
                                if ($instance->enrolperiod) {
                                    $timestart = time();
                                    $timeend   = $timestart + $instance->enrolperiod;
                                } else {
                                    if($instance->enrolstartdate) {
                                        $timestart = $instance->enrolstartdate;
                                    } else {
                                        $timestart = 0;
                                    }
                                    if ($instance->enrolenddate) {
                                        $timeend   = $instance->enrolenddate;
                                    } else {
                                        $timeend = 0;
                                    }
                                }

                                $plugin->enrol_user($enrolinstance, $USER->id, $enrolinstance->roleid, $timestart, $timeend, ENROL_USER_ACTIVE);
                                try
                                {
                                    $transaction = $DB->start_delegated_transaction();
                                    $DB->update_record('enrol_freshbooks_data', (object) array('id' => $enrolinstance->tmpid,'enrolstatus' => 1));
                                    $supportuser = core_user::get_support_user();
                                      // Enrol useri
                                    // Pass $view=true to filter hidden caps if the user cannot see them
                                    $course = $DB->get_record('course', array('id' => $enrolinstance->courseid));
                                    $coursecontext = context_course::instance($enrolinstance->courseid);
                                    $shortname = format_string($course->shortname, true, array('context' => $coursecontext));
                                    if ($users = get_users_by_capability($coursecontext, 'moodle/course:update', 'u.*', 'u.id ASC',
                                        '', '', '', '', false, true)) {
                                        $users = sort_by_roleassignment_authority($users, $coursecontext);
                                        $teacher = array_shift($users);
                                    } else {
                                        $teacher = false;
                                    }
                                    $mailstudents = $plugin->get_config('mailstudents');
                                    $mailteachers = $plugin->get_config('mailteachers');
                                    $mailadmins   = $plugin->get_config('mailadmins');
                                    $shortname = format_string($course->shortname, true, array('context' => $coursecontext));
                                    $user = $USER;
                                   if (!empty($mailstudents)) {
                                        $a = new stdClass();
                                        $a->coursename = format_string($course->fullname, true, array('context' => $coursecontext));
                                        $a->profileurl = "$CFG->wwwroot/user/view.php?id=$user->id";
                                        $eventdata = new stdClass();
                                        $eventdata->modulename        = 'moodle';
                                        $eventdata->component         = 'enrol_freshbooks';
                                        $eventdata->name              = 'freshbooks_enrolment';
                                        $eventdata->userfrom          = empty($teacher) ? core_user::get_support_user() : $teacher;
                                        $eventdata->userto            = $user;
                                        $eventdata->subject           = get_string("enrolmentnew", 'enrol', $shortname);
                                        $eventdata->fullmessage       = get_string('welcometocoursetext', '', $a);
                                        $eventdata->fullmessageformat = FORMAT_PLAIN;
                                        $eventdata->fullmessagehtml   = '';
                                        $eventdata->smallmessage      = '';
                                        message_send($eventdata);
                                    }
                                    if (!empty($mailteachers) && !empty($teacher)) {
                                        $a->course = format_string($course->fullname, true, array('context' => $coursecontext));
                                        $a->user = fullname($user);
                                        $eventdata = new stdClass();
                                        $eventdata->modulename        = 'moodle';
                                        $eventdata->component         = 'enrol_freshbooks';
                                        $eventdata->name              = 'freshbooks_enrolment';
                                        $eventdata->userfrom          = $user;
                                        $eventdata->userto            = $teacher;
                                        $eventdata->subject           = get_string("enrolmentnew", 'enrol', $shortname);
                                        $eventdata->fullmessage       = get_string('enrolmentnewuser', 'enrol', $a);
                                        $eventdata->fullmessageformat = FORMAT_PLAIN;
                                        $eventdata->fullmessagehtml   = '';
                                        $eventdata->smallmessage      = '';
                                        message_send($eventdata);
                                    }
                                    if (!empty($mailadmins)) {
                                        $a->course = format_string($course->fullname, true, array('context' => $coursecontext));
                                        $a->user = fullname($user);
                                        $admins = get_admins();
                                        foreach ($admins as $admin) {
                                            $eventdata = new stdClass();
                                            $eventdata->modulename        = 'moodle';
                                            $eventdata->component         = 'enrol_freshbooks';
                                            $eventdata->name              = 'freshbooks_enrolment';
                                            $eventdata->userfrom          = $user;
                                            $eventdata->userto            = $admin;
                                            $eventdata->subject           = get_string("enrolmentnew", 'enrol', $shortname);
                                            $eventdata->fullmessage       = get_string('enrolmentnewuser', 'enrol', $a);
                                            $eventdata->fullmessageformat = FORMAT_PLAIN;
                                            $eventdata->fullmessagehtml   = '';
                                            $eventdata->smallmessage      = '';
                                            message_send($eventdata);
                                        }
                                    } 
                                    $transaction->allow_commit();
                                } catch (Exception $e) {
                                    $transaction->rollback($e);
                                }
                            }
                        }
                    }
                }
            }
        }        
    }
    return;
}
