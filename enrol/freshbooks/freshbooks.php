<?php

/**
 *  Include required library
 */
require_once("../../config.php");
require_once $CFG->libdir.'/enrollib.php';
require_once 'Freshbooks/FreshBooksApi.php';
require_once 'Freshbooks/FreshBooksApiException.php';
require_once 'Freshbooks/XmlDomConstruct.php';

use Freshbooks\FreshBooksApi;

$courseid = required_param('courseid', PARAM_INT);
$instanceid = required_param('instance', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$instance = $DB->get_record('enrol', array('id' => $instanceid), '*', MUST_EXIST);

$PAGE->set_pagelayout('admin');
$PAGE->set_context(null);
$PAGE->set_url(new moodle_url($CFG->wwwroot.'/enrol/freshbooks/freshbooks.php', array('courseid' => $courseid)));
echo $OUTPUT->header();
// Setup the login credentials
$domain = get_config('enrol_freshbooks', 'domain');
$token = get_config('enrol_freshbooks', 'token');
$gsttax = get_config('enrol_freshbooks', 'gsttax');
$psttax = get_config('enrol_freshbooks', 'psttax');

$errors = [];
$fb = new FreshBooksApi($domain, $token);
if(!$client = $DB->get_record('enrol_freshbooks_client', array('userid' => $USER->id), 'client_id'))
{
    // Find client
    $fb->setMethod('client.list');
    $fb->post(array(
            'email' => $USER->email,
            'folder' => 'active'
    ));
    $fb->request();
    if ($fb->success())
    {
        $response = $fb->getResponse();
        if(isset($response['clients']['client']))
        {
            $client = $response['clients']['client'];
        }
        else
        {
            // Create client
            $fb->setMethod('client.create');
            $fb->post(array(
                'client' => array('first_name' => $USER->firstname, 'last_name' => $USER->lastname, 'organization' => $USER->institution, 'email' => $USER->email,
                    'username' => '', 'password' => '', 'contacts' => '', 'work_phone' => $USER->phone1, 'home_phone' => $USER->phone1,
                    'mobile' => $USER->phone2, 'fax' => '', 'language' => $USER->lang, 'currency_code' => get_config('currency', 'enrol_freshbooks'),
                    'notes' => '', 'p_street1' => $USER->address, 'p_street2' => '', 'p_city' => $USER->city, 'p_state' => '', 'p_country' => $USER->country,
                    'p_code' => '', 's_street1' => '', 's_street2' => '', 's_city' => '', 's_state' => '', 's_country' => '', 's_code' => '', 'vat_name' => '',
                    'vat_number' => '')));
            $fb->request();
            if ($fb->success())
            {
                $client = $fb->getResponse();
                try {
                    $transaction = $DB->start_delegated_transaction();
                    //Insert newly created client on { enrol_freshbook_client } table
                    $client['id'] = $DB->insert_record('enrol_freshbooks_client', array('userid' => $USER->id, 'client_id' => $client['client_id']));
                    $transaction->allow_commit();
                } catch (Exception $e) {
                    $transaction->rollback($e);
                }
            }
            else
            {
                $errors[] = $fb->getError();
            }
        }
    }
    else
    {
        $errors[] = $fb->getError();
    }
}
$client = (array) $client;
if(isset($client['client_id']))
{
    // Check if any invoice for this course
    $message = get_string('invoicealreadysent', 'enrol_freshbooks');
    if(!$invoice = $DB->get_record('enrol_freshbooks_data', array('client_id' => $client['client_id'], 'courseid' => $courseid)))
    {
        // Create invoice for client
        $fb->setMethod('invoice.create');
        $fb->post(array(
            'invoice' => array('client_id' => $client['client_id'], 'contacts' => null, 'number' => uniquekeys(), 'status' => 'sent',
            'date' => date('Y-m-d'), 'po_number' => '', 'discount' => 0, 'notes' => '', 'currency_code' => $instance->currency, 'language' => 'en',
            'terms' => '', 'return_uri' => '', 'first_name' => $USER->firstname, 'last_name' => $USER->lastname,'organization' => $USER->institution,'p_street1' => $USER->address, 
            'p_street2' => '','p_city' => $USER->city, 'p_state' => '', 'p_country' => $USER->country, 'p_code' => '','vat_name' => '', 'vat_number' => '',
            'lines' => array(
                'line' => array(
                        array(
                            'name' => $course->fullname,
                            'description' => '',
                            'unit_cost' => $instance->cost,
                            'quantity' => 1,
                            'tax1_name' => 'GST',
                            'tax2_name' => 'PST',
                            'tax1_percent' => $gsttax,
                            'tax2_percent' => $psttax,
                            'type' => 'Item',
                            )
                       )
                )
            )
        ));
        $fb->request();
        if ($fb->success())
        {
            try {
                $invoice = $fb->getResponse();
                $transaction = $DB->start_delegated_transaction();
                //Insert newly created invoice on { enrol_freshbook_data } table
                $DB->insert_record('enrol_freshbooks_data', array('client_id' => $client['client_id'], 'courseid'=> $courseid, 'instance'=> $instance->id, 'invoice_id' => $invoice['invoice_id'], 'status' => 0, 'timecreated' => time()));
                $DB->insert_record('enrol_freshbooks', array(
                    'business' => get_config('enrol_freshbooks', 'freshbooksbusiness'),
                    'receiver_email' => '',
                    'receiver_id' => '',
                    'item_name' => $course->fullname,
                    'courseid' => $courseid,
                    'userid' => $USER->id,
                    'instanceid' => $instanceid,
                    'memo' => $invoice['invoice_id'],
                    'tax' => '',
                    'payment_status' => 'draft',
                    'pending_reason' => '',
                    'reason_code' => '',
                    'txn_id' => '',
                    'parent_txn_id' => '',
                    'payment_type' => '',
                    'timeupdated' => time(),
                ));
                $transaction->allow_commit();
            } catch (Exception $e) {
                $transaction->rollback($e);
            }
        }
        else
        {
            $errors[] = $fb->getError();
        }
        $message = get_string('invoicesuccess', 'enrol_freshbooks');
    }
}
 $invoice = (array) $invoice;
if(isset($invoice['invoice_id']))
{
    // Send email to client
    $fb->setMethod('invoice.sendByEmail');
    $fb->post(array(
                'invoice_id' => $invoice['invoice_id']
            ));
    $fb->request();
    if ($fb->success())
    {
        $mail = $fb->getResponse();
    }
    else
    {
        $errors[] = $fb->getError();
    }
}
if(count($errors) > 0)
{
    echo html_writer::div(html_writer::tag('ul', implode('', $errors)), 'alert alert-warning');
}
if(!empty($message)){
    echo html_writer::div($message, 'alert alert-success');
}
echo $OUTPUT->footer();


function uniquekeys()
{
    return substr(uniqid('E'), 1, 8);
}

