<?php
header("Cache-Control:no-cache, must-revalidate");
header('Content-type:image/png');

$EncodedPNG = $_POST['imgsrc'];
$usid = $_POST['UserId'];
$crsid = $_POST['CourseID'];

//Replace spaces with +
$EncodedPNG = str_replace(' ','+',$EncodedPNG);
//Remove identifier string from begining of data.
$EncodedPNG =  str_replace('data:image/png;base64,', '', $EncodedPNG);

$FileName = 'chart_u'.  $_POST['UserId'] . '_c'. $_POST['CourseID'] . '.png';

$decoded = base64_decode($EncodedPNG);
//readfile($decoded);
file_put_contents("charts/" . $FileName, $decoded);