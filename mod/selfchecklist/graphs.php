<?php
header("Cache-Control: no-cache, must-revalidate");

require_once("../../config.php");
require_once($CFG->dirroot . '/lib/accesslib.php');
global $CFG, $DB, $USER, $COURSE, $PAGE;
require_login();
$id = required_param('id', PARAM_INT);
$course = get_course($id);
purge_all_caches();
$PAGE->set_url('/mod/selfchecklist/graphs.php', array('id' => $id));
$PAGE->set_pagelayout('course');
// Print the header.
$pagetitle = get_string('selfbread', 'selfchecklist');
$PAGE->set_context(context_course::instance(CONTEXT_COURSE, $id));
$PAGE->set_title($pagetitle);
$PAGE->set_heading($pagetitle);

// For Breadcrumb.
$strselfchecklists = get_string("modulenameplural", "selfchecklist");
$PAGE->navbar->add($course->fullname, new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
$PAGE->navbar->add($strselfchecklists);

echo $OUTPUT->header();

$loggedinuser = $USER->id;
$editingteacher = user_has_role_assignment($loggedinuser, 3, 0);
if (is_siteadmin() || $editingteacher) {
    $sql = "SELECT DISTINCT qrr.id, q.course, qrr.rank, q.addtype, qrr.question_id AS quesid, qrr.response_id,
	qrr.choice_id, qr.username
	FROM {selfchecklist_question} qq
	JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
	JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
	JOIN {selfchecklist} q ON q.id=qq.survey_id 
	WHERE q.addtype='beg' AND q.course=$id
	ORDER BY qrr.id ASC";
    $rs = $DB->get_records_sql($sql);
    //  print_object($rs);
    if ($rs) {
        foreach ($rs as $result) {
            $useidd[] = $result->username;
            $course = $result->course;
        }
        $rankeduser = array_unique($useidd);
        $table = new html_table();
        $table->head = (array) get_strings(['uname', 'vgraph'], 'selfchecklist');
        $count = 1;
        foreach ($rankeduser as $key => $val) {
            $userobj = $DB->get_record_sql("SELECT * FROM {user} WHERE id=$val");
            $username = fullname($userobj);
            $table->data[] = array(
                html_writer::link(new \moodle_url($CFG->wwwroot . '/user/profile.php?', ['id' => $val]), $username),
                html_writer::link(new \moodle_url($CFG->wwwroot . '/mod/selfchecklist/graph.php?', ['userid' => $val, 'courseid' => $course]), 'Click to View/Download'),
            );
        }
        echo html_writer::table($table);
    } else {
        echo '<h3>'.get_string('nosurveycompleted', 'selfchecklist').'</h3>';
    }
} else {
    echo '<div alert alert-warning>'.get_string('nostudentaccess', 'selfchecklist').'</div>';
}

echo $OUTPUT->footer();
