<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// This page displays a non-completable instance of selfchecklist.

require_once("../../config.php");
require_once($CFG->dirroot.'/mod/selfchecklist/selfchecklist.class.php');

$id     = optional_param('id', 0, PARAM_INT);
$sid    = optional_param('sid', 0, PARAM_INT);
$popup  = optional_param('popup', 0, PARAM_INT);
$qid    = optional_param('qid', 0, PARAM_INT);
$currentgroupid = optional_param('group', 0, PARAM_INT); // Groupid.

if ($id) {
    if (! $cm = get_coursemodule_from_id('selfchecklist', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $selfchecklist = $DB->get_record("selfchecklist", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }
} else {
    if (! $survey = $DB->get_record("selfchecklist_survey", array("id" => $sid))) {
        print_error('surveynotexists', 'selfchecklist');
    }
    if (! $course = $DB->get_record("course", array("id" => $survey->owner))) {
        print_error('coursemisconf');
    }
    // Dummy selfchecklist object.
    $selfchecklist = new Object();
    $selfchecklist->id = 0;
    $selfchecklist->course = $course->id;
    $selfchecklist->name = $survey->title;
    $selfchecklist->sid = $sid;
    $selfchecklist->resume = 0;
    // Dummy cm object.
    if (!empty($qid)) {
        $cm = get_coursemodule_from_instance('selfchecklist', $qid, $course->id);
    } else {
        $cm = false;
    }
}

// Check login and get context.
// Do not require login if this selfchecklist is viewed from the Add selfchecklist page
// to enable teachers to view template or public selfchecklists located in a course where they are not enroled.
if (!$popup) {
    require_login($course->id, false, $cm);
}
$context = $cm ? context_module::instance($cm->id) : false;

$url = new moodle_url('/mod/selfchecklist/preview.php');
if ($id !== 0) {
    $url->param('id', $id);
}
if ($sid) {
    $url->param('sid', $sid);
}
$PAGE->set_url($url);

$PAGE->set_context($context);
$PAGE->set_cm($cm);   //CONTRIB-5872 - I don't know why this is needed.

$selfchecklist = new selfchecklist($qid, $selfchecklist, $course, $cm);

$canpreview = (!isset($selfchecklist->capabilities) &&
               has_capability('mod/selfchecklist:preview', context_course::instance($course->id))) ||
              (isset($selfchecklist->capabilities) && $selfchecklist->capabilities->preview);
if (!$canpreview && !$popup) {
    // Should never happen, unless called directly by a snoop...
    print_error('nopermissions', 'selfchecklist', $CFG->wwwroot.'/mod/selfchecklist/view.php?id='.$cm->id);
}

if (!isset($SESSION->selfchecklist)) {
    $SESSION->selfchecklist = new stdClass();
}
$SESSION->selfchecklist->current_tab = new stdClass();
$SESSION->selfchecklist->current_tab = 'preview';

$qp = get_string('preview_selfchecklist', 'selfchecklist');
$pq = get_string('previewing', 'selfchecklist');

// Print the page header.
if ($popup) {
    $PAGE->set_pagelayout('popup');
}
$PAGE->set_title(format_string($qp));
if (!$popup) {
    $PAGE->set_heading(format_string($course->fullname));
}

// Include the needed js.


$PAGE->requires->js('/mod/selfchecklist/module.js');
// Print the tabs.


echo $OUTPUT->header();
if (!$popup) {
    require('tabs.php');
}
echo $OUTPUT->heading($pq);

if ($selfchecklist->capabilities->printblank) {
    // Open print friendly as popup window.

    $linkname = '&nbsp;'.get_string('printblank', 'selfchecklist');
    $title = get_string('printblanktooltip', 'selfchecklist');
    $url = '/mod/selfchecklist/print.php?qid='.$selfchecklist->id.'&amp;rid=0&amp;'.'courseid='.
            $selfchecklist->course->id.'&amp;sec=1';
    $options = array('menubar' => true, 'location' => false, 'scrollbars' => true, 'resizable' => true,
                    'height' => 600, 'width' => 800, 'title' => $title);
    $name = 'popup';
    $link = new moodle_url($url);
    $action = new popup_action('click', $link, $name, $options);
    $class = "floatprinticon";
    echo $OUTPUT->action_link($link, $linkname, $action, array('class' => $class, 'title' => $title),
            new pix_icon('t/print', $title));
}
$selfchecklist->survey_print_render('', 'preview', $course->id, $rid = 0, $popup);
if ($popup) {
    echo $OUTPUT->close_window_button();
}
echo $OUTPUT->footer($course);

// Log this selfchecklist preview.
$context = context_module::instance($selfchecklist->cm->id);
$anonymous = $selfchecklist->respondenttype == 'anonymous';

$event = \mod_selfchecklist\event\selfchecklist_previewed::create(array(
                'objectid' => $selfchecklist->id,
                'anonymous' => $anonymous,
                'context' => $context
));
$event->trigger();
