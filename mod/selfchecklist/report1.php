<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $SESSION, $CFG;
require_once("../../config.php");
require_once($CFG->dirroot . '/mod/selfchecklist/selfchecklist.class.php');

$instance = optional_param('instance', false, PARAM_INT);   // selfchecklist ID.
$action = optional_param('action', 'vall', PARAM_ALPHA);
$sid = optional_param('sid', null, PARAM_INT);              // Survey id.
$rid = optional_param('rid', false, PARAM_INT);
$type = optional_param('type', '', PARAM_ALPHA);
$byresponse = optional_param('byresponse', false, PARAM_INT);
$individualresponse = optional_param('individualresponse', false, PARAM_INT);
$currentgroupid = optional_param('group', 0, PARAM_INT); // Groupid.
$user = optional_param('user', '', PARAM_INT);
$userid = $USER->id;
switch ($action) {
    case 'vallasort':
        $sort = 'ascending';
        break;
    case 'vallarsort':
        $sort = 'descending';
        break;
    default:
        $sort = 'default';
}

if ($instance === false) {
    if (!empty($SESSION->instance)) {
        $instance = $SESSION->instance;
    } else {
        print_error('requiredparameter', 'selfchecklist');
    }
}
$SESSION->instance = $instance;
$usergraph = get_config('selfchecklist', 'usergraph');

if (!$selfchecklist = $DB->get_record("selfchecklist", array("id" => $instance))) {
    print_error('incorrectselfchecklist', 'selfchecklist');
}
if (!$course = $DB->get_record("course", array("id" => $selfchecklist->course))) {
    print_error('coursemisconf');
}
if (!$cm = get_coursemodule_from_instance("selfchecklist", $selfchecklist->id, $course->id)) {
    print_error('invalidcoursemodule');
}

require_course_login($course, true, $cm);

$selfchecklist = new selfchecklist(0, $selfchecklist, $course, $cm);

// If you can't view the selfchecklist, or can't view a specified response, error out.
$context = context_module::instance($cm->id);
if (!has_capability('mod/selfchecklist:readallresponseanytime', $context) &&
        !($selfchecklist->capabilities->view && $selfchecklist->can_view_response($rid))) {
    // Should never happen, unless called directly by a snoop...
    print_error('nopermissions', 'moodle', $CFG->wwwroot . '/mod/selfchecklist/view.php?id=' . $cm->id);
}

$selfchecklist->canviewallgroups = has_capability('moodle/site:accessallgroups', $context);
$sid = $selfchecklist->survey->id;

$url = new moodle_url($CFG->wwwroot . '/mod/selfchecklist/report.php');
if ($instance) {
    $url->param('instance', $instance);
}

$url->param('action', $action);

if ($type) {
    $url->param('type', $type);
}
if ($byresponse || $individualresponse) {
    $url->param('byresponse', 1);
}
if ($user) {
    $url->param('user', $user);
}
if ($action == 'dresp') {
    $url->param('action', 'dresp');
    $url->param('byresponse', 1);
    $url->param('rid', $rid);
    $url->param('individualresponse', 1);
}
if ($currentgroupid !== null) {
    $url->param('group', $currentgroupid);
}

$PAGE->set_url($url);
$PAGE->set_context($context);

// Tab setup.
if (!isset($SESSION->selfchecklist)) {
    $SESSION->selfchecklist = new stdClass();
}
$SESSION->selfchecklist->current_tab = 'allreport';

// Get all responses for further use in viewbyresp and deleteall etc.
// All participants.
$sql = "SELECT R.id, R.survey_id, R.submitted, R.username
         FROM {selfchecklist_response} R
         WHERE R.survey_id = ? AND
               R.complete='y'
         ORDER BY R.id";
if (!($respsallparticipants = $DB->get_records_sql($sql, array($sid)))) {
    $respsallparticipants = array();
}
$SESSION->selfchecklist->numrespsallparticipants = count($respsallparticipants);
$SESSION->selfchecklist->numselectedresps = $SESSION->selfchecklist->numrespsallparticipants;
$castsql = $DB->sql_cast_char2int('R.username');

// Available group modes (0 = no groups; 1 = separate groups; 2 = visible groups).
$groupmode = groups_get_activity_groupmode($cm, $course);
$selfchecklistgroups = '';
$groupscount = 0;
$SESSION->selfchecklist->respscount = 0;
$SESSION->selfchecklist_survey_id = $sid;

if ($groupmode > 0) {
    if ($groupmode == 1) {
        $selfchecklistgroups = groups_get_all_groups($course->id, $userid);
    }
    if ($groupmode == 2 || $selfchecklist->canviewallgroups) {
        $selfchecklistgroups = groups_get_all_groups($course->id);
    }

    if (!empty($selfchecklistgroups)) {
        $groupscount = count($selfchecklistgroups);
        foreach ($selfchecklistgroups as $key) {
            $firstgroupid = $key->id;
            break;
        }
        if ($groupscount === 0 && $groupmode == 1) {
            $currentgroupid = 0;
        }
        if ($groupmode == 1 && !$selfchecklist->canviewallgroups && $currentgroupid == 0) {
            $currentgroupid = $firstgroupid;
        }

        // Current group members.
        $sql = "SELECT R.id, R.survey_id, R.submitted, R.username
            FROM {selfchecklist_response} R,
                {groups_members} GM
             WHERE R.survey_id= ? AND
               R.complete='y' AND
               GM.groupid = ? AND " . $castsql . "=GM.userid
            ORDER BY R.id";
        if (!($currentgroupresps = $DB->get_records_sql($sql, array($sid, $currentgroupid)))) {
            $currentgroupresps = array();
        }
        $SESSION->selfchecklist->numcurrentgroupresps = count($currentgroupresps);
    } else {
        // Groupmode = separate groups but user is not member of any group
        // and does not have moodle/site:accessallgroups capability -> refuse view responses.
        if (!$selfchecklist->canviewallgroups) {
            $currentgroupid = 0;
        }
    }

    if ($currentgroupid > 0) {
        $groupname = get_string('group') . ' <strong>' . groups_get_group_name($currentgroupid) . '</strong>';
    } else {
        $groupname = '<strong>' . get_string('allparticipants') . '</strong>';
    }
}
if ($usergraph) {
    $charttype = $selfchecklist->survey->chart_type;
    if ($charttype) {
        $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.common.core.js');

        switch ($charttype) {
            case 'bipolar':
                $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.bipolar.js');
                break;
            case 'hbar':
                $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.hbar.js');
                break;
            case 'radar':
                $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.radar.js');
                break;
            case 'rose':
                $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.rose.js');
                break;
            case 'vprogress':
                $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.vprogress.js');
                break;
        }
    }
}

switch ($action) {

    case 'dresp':  // Delete individual response? Ask for confirmation.

        require_capability('mod/selfchecklist:deleteresponses', $context);

        if (empty($selfchecklist->survey)) {
            $id = $selfchecklist->survey;
            notify("selfchecklist->survey = /$id/");
            print_error('surveynotexists', 'selfchecklist');
        } else if ($selfchecklist->survey->owner != $course->id) {
            print_error('surveyowner', 'selfchecklist');
        } else if (!$rid || !is_numeric($rid)) {
            print_error('invalidresponse', 'selfchecklist');
        } else if (!($resp = $DB->get_record('selfchecklist_response', array('id' => $rid)))) {
            print_error('invalidresponserecord', 'selfchecklist');
        }

        $ruser = false;
        if (is_numeric($resp->username)) {
            if ($user = $DB->get_record('user', array('id' => $resp->username))) {
                $ruser = fullname($user);
            } else {
                $ruser = '- ' . get_string('unknown', 'selfchecklist') . ' -';
            }
        } else {
            $ruser = $resp->username;
        }

        // Print the page header.
        $PAGE->set_title(get_string('deletingresp', 'selfchecklist'));
        $PAGE->set_heading(format_string($course->fullname));
        echo $OUTPUT->header();

        // Print the tabs.
        $SESSION->selfchecklist->current_tab = 'deleteresp';
        include('tabs.php');

        $timesubmitted = '<br />' . get_string('submitted', 'selfchecklist') . '&nbsp;' . userdate($resp->submitted);
        if ($selfchecklist->respondenttype == 'anonymous') {
            $ruser = '- ' . get_string('anonymous', 'selfchecklist') . ' -';
            $timesubmitted = '';
        }

        // Print the confirmation.
        echo '<p>&nbsp;</p>';
        $msg = '<div class="warning centerpara">';
        $msg .= get_string('confirmdelresp', 'selfchecklist', $ruser . $timesubmitted);
        $msg .= '</div>';
        $urlyes = new moodle_url('report.php', array('action' => 'dvresp',
            'rid' => $rid, 'individualresponse' => 1, 'instance' => $instance, 'group' => $currentgroupid));
        $urlno = new moodle_url('report.php', array('action' => 'vresp', 'instance' => $instance,
            'rid' => $rid, 'individualresponse' => 1, 'group' => $currentgroupid));
        $buttonyes = new single_button($urlyes, get_string('yes'), 'post');
        $buttonno = new single_button($urlno, get_string('no'), 'get');
        echo $OUTPUT->confirm($msg, $buttonyes, $buttonno);

        // Finish the page.
        echo $OUTPUT->footer($course);
        break;

    case 'delallresp': // Delete all responses? Ask for confirmation.

        require_capability('mod/selfchecklist:deleteresponses', $context);

        $select = 'survey_id=' . $sid . ' AND complete = \'y\'';

        if (!($responses = $DB->get_records_select('selfchecklist_response', $select, null, 'id', 'id'))) {
            return;
        }

        // Print the page header.
        $PAGE->set_title(get_string('deletingresp', 'selfchecklist'));
        $PAGE->set_heading(format_string($course->fullname));
        echo $OUTPUT->header();

        // Print the tabs.
        $SESSION->selfchecklist->current_tab = 'deleteall';
        include('tabs.php');

        // Print the confirmation.
        echo '<p>&nbsp;</p>';
        $msg = '<div class="warning centerpara">';
        if ($groupmode == 0) {   // No groups or visible groups.
            $msg .= get_string('confirmdelallresp', 'selfchecklist');
        } else {                 // Separate groups.
            $msg .= get_string('confirmdelgroupresp', 'selfchecklist', $groupname);
        }
        $msg .= '</div>';

        $urlyes = new moodle_url('report.php', array('action' => 'dvallresp', 'sid' => $sid,
            'instance' => $instance, 'group' => $currentgroupid));
        $urlno = new moodle_url('report.php', array('instance' => $instance, 'group' => $currentgroupid));
        $buttonyes = new single_button($urlyes, get_string('yes'), 'post');
        $buttonno = new single_button($urlno, get_string('no'), 'get');

        echo $OUTPUT->confirm($msg, $buttonyes, $buttonno);

        // Finish the page.
        echo $OUTPUT->footer($course);
        break;

    case 'dvresp': // Delete single response. Do it!

        require_capability('mod/selfchecklist:deleteresponses', $context);

        if (empty($selfchecklist->survey)) {
            print_error('surveynotexists', 'selfchecklist');
        } else if ($selfchecklist->survey->owner != $course->id) {
            print_error('surveyowner', 'selfchecklist');
        } else if (!$rid || !is_numeric($rid)) {
            print_error('invalidresponse', 'selfchecklist');
        } else if (!($response = $DB->get_record('selfchecklist_response', array('id' => $rid)))) {
            print_error('invalidresponserecord', 'selfchecklist');
        }

        $ruser = false;
        if (is_numeric($response->username)) {
            if ($user = $DB->get_record('user', array('id' => $response->username))) {
                $ruser = fullname($user);
            } else {
                $ruser = '- ' . get_string('unknown', 'selfchecklist') . ' -';
            }
        } else {
            $ruser = $response->username;
        }

        if (selfchecklist_delete_response($response, $selfchecklist)) {
            if ($selfchecklist->respondenttype == 'anonymous') {
                $ruser = '- ' . get_string('anonymous', 'selfchecklist') . ' -';
            }
            $sql = "SELECT R.id, R.survey_id, R.submitted, R.username
                FROM {selfchecklist_response} R
                WHERE R.survey_id = ? AND
                R.complete='y'
                ORDER BY R.id";
            $resps = $DB->get_records_sql($sql, array($sid));
            if (empty($resps)) {
                $redirection = $CFG->wwwroot . '/mod/selfchecklist/view.php?id=' . $cm->id;
            } else {
                $redirection = $CFG->wwwroot . '/mod/selfchecklist/report.php?action=vresp&amp;instance=' .
                        $instance . '&amp;byresponse=1';
            }

            // Log this selfchecklist delete single response action.
            $anonymous = $selfchecklist->respondenttype == 'anonymous';

            $params = array(
                'objectid' => $selfchecklist->survey->id,
                'context' => $selfchecklist->context,
                'courseid' => $selfchecklist->course->id,
                'relateduserid' => $user->id
            );
            $event = \mod_selfchecklist\event\response_deleted::create($params);
            $event->trigger();

            redirect($redirection);
        } else {
            error(get_string('couldnotdelresp', 'selfchecklist') . $rid . get_string('by', 'selfchecklist') . $ruser . '?', $CFG->wwwroot . '/mod/selfchecklist/report.php?action=vresp&amp;sid=' . $sid . '&amp;&amp;instance=' .
                    $instance . 'byresponse=1');
        }
        break;

    case 'dvallresp': // Delete all responses in selfchecklist (or group). Do it!

        require_capability('mod/selfchecklist:deleteresponses', $context);

        if (empty($selfchecklist->survey)) {
            print_error('surveynotexists', 'selfchecklist');
        } else if ($selfchecklist->survey->owner != $course->id) {
            print_error('surveyowner', 'selfchecklist');
        }

        // Available group modes (0 = no groups; 1 = separate groups; 2 = visible groups).
        if ($groupmode > 0) {
            switch ($currentgroupid) {
                case 0:     // All participants.
                    $resps = $respsallparticipants;
                    break;
                default:     // Members of a specific group.
                    $sql = "SELECT R.id, R.survey_id, R.submitted, R.username
                        FROM {selfchecklist_response} R,
                            {groups_members} GM
                         WHERE R.survey_id = ? AND
                           R.complete='y' AND
                           GM.groupid = ? AND " . $castsql . "=GM.userid
                        ORDER BY R.id";
                    if (!($resps = $DB->get_records_sql($sql, array($sid, $currentgroupid)))) {
                        $resps = array();
                    }
            }
            if (empty($resps)) {
                $noresponses = true;
            } else {
                if ($rid === false) {
                    $resp = current($resps);
                    $rid = $resp->id;
                } else {
                    $resp = $DB->get_record('selfchecklist_response', array('id' => $rid));
                }
                if (is_numeric($resp->username)) {
                    if ($user = $DB->get_record('user', array('id' => $resp->username))) {
                        $ruser = fullname($user);
                    } else {
                        $ruser = '- ' . get_string('unknown', 'selfchecklist') . ' -';
                    }
                } else {
                    $ruser = $resp->username;
                }
            }
        } else {
            $resps = $respsallparticipants;
        }

        if (!empty($resps)) {
            foreach ($resps as $response) {
                selfchecklist_delete_response($response, $selfchecklist);
            }
            $sql = "SELECT R.id, R.survey_id, R.submitted, R.username
                     FROM {selfchecklist_response} R
                     WHERE R.survey_id = ? AND
                           R.complete='y'
                     ORDER BY R.id";
            if (!($resps = $DB->get_records_sql($sql, array($sid)))) {
                $respsallparticipants = array();
            }
            if (empty($resps)) {
                $redirection = $CFG->wwwroot . '/mod/selfchecklist/view.php?id=' . $cm->id;
            } else {
                $redirection = $CFG->wwwroot . '/mod/selfchecklist/report.php?action=vall&amp;sid=' . $sid . '&amp;instance=' . $instance;
            }

            // Log this selfchecklist delete all responses action.
            $context = context_module::instance($selfchecklist->cm->id);
            $anonymous = $selfchecklist->respondenttype == 'anonymous';

            $event = \mod_selfchecklist\event\all_responses_deleted::create(array(
                        'objectid' => $selfchecklist->id,
                        'anonymous' => $anonymous,
                        'context' => $context
            ));
            $event->trigger();

            redirect($redirection);
        } else {
            error(get_string('couldnotdelresp', 'selfchecklist'), $CFG->wwwroot . '/mod/selfchecklist/report.php?action=vall&amp;sid=' . $sid . '&amp;instance=' . $instance);
        }
        break;

    case 'dwnpg': // Download page options.

        require_capability('mod/selfchecklist:downloadresponses', $context);

        $PAGE->set_title(get_string('selfchecklistreport', 'selfchecklist'));
        $PAGE->set_heading(format_string($course->fullname));
        echo $OUTPUT->header();

        // Print the tabs.
        // Tab setup.
        if (empty($user)) {
            $SESSION->selfchecklist->current_tab = 'downloadcsv';
        } else {
            $SESSION->selfchecklist->current_tab = 'mydownloadcsv';
        }

        include('tabs.php');

        $groupname = '';
        if ($groupmode > 0) {
            switch ($currentgroupid) {
                case 0:     // All participants.
                    $groupname = get_string('allparticipants');
                    break;
                default:     // Members of a specific group.
                    $groupname = get_string('membersofselectedgroup', 'group') . ' ' . get_string('group') . ' ' .
                            $selfchecklistgroups[$currentgroupid]->name;
            }
        }
        echo "<br /><br />\n";
        echo $OUTPUT->help_icon('downloadtextformat', 'selfchecklist');
        echo '&nbsp;' . (get_string('downloadtext')) . ':&nbsp;' . get_string('responses', 'selfchecklist') . '&nbsp;' . $groupname;
        echo $OUTPUT->heading(get_string('textdownloadoptions', 'selfchecklist'));
        echo $OUTPUT->box_start();
        echo "<form action=\"{$CFG->wwwroot}/mod/selfchecklist/report.php\" method=\"GET\">\n";
        echo "<input type=\"hidden\" name=\"instance\" value=\"$instance\" />\n";
        echo "<input type=\"hidden\" name=\"user\" value=\"$user\" />\n";
        echo "<input type=\"hidden\" name=\"sid\" value=\"$sid\" />\n";
        echo "<input type=\"hidden\" name=\"action\" value=\"dcsv\" />\n";
        echo "<input type=\"hidden\" name=\"group\" value=\"$currentgroupid\" />\n";
        echo html_writer::checkbox('choicecodes', 1, true, get_string('includechoicecodes', 'selfchecklist'));
        echo "<br />\n";
        echo html_writer::checkbox('choicetext', 1, true, get_string('includechoicetext', 'selfchecklist'));
        echo "<br />\n";
        echo "<br />\n";
        echo "<input type=\"submit\" name=\"submit\" value=\"" . get_string('download', 'selfchecklist') . "\" />\n";
        echo "</form>\n";
        echo $OUTPUT->box_end();

        echo $OUTPUT->footer('none');

        // Log saved as text action.
        $params = array(
            'objectid' => $selfchecklist->id,
            'context' => $selfchecklist->context,
            'courseid' => $course->id,
            'other' => array('action' => $action, 'instance' => $instance, 'currentgroupid' => $currentgroupid)
        );
        $event = \mod_selfchecklist\event\all_responses_saved_as_text::create($params);
        $event->trigger();

        exit();
        break;

    case 'dcsv': // Download responses data as text (cvs) format.
        require_capability('mod/selfchecklist:downloadresponses', $context);

        // Use the selfchecklist name as the file name. Clean it and change any non-filename characters to '_'.
        $name = clean_param($selfchecklist->name, PARAM_FILE);
        $name = preg_replace("/[^A-Z0-9]+/i", "_", trim($name));

        $choicecodes = optional_param('choicecodes', '0', PARAM_INT);
        $choicetext = optional_param('choicetext', '0', PARAM_INT);
        $output = $selfchecklist->generate_csv('', $user, $choicecodes, $choicetext, $currentgroupid);
        // CSV
        // SEP. 2007 JR changed file extension to *.txt for non-English Excel users' sake
        // and changed separator to tabulation
        // JAN. 2008 added \r carriage return for better Windows implementation.
        header("Content-Disposition: attachment; filename=$name.txt");
        header("Content-Type: text/comma-separated-values");
        foreach ($output as $row) {
            $text = implode("\t", $row);
            echo $text . "\r\n";
        }
        exit();
        break;

    case 'vall':         // View all responses.
    case 'vallasort':    // View all responses sorted in ascending order.
    case 'vallarsort':   // View all responses sorted in descending order.

        $PAGE->set_title(get_string('selfchecklistreport', 'selfchecklist'));
        $PAGE->set_heading(format_string($course->fullname));
        echo $OUTPUT->header();
        if (!$selfchecklist->capabilities->readallresponses && !$selfchecklist->capabilities->readallresponseanytime) {

            // Should never happen, unless called directly by a snoop.
            print_error('nopermissions', '', '', get_string('viewallresponses', 'selfchecklist'));

            // Finish the page.
            echo $OUTPUT->footer($course);
            break;
        }

        // Print the tabs.
        switch ($action) {
            case 'vallasort':
                $SESSION->selfchecklist->current_tab = 'vallasort';
                break;
            case 'vallarsort':
                $SESSION->selfchecklist->current_tab = 'vallarsort';
                break;
            default:
                $SESSION->selfchecklist->current_tab = 'valldefault';
        }
        include('tabs.php');

        $resps = array();
        // Enable choose_group if there are selfchecklist groups and groupmode is not set to "no groups"
        // and if there are more goups than 1 (or if user can view all groups).
        if (is_array($selfchecklistgroups) && $groupmode > 0) {
            $groupselect = groups_print_activity_menu($cm, $url->out(), true);
            // Count number of responses in each group.
            $castsql = $DB->sql_cast_char2int('R.username');
            foreach ($selfchecklistgroups as $group) {
                $sql = "SELECT R.id, GM.id as groupid
                    FROM {selfchecklist_response} R, {groups_members} GM
                    WHERE R.survey_id= ? AND
                          R.complete='y' AND
                          GM.groupid= ? AND " . $castsql . "=GM.userid";
                if (!($resps = $DB->get_records_sql($sql, array($sid, $group->id)))) {
                    $resps = array();
                }
                $thisgroupname = groups_get_group_name($group->id);
                $escapedgroupname = preg_quote($thisgroupname, '/');
                if (!empty($resps)) {
                    // Add number of responses to name of group in the groups select list.
                    $respscount = count($resps);
                    $groupresps[$group->id] = $resps;
                    $groupselect = preg_replace('/\<option value="' . $group->id . '">' . $escapedgroupname . '<\/option>/', '<option value="' . $group->id . '">' . $thisgroupname . ' (' . $respscount . ')</option>', $groupselect);
                } else {
                    // Remove groups with no responses from the groups select list.
                    $groupselect = preg_replace('/\<option value="' . $group->id . '">' . $escapedgroupname .
                            '<\/option>/', '', $groupselect);
                }
            }
            echo isset($groupselect) ? $groupselect : '';
            $currentgroupid = groups_get_activity_group($cm);
        } else {
            echo ('<br />');
        }
        if ($currentgroupid > 0) {
            $groupname = get_string('group') . ': <strong>' . groups_get_group_name($currentgroupid) . '</strong>';
        } else {
            $groupname = '<strong>' . get_string('allparticipants') . '</strong>';
        }

        // Available group modes (0 = no groups; 1 = separate groups; 2 = visible groups).
        if ($groupmode > 0) {
            switch ($currentgroupid) {
                case 0:     // All participants.
                    $resps = $respsallparticipants;
                    break;
                default:     // Members of a specific group.
                    if (isset($groupresps [$currentgroupid])) {
                        $resps = $groupresps [$currentgroupid];
                    } else {
                        $resps = '';
                    }
            }
            if (empty($resps)) {
                $noresponses = true;
            }
        } else {
            $resps = $respsallparticipants;
        }
        if (!empty($resps)) {
            $ret = $selfchecklist->response_analysis($rid = 0, $resps, $compare = false, $isgroupmember = false, $allresponses = true, $currentgroupid);
        }

        $params = array(
            'objectid' => $selfchecklist->id,
            'context' => $context,
            'courseid' => $course->id,
            'other' => array('action' => $action, 'instance' => $instance, 'groupid' => $currentgroupid)
        );
        $event = \mod_selfchecklist\event\all_responses_viewed::create($params);
        $event->trigger();

        echo'<div class = "generalbox">';
        echo (get_string('viewallresponses', 'selfchecklist') . '. ' . $groupname . '. ');
        $strsort = get_string('order_' . $sort, 'selfchecklist');
        echo $strsort;
        echo $OUTPUT->help_icon('orderresponses', 'selfchecklist');

        //Added by Shiuli on 7th march.
        $student = user_has_role_assignment($USER->id, 5);
        if ($student && ($selfchecklist->resp_view == 0)) {
            echo '&nbsp;&nbsp;<div class="alert alert-warning">' . get_string('nopermissionforstudent', 'selfchecklist') . '</div>';
        } else {
            $ret = $selfchecklist->survey_results(1, 1, '', '', '', $uid = false, $currentgroupid, $sort);
        }
        echo '</div>';

        // Finish the page.
        echo $OUTPUT->footer($course);
        break;

    case 'vresp': // View by response.

    default:
        if (empty($selfchecklist->survey)) {
            print_error('surveynotexists', 'selfchecklist');
        } else if ($selfchecklist->survey->owner != $course->id) {
            print_error('surveyowner', 'selfchecklist');
        }
        $ruser = false;
        $noresponses = false;
        if ($usergraph) {
            $charttype = $selfchecklist->survey->chart_type;
            if ($charttype) {
                $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.common.core.js');

                switch ($charttype) {
                    case 'bipolar':
                        $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.bipolar.js');
                        break;
                    case 'hbar':
                        $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.hbar.js');
                        break;
                    case 'radar':
                        $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.radar.js');
                        break;
                    case 'rose':
                        $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.rose.js');
                        break;
                    case 'vprogress':
                        $PAGE->requires->js('/mod/selfchecklist/javascript/RGraph/RGraph.vprogress.js');
                        break;
                }
            }
        }

        if ($byresponse || $rid) {
            // Available group modes (0 = no groups; 1 = separate groups; 2 = visible groups).
            if ($groupmode > 0) {
                switch ($currentgroupid) {
                    case 0:     // All participants.
                        $resps = $respsallparticipants;
                        break;
                    default:     // Members of a specific group.
                        $sql = "SELECT R.id, R.survey_id, R.submitted, R.username
                            FROM {selfchecklist_response} R,
                                {groups_members} GM
                             WHERE R.survey_id= ? AND
                               R.complete='y' AND
                               GM.groupid= ? AND " . $castsql . "=GM.userid
                              ORDER BY R.id";
                        if (!($resps = $DB->get_records_sql($sql, array($sid, $currentgroupid)))) {
                            $resps = array();
                        }
                }
                if (empty($resps)) {
                    $noresponses = true;
                } else {
                    if ($rid === false) {
                        $resp = current($resps);
                        $rid = $resp->id;
                    } else {
                        $resp = $DB->get_record('selfchecklist_response', array('id' => $rid));
                    }
                    if (is_numeric($resp->username)) {
                        if ($user = $DB->get_record('user', array('id' => $resp->username))) {
                            $ruser = fullname($user);
                        } else {
                            $ruser = '- ' . get_string('unknown', 'selfchecklist') . ' -';
                        }
                    } else {
                        $ruser = $resp->username;
                    }
                }
            } else {
                $resps = $respsallparticipants;
            }
        }
        $rids = array_keys($resps);
        if (!$rid && !$noresponses) {
            $rid = $rids[0];
        }

        // Print the page header.
        $PAGE->set_title(get_string('selfchecklistreport', 'selfchecklist'));
        $PAGE->set_heading(format_string($course->fullname));
        echo $OUTPUT->header();

        // Print the tabs.
        if ($byresponse) {
            $SESSION->selfchecklist->current_tab = 'vrespsummary';
        }
        if ($individualresponse) {
            $SESSION->selfchecklist->current_tab = 'individualresp';
        }
        include('tabs.php');

        // Print the main part of the page.
        // TODO provide option to select how many columns and/or responses per page.

        echo $OUTPUT->box_start();

        if ($noresponses) {
            echo (get_string('group') . ' <strong>' . groups_get_group_name($currentgroupid) . '</strong>: ' .
            get_string('noresponses', 'selfchecklist'));
        } else {
            $groupname = get_string('group') . ': <strong>' . groups_get_group_name($currentgroupid) . '</strong>';
            if ($currentgroupid == 0) {
                $groupname = get_string('allparticipants');
            }
            if ($byresponse) {
                echo $OUTPUT->box_start();
                echo $OUTPUT->help_icon('viewindividualresponse', 'selfchecklist') . '&nbsp;';
                echo (get_string('viewindividualresponse', 'selfchecklist') . ' <strong> : ' . $groupname . '</strong>');
                echo $OUTPUT->box_end();
            }
            $selfchecklist->survey_results_navbar_alpha($rid, $currentgroupid, $cm, $byresponse);
            if (!$byresponse) { // Show respondents individual responses.
                $selfchecklist->view_response($rid, $referer = '', $blankselfchecklist = false, $resps, $compare = true, $isgroupmember = true, $allresponses = false, $currentgroupid);
            }
        }
        echo $OUTPUT->box_end();

        // Finish the page.
        echo $OUTPUT->footer($course);
        break;
}