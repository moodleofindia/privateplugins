<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print the form to add or edit a selfchecklist-instance
 *
 * @author Mike Churchward
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package selfchecklist
 */

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/selfchecklist/selfchecklist.class.php');
require_once($CFG->dirroot.'/mod/selfchecklist/locallib.php');

class mod_selfchecklist_mod_form extends moodleform_mod {

    protected function definition() {
        global $COURSE;

        $selfchecklist = new selfchecklist($this->_instance, null, $COURSE, $this->_cm);

        $mform    =& $this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name', 'selfchecklist'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');

        $this->standard_intro_elements(false, get_string('description'));

        //added by Shiuli on 3rd feb.
        $options['beg'] = get_string('addtype_beg', 'selfchecklist');
        $options['end'] = get_string('addtype_end', 'selfchecklist');
        $select = $mform->addElement('select', 'addtype', get_string('addtype', 'selfchecklist'),
        $options);

        $mform->addElement('header', 'timinghdr', get_string('timing', 'form'));

        $enableopengroup = array();
        $enableopengroup[] =& $mform->createElement('checkbox', 'useopendate', get_string('opendate', 'selfchecklist'));
        $enableopengroup[] =& $mform->createElement('date_time_selector', 'opendate', '');
        $mform->addGroup($enableopengroup, 'enableopengroup', get_string('opendate', 'selfchecklist'), ' ', false);
        $mform->addHelpButton('enableopengroup', 'opendate', 'selfchecklist');
        $mform->disabledIf('enableopengroup', 'useopendate', 'notchecked');

        $enableclosegroup = array();
        $enableclosegroup[] =& $mform->createElement('checkbox', 'useclosedate', get_string('closedate', 'selfchecklist'));
        $enableclosegroup[] =& $mform->createElement('date_time_selector', 'closedate', '');
        $mform->addGroup($enableclosegroup, 'enableclosegroup', get_string('closedate', 'selfchecklist'), ' ', false);
        $mform->addHelpButton('enableclosegroup', 'closedate', 'selfchecklist');
        $mform->disabledIf('enableclosegroup', 'useclosedate', 'notchecked');

        global $selfchecklisttypes, $selfchecklistrespondents, $selfchecklistresponseviewers, $selfchecklistrealms, $autonumbering;
        $mform->addElement('header', 'selfchecklisthdr', get_string('responseoptions', 'selfchecklist'));

        $mform->addElement('select', 'qtype', get_string('qtype', 'selfchecklist'), $selfchecklisttypes);
        $mform->addHelpButton('qtype', 'qtype', 'selfchecklist');

        $mform->addElement('hidden', 'cannotchangerespondenttype');
        $mform->setType('cannotchangerespondenttype', PARAM_INT);
        $mform->addElement('select', 'respondenttype', get_string('respondenttype', 'selfchecklist'), $selfchecklistrespondents);
        $mform->addHelpButton('respondenttype', 'respondenttype', 'selfchecklist');
        $mform->disabledIf('respondenttype', 'cannotchangerespondenttype', 'eq', 1);

        $mform->addElement('select', 'resp_view', get_string('responseview', 'selfchecklist'), $selfchecklistresponseviewers);
        $mform->addHelpButton('resp_view', 'responseview', 'selfchecklist');

        $options = array('0' => get_string('no'), '1' => get_string('yes'));
        $mform->addElement('select', 'resume', get_string('resume', 'selfchecklist'), $options);
        $mform->addHelpButton('resume', 'resume', 'selfchecklist');

        $options = array('0' => get_string('no'), '1' => get_string('yes'));
        $mform->addElement('select', 'navigate', get_string('navigate', 'selfchecklist'), $options);
        $mform->addHelpButton('navigate', 'navigate', 'selfchecklist');

        $mform->addElement('select', 'autonum', get_string('autonumbering', 'selfchecklist'), $autonumbering);
        $mform->addHelpButton('autonum', 'autonumbering', 'selfchecklist');
        // Default = autonumber both questions and pages.
        $mform->setDefault('autonum', 3);

        // Removed potential scales from list of grades. CONTRIB-3167.
        $grades[0] = get_string('nograde');
        for ($i = 100; $i >= 1; $i--) {
            $grades[$i] = $i;
        }
        $mform->addElement('select', 'grade', get_string('grade', 'selfchecklist'), $grades);

        if (empty($selfchecklist->sid)) {
            if (!isset($selfchecklist->id)) {
                $selfchecklist->id = 0;
            }

            $mform->addElement('header', 'contenthdr', get_string('contentoptions', 'selfchecklist'));
            $mform->addHelpButton('contenthdr', 'createcontent', 'selfchecklist');

            $mform->addElement('radio', 'create', get_string('createnew', 'selfchecklist'), '', 'new-0');

            // Retrieve existing private selfchecklists from current course.
            $surveys = selfchecklist_get_survey_select($selfchecklist->id, $COURSE->id, 0, 'private');
            if (!empty($surveys)) {
                $prelabel = get_string('useprivate', 'selfchecklist');
                foreach ($surveys as $value => $label) {
                    $mform->addElement('radio', 'create', $prelabel, $label, $value);
                    $prelabel = '';
                }
            }
            // Retrieve existing template selfchecklists from this site.
            $surveys = selfchecklist_get_survey_select($selfchecklist->id, $COURSE->id, 0, 'template');
            if (!empty($surveys)) {
                $prelabel = get_string('usetemplate', 'selfchecklist');
                foreach ($surveys as $value => $label) {
                    $mform->addElement('radio', 'create', $prelabel, $label, $value);
                    $prelabel = '';
                }
            } else {
                $mform->addElement('static', 'usetemplate', get_string('usetemplate', 'selfchecklist'),
                                '('.get_string('notemplatesurveys', 'selfchecklist').')');
            }

            // Retrieve existing public selfchecklists from this site.
            $surveys = selfchecklist_get_survey_select($selfchecklist->id, $COURSE->id, 0, 'public');
            if (!empty($surveys)) {
                $prelabel = get_string('usepublic', 'selfchecklist');
                foreach ($surveys as $value => $label) {
                    $mform->addElement('radio', 'create', $prelabel, $label, $value);
                    $prelabel = '';
                }
            } else {
                $mform->addElement('static', 'usepublic', get_string('usepublic', 'selfchecklist'),
                                   '('.get_string('nopublicsurveys', 'selfchecklist').')');
            }

            $mform->setDefault('create', 'new-0');
        }

        $this->standard_coursemodule_elements();

        // Buttons.
        $this->add_action_buttons();
    }

    public function data_preprocessing(&$defaultvalues) {
        global $DB;
        if (empty($defaultvalues['opendate'])) {
            $defaultvalues['useopendate'] = 0;
        } else {
            $defaultvalues['useopendate'] = 1;
        }
        if (empty($defaultvalues['closedate'])) {
            $defaultvalues['useclosedate'] = 0;
        } else {
            $defaultvalues['useclosedate'] = 1;
        }
        // Prevent selfchecklist set to "anonymous" to be reverted to "full name".
        $defaultvalues['cannotchangerespondenttype'] = 0;
        if (!empty($defaultvalues['respondenttype']) && $defaultvalues['respondenttype'] == "anonymous") {
            // If this selfchecklist has responses.
            $numresp = $DB->count_records('selfchecklist_response',
                            array('survey_id' => $defaultvalues['sid'], 'complete' => 'y'));
            if ($numresp) {
                $defaultvalues['cannotchangerespondenttype'] = 1;
            }
        }
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }

    public function add_completion_rules() {
        $mform =& $this->_form;
        $mform->addElement('checkbox', 'completionsubmit', '', get_string('completionsubmit', 'selfchecklist'));
        return array('completionsubmit');
    }

    public function completion_rule_enabled($data) {
        return !empty($data['completionsubmit']);
    }

}