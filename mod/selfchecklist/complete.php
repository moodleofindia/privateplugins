<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// This page prints a particular instance of selfchecklist.

require_once("../../config.php");
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot.'/mod/selfchecklist/selfchecklist.class.php');

if (!isset($SESSION->selfchecklist)) {
    $SESSION->selfchecklist = new stdClass();
}
$SESSION->selfchecklist->current_tab = 'view';

$id = optional_param('id', null, PARAM_INT);    // Course Module ID.
$a = optional_param('a', null, PARAM_INT);      // selfchecklist ID.

$sid = optional_param('sid', null, PARAM_INT);  // Survey id.
$resume = optional_param('resume', null, PARAM_INT);    // Is this attempt a resume of a saved attempt?

if ($id) {
    if (! $cm = get_coursemodule_from_id('selfchecklist', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $selfchecklist = $DB->get_record("selfchecklist", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }

} else {
    if (! $selfchecklist = $DB->get_record("selfchecklist", array("id" => $a))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $selfchecklist->course))) {
        print_error('coursemisconf');
    }
    if (! $cm = get_coursemodule_from_instance("selfchecklist", $selfchecklist->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
}

// Check login and get context.
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/selfchecklist:view', $context);

$url = new moodle_url($CFG->wwwroot.'/mod/selfchecklist/complete.php');
if (isset($id)) {
    $url->param('id', $id);
} else {
    $url->param('a', $a);
}

$PAGE->set_url($url);
$PAGE->set_context($context);
$selfchecklist = new selfchecklist(0, $selfchecklist, $course, $cm);

$selfchecklist->strselfchecklists = get_string("modulenameplural", "selfchecklist");
$selfchecklist->strselfchecklist  = get_string("modulename", "selfchecklist");

// Mark as viewed.
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

if ($resume) {
    $context = context_module::instance($selfchecklist->cm->id);
    $anonymous = $selfchecklist->respondenttype == 'anonymous';

    $event = \mod_selfchecklist\event\attempt_resumed::create(array(
                    'objectid' => $selfchecklist->id,
                    'anonymous' => $anonymous,
                    'context' => $context
    ));
    $event->trigger();
}

$selfchecklist->view();
