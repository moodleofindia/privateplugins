<?php

//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */
// Include the main TCPDF library (search for installation path).
require_once (dirname(dirname(dirname(__FILE__))) . '/config.php');
global $PAGE;
require_once("{$CFG->libdir}/tcpdf/tcpdf.php");
require_once("{$CFG->libdir}/tcpdf/fonts/times.php");
//$uid = $_GET['userid'];
//$cid = $_GET['courseid'];
global $USER, $DB;
$cid = required_param('courseid', PARAM_INT);
$loggedinuser = $USER->id;
$editingteacher = user_has_role_assignment($loggedinuser, 3, 0);
if (is_siteadmin() || $editingteacher) {
    $uid = required_param('userid', PARAM_INT);
} else {
    $uid = $USER->id;
}

// extend TCPF with custom functions
class MYPDF extends TCPDF {

    // Load table data from file
    public function LoadData($userid, $cid) {
        // Read file lines
        global $DB, $PAGE;
        $adminbegsql = "SELECT DISTINCT qrr.id, qq.content AS blockcontent, qqc.content, qrr.rank
                FROM {selfchecklist_question} qq
                JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
                JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
                JOIN {selfchecklist} q ON q.id=qq.survey_id
                JOIN {selfchecklist_quest_choice} qqc ON qqc.question_id=qrr.question_id
                WHERE q.addtype='beg' AND qr.username='$userid' AND q.course=$cid AND qrr.choice_id=qqc.id
                ORDER BY qrr.id ASC";
        $lines = $DB->get_records_sql($adminbegsql);

        // Admin Ending Graph starts.
        $adminendsql = "SELECT DISTINCT qrr.id, q.course, qq.content AS blockcontent, qqc.content, qrr.rank, q.addtype, qrr.question_id AS quesid, qrr.response_id,
                qrr.choice_id, qr.username
                FROM {selfchecklist_question} qq
                JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
                JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
                JOIN {selfchecklist} q ON q.id=qq.survey_id
                JOIN {selfchecklist_quest_choice} qqc ON qqc.question_id=qrr.question_id
                WHERE q.addtype='end' AND qr.username='$userid' AND q.course=$cid AND qrr.choice_id=qqc.id
                ORDER BY qrr.id ASC";
        $adminendrs = $DB->get_records_sql($adminendsql);
        $adminendranks = array();
        foreach ($adminendrs as $adminendrsval) {
            $adminendranks[] = $adminendrsval->rank;
        }

        //Average rank portion.
        $adminbegarrrank = array();
        foreach ($lines as $adminbegresult) {
            $adminbegarrrank[] = $adminbegresult->rank;
        }
        $begrankarr = $adminbegarrrank;
        $endrankarr = $adminendranks;
        $arraysum = $this->array_add_by_keys($begrankarr, $endrankarr);
        foreach ($arraysum as $k => $a) {
            if (array_key_exists($k, $arraysum)) {
                $arraysum[$k] = $a / 2;
            } else {
                $arraysum[$k] = $a;
            }
            $arravg = $arraysum;
        }
        $diffArr = $this->array_diff_by_keys($endrankarr, $begrankarr);
        // Sum of begging ranks, ending ranks, avg ranks.
        $begrankAvg = array_sum($begrankarr) / 15;
        $endrankAvg = array_sum($endrankarr) / 15;
        $diffrankAvg = array_sum($diffArr) / 15;

        $data = array();
        $merge = array();
        $endi = 0;
        $avgi = 0;

        foreach ($lines as $val) {
            $arrkeys = array_key_exists($endi, $adminendranks);
            if (!$arrkeys) {
                $adminendranks[$endi] = 0;
            }
            $arrkey = array_key_exists($avgi, $arravg);
            if (!$arrkey) {
                $arravg[$avgi] = 0;
            }
            $data1 = strip_tags($val->blockcontent) . '&&&&&&';
            $data1 .= $val->content . '&&&&&&';
            $data1 .= $val->rank . '&&&&&&';
            $data1 .= $adminendranks[$endi] . '&&&&&&';
            $data1 .= $diffArr[$avgi];
            //var_dump($data1);
            $data[] = explode('&&&&&&', $data1);
            $endi++;
            $avgi++;
        }
        $lastrow = array(
            array(
                0 => get_string('colavg', 'selfchecklist'),
                1 => get_string('colavg1', 'selfchecklist'),
                2 => round($begrankAvg),
                3 => round($endrankAvg), 4 => round($diffrankAvg)
            )
        );
        $merge = array_merge($data, $lastrow);
        //print_object($merge);
        return $merge;
    }

    // Fuction to sum values of the array of the same key.
    public function array_add_by_keys($array1, $array2) {
        foreach ($array2 as $k => $a) {
            if (array_key_exists($k, $array1)) {
                $array1[$k] += $a;
            } else {
                $array1[$k] = $a;
            }
        }
        return $array1;
    }

    // Fuction to difference values of the array of the same key.
    function array_diff_by_keys($array2, $array1) {
        foreach ($array1 as $k => $a) {
            if (array_key_exists($k, $array2)) {
                $array2[$k] -= $a;
            } else {
                $array2[$k] = $a;
            }
        }
        return $array2;
    }

    //Separated Header Drawing into it's own function for reuse.
    public function DrawHeader($header, $w) {
        // Colors, line width and bold font
        // Header
        $this->SetFillColor(233, 136, 64);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');
        $num_headers = count($header);
        for ($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($w[$i], 5, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
    }
    // Colored table
    public function ColoredTable($w, $header, $data) {
        $w = array(55, 70, 30, 30, 35);
        $this->DrawHeader($header, $w);
        $this->startTransaction();
        // Data
        $fill = 0;
        foreach ($data as $row) {
            $num_pages = $this->getNumPages();
            $this->startTransaction();
            //var_dump($row);
            $cellcount = array();
            //write text first
            $startX = $this->GetX();
            $startY = $this->GetY();
            //draw cells and record maximum cellcount
            //cell height is 6 and width is 80
            foreach ($row as $key => $column):
                $cellcount[] = $this->MultiCell($w[$key], 5, $column, 0, 'L', $fill, 0);
            endforeach;

            $this->SetXY($startX, $startY);
            //now do borders and fill
            //cell height is 6 times the max number of cells
            $maxnocells = max($cellcount);
            foreach ($row as $key => $column):
                $this->MultiCell($w[$key], $maxnocells * 5, '', 'LR', 'L', $fill, 0);
            endforeach;
            $this->Ln();
            if ($num_pages < $this->getNumPages()) {
                //Undo adding the row.
                $this->rollbackTransaction(true);
                //Adds a bottom line onto the current page. 
                //Note: May cause page break itself.
                $this->Cell(array_sum($w), 0, '', 'T');
                //Add a new page.
                $this->AddPage();
                //Draw the header.
                $this->DrawHeader($header, $w);
				
				$cellcount = array();
				//write text first
				$startX = $this->GetX();
				$startY = $this->GetY();
				//draw cells and record maximum cellcount
				//cell height is 6 and width is 80
				foreach ($row as $key => $column):
					$cellcount[] = $this->MultiCell($w[$key], 5, $column, 0, 'L', $fill, 0);
				endforeach;

				$this->SetXY($startX, $startY);
				//now do borders and fill
				//cell height is 6 times the max number of cells
				$maxnocells = max($cellcount);
				foreach ($row as $key => $column):
					$this->MultiCell($w[$key], $maxnocells * 5, '', 'LR', 'L', $fill, 0);
				endforeach;
				$this->Ln();
            } else {
                //Otherwise we are fine with this row, discard undo history.
                $this->commitTransaction();
            }

            // fill equals not fill (flip/flop)
            $fill = !$fill;
        }

        // draw bottom row border
        $this->Cell(array_sum($w), 0, '', 'T');
    }

}

//// Create new PDF document
$customlayout = array(300, 250);
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, $customlayout, true, 'UTF-8', false);

// create new PDF document
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// Set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Eduopen');
$pdf->SetTitle('Download Response Table');
$pdf->SetSubject('Selfchecklistl');
//$pdf->SetKeywords('EDUOPEn, PDF, example, test, guide');
// set default header data
$checklistname = $DB->get_records_sql("SELECT name FROM {selfchecklist} WHERE course='$cid'");
$i = 0;
$reccount = count($checklistname);
foreach ($checklistname as $chname) {
    $checklists[] = $chname->name;
}
if ($checklists[0] || $checklists[1]) {
    $pdf->SetHeaderData('', '', '' . ' Checklists: ' . $checklists[0] . ' & ' . $checklists[1], '');
}

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '16px'));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// set font
$pdf->SetFont('helvetica', '', 10);
// add a page
$pdf->AddPage();

// column titles
$header = array(get_string('blocks', 'selfchecklist'), get_string('qno', 'selfchecklist'),
    get_string('begranks', 'selfchecklist'), get_string('endranks', 'selfchecklist'),
    get_string('diffranks', 'selfchecklist'));
// data loading
$data = $pdf->LoadData($uid, $cid);
// print colored table
$ww = array(55, 70, 30, 30, 35);
$pdf->ColoredTable($ww, $header, $data);
// ---------------------------------------------------------
// Add a page before the graph starts.
$pdf->AddPage();
// set image scale factor.
$pdf->SetX(5);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->Image('charts/chart_u' . $uid . '_c' . $cid . '.png', '', '', 250, 120, '', '', '', false, 0, '', false, false, 1, false, false, false);

// close and output PDF document
$pdf->Output('charts-user' . $uid . '-course' . $cid . '.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
