<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This library replaces the phpESP application with Moodle specific code. It will eventually
 * replace all of the phpESP application, removing the dependency on that.
 */
/**
 * Updates the contents of the survey with the provided data. If no data is provided,
 * it checks for posted data.
 *
 * @param int $survey_id The id of the survey to update.
 * @param string $old_tab The function that was being executed.
 * @param object $sdata The data to update the survey with.
 *
 * @return string|boolean The function to go to, or false on error.
 *
 */
require_once($CFG->libdir . '/eventslib.php');
require_once($CFG->dirroot . '/calendar/lib.php');
require_once($CFG->dirroot . '/mod/selfchecklist/questiontypes/questiontypes.class.php');
// Constants.

define('SELFCHECKLISTUNLIMITED', 0);
define('SELFCHECKLISTONCE', 1);
define('SELFCHECKLISTDAILY', 2);
define('SELFCHECKLISTWEEKLY', 3);
define('SELFCHECKLISTMONTHLY', 4);

define('SELFCHECKLIST_STUDENTVIEWRESPONSES_NEVER', 0);
define('SELFCHECKLIST_STUDENTVIEWRESPONSES_WHENANSWERED', 1);
define('SELFCHECKLIST_STUDENTVIEWRESPONSES_WHENCLOSED', 2);
define('SELFCHECKLIST_STUDENTVIEWRESPONSES_ALWAYS', 3);
//define('SELFCHECKLIST_STUDENTVIEWRESPONSES_NEVER', 4);

define('SELFCHECKLIST_MAX_EVENT_LENGTH', 5 * 24 * 60 * 60);   // 5 days maximum.

define('SELFCHECKLIST_DEFAULT_PAGE_COUNT', 20);

global $selfchecklisttypes;
$selfchecklisttypes = array(SELFCHECKLISTUNLIMITED => get_string('qtypeunlimited', 'selfchecklist'),
    SELFCHECKLISTONCE => get_string('qtypeonce', 'selfchecklist'),
    SELFCHECKLISTDAILY => get_string('qtypedaily', 'selfchecklist'),
    SELFCHECKLISTWEEKLY => get_string('qtypeweekly', 'selfchecklist'),
    SELFCHECKLISTMONTHLY => get_string('qtypemonthly', 'selfchecklist'));

global $selfchecklistrespondents;
$selfchecklistrespondents = array('fullname' => get_string('respondenttypefullname', 'selfchecklist'),
    'anonymous' => get_string('respondenttypeanonymous', 'selfchecklist'));

global $selfchecklistrealms;
$selfchecklistrealms = array('private' => get_string('private', 'selfchecklist'),
    'public' => get_string('public', 'selfchecklist'),
    'template' => get_string('template', 'selfchecklist'));

global $selfchecklistresponseviewers;
$selfchecklistresponseviewers = array(SELFCHECKLIST_STUDENTVIEWRESPONSES_WHENANSWERED => get_string('responseviewstudentswhenanswered', 'selfchecklist'),
    SELFCHECKLIST_STUDENTVIEWRESPONSES_WHENCLOSED => get_string('responseviewstudentswhenclosed', 'selfchecklist'),
    SELFCHECKLIST_STUDENTVIEWRESPONSES_ALWAYS => get_string('responseviewstudentsalways', 'selfchecklist'),
    SELFCHECKLIST_STUDENTVIEWRESPONSES_NEVER => get_string('responseviewstudentsnever', 'selfchecklist')
);

global $autonumbering;
$autonumbering = array(0 => get_string('autonumberno', 'selfchecklist'),
    1 => get_string('autonumberquestions', 'selfchecklist'),
    2 => get_string('autonumberpages', 'selfchecklist'),
    3 => get_string('autonumberpagesandquestions', 'selfchecklist'));

function selfchecklist_check_date($thisdate, $insert = false) {
    $dateformat = get_string('strfdate', 'selfchecklist');
    if (preg_match('/(%[mdyY])(.+)(%[mdyY])(.+)(%[mdyY])/', $dateformat, $matches)) {
        $datepieces = explode($matches[2], $thisdate);
        foreach ($datepieces as $datepiece) {
            if (!is_numeric($datepiece)) {
                return 'wrongdateformat';
            }
        }
        $pattern = "/[^dmy]/i";
        $dateorder = strtolower(preg_replace($pattern, '', $dateformat));
        $countpieces = count($datepieces);
        if ($countpieces == 1) { // Assume only year entered.
            switch ($dateorder) {
                case 'dmy': // Most countries.
                case 'mdy': // USA.
                    $datepieces[2] = $datepieces[0]; // year
                    $datepieces[0] = '1'; // Assumed 1st month of year.
                    $datepieces[1] = '1'; // Assumed 1st day of month.
                    break;
                case 'ymd': // ISO 8601 standard
                    $datepieces[1] = '1'; // Assumed 1st month of year.
                    $datepieces[2] = '1'; // Assumed 1st day of month.
                    break;
            }
        }
        if ($countpieces == 2) { // Assume only month and year entered.
            switch ($dateorder) {
                case 'dmy': // Most countries.
                    $datepieces[2] = $datepieces[1]; // Year.
                    $datepieces[1] = $datepieces[0]; // Month.
                    $datepieces[0] = '1'; // Assumed 1st day of month.
                    break;
                case 'mdy': // USA
                    $datepieces[2] = $datepieces[1]; // Year.
                    $datepieces[0] = $datepieces[0]; // Month.
                    $datepieces[1] = '1'; // Assumed 1st day of month.
                    break;
                case 'ymd': // ISO 8601 standard
                    $datepieces[2] = '1'; // Assumed 1st day of month.
                    break;
            }
        }
        if (count($datepieces) > 1) {
            if ($matches[1] == '%m') {
                $month = $datepieces[0];
            }
            if ($matches[1] == '%d') {
                $day = $datepieces[0];
            }
            if ($matches[1] == '%y') {
                $year = strftime('%C') . $datepieces[0];
            }
            if ($matches[1] == '%Y') {
                $year = $datepieces[0];
            }

            if ($matches[3] == '%m') {
                $month = $datepieces[1];
            }
            if ($matches[3] == '%d') {
                $day = $datepieces[1];
            }
            if ($matches[3] == '%y') {
                $year = strftime('%C') . $datepieces[1];
            }
            if ($matches[3] == '%Y') {
                $year = $datepieces[1];
            }

            if ($matches[5] == '%m') {
                $month = $datepieces[2];
            }
            if ($matches[5] == '%d') {
                $day = $datepieces[2];
            }
            if ($matches[5] == '%y') {
                $year = strftime('%C') . $datepieces[2];
            }
            if ($matches[5] == '%Y') {
                $year = $datepieces[2];
            }

            $month = min(12, $month);
            $month = max(1, $month);
            if ($month == 2) {
                $day = min(29, $day);
            } else if ($month == 4 || $month == 6 || $month == 9 || $month == 11) {
                $day = min(30, $day);
            } else {
                $day = min(31, $day);
            }
            $day = max(1, $day);
            if (!$thisdate = gmmktime(0, 0, 0, $month, $day, $year)) {
                return 'wrongdaterange';
            } else {
                if ($insert) {
                    $thisdate = trim(userdate($thisdate, '%Y-%m-%d', '1', false));
                } else {
                    $thisdate = trim(userdate($thisdate, $dateformat, '1', false));
                }
            }
            return $thisdate;
        }
    } else {
        return ('wrongdateformat');
    }
}

// A variant of Moodle's notify function, with a different formatting.
function selfchecklist_notify($message) {
    $message = clean_text($message);
    $errorstart = '<div class="notifyproblem">';
    $errorend = '</div>';
    $output = $errorstart . $message . $errorend;
    echo $output;
}

function selfchecklist_choice_values($content) {

    // If we run the content through format_text first, any filters we want to use (e.g. multilanguage) should work.
    // examines the content of a possible answer from radio button, check boxes or rate question
    // returns ->text to be displayed, ->image if present, ->modname name of modality, image ->title.
    $contents = new stdClass();
    $contents->text = '';
    $contents->image = '';
    $contents->modname = '';
    $contents->title = '';
    // Has image.
    if ($count = preg_match('/(<img)\s .*(src="(.[^"]{1,})")/isxmU', $content, $matches)) {
        $contents->image = $matches[0];
        $imageurl = $matches[3];
        // Image has a title or alt text: use one of them.
        if (preg_match('/(title=.)([^"]{1,})/', $content, $matches) || preg_match('/(alt=.)([^"]{1,})/', $content, $matches)) {
            $contents->title = $matches[2];
        } else {
            // Image has no title nor alt text: use its filename (without the extension).
            preg_match("/.*\/(.*)\..*$/", $imageurl, $matches);
            $contents->title = $matches[1];
        }
        // Content has text or named modality plus an image.
        if (preg_match('/(.*)(<img.*)/', $content, $matches)) {
            $content = $matches[1];
        } else {
            // Just an image.
            return $contents;
        }
    }

    // Check for score value first (used e.g. by personality test feature).
    $r = preg_match_all("/^(\d{1,2}=)(.*)$/", $content, $matches);
    if ($r) {
        $content = $matches[2][0];
    }

    // Look for named modalities.
    $contents->text = $content;
    // DEV JR from version 2.5, a double colon :: must be used here instead of the equal sign.
    if ($pos = strpos($content, '::')) {
        $contents->text = substr($content, $pos + 2);
        $contents->modname = substr($content, 0, $pos);
    }
    return $contents;
}

/**
 * Get the information about the standard selfchecklist JavaScript module.
 * @return array a standard jsmodule structure.
 */
function selfchecklist_get_js_module() {
    global $PAGE;
    return array(
        'name' => 'mod_selfchecklist',
        'fullpath' => '/mod/selfchecklist/module.js',
        'requires' => array('base', 'dom', 'event-delegate', 'event-key',
            'core_question_engine', 'moodle-core-formchangechecker'),
        'strings' => array(
            array('cancel', 'moodle'),
            array('flagged', 'question'),
            array('functiondisabledbysecuremode', 'quiz'),
            array('startattempt', 'quiz'),
            array('timesup', 'quiz'),
            array('changesmadereallygoaway', 'moodle'),
        ),
    );
}

/**
 * Get all the selfchecklist responses for a user
 */
function selfchecklist_get_user_responses($surveyid, $userid, $complete = true) {
    global $DB;
    $andcomplete = '';
    if ($complete) {
        $andcomplete = " AND complete = 'y' ";
    }
    return $DB->get_records_sql("SELECT *
        FROM {selfchecklist_response}
        WHERE survey_id = ?
        AND username = ?
        " . $andcomplete . "
        ORDER BY submitted ASC ", array($surveyid, $userid));
}

/**
 * get the capabilities for the selfchecklist
 * @param int $cmid
 * @return object the available capabilities from current user
 */
function selfchecklist_load_capabilities($cmid) {
    static $cb;

    if (isset($cb)) {
        return $cb;
    }

    $context = selfchecklist_get_context($cmid);

    $cb = new object;
    $cb->view = has_capability('mod/selfchecklist:view', $context);
    $cb->submit = has_capability('mod/selfchecklist:submit', $context);
    $cb->viewsingleresponse = has_capability('mod/selfchecklist:viewsingleresponse', $context);
    $cb->downloadresponses = has_capability('mod/selfchecklist:downloadresponses', $context);
    $cb->deleteresponses = has_capability('mod/selfchecklist:deleteresponses', $context);
    $cb->manage = has_capability('mod/selfchecklist:manage', $context);
    $cb->editquestions = has_capability('mod/selfchecklist:editquestions', $context);
    $cb->createtemplates = has_capability('mod/selfchecklist:createtemplates', $context);
    $cb->createpublic = has_capability('mod/selfchecklist:createpublic', $context);
    $cb->readownresponses = has_capability('mod/selfchecklist:readownresponses', $context);
    $cb->readallresponses = has_capability('mod/selfchecklist:readallresponses', $context);
    $cb->readallresponseanytime = has_capability('mod/selfchecklist:readallresponseanytime', $context);
    $cb->printblank = has_capability('mod/selfchecklist:printblank', $context);
    $cb->preview = has_capability('mod/selfchecklist:preview', $context);

    $cb->viewhiddenactivities = has_capability('moodle/course:viewhiddenactivities', $context, null, false);

    return $cb;
}

/**
 * returns the context-id related to the given coursemodule-id
 * @param int $cmid the coursemodule-id
 * @return object $context
 */
function selfchecklist_get_context($cmid) {
    static $context;

    if (isset($context)) {
        return $context;
    }

    if (!$context = context_module::instance($cmid)) {
        print_error('badcontext');
    }
    return $context;
}

// This function *really* shouldn't be needed, but since sometimes we can end up with
// orphaned surveys, this will clean them up.
function selfchecklist_cleanup() {
    global $DB;

    // Find surveys that don't have selfchecklists associated with them.
    $sql = 'SELECT qs.* FROM {selfchecklist_survey} qs ' .
            'LEFT JOIN {selfchecklist} q ON q.sid = qs.id ' .
            'WHERE q.sid IS NULL';

    if ($surveys = $DB->get_records_sql($sql)) {
        foreach ($surveys as $survey) {
            selfchecklist_delete_survey($survey->id, 0);
        }
    }
    // Find deleted questions and remove them from database (with their associated choices, etc.).
    return true;
}

function selfchecklist_record_submission(&$selfchecklist, $userid, $rid = 0) {
    global $DB;

    $attempt['qid'] = $selfchecklist->id;
    $attempt['userid'] = $userid;
    $attempt['rid'] = $rid;
    $attempt['timemodified'] = time();
    return $DB->insert_record("selfchecklist_attempts", (object) $attempt, false);
}

function selfchecklist_delete_survey($sid, $selfchecklistid) {
    global $DB;
    $status = true;
    // Delete all survey attempts and responses.
    if ($responses = $DB->get_records('selfchecklist_response', array('survey_id' => $sid), 'id')) {
        foreach ($responses as $response) {
            $status = $status && selfchecklist_delete_response($response);
        }
    }

    // There really shouldn't be any more, but just to make sure...
    $DB->delete_records('selfchecklist_response', array('survey_id' => $sid));
    $DB->delete_records('selfchecklist_attempts', array('qid' => $selfchecklistid));

    // Delete all question data for the survey.
    if ($questions = $DB->get_records('selfchecklist_question', array('survey_id' => $sid), 'id')) {
        foreach ($questions as $question) {
            $DB->delete_records('selfchecklist_quest_choice', array('question_id' => $question->id));
        }
        $status = $status && $DB->delete_records('selfchecklist_question', array('survey_id' => $sid));
    }

    // Delete all feedback sections and feedback messages for the survey.
    if ($fbsections = $DB->get_records('selfchecklist_fb_sections', array('survey_id' => $sid), 'id')) {
        foreach ($fbsections as $fbsection) {
            $DB->delete_records('selfchecklist_feedback', array('section_id' => $fbsection->id));
        }
        $status = $status && $DB->delete_records('selfchecklist_fb_sections', array('survey_id' => $sid));
    }

    $status = $status && $DB->delete_records('selfchecklist_survey', array('id' => $sid));

    return $status;
}

function selfchecklist_delete_response($response, $selfchecklist = '') {
    global $DB;
    $status = true;
    $cm = '';
    $rid = $response->id;
    // The selfchecklist_delete_survey function does not send the selfchecklist array.
    if ($selfchecklist != '') {
        $cm = get_coursemodule_from_instance("selfchecklist", $selfchecklist->id, $selfchecklist->course->id);
    }

    // Delete all of the response data for a response.
    $DB->delete_records('selfchecklist_response_bool', array('response_id' => $rid));
    $DB->delete_records('selfchecklist_response_date', array('response_id' => $rid));
    $DB->delete_records('selfchecklist_resp_multiple', array('response_id' => $rid));
    $DB->delete_records('selfchecklist_response_other', array('response_id' => $rid));
    $DB->delete_records('selfchecklist_response_rank', array('response_id' => $rid));
    $DB->delete_records('selfchecklist_resp_single', array('response_id' => $rid));
    $DB->delete_records('selfchecklist_response_text', array('response_id' => $rid));

    $status = $status && $DB->delete_records('selfchecklist_response', array('id' => $rid));
    $status = $status && $DB->delete_records('selfchecklist_attempts', array('rid' => $rid));

    if ($status && $cm) {
        // Update completion state if necessary.
        $completion = new completion_info($selfchecklist->course);
        if ($completion->is_enabled($cm) == COMPLETION_TRACKING_AUTOMATIC && $selfchecklist->completionsubmit) {
            $completion->update_state($cm, COMPLETION_INCOMPLETE, $response->username);
        }
    }

    return $status;
}

function selfchecklist_delete_responses($qid) {
    global $DB;

    $status = true;

    // Delete all of the response data for a question.
    $DB->delete_records('selfchecklist_response_bool', array('question_id' => $qid));
    $DB->delete_records('selfchecklist_response_date', array('question_id' => $qid));
    $DB->delete_records('selfchecklist_resp_multiple', array('question_id' => $qid));
    $DB->delete_records('selfchecklist_response_other', array('question_id' => $qid));
    $DB->delete_records('selfchecklist_response_rank', array('question_id' => $qid));
    $DB->delete_records('selfchecklist_resp_single', array('question_id' => $qid));
    $DB->delete_records('selfchecklist_response_text', array('question_id' => $qid));

    $status = $status && $DB->delete_records('selfchecklist_response', array('id' => $qid));
    $status = $status && $DB->delete_records('selfchecklist_attempts', array('rid' => $qid));

    return $status;
}

function selfchecklist_get_survey_list($courseid = 0, $type = '') {
    global $DB;

    if ($courseid == 0) {
        if (isadmin()) {
            $sql = "SELECT id,name,owner,realm,status " .
                    "{selfchecklist_survey} " .
                    "ORDER BY realm,name ";
            $params = null;
        } else {
            return false;
        }
    } else {
        $castsql = $DB->sql_cast_char2int('s.owner');
        if ($type == 'public') {
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,s.title,q.id as qid,q.name as qname " .
                    "FROM {selfchecklist} q " .
                    "INNER JOIN {selfchecklist_survey} s ON s.id = q.sid AND " . $castsql . " = q.course " .
                    "WHERE realm = ? " .
                    "ORDER BY realm,name ";
            $params = array($type);
        } else if ($type == 'template') {
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,s.title,q.id as qid,q.name as qname " .
                    "FROM {selfchecklist} q " .
                    "INNER JOIN {selfchecklist_survey} s ON s.id = q.sid AND " . $castsql . " = q.course " .
                    "WHERE (realm = ?) " .
                    "ORDER BY realm,name ";
            $params = array($type);
        } else if ($type == 'private') {
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,q.id as qid,q.name as qname " .
                    "FROM {selfchecklist} q " .
                    "INNER JOIN {selfchecklist_survey} s ON s.id = q.sid " .
                    "WHERE owner = ? and realm = ? " .
                    "ORDER BY realm,name ";
            $params = array($courseid, $type);
        } else {
            // Current get_survey_list is called from function selfchecklist_reset_userdata so we need to get a
            // complete list of all selfchecklists in current course to reset them.
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,q.id as qid,q.name as qname " .
                    "FROM {selfchecklist} q " .
                    "INNER JOIN {selfchecklist_survey} s ON s.id = q.sid AND " . $castsql . " = q.course " .
                    "WHERE owner = ? " .
                    "ORDER BY realm,name ";
            $params = array($courseid);
        }
    }
    return $DB->get_records_sql($sql, $params);
}

function selfchecklist_get_survey_select($instance, $courseid = 0, $sid = 0, $type = '') {
    global $OUTPUT, $DB;

    $surveylist = array();

    if ($surveys = selfchecklist_get_survey_list($courseid, $type)) {
        $strpreview = get_string('preview_selfchecklist', 'selfchecklist');
        foreach ($surveys as $survey) {
            $originalcourse = $DB->get_record('course', array('id' => $survey->owner));
            if (!$originalcourse) {
                // This should not happen, but we found a case where a public survey
                // still existed in a course that had been deleted, and so this
                // code lead to a notice, and a broken link. Since that is useless
                // we just skip surveys like this.
                continue;
            }

            // Prevent creating a copy of a public selfchecklist IN THE SAME COURSE as the original.
            if ($type == 'public' && $survey->owner == $courseid) {
                continue;
            } else {
                $args = "sid={$survey->id}&popup=1";
                if (!empty($survey->qid)) {
                    $args .= "&qid={$survey->qid}";
                }
                $link = new moodle_url("/mod/selfchecklist/preview.php?{$args}");
                $action = new popup_action('click', $link);
                $label = $OUTPUT->action_link($link, $survey->qname . ' [' . $originalcourse->fullname . ']', $action, array('title' => $strpreview));
                $surveylist[$type . '-' . $survey->id] = $label;
            }
        }
    }
    return $surveylist;
}

function selfchecklist_get_type($id) {
    switch ($id) {
        case 1:
            return get_string('yesno', 'selfchecklist');
        case 2:
            return get_string('textbox', 'selfchecklist');
        case 3:
            return get_string('essaybox', 'selfchecklist');
        case 4:
            return get_string('radiobuttons', 'selfchecklist');
        case 5:
            return get_string('checkboxes', 'selfchecklist');
        case 6:
            return get_string('dropdown', 'selfchecklist');
        case 8:
            return get_string('ratescale', 'selfchecklist');
        case 9:
            return get_string('date', 'selfchecklist');
        case 10:
            return get_string('numeric', 'selfchecklist');
        case 100:
            return get_string('sectiontext', 'selfchecklist');
        case 99:
            return get_string('sectionbreak', 'selfchecklist');
        default:
            return $id;
    }
}

/**
 * This creates new events given as opendate and closedate by $selfchecklist.
 * @param object $selfchecklist
 * @return void
 */
/* added by JR 16 march 2009 based on lesson_process_post_save script */

function selfchecklist_set_events($selfchecklist) {
    // Adding the selfchecklist to the eventtable.
    global $DB;
    if ($events = $DB->get_records('event', array('modulename' => 'selfchecklist', 'instance' => $selfchecklist->id))) {
        foreach ($events as $event) {
            $event = calendar_event::load($event);
            $event->delete();
        }
    }

    // The open-event.
    $event = new stdClass;
    $event->description = $selfchecklist->name;
    $event->courseid = $selfchecklist->course;
    $event->groupid = 0;
    $event->userid = 0;
    $event->modulename = 'selfchecklist';
    $event->instance = $selfchecklist->id;
    $event->eventtype = 'open';
    $event->timestart = $selfchecklist->opendate;
    $event->visible = instance_is_visible('selfchecklist', $selfchecklist);
    $event->timeduration = ($selfchecklist->closedate - $selfchecklist->opendate);

    if ($selfchecklist->closedate and $selfchecklist->opendate and $event->timeduration <= SELFCHECKLIST_MAX_EVENT_LENGTH) {
        // Single event for the whole selfchecklist.
        $event->name = $selfchecklist->name;
        calendar_event::create($event);
    } else {
        // Separate start and end events.
        $event->timeduration = 0;
        if ($selfchecklist->opendate) {
            $event->name = $selfchecklist->name . ' (' . get_string('selfchecklistopens', 'selfchecklist') . ')';
            calendar_event::create($event);
            unset($event->id); // So we can use the same object for the close event.
        }
        if ($selfchecklist->closedate) {
            $event->name = $selfchecklist->name . ' (' . get_string('selfchecklistcloses', 'selfchecklist') . ')';
            $event->timestart = $selfchecklist->closedate;
            $event->eventtype = 'close';
            calendar_event::create($event);
        }
    }
}

/**
 * Get users who have not completed the selfchecklist
 *
 * @global object
 * @uses CONTEXT_MODULE
 * @param object $cm
 * @param int $group single groupid
 * @param string $sort
 * @param int $startpage
 * @param int $pagecount
 * @return object the userrecords
 */
function selfchecklist_get_incomplete_users($cm, $sid, $group = false, $sort = '', $startpage = false, $pagecount = false) {

    global $DB;

    $context = context_module::instance($cm->id);

    // First get all users who can complete this selfchecklist.
    $cap = 'mod/selfchecklist:submit';
    $fields = 'u.id, u.username';
    if (!$allusers = get_users_by_capability($context, $cap, $fields, $sort, '', '', $group, '', true)) {
        return false;
    }
    $allusers = array_keys($allusers);

    // Nnow get all completed selfchecklists.
    $params = array('survey_id' => $sid, 'complete' => 'y');
    $sql = "SELECT username FROM {selfchecklist_response} "
            . "WHERE survey_id = $sid AND complete = 'y' "
            . "GROUP BY username ";

    if (!$completedusers = $DB->get_records_sql($sql)) {
        return $allusers;
    }
    $completedusers = array_keys($completedusers);
    // Now strike all completedusers from allusers.
    $allusers = array_diff($allusers, $completedusers);
    // For paging I use array_slice().
    if ($startpage !== false AND $pagecount !== false) {
        $allusers = array_slice($allusers, $startpage, $pagecount);
    }
    return $allusers;
}

/**
 * Called by HTML editor in showrespondents and Essay question. Based on question/essay/renderer.
 * Pending general solution to using the HTML editor outside of moodleforms in Moodle pages.
 */
function selfchecklist_get_editor_options($context) {
    return array(
        'subdirs' => 0,
        'maxbytes' => 0,
        'maxfiles' => -1,
        'context' => $context,
        'noclean' => 0,
        'trusttext' => 0
    );
}

// Skip logic: we need to find out how many questions will actually be displayed on next page/section.
function selfchecklist_nb_questions_on_page($questionsinselfchecklist, $questionsinsection, $rid) {
    global $DB;
    $questionstodisplay = array();
    foreach ($questionsinsection as $question) {
        if ($question->dependquestion != 0) {
            switch ($questionsinselfchecklist[$question->dependquestion]->type_id) {
                case QUESYESNO:
                    if ($question->dependchoice == 0) {
                        $questiondependchoice = "'y'";
                    } else {
                        $questiondependchoice = "'n'";
                    }
                    $responsetable = 'response_bool';
                    break;
                default:
                    $questiondependchoice = $question->dependchoice;
                    $responsetable = 'resp_single';
            }
            $sql = 'SELECT * FROM {selfchecklist}_' . $responsetable . ' WHERE response_id = ' . $rid .
                    ' AND question_id = ' . $question->dependquestion . ' AND choice_id = ' . $questiondependchoice;
            if ($DB->get_record_sql($sql)) {
                $questionstodisplay [] = $question->id;
            }
        } else {
            $questionstodisplay [] = $question->id;
        }
    }
    return $questionstodisplay;
}

function selfchecklist_get_dependencies($questions, $position) {
    $dependencies = array();
    $dependencies[''][0] = get_string('choosedots');

    foreach ($questions as $question) {
        if (($question->type_id == QUESRADIO || $question->type_id == QUESDROP || $question->type_id == QUESYESNO) && $question->position < $position) {
            if (($question->type_id == QUESRADIO || $question->type_id == QUESDROP) && $question->name != '') {
                foreach ($question->choices as $key => $choice) {
                    $contents = selfchecklist_choice_values($choice->content);
                    if ($contents->modname) {
                        $choice->content = $contents->modname;
                    } else if ($contents->title) { // Must be an image; use its title for the dropdown list.
                        $choice->content = $contents->title;
                    } else {
                        $choice->content = $contents->text;
                    }
                    $dependencies[$question->name][$question->id . ',' . $key] = $question->name . '->' . $choice->content;
                }
            }
            if ($question->type_id == QUESYESNO && $question->name != '') {
                $dependencies[$question->name][$question->id . ',0'] = $question->name . '->' . get_string('yes');
                $dependencies[$question->name][$question->id . ',1'] = $question->name . '->' . get_string('no');
            }
        }
    }
    return $dependencies;
}

// Get the parent of a child question.
function selfchecklist_get_parent($question) {
    global $DB;
    $qid = $question->id;
    $parent = array();
    $dependquestion = $DB->get_record('selfchecklist_question', array('id' => $question->dependquestion), $fields = 'id, position, name, type_id');
    if (is_object($dependquestion)) {
        $qdependchoice = '';
        switch ($dependquestion->type_id) {
            case QUESRADIO:
            case QUESDROP:
                $dependchoice = $DB->get_record('selfchecklist_quest_choice', array('id' => $question->dependchoice), $fields = 'id,content');
                $qdependchoice = $dependchoice->id;
                $dependchoice = $dependchoice->content;

                $contents = selfchecklist_choice_values($dependchoice);
                if ($contents->modname) {
                    $dependchoice = $contents->modname;
                }
                break;
            case QUESYESNO:
                switch ($question->dependchoice) {
                    case 0:
                        $dependchoice = get_string('yes');
                        $qdependchoice = 'y';
                        break;
                    case 1:
                        $dependchoice = get_string('no');
                        $qdependchoice = 'n';
                        break;
                }
                break;
        }
        // Qdependquestion, parenttype and qdependchoice fields to be used in preview mode.
        $parent [$qid]['qdependquestion'] = 'q' . $dependquestion->id;
        $parent [$qid]['qdependchoice'] = $qdependchoice;
        $parent [$qid]['parenttype'] = $dependquestion->type_id;
        // Other fields to be used in Questions edit mode.
        $parent [$qid]['position'] = $question->position;
        $parent [$qid]['name'] = $question->name;
        $parent [$qid]['content'] = $question->content;
        $parent [$qid]['parentposition'] = $dependquestion->position;
        $parent [$qid]['parent'] = $dependquestion->name . '->' . $dependchoice;
    }
    return $parent;
}

// Get parent position of all child questions in current selfchecklist.
function selfchecklist_get_parent_positions($questions) {
    global $DB;
    $parentpositions = array();
    foreach ($questions as $question) {
        $dependquestion = $question->dependquestion;
        if ($dependquestion != 0) {
            $childid = $question->id;
            $parentpos = $questions[$dependquestion]->position;
            $parentpositions[$childid] = $parentpos;
        }
    }
    return $parentpositions;
}

// Get child position of all parent questions in current selfchecklist.
function selfchecklist_get_child_positions($questions) {
    global $DB;
    $childpositions = array();
    foreach ($questions as $question) {
        $dependquestion = $question->dependquestion;
        if ($dependquestion != 0) {
            $parentid = $questions[$dependquestion]->id;
            if (!isset($firstchildfound[$parentid])) {
                $firstchildfound[$parentid] = true;
                $childpos = $question->position;
                $childpositions[$parentid] = $childpos;
            }
        }
    }
    return $childpositions;
}

// Check if current selfchecklist contains child questions.
function selfchecklist_has_dependencies($questions) {
    foreach ($questions as $question) {
        if ($question->dependquestion != 0) {
            return true;
            break;
        }
    }
    return false;
}

// Check that the needed page breaks are present to separate child questions.
function selfchecklist_check_page_breaks($selfchecklist) {
    global $DB;
    $msg = '';
    // Store the new page breaks ids.
    $newpbids = array();
    $delpb = 0;
    $sid = $selfchecklist->survey->id;
    $questions = $DB->get_records('selfchecklist_question', array('survey_id' => $sid, 'deleted' => 'n'), 'id');
    $positions = array();
    foreach ($questions as $key => $qu) {
        $positions[$qu->position]['question_id'] = $key;
        $positions[$qu->position]['dependquestion'] = $qu->dependquestion;
        $positions[$qu->position]['dependchoice'] = $qu->dependchoice;
        $positions[$qu->position]['type_id'] = $qu->type_id;
        $positions[$qu->position]['qname'] = $qu->name;
        $positions[$qu->position]['qpos'] = $qu->position;
    }
    $count = count($positions);

    for ($i = $count; $i > 0; $i--) {
        $qu = $positions[$i];
        $questionnb = $i;
        if ($qu['type_id'] == QUESPAGEBREAK) {
            $questionnb--;
            // If more than one consecutive page breaks, remove extra one(s).
            $prevqu = null;
            $prevtypeid = null;
            if ($i > 1) {
                $prevqu = $positions[$i - 1];
                $prevtypeid = $prevqu['type_id'];
            }
            // If $i == $count then remove that extra page break in last position.
            if ($prevtypeid == QUESPAGEBREAK || $i == $count || $qu['qpos'] == 1) {
                $qid = $qu['question_id'];
                $delpb ++;
                $msg .= get_string("checkbreaksremoved", "selfchecklist", $delpb) . '<br />';
                // Need to reload questions.
                $questions = $DB->get_records('selfchecklist_question', array('survey_id' => $sid, 'deleted' => 'n'), 'id');
                $DB->set_field('selfchecklist_question', 'deleted', 'y', array('id' => $qid, 'survey_id' => $sid));
                $select = 'survey_id = ' . $sid . ' AND deleted = \'n\' AND position > ' .
                        $questions[$qid]->position;
                if ($records = $DB->get_records_select('selfchecklist_question', $select, null, 'position ASC')) {
                    foreach ($records as $record) {
                        $DB->set_field('selfchecklist_question', 'position', $record->position - 1, array('id' => $record->id));
                    }
                }
            }
        }
        // Add pagebreak between question child and not dependent question that follows.
        if ($qu['type_id'] != QUESPAGEBREAK) {
            $qname = $positions[$i]['qname'];
            $j = $i - 1;
            if ($j != 0) {
                $prevtypeid = $positions[$j]['type_id'];
                $prevdependquestion = $positions[$j]['dependquestion'];
                $prevdependchoice = $positions[$j]['dependchoice'];
                $prevdependquestionname = $positions[$j]['qname'];
                $prevqname = $positions[$j]['qname'];
                if (($prevtypeid != QUESPAGEBREAK && ($prevdependquestion != $qu['dependquestion'] || $prevdependchoice != $qu['dependchoice'])) || ($qu['dependquestion'] == 0 && $prevdependquestion != 0)) {
                    $sql = 'SELECT MAX(position) as maxpos FROM {selfchecklist_question} ' .
                            'WHERE survey_id = ' . $selfchecklist->survey->id . ' AND deleted = \'n\'';
                    if ($record = $DB->get_record_sql($sql)) {
                        $pos = $record->maxpos + 1;
                    } else {
                        $pos = 1;
                    }
                    $question = new Object();
                    $question->survey_id = $selfchecklist->survey->id;
                    $question->type_id = QUESPAGEBREAK;
                    $question->position = $pos;
                    $question->content = 'break';
                    if (!($newqid = $DB->insert_record('selfchecklist_question', $question))) {
                        return(false);
                    }
                    $newpbids[] = $newqid;
                    $movetopos = $i;
                    $selfchecklist = new selfchecklist($selfchecklist->id, null, $course, $cm);
                    $selfchecklist->move_question($newqid, $movetopos);
                }
            }
        }
    }
    if (empty($newpbids) && !$msg) {
        $msg = get_string('checkbreaksok', 'selfchecklist');
    } else if ($newpbids) {
        $msg .= get_string('checkbreaksadded', 'selfchecklist') . '&nbsp;';
        $newpbids = array_reverse($newpbids);
        $selfchecklist = new selfchecklist($selfchecklist->id, null, $course, $cm);
        foreach ($newpbids as $newpbid) {
            $msg .= $selfchecklist->questions[$newpbid]->position . '&nbsp;';
        }
    }
    return($msg);
}

// Get all descendants and choices for questions with descendants.
function selfchecklist_get_descendants_and_choices($questions) {
    global $DB;
    $questions = array_reverse($questions, true);
    $qu = array();
    foreach ($questions as $question) {
        if ($question->dependquestion) {
            $dq = $question->dependquestion;
            $dc = $question->dependchoice;
            $qid = $question->id;

            $qu['descendants'][$dq][] = 'qn-' . $qid;
            if (array_key_exists($qid, $qu['descendants'])) {
                foreach ($qu['descendants'][$qid] as $q) {
                    $qu['descendants'][$dq][] = $q;
                }
            }
            $qu['choices'][$dq][$dc][] = 'qn-' . $qid;
        }
    }
    return($qu);
}

// Get all descendants for a question to be deleted.
function selfchecklist_get_descendants($questions, $questionid) {
    global $DB;
    $questions = array_reverse($questions, true);
    $qu = array();
    foreach ($questions as $question) {
        if ($question->dependquestion) {
            $dq = $question->dependquestion;
            $qid = $question->id;
            $qpos = $question->position;
            $qu[$dq][] = $qid;
            if (array_key_exists($qid, $qu)) {
                foreach ($qu[$qid] as $q) {
                    $qu[$dq][] = $q;
                }
            }
        }
    }
    $descendants = array();
    if (isset($qu[$questionid])) {
        foreach ($qu[$questionid] as $descendant) {
            $childquestion = $questions[$descendant];
            $descendants += selfchecklist_get_parent($childquestion);
        }
        uasort($descendants, 'selfchecklist_cmp');
    }
    return($descendants);
}

// Function to sort descendants array in selfchecklist_get_descendants function.
function selfchecklist_cmp($a, $b) {
    if ($a == $b) {
        return 0;
    } else if ($a < $b) {
        return -1;
    } else {
        return 1;
    }
}
